//
//  CardView.swift
//  GuideHero
//
//  Created by Abhishek Sengar on 10/09/2016.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AlamofireImage
import YouTubePlayer

class CardView: UIView {

    // MARK: - Outlets

    @IBOutlet weak var imageView: MaskImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var bottomTextLabel: UILabel!
    @IBOutlet weak var speakerButton: UIButton!
    @IBOutlet weak var youTubeView: YouTubePlayerView!
    @IBOutlet weak var webView: UIWebView!

    // MARK: - Vars

    var content: ContentNode! {
        didSet {
            self.updateUI(self.content)
        }
    }

    // Set to true to show the view as a card (with rounded corners and shadow).
    var showAsCard: Bool = false {
        didSet {
            if self.showAsCard {
                self.layer.cornerRadius = 20
                self.layer.shadowColor = UIColor.blackColor().CGColor
                self.layer.shadowOffset = CGSizeZero
                self.layer.shadowRadius = 5
                self.layer.shadowOpacity = 0.5
            }
            else {
                self.layer.cornerRadius = 0
                self.layer.shadowRadius = 0
                self.layer.shadowOpacity = 0
            }
        }
    }

    // MARK: - Private vars

    private var currentCardIndex: Int = 0 {
        didSet {
            self.updateUI(self.content.children[self.currentCardIndex])
        }
    }

    // We need a reference when the user clicks the speaker button because this can be nested content.
    private var audioContent: ContentNode? {
        didSet {
            self.speakerButton.hidden = (self.audioContent == nil)
        }
    }

    // We need a reference to load the video later.
    var videoContent: ContentNode?

    // MARK: - View lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupYouTubeView()
    }

    private func setupYouTubeView() {
        self.youTubeView!.playerVars = ["playsinline": 1]
    }

    // MARK: - Content displaying

    func showNextCard() {
        if self.content.children.count > 0 {
            self.currentCardIndex = (self.currentCardIndex + 1) % self.content.children.count
        }
    }

    private func resetUI() {
        self.youTubeView.hidden = true
        self.webView.hidden = true
        self.textLabel.hidden = true
        self.bottomTextLabel.hidden = true
        self.imageView.hidden = true
        self.imageView.image = nil
        self.audioContent = nil
    }

    private func updateUI(content: ContentNode) {

        self.resetUI()

        // TODO: create a CardView subclass for each content type 
        switch content.type {
        case .Deck:
            self.textLabel.hidden = false
            // Show content of first text card in the deck, or the name if none is found.
            // This matches the deckImage implementation in the ContentNode class, 
            // and so it keeps the list and loop modes consistent. 
            // TODO: Unify these implementations.
            var text = content.name
            for child in content.children {
                if child.type == .Text {
                    text = child.content!
                    break
                }
            }
            self.textLabel.text = text
        case .Text:
            self.textLabel.hidden = false
            self.textLabel.text = content.content
            if content.pronunciation {
                self.audioContent = content
            }
        case .Image:
            self.imageView.hidden = false
            imageView.setImageWithContent(content)
//            if let imageURL = content.imageURL {
//                self.imageView.af_setImageWithURL(imageURL)
//            }
        case .Web:
            self.webView.hidden = false
            if let URL = content.contentURL {
                let request = NSURLRequest(URL: URL)
                self.webView.loadRequest(request)
            }
        case .Video:
            self.youTubeView.hidden = false
            self.videoContent = content
        }
    }

    // MARK: - Actions

    @IBAction func speak(sender: AnyObject) {
        guard let text = self.audioContent?.content else {
            print("ERROR: missing audio content")
            return
        }
        guard let language = self.audioContent?.language else {
            print("ERROR: missing audio language")
            return
        }
        TextToSpeechManager.sharedInstance.textToSpeech(text, language: language)
    }

    @IBAction func tap(sender: UITapGestureRecognizer) {
        self.showNextCard()
    }
}
