//
//  FullScreenTableViewCell.swift
//  GuideHero
//
//  Created by SoftDev on 12/8/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

protocol FullScreenCollectionViewCellDelegate {
    func moveToNextCard(gestureRecognizer: UIGestureRecognizer)
    func onFinishedPlaying(deckIndex: Int, cardIndex: Int)
}

class FullScreenCollectionViewCell: UICollectionViewCell{

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var hideControlsButton: UIButton! {
        didSet {
            hideControlsButton.setImage(UIImage(appImage:. arrowUp), forState: .Normal)
        }
    }

    @IBOutlet weak var showControlsButton: UIButton! {
        didSet {
            showControlsButton.setImage(UIImage(appImage: .arrowDown), forState: .Normal)
        }
    }

    @IBOutlet weak var detailBar: UIView!
    @IBOutlet weak var socialBar: UIView!
    
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var userBar : UIView!
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var thumbTitle: UILabel!
    @IBOutlet weak var cardTitle: UILabel!
    @IBOutlet weak var cardDescription : UILabel!
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var videoControlsView: UIView!
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var skipBackButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var skipForwardButton: UIButton!
    @IBOutlet weak var playedTime: UILabel!
    @IBOutlet weak var remainingTime: UILabel!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likeCount: UILabel!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentCount: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var shareCount: UILabel!
    
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var lblAskTitle : UILabel!
    
    @IBOutlet weak var arrowButtonY : NSLayoutConstraint!
    @IBOutlet weak var btnClose : UIButton!
    @IBOutlet weak var imgGradientBottom : UIImageView!
    @IBOutlet weak var imgGradientTop : UIImageView!
    
    var delegate: FullScreenCollectionViewCellDelegate?
    var content: ContentNode?
    var width: CGFloat = 1.0
    var height: CGFloat  = 1.0
    var deckIndex: Int = 0
    var cardIndex: Int = 0
    
    var player: AVPlayer?
    var playTimer: NSTimer?
    var isPlaying: Bool = false
    var duration: Float = 0.0
    var currentPlayTime: Float = 0.0   // represen

    var progressViews = [UIView]()

    var currentCardProgressBar: UIProgressView?

    var isFullScreen = false
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//

    func configureCell(contentNode: ContentNode?, index: Int, cIndex: Int, screenWidth: CGFloat, screenHeight: CGFloat) {
        content = contentNode
        width = screenWidth
        height = screenHeight
        
        mainView.tag = index
        hideControlsButton.tag = index
        showControlsButton.tag = index
        likeButton.tag = index
        commentButton.tag = index
        shareButton.tag = index
        
        deckIndex = index
        
        cardIndex = cIndex
        
        makeUI()
    }

    func handleImageAutoPlay() {
        self.duration = 5 // we want this to show for 5 seconds
        playTimer?.invalidate()

        self.currentPlayTime = 0
        playTimer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(self.handleImageAutoPlayCompleted), userInfo: nil, repeats: true)
    }

    func handleImageAutoPlayCompleted() {
        self.currentPlayTime += 0.2
        if self.currentPlayTime <= self.duration {
            handleUpdateProgressBar(self.currentPlayTime)
        } else {
            playTimer?.invalidate()
            delegate!.onFinishedPlaying(deckIndex, cardIndex: cardIndex)
        }
    }

    func handleUpdateProgressBar(current: Float) {
        let updateTo = Float(current) / self.duration
        dispatch_async(dispatch_get_main_queue()) { 
            self.currentCardProgressBar?.setProgress(updateTo, animated: true)
        }
    }
   
    func moveToNextCard(gestureRecognizer: UIGestureRecognizer) {
        
        if videoView.hidden == false {//video
            if hideControlsButton.hidden == true {
                if videoControlsView.hidden == true {
                    videoControlsView.hidden = false
                    topBar.hidden = true
                    showControlsButton.hidden = false
                } else {
                    videoControlsView.hidden = true
                    topBar.hidden = true
                    showControlsButton.hidden = true
                }
            } else {
                if isPlaying {
                    player!.pause()
                    playButton.setImage(UIImage(appImage: .videoPlayIcon), forState: .Normal)
                    playTimer?.invalidate()
                }

                self.delegate?.moveToNextCard(gestureRecognizer)
            }
            
        } else {//image
            
            if isFullScreen == true {
                self.showControlsButton.hidden = false
            } else {
                if isPlaying {
                    player!.pause()
                    playButton.setImage(UIImage(appImage: .videoPlayIcon), forState: .Normal)
                    playTimer?.invalidate()
                }
                
                self.delegate?.moveToNextCard(gestureRecognizer)
            }
        }
    }
    
    func moveToNextCardFromGive(gestureRecognizer: UIGestureRecognizer) {
        
        if videoView.hidden == false {//video
            
            if hideControlsButton.hidden == true {
                if videoControlsView.hidden == true {
                    videoControlsView.hidden = false
                    topBar.hidden = true
                    showControlsButton.hidden = false
                } else {
                    videoControlsView.hidden = true
                    topBar.hidden = true
                    showControlsButton.hidden = true
                }
            } else {
              // is there suppose to be some other condition
            }
        } else {//image
            if hideControlsButton.hidden == true {
                if topBar.hidden == true {
                    topBar.hidden = false
                } else {
                    topBar.hidden = true
                }
            } else {
              // is there suppose to be some other condition
            }
        }
        
    }
    
    func makeElementsUI(){

        userBar.layer.shadowColor = UIColor.blackColor().CGColor
        userBar.layer.shadowOffset = CGSizeMake(0, 2)
        userBar.layer.shadowOpacity = 0.5
        userBar.layer.shadowRadius = 10
        
        btnClose.layer.shadowColor = UIColor.blackColor().CGColor
        btnClose.layer.shadowOffset = CGSizeMake(0, 2)
        btnClose.layer.shadowOpacity = 0.5
        btnClose.layer.shadowRadius = 4
        
        hideControlsButton.layer.shadowColor = UIColor.blackColor().CGColor
        hideControlsButton.layer.shadowOffset = CGSizeMake(0, 2)
        hideControlsButton.layer.shadowOpacity = 0.5
        hideControlsButton.layer.shadowRadius = 4
        
        cardTitle.layer.shadowColor = UIColor.blackColor().CGColor
        cardTitle.layer.shadowOffset = CGSizeMake(0, 2)
        cardTitle.layer.shadowOpacity = 0.5
        cardTitle.layer.shadowRadius = 4
        
        cardDescription.layer.shadowColor = UIColor.blackColor().CGColor
        cardDescription.layer.shadowOffset = CGSizeMake(0, 2)
        cardDescription.layer.shadowOpacity = 0.5
        cardDescription.layer.shadowRadius = 4
        
        showControlsButton.layer.shadowColor = UIColor.blackColor().CGColor
        showControlsButton.layer.shadowOffset = CGSizeMake(0, 2)
        showControlsButton.layer.shadowOpacity = 0.5
        showControlsButton.layer.shadowRadius = 2
        
        commentButton.layer.shadowColor = UIColor.blackColor().CGColor
        commentButton.layer.shadowOffset = CGSizeMake(0, 2)
        commentButton.layer.shadowOpacity = 0.5
        commentButton.layer.shadowRadius = 4
        
        commentCount.layer.shadowColor = UIColor.blackColor().CGColor
        commentCount.layer.shadowOffset = CGSizeMake(0, 2)
        commentCount.layer.shadowOpacity = 0.5
        commentCount.layer.shadowRadius = 4
        
        likeCount.layer.shadowColor = UIColor.blackColor().CGColor
        likeCount.layer.shadowOffset = CGSizeMake(0, 2)
        likeCount.layer.shadowOpacity = 0.5
        likeCount.layer.shadowRadius = 4
        
        likeButton.layer.shadowColor = UIColor.blackColor().CGColor
        likeButton.layer.shadowOffset = CGSizeMake(0, 2)
        likeButton.layer.shadowOpacity = 0.5
        likeButton.layer.shadowRadius = 4
        
        shareButton.layer.shadowColor = UIColor.blackColor().CGColor
        shareButton.layer.shadowOffset = CGSizeMake(0, 2)
        shareButton.layer.shadowOpacity = 0.5
        shareButton.layer.shadowRadius = 4
        
        shareCount.layer.shadowColor = UIColor.blackColor().CGColor
        shareCount.layer.shadowOffset = CGSizeMake(0, 2)
        shareCount.layer.shadowOpacity = 0.5
        shareCount.layer.shadowRadius = 4
        
        
        videoControlsView.layer.shadowColor = UIColor.blackColor().CGColor
        videoControlsView.layer.shadowOffset = CGSizeMake(0, 2)
        videoControlsView.layer.shadowOpacity = 0.5
        videoControlsView.layer.shadowRadius = 4

    }
    
    func makeUI() {
        
        self.mainView.clipsToBounds = true
        self.mainView.layer.cornerRadius = 20
        
        makeElementsUI()
        makeProfileUI()
        makeContentUI()
        makeSocialUI()
    }
    
    func makeProfileUI() {
        var currentContent = content
        if (currentContent == nil) {
            return
        }
        
        for progressView in progressViews {
            progressView.removeFromSuperview()
        }
        progressViews.removeAll()
    
        var position = 1
        var total = 1
        if (currentContent!.children.count > 0) {
            currentContent = currentContent!.children[cardIndex]
            position = cardIndex + 1
            total = currentContent!.parentContent!.children.count
        }
    
        let segmentWidth = (width - CGFloat(5 * (total + 1))) / CGFloat(total)
        for i in 0...total {
            let barFrame = CGRect(x: 0.0, y: 1.5, width: segmentWidth, height: 5.0)
            let progressView = UIView(frame: CGRect(x: 5.0 + CGFloat(i) * (segmentWidth + 5.0), y: 7.0, width: segmentWidth, height: 5.0))

            if (i < position) {
                progressView.backgroundColor = UIColor.whiteColor()

                if (i == position - 1) {
                    // current one, show progress bar
                    currentCardProgressBar = UIProgressView(frame: barFrame)
                    currentCardProgressBar!.transform = CGAffineTransformScale(currentCardProgressBar!.transform, 1, 2.5)
                    currentCardProgressBar!.layer.cornerRadius = 2.0
                    currentCardProgressBar?.backgroundColor = UIColor.progressBarCurrent
                    currentCardProgressBar?.setProgress(0, animated: false)

                    currentCardProgressBar?.trackTintColor = UIColor.clearColor() //  UIColor.whiteColor().colorWithAlphaComponent(0.5)
                    currentCardProgressBar?.progressTintColor = UIColor.whiteColor()
                    progressView.backgroundColor = UIColor.clearColor()
                    progressView.addSubview(currentCardProgressBar!)
                }
            }
            else {
                progressView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
            }

            progressView.layer.cornerRadius = 2.0
            topBar.addSubview(progressView)
            progressViews.append(progressView)
        }
        
        if let creatorPhotoUrl = currentContent!.creatorThumbnailURL {
            userPhoto.af_setImageWithURL(creatorPhotoUrl)
        }

        let nameAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(appFont: .bold, size: 20)]

        let idAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(appFont: .semiBold, size: 15)]

        let bioAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(appFont: .semiBold, size: 13)]

        let timeAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(appFont: .semiBold, size: 13)]
        
        let partName = NSMutableAttributedString(string: currentContent!.creatorInfo!.first_name + " " + currentContent!.creatorInfo!.last_name + "     ", attributes: nameAttributes)
        let partId = NSMutableAttributedString(string: currentContent!.creator + "\n", attributes: idAttributes)
        let partBio = NSMutableAttributedString(string: currentContent!.creatorInfo!.bio + "     ", attributes: bioAttributes)
        let partTime = NSMutableAttributedString(string: currentContent!.formattedCreationDate, attributes: timeAttributes)

        
        let combination = NSMutableAttributedString()
        
        combination.appendAttributedString(partName)
        combination.appendAttributedString(partId)
        combination.appendAttributedString(partBio)
        combination.appendAttributedString(partTime)
        
        userName.attributedText = combination
    }
    
    func makeContentUI() {
        hideControls(false)
        
        var currentContent = content
        if (currentContent == nil) {
            return
        }
        if (currentContent!.children.count > 0) {
            currentContent = currentContent!.children[cardIndex]
        }
        
//        let titleAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: Helper.proximaNova("Bold", font: 20)]
//        let descAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: Helper.proximaNova("semibold", font: 15)]
//        
//        let parttitle = NSMutableAttributedString(string: currentContent!.name + "\n\n", attributes: titleAttributes)
//        let partdesc = NSMutableAttributedString(string: currentContent!.description!, attributes: descAttributes)
//        
//        let combination = NSMutableAttributedString()
//        combination.appendAttributedString(parttitle)
//        combination.appendAttributedString(partdesc)
//        
//        cardTitle.attributedText = combination
        
        cardTitle.text = currentContent!.name
        cardDescription.text = currentContent!.description!
        
        if self.content!.ask_enabled == true {
            self.lblAskTitle.hidden = false
            self.lblAskTitle.text = "Anyone"
            
        } else {
            if self.content!.joined_users.count > 1 {
                self.lblAskTitle.hidden = true
                
            } else if self.content!.joined_users.count == 1 {
                self.lblAskTitle.hidden = true
            } else {
                self.lblAskTitle.hidden = false
                self.lblAskTitle.text = "Anyone"
            }
        }
        
        let endDate = self.content!.evaluation_end_dt
        
        if endDate == nil || endDate <= NSDate() {
            lblTime.text = "Ended"
        } else {
            let diff = endDate!.difference(NSDate(), unitFlags: NSCalendarUnit(rawValue: UInt.max))
            let hourDiff = -(diff.hour)
            let minDiff = -(diff.minute)
            lblTime.text = "\(hourDiff)h \(minDiff)m"
        }
        
        cardImage.image = nil
        backgroundImage.image = nil
        thumbTitle.hidden = true
        cardImage.backgroundColor = UIColor.clearColor()
        
        videoControlsView.hidden = true
        videoView.hidden = true

        if (videoView.layer.sublayers != nil) {
            for layer in videoView.layer.sublayers! {
                layer.removeFromSuperlayer()
            }
        }

        timeSlider.minimumValue = 0.0
        timeSlider.value = 0.0
        isPlaying = false
        playButton.setImage(UIImage(appImage: .videoPlayIcon), forState: .Normal)
        playButton.enabled = false
        timeSlider.enabled = false

        playTimer?.invalidate()
        
        if let imageUrl = currentContent!.imageURL {
            cardImage.backgroundColor = UIColor.clearColor()
            let imageDownloader = cardImage.af_imageDownloader ?? UIImageView.af_sharedImageDownloader
            let request = NSMutableURLRequest(URL: imageUrl)
            let imageCache = GuideHeroImageCache.sharedInstance.getCache()
            let cachedImage = imageCache.imageForRequest(request)

            if cachedImage != nil {
                self.setImage(cachedImage!, currentContent: currentContent!)
            } else {
                imageDownloader.downloadImage(URLRequest: request) { response in
                    if response.result.isSuccess {
                        let image = response.result.value
                        self.setImage(image!, currentContent: currentContent!)
                        imageCache.addImage(image!, forRequest: request)
                    }
                }
            }
            
            if isPlaying {
                player!.pause()
                playButton.setImage(UIImage(appImage: .videoPlayIcon), forState: .Normal)
                playTimer?.invalidate()
            }

            self.handleImageAutoPlay()
        }
        else if let videoUrl = currentContent!.videoURL {
//            hideControlsButtonBottomConstraint.constant = -85
            videoControlsView.hidden = true
            videoView.hidden = false
            
            self.player = AVPlayer(URL: videoUrl)
            
            let playerLayer = AVPlayerLayer(player: self.player)
            
            playerLayer.frame = self.videoView.frame
            self.videoView.layer.addSublayer(playerLayer)
            self.player!.pause()
            
            let qualityOfServiceClass = QOS_CLASS_BACKGROUND
            let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
            dispatch_async(backgroundQueue, {
                print("This is run on the background queue")
                
                self.duration = Float(CMTimeGetSeconds(self.player!.currentItem!.asset.duration))
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    print("This is run on the main queue, after the previous code in outer block")
                    
                    self.timeSlider.maximumValue = self.duration * 100
                    self.playButton.enabled = true
                    self.skipBackButton.enabled = true
                    self.skipForwardButton.enabled = true
                    self.timeSlider.enabled = true
                    self.onPlay(self)
                })
            })
        }
        else {
            if isPlaying {
                
                player!.pause()
                playButton.setImage(UIImage(appImage: .videoPlayIcon), forState: .Normal)
                playTimer?.invalidate()
            }
            thumbTitle.text = currentContent!.name
            thumbTitle.hidden = false
            cardImage.backgroundColor = UIColor.whiteColor()
        }
    }
    
    func setImage(image: UIImage, currentContent: ContentNode) {
        let size = image.size
        switch currentContent.image_scale {
            case 0:
                self.cardImage.image = nil
                self.backgroundImage.image = image
                if (self.width / size.width > self.height / size.height) {
                    self.backgroundImage.contentMode = .ScaleAspectFill
                }
                else {
                    self.backgroundImage.contentMode = .ScaleAspectFit
                }
                break
            case 1:
                self.cardImage.image = nil
                self.backgroundImage.image = image
                if (self.height / size.height > self.width / size.width) {
                    self.backgroundImage.contentMode = .ScaleAspectFill
                }
                else {
                    self.backgroundImage.contentMode = .ScaleAspectFit
                }
                break
            case 2:
                self.cardImage.image = nil
                self.backgroundImage.image = image
                if (self.height / size.width > self.height / size.height) {
                    backgroundImage.contentMode = .ScaleAspectFill
                }
                else {
                    backgroundImage.contentMode = .ScaleAspectFit
                }
                break
            case 3:
                self.cardImage.image = nil
                self.backgroundImage.image = image
                if (self.height / size.height > self.width / size.width) {
                    backgroundImage.contentMode = .ScaleAspectFill
                }
                else {
                    backgroundImage.contentMode = .ScaleAspectFit
                }
                break

            default:
                break
        }
    }

    func makeSocialUI() {
        var currentContent = content
        if (currentContent == nil) {
            return
        }
        if (currentContent!.children.count > 0) {
            currentContent = currentContent!.children[cardIndex]
        }
        
        likeCount.text = String(currentContent!.likes)
        if (currentContent!.liked_by_me) {
            likeButton.setImage(UIImage(appImage: .heartSFilled), forState: .Normal)
        }
        else {
            likeButton.setImage(UIImage(appImage: .heartS), forState: .Normal)
        }
        commentCount.text = String(currentContent!.comments.count)
        shareCount.text = String(currentContent!.followers)
    }
    
    
    ////////////////////////////////
    
    func onHideFullScreen() {
    
        var currentContent = content
        if (currentContent == nil) {
            return
        }

        isFullScreen = false

        imgGradientBottom.hidden = false
        imgGradientTop.hidden = false
        topBar.hidden = false
        hideControlsButton.hidden = false
        detailBar.hidden = false
        socialBar.hidden = false
        cardTitle.hidden = false
        cardDescription.hidden = false
        showControlsButton.hidden = true
        
        if (currentContent!.children.count > 0) {
            currentContent = currentContent!.children[cardIndex]
        }
        
        videoControlsView.hidden = true
        
        arrowButtonY.constant = 18

        self.mainView.clipsToBounds = true
        self.mainView.layer.cornerRadius = 20
    }
    
    func onShowFullScreen() {
        
        var currentContent = content
        if (currentContent == nil) {
            return
        }

        isFullScreen = true

        imgGradientBottom.hidden = true
        imgGradientTop.hidden = true
        topBar.hidden = true
        hideControlsButton.hidden = true

        detailBar.hidden = true
        socialBar.hidden = true
        cardTitle.hidden = true
        cardDescription.hidden = true
        if (currentContent!.children.count > 0) {
            currentContent = currentContent!.children[cardIndex]
        }
        
        videoControlsView.hidden = true
        
        if videoView.hidden == true {
            arrowButtonY.constant = 18
            showControlsButton.hidden = true
        } else {
            arrowButtonY.constant = 155
            showControlsButton.hidden = true
        }

        self.mainView.clipsToBounds = false
        self.mainView.layer.cornerRadius = 0
    }
    
    func hideControls(isHide: Bool) {
        hideControlsButton.hidden = isHide
        showControlsButton.hidden = !isHide
        detailBar.hidden = isHide
        socialBar.hidden = isHide
        cardTitle.hidden = isHide
        cardDescription.hidden = isHide
        
        var currentContent = content
        if (currentContent == nil) {
            return
        }
        if (currentContent!.children.count > 0) {
            currentContent = currentContent!.children[cardIndex]
        }
        
        videoControlsView.hidden = !isHide
        
        if currentContent!.videoURL == nil {
            videoControlsView.hidden = true
            arrowButtonY.constant = 18
        }else{
            arrowButtonY.constant = 18+90+20
        }
        
        topBar.hidden = false
    }
    
    ////////////////////////////////
    
    @IBAction func onSkipBack(sender: AnyObject) {
        var currentPosition = Float(CMTimeGetSeconds(player!.currentItem!.currentTime()))
        currentPosition = currentPosition - 15
        if (currentPosition < 0) {
            currentPosition = 0
        }
        timeSlider.value = currentPosition * 100
        onSlider(self)
    }
    
    @IBAction func onSkipForward(sender: AnyObject) {
        var currentPosition = Float(CMTimeGetSeconds(player!.currentItem!.currentTime()))
        currentPosition = currentPosition + 15
        if (currentPosition > self.duration) {
            currentPosition = self.duration
        }
        timeSlider.value = currentPosition * 100
        onSlider(self)
    }
    
    @IBAction func onPlay(sender: AnyObject) {
        isPlaying = !isPlaying
        if (isPlaying) {
            player!.play()
            playButton.setImage(UIImage(appImage: .videoPauseIcon), forState: .Normal)
            playTimer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(onMoveSlider), userInfo: nil, repeats: true)
        }
        else {
            player!.pause()
            playButton.setImage(UIImage(appImage: .videoPlayIcon), forState: .Normal)
            playTimer?.invalidate()
        }
    }
    
    @IBAction func onSlider(sender: AnyObject) {
        playedTime.text = makeTimeString(Int(timeSlider.value) / 100)
        remainingTime.text = "-" + makeTimeString(Int(duration * 100) / 100 - Int(timeSlider.value) / 100)
        player!.seekToTime(CMTime(seconds: Double(timeSlider.value) / 100, preferredTimescale: player!.currentItem!.asset.duration.timescale), toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
    }
    
    func onMoveSlider() {
        timeSlider.value = Float(CMTimeGetSeconds(player!.currentItem!.currentTime())) * 100.0

        self.handleUpdateProgressBar(timeSlider.value / 100)
        playedTime.text = makeTimeString(Int(timeSlider.value) / 100)
        remainingTime.text = "-" + makeTimeString(Int(duration * 100) / 100 - Int(timeSlider.value) / 100)
        if (timeSlider.value == Float(duration * 100.0)) {
            isPlaying = false
            playTimer?.invalidate()
            playButton.setImage(UIImage(appImage: .videoPlayIcon), forState: .Normal)
            timeSlider.value = 0.0
            player!.pause()
            onSlider(self)
            
            if hideControlsButton.hidden == true {
                videoControlsView.hidden = false
            }
            
            topBar.hidden = false
            
            delegate!.onFinishedPlaying(deckIndex, cardIndex: cardIndex)
        }
    }
    
    func makeTimeString(timeInSec: Int) -> String {
        var secString = String(timeInSec % 60)
        if (timeInSec % 60 < 10) {
            secString = "0" + secString
        }
        
        var minString = String(timeInSec % 3600 / 60)
        if (timeInSec % 3600 / 60 < 10) {
            minString = "0" + minString
        }
        
        return minString + ":" + secString
    }
   
    ////////////////////////////////
}
extension CAGradientLayer {
    
    func turquoiseColor() -> CAGradientLayer {
        let topColor = UIColor(red: (15/255.0), green: (118/255.0), blue: (128/255.0), alpha: 1)
        let bottomColor = UIColor(red: (84/255.0), green: (187/255.0), blue: (187/255.0), alpha: 1)
        
        let gradientColors: [CGColor] = [topColor.CGColor, bottomColor.CGColor]
        let gradientLocations: [Float] = [0.0, 1.0]
        
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.locations = gradientLocations
        
        return gradientLayer
    }
}
