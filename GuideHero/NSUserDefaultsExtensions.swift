//
//  NSUserDefaultsExtensions.swift
//  GuideHero
//
//  Created by Justin Holman on 1/31/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import Foundation

extension NSUserDefaults {

    var lastFeedTab: String {
        get {
            if let value = NSUserDefaults.standardUserDefaults().valueForKey("lastFeedTab") as? String {
                return value
            }

            return "All"
        }
        set (value) {
            NSUserDefaults.standardUserDefaults().setObject(value, forKey: "lastFeedTab")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }

    var userInfo: [String: AnyObject]? {
        get {
            if let userInfo =  NSUserDefaults.standardUserDefaults().valueForKey("guidehero.user") as? [String: AnyObject] {
                return userInfo
            }
            return nil
        }
        set (userData) {
            NSUserDefaults.standardUserDefaults().setObject(authToken, forKey: "guidehero.user")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }

    var authToken: String {
        get {
            return NSUserDefaults.standardUserDefaults().valueForKey("guidehero.authtoken") as! String
        }
        set (authToken) {
            NSUserDefaults.standardUserDefaults().setObject(authToken, forKey: "guidehero.authtoken")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
}
