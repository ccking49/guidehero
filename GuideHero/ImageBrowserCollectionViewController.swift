//
//  ImageBrowserCollectionViewController.swift
//  GuideHero
//
//  Created by Dino Bartosak on 11/10/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

private let reuseIdentifier = "ImageBrowserCollectionViewCell"

protocol ImageBrowserCollectionViewControllerDelegate {
    func imageBrowserCollectionViewController(controller: ImageBrowserCollectionViewController,
                                              didSelectPhotoWithURL photoURL: NSURL);
}

class ImageBrowserCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var photos: [NSURL]!
    var delegate: ImageBrowserCollectionViewControllerDelegate?
    
    // MARK: UIViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.navigationController?.setNavigationBarHidden(false, animated: true);
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated);
        self.navigationController?.setNavigationBarHidden(true, animated: true);
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count;
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: ImageBrowserCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ImageBrowserCollectionViewCell;
        
        let photoURL = photos[indexPath.row];
        cell.photoURL = photoURL;
        
        return cell;
    }

    // MARK: UICollectionViewDelegate

    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        delegate?.imageBrowserCollectionViewController(self, didSelectPhotoWithURL: photos[indexPath.row]);
    }
    
    // MARK: UICollectionViewDelegateFlowLayout
    
    private let cellInset: CGFloat = 3.0;
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let cellSize = (self.view.bounds.size.width - cellInset) / 2.0;
        return CGSizeMake(cellSize, cellSize)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return cellInset;
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return cellInset;
    }
    
}
