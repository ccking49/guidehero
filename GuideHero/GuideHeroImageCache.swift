//
//  ImageCache.swift
//  GuideHero
//
//  Created by Yohei Oka on 12/24/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import AlamofireImage


class GuideHeroImageCache {
    static let sharedInstance = GuideHeroImageCache()
    private let imageCache = AutoPurgingImageCache()
    private init() {}

    func getCache() -> AutoPurgingImageCache {
        return imageCache
    }
}
