//
//  AskEditCell.swift
//  GuideHero
//
//  Created by HC on 11/15/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya
import KMPlaceholderTextView


class AskJoinCell: BaseTableViewCell, UITextViewDelegate, UITextFieldDelegate {

    
    @IBOutlet weak var lblOriginalPoint: UILabel!
    @IBOutlet weak var lblJoinPoint : UILabel!
    @IBOutlet weak var btnJoinUs : UIButton!
    
    var onJoinAsk: (() -> Void)!
    
    var content: ContentNode! {
        didSet {
            updateUI()
        }
    }
    
    func updateUI() {
        
    }
    
    @IBAction func onJoinAsk(sender: UIButton) {
     
        onJoinAsk()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: - @IBAction
    
    
}
