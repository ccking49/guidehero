//
//  TrendingViewController.swift
//  GuideHero
//
//  Created by forever on 12/9/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AlamofireImage
import Moya

public protocol TrendingViewControllerDelegate: class {
    func changeBottomConstraint(constant:CGFloat)
}

class TrendingViewController: BasePostLoginController, FeedTableViewCellDelegate, UIScrollViewDelegate {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.registerFeedTableViewCell()
            tableView.setDefaultFooter()
            
        }
    }

    var categoryType = "All"
    var filteredIndexArray = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.showProgressHUD()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        tabBarController!.tabBar.hidden = false
        loadData()

        if (tableView.contentSize.height > tableView.frame.size.height) {
            //reach bottom
            NSNotificationCenter.defaultCenter().postNotificationName("ChangeBottomConstraint", object: self, userInfo: ["constraint": -49])
        } else {
            NSNotificationCenter.defaultCenter().postNotificationName("ChangeBottomConstraint", object: self, userInfo: ["constraint": 0])
        }
    }
    
    func updatePageChanged(data:NSNotification) {
        
        var tIndex : Int
        tIndex = data.userInfo!["tagIndex"] as! Int

        if Constants.showPrintDebug {
            print("Index passed in: \(tIndex), current view tag: \(self.view.tag)")
        }

        if Int(self.view.tag) == tIndex {
            if (tableView.contentSize.height > tableView.frame.size.height) {
                //reach bottom
                NSNotificationCenter.defaultCenter().postNotificationName("ChangeBottomConstraint", object: self, userInfo: ["constraint": -49])
            } else {
                NSNotificationCenter.defaultCenter().postNotificationName("ChangeBottomConstraint", object: self, userInfo: ["constraint": 0])
            }
        }
    }

    func loadData() {
        filteredIndexArray.removeAll()
        
        if (DataStore.mainContent == nil) {
            return
        }
        
        if (categoryType == "All") {
            for (index, _) in DataStore.mainContent!.children.enumerate() {
                filteredIndexArray.append(index)
            }
        }
        else if (categoryType == "Ongoing Contests") {
            let now = NSDate()
            for (index, child) in DataStore.mainContent!.children.enumerate() {
                if child.evaluation_end_dt?.compare(now) == NSComparisonResult.OrderedDescending || child.evaluation_end_dt?.compare(now) == NSComparisonResult.OrderedSame {
                    filteredIndexArray.append(index)
                }
            }
        }
        else if (categoryType == "Trending") {
            for (index, child) in DataStore.mainContent!.children.enumerate() {
                if child.likes > 0 {
                    filteredIndexArray.append(index)
                }
            }
            
//            filteredIndexArray.so
            
//            filteredIndexArray.sortInPlace({ (node1, node2) -> Bool in
//                return node1.likes > node2.likes
//            })
        }
        else {
            for (index, mainChild) in DataStore.mainContent!.children.enumerate() {
                var bFlag = false
                for children in mainChild.children {
                    for tag in children.tags {
                        if tag.lowercaseString.rangeOfString(categoryType.lowercaseString) != nil {
                            bFlag = true
                            break
                        }
                    }
                }
                
                if (bFlag) {
                    filteredIndexArray.append(index)
                }
            }
        }
        
        tableView.reloadData()
    }
    
    func onGotoProfilePage(sender : UIButton){
        let profileVC = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "TabProfileController") as! TabProfileController
        
        let cData : ContentNode = DataStore.mainContent!.children[filteredIndexArray[sender.tag]]
        profileVC.userInfo = cData.creatorInfo
        
        profileVC.modalPresentationStyle = .OverFullScreen
        profileVC.isShow = true
        self.presentViewController(profileVC, animated: true, completion: nil)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            //reach bottom
            NSNotificationCenter.defaultCenter().postNotificationName("ChangeBottomConstraint", object: self, userInfo: ["constraint": 0])
        }
        
        if (scrollView.contentOffset.y < 0){
            //reach top
            NSNotificationCenter.defaultCenter().postNotificationName("ChangeBottomConstraint", object: self, userInfo: ["constraint": -49])
        }
        
        if (scrollView.contentOffset.y > 0 && scrollView.contentOffset.y < (scrollView.contentSize.height - scrollView.frame.size.height)){
            //not top and not bottom
            NSNotificationCenter.defaultCenter().postNotificationName("ChangeBottomConstraint", object: self, userInfo: ["constraint":-49])
        }
        
    }/*
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset
        let  bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y: Float = Float(offset.y) + Float(bounds.size.height) + Float(inset.bottom)
        let height: Float = Float(size.height)
        
        if (y >= height) || (scrollView.contentOffset.y <= 0) {
//            self.trendingVCDelegate?.changeBottomConstraint(0)
            NSNotificationCenter.defaultCenter().postNotificationName("ChangeBottomConstraint", object: self, userInfo: ["constraint": 0])
        } else {
            NSNotificationCenter.defaultCenter().postNotificationName("ChangeBottomConstraint", object: self, userInfo: ["constraint":-49])
        }
        
    }*/
    
    // MARK: - Private methods

    func showFullScreen(gestureRecognizer: UIGestureRecognizer) {
        let index = gestureRecognizer.view!.tag
        
        let fullScreenViewController = storyboard!.instantiateViewControllerWithIdentifier("FullScreenViewController") as! FullScreenViewController
        
        fullScreenViewController.content = DataStore.mainContent!
        fullScreenViewController.deckIndex = index

        fullScreenViewController.modalPresentationStyle = .OverFullScreen
        self.presentViewController(fullScreenViewController, animated: true, completion: nil)
    }
    
    ////////////////////////////////
    
    func onUIUpdate() {
        loadData()
    }
    

    ////////////////////////////////
}

private typealias TableDatasource = TrendingViewController
extension TableDatasource: UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = nil

        if filteredIndexArray.count == 0 {
            tableView.createBackgroundView(withText: "Sorry, there are no feeds")
        }
        return filteredIndexArray.count+1
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            return tableView.dequeueReusableCellWithIdentifier("Blank")!
        }
        let cell = tableView.dequeueReusableCellWithIdentifier("FeedTableViewCell", forIndexPath: indexPath) as! FeedTableViewCell
        cell.delegate = self
        cell.content = DataStore.mainContent!.children[filteredIndexArray[indexPath.row-1]]
        cell.cardView.tag = indexPath.row - 1
        cell.btnLike.tag = indexPath.row - 1
        cell.imgLock.hidden = true

        cell.btnProfileName.tag = indexPath.row - 1
        cell.btnProfilePhoto.tag = indexPath.row - 1

        cell.btnProfileName.addTarget(self, action: #selector(self.onGotoProfilePage(_:)), forControlEvents: .TouchUpInside)
        cell.btnProfilePhoto.addTarget(self, action: #selector(self.onGotoProfilePage(_:)), forControlEvents: .TouchUpInside)

        let singleTap = UITapGestureRecognizer(target: self, action: #selector(self.showFullScreen))
        cell.cardView.addGestureRecognizer(singleTap)

        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return 5
        }
        return 221
    }
}

private typealias TableDelegate = TrendingViewController
extension TableDelegate: UITableViewDelegate {

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let cardDetailVC = UIStoryboard.loadViewController(storyboardName: "CardDetail", viewControllerIdentifier: "CardDetailPageVC") as! CardDetailPageVC
        cardDetailVC.content = DataStore.mainContent!.children[filteredIndexArray[indexPath.row - 1]]

        cardDetailVC.isFirst = true

        cardDetailVC.contentIndex = indexPath.row - 1

        cardDetailVC.submittedCallback = { rContent in
            // Card submitted here

            //            cardDetailVC.content = rContent
            if rContent != nil {
                DataStore.mainContent!.children[self.filteredIndexArray[indexPath.row-1]] = rContent
            }

            self.tableView.reloadData()
        }

        tabBarController?.presentViewController(cardDetailVC, animated: true, completion: nil)
    }
}
