//
//  PannableCardView.swift
//  GuideHero
//
//  Created by Promising Change on 16/01/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import Foundation
import UIKit

public protocol PannableCardViewDelegate: class {
    func cardSwipedAway()
    func cardWasDragged(dragPercentage: CGFloat)
    func cardStartedDismissal(duration: NSTimeInterval, dragPercentage: Float)
    func resetCardPosition()
}

public class PannableCardView: UIView {
    
    // MARK: - Constants
    
    private let cardSwipeActionAnimationDuration: NSTimeInterval  = 0.3
    private let cardResetAnimationDuration: NSTimeInterval = 0.2
    
    // MARK: - Variables
    
    weak var delegate: PannableCardViewDelegate?
    
    var isRotating = false
    
    var firstTouchPoint = CGPoint.zero
    var selfCenterPoint = CGPoint.zero
    
    // MARK: - Methods
    
    func handlePanGesture(panGesture: UIPanGestureRecognizer) {
        
        let factor: CGFloat = 2
        let angleLimit = CGFloat(M_PI_4)
        
        switch panGesture.state {
        case .Began:
            
            firstTouchPoint = panGesture.locationInView(self)
            selfCenterPoint = self.center
            let velocity = panGesture.velocityInView(self)
            
            isRotating = fabs(velocity.x) > fabs(velocity.y)
            
        case .Changed:
            
            let touchLocation = panGesture.locationInView(self.superview)
            var dragPercentage: CGFloat = 0.0
            
            if isRotating {
                
                let rotationAngle = atan((touchLocation.x - firstTouchPoint.x) / (bounds.size.height * factor - touchLocation.y))
                var direction: CGFloat = 1
                if touchLocation.x != firstTouchPoint.x {
                    direction = (firstTouchPoint.x - touchLocation.x) / fabs(touchLocation.x - firstTouchPoint.x)
                }
                
                var transform = CGAffineTransformMakeTranslation( self.bounds.size.width * 0.5 * direction, self.bounds.size.height * (factor - 0.5))
                transform = CGAffineTransformRotate(transform, rotationAngle)
                transform = CGAffineTransformTranslate(transform, -self.bounds.size.width * 0.5 * direction, -self.bounds.size.height * (factor - 0.5))
                self.transform = transform
                
                dragPercentage = fabs(touchLocation.x - firstTouchPoint.x) / self.bounds.size.width
                
            } else {
                
                self.center = CGPoint(x: selfCenterPoint.x, y: selfCenterPoint.y + touchLocation.y - firstTouchPoint.y)
                dragPercentage = fabs(touchLocation.y - firstTouchPoint.y) / self.bounds.size.height
            }
            
            delegate?.cardWasDragged(dragPercentage)
            
        case .Ended:
            
            let touchLocation = panGesture.locationInView(self.superview)
            let velocity = panGesture.velocityInView(self)
            
            var dragPercentage: CGFloat = 0.0
            
            if isRotating {
                // Left or Right
                var startDirection = 1
                if touchLocation.x != firstTouchPoint.x {
                    startDirection = Int(round((touchLocation.x - firstTouchPoint.x) / fabs(touchLocation.x - firstTouchPoint.x)))
                }
                var velocityDirection = -startDirection
                if velocity.x != 0 {
                    velocityDirection = Int(round(velocity.x / fabs(velocity.x)))
                }
                if startDirection == velocityDirection { // should dismiss
                    dragPercentage = fabs(touchLocation.x - firstTouchPoint.x) / self.bounds.size.width
                    delegate?.cardStartedDismissal(cardSwipeActionAnimationDuration, dragPercentage: Float(dragPercentage))
                    
                    UIView.animateWithDuration(cardSwipeActionAnimationDuration, animations: { 
                        var transform = CGAffineTransformMakeTranslation(-self.bounds.size.width * 0.5 * CGFloat(startDirection), self.bounds.size.height * (factor - 0.5))
                        transform = CGAffineTransformRotate(transform, angleLimit * CGFloat(startDirection))
                        transform = CGAffineTransformTranslate(transform, self.bounds.size.width * 0.5 * CGFloat(startDirection), -self.bounds.size.height * (factor - 0.5))
                        
                        self.transform = transform
                        }, completion: { (finished) in
                            self.delegate?.cardSwipedAway()
                    })
                } else {
                    UIView.animateWithDuration(cardResetAnimationDuration, animations: { 
                        self.transform = CGAffineTransformIdentity
                        }, completion: { (finished) in
                            self.delegate?.resetCardPosition()
                    })
                }
                
            } else {
                // Up or Down
                var startDirection = 1
                if touchLocation.y != firstTouchPoint.y {
                    startDirection = Int(round((touchLocation.y - firstTouchPoint.y) / fabs(touchLocation.y - firstTouchPoint.y)))
                }
                var velocityDirection = -startDirection
                if velocity.y != 0 {
                    velocityDirection = Int(round(velocity.y / fabs(velocity.y)))
                }
                if startDirection == velocityDirection { // should dismiss
                    dragPercentage = fabs(touchLocation.y - firstTouchPoint.y) / self.bounds.size.height
                    delegate?.cardStartedDismissal(cardSwipeActionAnimationDuration, dragPercentage: Float(dragPercentage))
                    
                    UIView.animateWithDuration(cardSwipeActionAnimationDuration, animations: {
                        if (velocity.y > 0) {
                            // Down
                            self.center = CGPoint(x: self.selfCenterPoint.x, y: self.bounds.size.height * 1.5)
                        } else {
                            // Up
                            self.center = CGPoint(x: self.selfCenterPoint.x, y: -self.bounds.size.height * 0.5)
                        }

                        }, completion: { (finished) in
                            self.delegate?.cardSwipedAway()
                    })
                } else {
                    UIView.animateWithDuration(cardResetAnimationDuration, animations: {
                        self.center = self.selfCenterPoint
                        }, completion: { (finished) in
                            self.delegate?.resetCardPosition()
                    })
                    
                }
            }
            
        default:break
            
        }
    }
    
}
