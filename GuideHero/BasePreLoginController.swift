//
//  BasePreLoginController.swift
//  GuideHero
//
//  Created by Yohei Oka on 8/25/16.
//  Copyright © 2016 Twilio. All rights reserved.
//

import UIKit


class  BasePreLoginController: BaseController {
    
    func setAuthDataAndLogin(parseJSON: NSDictionary) {
        // set auth_token in NSUserDefaults
        let prefs = NSUserDefaults.standardUserDefaults()
        var userDic = parseJSON.valueForKeyPath("user") as! [String: AnyObject]
        for (key, value) in userDic {
            if value.isKindOfClass(NSNull) {
                userDic[key] = ""
            }
        }
        prefs.setValue(userDic, forKey: "guidehero.user")
        prefs.setValue(parseJSON.valueForKeyPath("auth_token"), forKey: "guidehero.authtoken")
        Session.sharedSession = Session()
        loginUser()
    }
    
    func loginUser() {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("mainTabController") as! UITabBarController
        
        // Update tabBarItem images
        let tabBar = vc.tabBar
        // Sets the default color of the background of the UITabBar
        tabBar.backgroundImage = UIImage.init()
        tabBar.shadowImage = UIImage.init()
        
        tabBar.backgroundColor = UIColor(white: 1, alpha: 0.8)
//        tabBar.barTintColor = UIColor.blueColor()
        
        let imageArray = ["tb_learn", "tb_create", "tb_notification", "tb_me"]
        
        for index in 0...imageArray.count - 1 {
            let tabBarItem = tabBar.items![index]
            tabBarItem.image = UIImage(named: imageArray[index])?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
            tabBarItem.selectedImage = UIImage(named: imageArray[index] + "_selected")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        }
        
        self.presentViewController(vc, animated: true, completion: nil)
    }
}
