//
//  UITableViewExtensions.swift
//  GuideHero
//
//  Created by Justin Holman on 1/31/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import Foundation

extension UITableView {

    func registerFeedTableViewCell() {
        let cellNib = UINib(nibName: "FeedTableViewCell", bundle: nil)
        self.registerNib(cellNib, forCellReuseIdentifier: "FeedTableViewCell")
    }

    func setDefaultFooter() {
        self.tableFooterView = UIView(frame:CGRectZero)
    }

    func createBackgroundView(withText message: String) {
        self.backgroundView = nil

        let containerView = UIView(frame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height))
        containerView.backgroundColor = UIColor.clearColor()

        let messageLabel = UILabel(frame: CGRectMake(0, 0, self.frame.size.width, 75))
        messageLabel.text = message

        let messageLabelHeight: CGFloat = 75

        messageLabel.sizeToFit()
        messageLabel.textAlignment = .Center
        messageLabel.backgroundColor = UIColor.clearColor()
        messageLabel.textColor = UIColor.whiteColor()

        messageLabel.font = UIFont(appFont: .standard, size: 15)

        messageLabel.translatesAutoresizingMaskIntoConstraints = false

        containerView.addSubview(messageLabel)

        let messageLeft = NSLayoutConstraint(item: messageLabel, attribute: NSLayoutAttribute.LeadingMargin, relatedBy: .Equal, toItem: containerView, attribute: .Leading, multiplier: 1.0, constant: 16)

        let messageRight = NSLayoutConstraint(item: messageLabel, attribute: NSLayoutAttribute.TrailingMargin, relatedBy: NSLayoutRelation.Equal, toItem: containerView, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: -16)

        let messageHeight = NSLayoutConstraint(item: messageLabel, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: messageLabelHeight)


        containerView.addConstraints([messageLeft, messageRight, messageHeight])


        let imageView = UIImageView() // frame: CGRectMake(0, 0, 100, 100)
        imageView.frame.size.height = 100
        imageView.frame.size.width = 100

//        imageView.tintColor = UIColor.appAssociateOpen

        imageView.contentMode = .ScaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        //        imageView.image = UIImage(appImage: .barcode)
        containerView.addSubview(imageView)

        let imageHeight = NSLayoutConstraint(item: imageView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 130)

        let imageWidth = NSLayoutConstraint(item: imageView, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 100)

        let imageCenter = NSLayoutConstraint(item: imageView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: containerView, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0)

        let imageCenterVertical = NSLayoutConstraint(item: imageView, attribute: .CenterY, relatedBy: .Equal, toItem: containerView, attribute: .CenterY, multiplier: 1, constant: 0)

        containerView.addConstraints([imageHeight, imageWidth, imageCenter, imageCenterVertical])

        let messageBottom = NSLayoutConstraint(item: messageLabel, attribute: .BottomMargin, relatedBy: .Equal, toItem: imageView, attribute: .Top, multiplier: 1, constant: -25)

        containerView.addConstraint(messageBottom)

        self.backgroundView = containerView
    }
}
