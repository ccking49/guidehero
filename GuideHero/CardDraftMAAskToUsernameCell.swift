//
//  CardDraftMAAskToUsernameCell.swift
//  GuideHero
//
//  Created by Promising Change on 01/02/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit

public class CardDraftMAAskToUsernameCell: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lbUsername: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configureView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.configureView()
    }
    
    /// Configures & Initializes the view.
    private func configureView() {
        NSBundle.mainBundle().loadNibNamed("CardDraftMAAskToUsernameCell", owner: self, options: nil)
        guard let content = contentView else { return }
        content.frame = self.bounds
        content.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.addSubview(content)
        
        self.setNeedsLayout()
    }
    
    public func bindUser(username: String) {
        lbUsername.text = "@" + username
    }

}
