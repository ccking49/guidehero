//
//  MoveCardNavigationController.swift
//  GuideHero
//
//  Created by Ascom on 10/29/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya

class MoveCardNavigationController: UINavigationController, UINavigationControllerDelegate {
    
    @IBOutlet var bottomView: UIView!
    @IBOutlet var hereButton: UIButton!
    var contentId: String = ""
    var cardIds: NSArray!
    var currentContent: ContentNode?
//    var drafts = [ContentNode]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let textAttributes = [
            NSFontAttributeName: UIFont.boldSystemFontOfSize(18),
            NSForegroundColorAttributeName: UIColor.whiteColor()
        ]
        self.navigationBar.titleTextAttributes = textAttributes
        delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let size = view.bounds.size
        let height = 44 as CGFloat
        bottomView.frame = CGRect(x: 0, y: size.height-height, width: size.width, height: height)
        view.addSubview(bottomView)
    }
    
    @IBAction func onHere(sender: UIButton) {
        contentId = currentContent?.id ?? ""
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(NetworkService.MoveCard(cardIds: cardIds as! [String], moveTo: contentId)) { result in
            let JSON = Helper.validateResponse(result, sender: self)
            if JSON != nil {
                print("success")
            }
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }

    @IBAction func onCancel(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        if viewController.isKindOfClass(CardDetailViewController) {
            let vc = viewController as! CardDetailViewController
            currentContent = vc.content
        } else if viewController.isKindOfClass(MoveCardRootViewController) {
            currentContent = nil
        }
    }
}
