//
//  CardDetailPlayVC.swift
//  GuideHero
//
//  Created by Promising Change on 23/12/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

protocol CardDraftDetailPlayVCDelegate {
    func onResponseContent(resContent : [ContentNode]! , nType : Int)
}

class CardDraftDetailPlayVC: CardSubPageBaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Variables
    
    @IBOutlet weak var tbCardList: UITableView!
    @IBOutlet weak var footerView : UIView!
    
    @IBOutlet weak var topView : UIView!
    
    var nHeaderHeight : Int = 540
    
    let tbHeaderView: UIView!
    var blankCellHeight: CGFloat!
    
    @IBOutlet weak var vwHeader: UIView!
    
    var cardDetailPlayDelegate : CardDraftDetailPlayVCDelegate?
    
    var isType : Int = 2
    
    var drafts = [ContentNode]()
    
    var content: ContentNode? {
        didSet {
            if isViewLoaded() {
                tbCardList.reloadData()
                self.view.layoutIfNeeded()
            }
        }
    }
    var submittedCallback: ((_: ContentNode!) -> Void)!
    
    // MARK: - View Lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        tbHeaderView = UIView()
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(nHeaderHeight)
        blankCellHeight = max(UIScreen.mainScreen().bounds.size.height - CGFloat(nHeaderHeight + 50) + 1, 0)
        
        // Do any additional setup after loading the view.
        
        
        scrollView = tbCardList as UIScrollView
        
        configureView()
        
        tbCardList.separatorStyle = .None
        
        if content?.children.count == 1 {
            singleCard = true
        }
        
        refreshData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    // MARK: - Custom Methods
    
    func configureView() {
        tbHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: CGFloat(nHeaderHeight))
        
        //        let cellNib = UINib(nibName: "ContentTableCell", bundle: nil)
        //        tbCardList.registerNib(cellNib, forCellReuseIdentifier: "ContentTableCell")
        
        let cellNib = UINib(nibName: "CardDetailTableViewCell", bundle: nil)
        tbCardList.registerNib(cellNib, forCellReuseIdentifier: "CardDetailTableViewCell")
        
        let blankCellNib = UINib(nibName: "RoundedBlankTableViewCell", bundle: nil)
        tbCardList.registerNib(blankCellNib, forCellReuseIdentifier: "RoundedBlankTableViewCell")

        tbCardList.rowHeight = UITableViewAutomaticDimension
        tbCardList.estimatedRowHeight = Constants.UIConstants.contentCellHeight
        tbCardList.separatorStyle = .SingleLine
        
        tbCardList.tableHeaderView = tbHeaderView
        //        tbCardList.tableFooterView = UIView(frame: CGRect.zero)
        
        footerView.layer.cornerRadius = 20
        footerView.layer.shadowRadius = 5
        footerView.layer.shadowOpacity = 0.7
        footerView.layer.shadowOffset = CGSizeZero
        footerView.layer.shadowColor = UIColor.blackColor().CGColor
        
//        footerView.layer.borderColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.5).CGColor
//        footerView.layer.borderWidth = 0.5
        footerView.clipsToBounds = true
        
        self.view.setNeedsLayout()
        
    }
    
    func refreshData() {
        tbCardList.reloadData()
    }
    
    // MARK: - UITableViewDelegate & UITableViewDataSource
//    
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        guard let content = content else { return 0 }
//        
//        
//        return (max(content.children.count, 1))
//    }
//    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        
//        return (content?.children.count ?? 0) > 0 ? 216 : blankCellHeight
//    }
//    
//    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let separatorView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 1))
//        separatorView.backgroundColor = UIColor(colorLiteralRed: 0.84, green: 0.84, blue: 0.84, alpha: 1)
//        
//        return separatorView
//    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let content = content else { return 0 }
        
        if content.ask_enabled == true {
            if content.children.count == 1 || content.children.count == 0 {
                return 1
            }
            
            return content.children.count-1
        }else{
            return content.children.count
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if self.content?.ask_enabled == true {
            if content!.children.count == 1 || content!.children.count == 0{
                return blankCellHeight
            }
            
            return 216
        }else{
            return 216
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let separatorView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 1))
        separatorView.backgroundColor = UIColor(colorLiteralRed: 0.84, green: 0.84, blue: 0.84, alpha: 1)
        
        return separatorView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        if self.content?.ask_enabled == true {
            
            if content?.children.count  == 0 || content?.children.count == 1 {
                return tableView.dequeueReusableCellWithIdentifier("Blank")!
            }
            
            let cell = tableView.dequeueReusableCellWithIdentifier("CardDetailTableViewCell", forIndexPath: indexPath) as! CardDetailTableViewCell
            cell.content = self.content!.children[indexPath.row + 1]
            
            cell.imgSeparate.hidden = false
            
            cell.separateView.hidden = true
            cell.imgSeparate.layer.shadowRadius = 2
            
            cell.imgSeparate.layer.shadowOffset = CGSizeZero
            cell.imgSeparate.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
            
            cell.btnProfilePhoto.tag = indexPath.row + 1
            cell.btnProfileName.tag = indexPath.row + 1
            cell.btnProfileName.addTarget(self, action: #selector(CardDraftDetailPlayVC.onGotoProfilePage(_:)), forControlEvents: .TouchUpInside)
            cell.btnProfilePhoto.addTarget(self, action: #selector(CardDraftDetailPlayVC.onGotoProfilePage(_:)), forControlEvents: .TouchUpInside)
            
            cell.cardView.tag = indexPath.row + 1
            cell.tag = indexPath.row + 1
            cell.imgLock.hidden = true
            
            
            let cellTap = UITapGestureRecognizer(target: self, action: #selector(CardDraftDetailPlayVC.openCellDetail))
            cell.addGestureRecognizer(cellTap)
            
            cell.cardView.tag = indexPath.row + 1
            let singleTap = UITapGestureRecognizer(target: self, action: #selector(CardDraftDetailPlayVC.showFullScreen))
            cell.cardView.addGestureRecognizer(singleTap)
            
            cell.layer.shadowRadius = 1
            cell.layer.shadowOffset = CGSizeZero
            cell.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
            
            cell.needsRoundBottom = true
            
            
            return cell
            
        }else{
            
            if content?.children.count  == 0 {
                return tableView.dequeueReusableCellWithIdentifier("Blank")!
            }
            
            let cell = tableView.dequeueReusableCellWithIdentifier("CardDetailTableViewCell", forIndexPath: indexPath) as! CardDetailTableViewCell
            cell.content = self.content!.children[indexPath.row]
            
            cell.imgSeparate.hidden = false
            
            cell.separateView.hidden = true
            cell.imgSeparate.layer.shadowRadius = 2
            
            cell.imgSeparate.layer.shadowOffset = CGSizeZero
            cell.imgSeparate.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
            
            cell.btnProfilePhoto.tag = indexPath.row
            cell.btnProfileName.tag = indexPath.row
            cell.btnProfileName.addTarget(self, action: #selector(CardDraftDetailPlayVC.onGotoProfilePage(_:)), forControlEvents: .TouchUpInside)
            cell.btnProfilePhoto.addTarget(self, action: #selector(CardDraftDetailPlayVC.onGotoProfilePage(_:)), forControlEvents: .TouchUpInside)
            
            cell.cardView.tag = indexPath.row
            cell.tag = indexPath.row
            cell.imgLock.hidden = true
            
            let cellTap = UITapGestureRecognizer(target: self, action: #selector(CardDraftDetailPlayVC.openCellDetail))
            cell.addGestureRecognizer(cellTap)
            
            cell.cardView.tag = indexPath.row
            let singleTap = UITapGestureRecognizer(target: self, action: #selector(CardDraftDetailPlayVC.showFullScreen))
            cell.cardView.addGestureRecognizer(singleTap)
            
            cell.layer.shadowRadius = 1
            cell.layer.shadowOffset = CGSizeZero
            cell.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
            
            cell.needsRoundBottom = true
            
            
            return cell
        }
        
    }
    func onGotoProfilePage(sender : UIButton){
        let profileVC = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "TabProfileController") as! TabProfileController
        
        let cData : ContentNode = self.content!.children[sender.tag]
        profileVC.userInfo = cData.creatorInfo
        profileVC.isShow = true
        profileVC.modalPresentationStyle = .OverFullScreen
        self.presentViewController(profileVC, animated: true, completion: nil)
    }
    
    func showFullScreen(gestureRecognizer: UIGestureRecognizer) {
        let index = gestureRecognizer.view!.tag
        
        let fullScreenViewController = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "FullScreenViewController") as! FullScreenViewController
        
        fullScreenViewController.content = content
        fullScreenViewController.deckIndex = index
        
        fullScreenViewController.modalPresentationStyle = .OverFullScreen
        self.presentViewController(fullScreenViewController, animated: true, completion: nil)
        
    }
    
    func openCellDetail(gestureRecognizer: UIGestureRecognizer) {
        let nIndex = gestureRecognizer.view!.tag
        let indexPath = NSIndexPath(forRow: nIndex, inSection: 0)
        
        if (content?.children.count ?? 0) != 0 {
            
            self.tbCardList.deselectRowAtIndexPath(indexPath, animated: true)
            let cardDetailVC = UIStoryboard.loadViewController(storyboardName: "CardDraft", viewControllerIdentifier: "CardDraftDetailVC") as! CardDraftDetailVC
            cardDetailVC.content = content?.children[nIndex]
            cardDetailVC.contentIndex = nIndex
            
            cardDetailVC.drafts = self.drafts
            
            cardDetailVC.modalPresentationStyle = .OverFullScreen
            
            cardDetailVC.submittedCallback = {rContent ,nType in
                // Card submitted here
               self.cardDetailPlayDelegate?.onResponseContent(rContent,nType: nType)
                
            }
//            let nav = UINavigationController()
//            nav.viewControllers = [cardDetailVC]
//            nav.navigationBarHidden = true
            self.presentViewController(cardDetailVC, animated: true, completion: nil)

        }
        
    }
    
    // MARK: - UIScrollViewDelegate
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
    }
    
}
