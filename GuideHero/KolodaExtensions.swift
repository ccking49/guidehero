//
//  KolodaExtensions.swift
//  GuideHero
//
//  Created by Ignacio Rodrigo on 9/13/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import Foundation
import Koloda

enum SwipeResultHorizontalDirection {
    case None
    case Left
    case Right
}

extension SwipeResultDirection {
    
    var horizontalDirection: SwipeResultHorizontalDirection {
        get {
            switch self {
            case .Left:
                return .Left
            case .Right:
                return .Right
            case .Up:
                return .None
            case .Down:
                return .None
            case .TopLeft:
                return .Left
            case .TopRight:
                return .Right
            case .BottomLeft:
                return .Left
            case .BottomRight:
                return .Right
            }
        }
        
    }
    
}
