//
//  UIImageExtensions.swift
//  GuideHero
//
//  Created by Justin Holman on 1/27/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {

    enum AppImages: String {
        case arrowDown = "arrow_down"
        case arrowUp = "arrow_up"
        case cameraContentNormal = "CameraContentNormal"
        case cameraRollContentNormal = "CameraRollContentNormal"
        case heartSFilled = "heart_sFilled"
        case heartS = "heart_s"
        case navBackground = "navbg"
        case placeholderPhoto = "photo_placeholder"
        case videoPlayIcon = "videoPlayIcon"
        case videoPauseIcon = "videoPauseIcon"
    }

    convenience init!(appImage: AppImages) {
        self.init(named: appImage.rawValue)
    }

    func cropInRect(rect: CGRect) -> UIImage {
        //        let scale = self.scale
        UIGraphicsBeginImageContext(CGSize(width: rect.width, height: rect.height))
        let contextImage: UIImage = UIImage(CGImage: self.CGImage!)
        ////        UIGraphicsBeginImageContext(CGSize(width: self.size.width * scale, height: self.size.height * scale))
        contextImage.drawInRect(CGRect(x: -rect.origin.x, y: -rect.origin.y, width: contextImage.size.width, height: contextImage.size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        return image!
        /*
         let contextImage: UIImage = UIImage(CGImage: self.CGImage!)
         // Create bitmap image from context using the rect
         let imageRef: CGImageRef = CGImageCreateWithImageInRect(contextImage.CGImage!, rect)!

         // Create a new image based on the imageRef and rotate back to the original orientation
         //        let image: UIImage = UIImage(CGImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
         let image: UIImage = UIImage(CGImage: imageRef)
         UIGraphicsBeginImageContext(CGSize(width: rect.width, height: rect.height))
         image.drawInRect(CGRect(x: -rect.origin.x, y: -rect.origin.y, width: image.size.width, height: image.size.height))
         let result = UIGraphicsGetImageFromCurrentImageContext()
         return result!*/
    }
}
