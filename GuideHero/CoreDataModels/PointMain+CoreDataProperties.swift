//
//  PointMain+CoreDataProperties.swift
//  
//
//  Created by Star on 1/6/17.
//
//

import Foundation
import CoreData


extension PointMain {

    @nonobjc public override class func fetchRequest() -> NSFetchRequest {
        return NSFetchRequest(entityName: "PointMain");
    }

    @NSManaged public var cumulative_earnings_gold: Int64
    @NSManaged public var cumulative_earnings_grey: Int64
    @NSManaged public var points_purchased_gold: Int64
    @NSManaged public var points_purchased_grey: Int64
    @NSManaged public var points_used_gold: Int64
    @NSManaged public var points_used_grey: Int64
    @NSManaged public var total_points_gold: Int64
    @NSManaged public var total_points_grey: Int64
    

}
