//
//  UsePoints+CoreDataProperties.swift
//  
//
//  Created by Star on 1/6/17.
//
//

import Foundation
import CoreData


extension UsePoints {

    @nonobjc public override class func fetchRequest() -> NSFetchRequest {
        return NSFetchRequest(entityName: "UsePoints");
    }

    @NSManaged public var use_point: Int64
    @NSManaged public var use_name: String?

}
