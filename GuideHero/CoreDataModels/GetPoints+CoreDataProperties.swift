//
//  GetPoints+CoreDataProperties.swift
//  
//
//  Created by Star on 1/6/17.
//
//

import Foundation
import CoreData


extension GetPoints {

    @nonobjc public override class func fetchRequest() -> NSFetchRequest {
        return NSFetchRequest(entityName: "GetPoints");
    }

    @NSManaged public var get_point: Int64
    @NSManaged public var get_name: String?
    

}
