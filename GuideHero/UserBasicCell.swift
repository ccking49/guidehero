//
//  UserBasicCell.swift
//  GuideHero
//
//  Created by HC on 11/19/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

class UserBasicCell: BaseTableViewCell {

    @IBOutlet var avatarImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var creatorLabel: UILabel!
    @IBOutlet var contributionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        avatarImage.roundView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populateWithUser(user: UserNode) {
        emptyLabels()
        avatarImage.af_setImageWithURL(NSURL(string: user.thumbnail_url)!)
        nameLabel.text = user.username
        timeLabel.text = "• 1h"
    }
    
    func populateAskerCellWithUser(user: UserNode) {
        emptyLabels()
        avatarImage.af_setImageWithURL(NSURL(string: user.thumbnail_url)!)
        nameLabel.text = user.username
//        timeLabel.text = "• 1h"
        if Session.sharedSession.user?.user_id == user.user_id {
            creatorLabel.text = "(Original Asker)"
        }
    }
    
    func populateContributionWithUser(user: UserNode) {
        emptyLabels()
        avatarImage.af_setImageWithURL(NSURL(string: user.thumbnail_url)!)
        nameLabel.text = user.username
        contributionLabel.text = String(user.contribution)
    }
    
    func emptyLabels () {
        nameLabel.text = ""
        timeLabel.text = ""
        creatorLabel.text = ""
        contributionLabel.text = ""
    }
}
