//
//  CardsViewController.swift
//  GuideHero
//
//  Created by Ascom on 11/2/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AlamofireImage
import Moya


class CardsViewController: BasePostLoginController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!

    @IBOutlet var titleLabel: UILabel!
    
    var content: ContentNode? {
        didSet {
            if isViewLoaded() {
                titleLabel.text = content?.name ?? "Learn"
                tableView.reloadData()
            }
        }
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showProgressHUD()
        setupUI()
        
        // Force loading notifications
//        let navVC = self.tabBarController?.viewControllers![2] as! UINavigationController
//        let notifVC = navVC.viewControllers.first
//        notifVC?.view
        
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CardsViewController.refreshData), name:Constants.Literals.contentRefreshNotification, object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController!.tabBar.hidden = false
//        if self.content == nil {
            self.fetchAllDecks()
//        }
    }

    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.content?.children.count ?? 0
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ContentTableCell", forIndexPath: indexPath) as! ContentTableCell
        cell.content = self.content!.children[indexPath.row]
        
        cell.thumbnailView.tag = indexPath.row
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(CardsViewController.showFullScreen))
        cell.thumbnailView.addGestureRecognizer(singleTap)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let vc = storyboard?.instantiateViewControllerWithIdentifier("CardDetailViewController") as! CardDetailViewController
        vc.content = content?.children[indexPath.row]
        vc.contentIndex = indexPath.row
//        navigationController!.presentViewController(vc, animated: true, completion: nil)
        self.presentViewController(vc, animated: true, completion: nil)
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Private methods
    
    private func setupUI() {
        let cellNib = UINib(nibName: "ContentTableCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: "ContentTableCell")
    }
    
    private func fetchAllDecks() {
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.GetAllDecks) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't download decks, please try again later")
                    self.hideProgressHUD()
                    return
                }
                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    print("Cards***")
                    print(JSON)
                    self.hideProgressHUD()
                    // artificial top level content container
                    let rootDir: [String: AnyObject] = [
                        "type": "deck",
                        "name": "Learn",
                        "children": JSON["all_decks"]!,
                        "id": "",
                        "creator": "",
                        "creator_thumbnail": "",
                        "created_at": 0,
                        "updated_at": 0,
                        ]
                    self.content = ContentNode(dictionary: rootDir)
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't parse decks, please try again later")
                    self.hideProgressHUD()
                }
            case let .Failure(error):
                print("can't fetch decks: %@", error)
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't download decks, please try again later")
                self.hideProgressHUD()
            }
        }
    }
    
    // MARK: - Actions
    
    @IBAction func onDotsButton(sender: AnyObject) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            // ...
        }
        alertController.addAction(cancelAction)
        
        let unpublishAction = UIAlertAction(title: "Unpublish", style: .Default) { (action) in

        }
        alertController.addAction(unpublishAction)
        
//        self.presentViewController(alertController, animated: true) {
//        }
    }
    
    ////////////////////////////////
    
    func showFullScreen(gestureRecognizer: UIGestureRecognizer) {
        let index = gestureRecognizer.view!.tag
        
        let fullScreenViewController = storyboard!.instantiateViewControllerWithIdentifier("FullScreenViewController") as! FullScreenViewController
        fullScreenViewController.content = content
        fullScreenViewController.deckIndex = index
        fullScreenViewController.modalPresentationStyle = .OverFullScreen
        navigationController?.presentViewController(fullScreenViewController, animated: true, completion: nil)
        
    }
    
    ////////////////////////////////
}
