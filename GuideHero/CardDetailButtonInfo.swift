//
//  CardDetailButtonInfo.swift
//  GuideHero
//
//  Created by Promising Change on 03/01/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import Foundation
import UIKit

public struct CardDetailButtonInfo {
    public var Id: Int
    public var title: String
    public var buttonImage: UIImage?
    public var buttonSelectedImage: UIImage?
    public var tintColor: UIColor?
    
    public init(Id: Int, title: String) {
        self.Id = Id
        self.title = title
    }
    
    public init(Id: Int, title: String, image: UIImage?) {
        self.init(Id: Id, title: title)
        self.buttonImage = image
    }
    
    public init(Id: Int, title: String, image: UIImage?, selectedImage: UIImage?) {
        self.init(Id: Id, title: title, image: image)
        self.buttonSelectedImage = selectedImage
    }
    
    public init(Id: Int, title: String, image: UIImage?, selectedImage: UIImage?, tintColor: UIColor?) {
        self.init(Id: Id, title: title, image: image, selectedImage: selectedImage)
        self.tintColor = tintColor
    }
    
    
}
