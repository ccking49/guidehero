//
//  MoveCardRootViewController.swift
//  GuideHero
//
//  Created by Ascom on 10/29/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya

class MoveCardRootViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    var drafts = [ContentNode]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cellNib = UINib(nibName: "ContentTableCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: "ContentTableCell")
        updateTable()
//        self.cellNib = [UINib nibWithNibName:@"MyCustomCell" bundle:nil];
//        [self.tableView registerNib:self.cellNib forCellReuseIdentifier:@"CustomCell"];
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    // MARK: Content Load
    
    private func updateTable() {
        drafts = Helper.drafts
        self.tableView.reloadData()
        /*
        if !self.isViewLoaded() {
            return;
        }
        
        self.showProgressHUD();
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.GetCreatedCards) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't get drafts, please try again later")
                    self.hideProgressHUD()
                    return
                }
                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    self.hideProgressHUD()
                    self.drafts = []
                    for draftDict in (JSON["created"] as? [[String : AnyObject]])! {
                        self.drafts.append(ContentNode(dictionary: draftDict))
                    }
                    self.tableView.reloadData()
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't get drafts, please try again later")
                    self.hideProgressHUD()
                }
            case let .Failure(error):
                print("Can't get drafts: %@", error)
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't get drafts, please try again later")
                self.hideProgressHUD()
            }
        }*/
    }
    
    
    // MARK: - UITableView
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.drafts.count ?? 0
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ContentTableCell", forIndexPath: indexPath) as! ContentTableCell
        cell.content = self.drafts[indexPath.row]
        
        cell.showLock = false
        if !self.drafts[indexPath.row].published {
            cell.showLock = true
        }
        cell.updateLock()
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! ContentTableCell
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("CardDetailViewController") as! CardDetailViewController
        vc.content = cell.content
//        vc.showNav = true
//        vc.showNavOnClose = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
