//
//  CardContainerView.swift
//  GuideHero
//
//  Created by Ascom on 11/2/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AVFoundation

public class CardContainerView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var imageView: MaskImageView!
    var textLabel: UILabel!
    var backView1: UIView!
    var backView2: UIView!
    var backView3: UIView!
    
    
    var isDeck: Bool! {
        didSet {
            backView1.hidden = !(isDeck ?? false)
            backView2.hidden = !(isDeck ?? false)
        }
    }
    
    func generateThumnail(url : NSURL, fromTime:Float64) -> UIImage? {
        
        let asset = AVAsset(URL: url)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        assetImgGenerate.requestedTimeToleranceAfter = kCMTimeZero;
        assetImgGenerate.requestedTimeToleranceBefore = kCMTimeZero;

        let time        : CMTime = CMTimeMakeWithSeconds(fromTime, 600)
        var img         : CGImageRef!
        do{
            img = try assetImgGenerate.copyCGImageAtTime(time, actualTime: nil)
            
            
        }catch{
            
        }
        if img != nil {
            let frameImg    : UIImage = UIImage(CGImage: img)
            return frameImg
        } else {
            return nil
        }
        
    }
    
    var content: ContentNode! {
        get {
            return self.content
        }
        set (newContent) {
            imageView.hidden = true
            textLabel.hidden = true
            switch (newContent.type) {
            case .Image :
                imageView.setImageWithUrlWhiteBackground(newContent.imageURL!, rect: newContent.maskRect)
                imageView.hidden = false
                break
            case .Text:
                textLabel.text = newContent.content
                textLabel.hidden = false
                break
            case .Video:
                
                let qualityOfServiceClass = QOS_CLASS_BACKGROUND
                let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
                dispatch_async(backgroundQueue, {
                    print("This is run on the background queue")
                    self.imageView.image = self.generateThumnail(newContent.videoURL!, fromTime: Float64(1))
                    
                })
                
                
                imageView.hidden = false
                break
            case .Deck:
                if newContent.children.count == 0 {
                    break
                } else {
                    self.content = newContent.children[0]
                }
                break
            default:
                break
            }
            
            backView1.hidden = !newContent.isDeck
            backView2.hidden = !newContent.isDeck
        }
    }
    
    var isInList: Bool = true {
        didSet {
            let fontSize = isInList ? 14 : 22 as CGFloat
            textLabel.font = Helper.proximaNova("Bold", font: fontSize)
        }
    }
    
    required public override init(frame: CGRect) {
        super.init(frame: frame)
        initilize()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initilize()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        let width = CGRectGetWidth(bounds)
        let height = CGRectGetHeight(bounds)
        backView1.frame = CGRect(x: 0, y: 8, width: width, height: width)
        backView2.frame = CGRect(x: 0, y: 4, width: width, height: width)
        backView3.frame = CGRect(x: 0, y: 0, width: width, height: width)
        imageView.frame = CGRect(x: 0, y: 0, width: width, height: width)
        var labelFrame = CGRect(x: 0, y: 0, width: width, height: width)
        labelFrame = CGRectInset(labelFrame, 5, 5)
        textLabel.frame = labelFrame
    }
    
    func initilize() {
        backgroundColor = UIColor.whiteColor()
        clipsToBounds = false
        
        backView1 = UIView(frame: CGRect.zero)
        setEffects(backView1)
        backView2 = UIView(frame: CGRect.zero)
        setEffects(backView2)
        backView3 = UIView(frame: CGRect.zero)
        setEffects(backView3)
        
        imageView = MaskImageView(frame: CGRect.zero)
        imageView.contentMode = .ScaleAspectFit
        imageView.clipsToBounds = true
        setEffects(imageView)
        imageView.hidden = true
        
        textLabel = UILabel(frame: CGRect.zero)
//        textLabel.font = Helper.proximaNova("Bold", font: 50)
        let fontSize = isInList ? 14 : 22 as CGFloat
        textLabel.font = Helper.proximaNova("Bold", font: fontSize)
        textLabel.textAlignment = .Center
        textLabel.adjustsFontSizeToFitWidth = true
        textLabel.minimumScaleFactor = 0.4
        textLabel.numberOfLines = 5
//        textLabel.clipsToBounds = true
//        setEffects(textLabel)
        textLabel.hidden = true
//        textLabel.backgroundColor = UIColor.blueColor()
        
        addSubview(backView1)
        addSubview(backView2)
        addSubview(backView3)
        addSubview(imageView)
        addSubview(textLabel)
    }
    
    func setEffects (view: UIView) {
        view.layer.cornerRadius = 4
        
        view.layer.shadowColor = UIColor.colorFromRGB(redValue: 155/255.5, greenValue: 155/255.5, blueValue: 155/255.5, alpha: 0.5).CGColor//Helper.colorFromRGB(0x9b9b9b, alpha: 0.5).CGColor
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowRadius = 1
        view.layer.shadowOpacity = 0.5
        
        view.backgroundColor = UIColor.whiteColor()
//        view.layer.borderColor = UIColor.colorFromRGB(redValue: 155/255.5, greenValue: 155/255.5, blueValue: 155/255.5, alpha: 0.5).CGColor
//        view.layer.borderWidth = 0.5
    }
}
