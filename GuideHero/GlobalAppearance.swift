//
//  GlobalAppearance.swift
//  GuideHero
//
//  Created by Abhishek Sengar on 02/09/2016.
//  Copyright © 2016 Twilio. All rights reserved.
//

import Foundation

struct GlobalAppearance {
    
    static func setAppearance () {
        setTabBarAppearance()
        setNavigationBarAppearance()
    }
    
    static func setTabBarAppearance() {
        UITabBar.appearance().tintColor = TabBarTintColor
        UITabBar.appearance().barTintColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0)
        
        UITabBar.appearance().backgroundImage = Helper.imageWithColor(UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.2))
    }
    
    static func setNavigationBarAppearance() {
        let navigationBarAppearance = UINavigationBar.appearance()
        let font = UIFont(name: "ProximaNova-Bold", size: 22.0)
        let titleTextAttributes = [
            NSFontAttributeName: font!,
            NSForegroundColorAttributeName: UIColor.darkGrayColor()
        ]
        navigationBarAppearance.titleTextAttributes = titleTextAttributes
        navigationBarAppearance.translucent = true
    }
    
    private static let TabBarTintColor = UIColor.colorFromRGB(redValue: 250, greenValue: 0, blueValue: 64, alpha: 1)
}
