//
//  CardDraftMAAskVC.swift
//  GuideHero
//
//  Created by Promising Change on 31/01/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit

protocol CardDraftMAAskVCDelegate: class {
    func cardDraftMAAskVCDidUpdateOriginalPrizePoint(point: Int)
}

class CardDraftMAAskVC: CardSubPageBaseVC {
    
    // MARK: - @IBOutlet Variables
    
    @IBOutlet weak var tbContent: UITableView!
    @IBOutlet weak var footerView : UIView!

    // MARK: - Variables
    
    weak var askDelegate: CardDraftMAAskVCDelegate?
    
    var aryTableHeader :NSMutableArray = []
    
    let tbHeaderView: UIView!
    
    var content: ContentNode? {
        didSet {
            if isViewLoaded() {
                tbContent.reloadData()
                self.view.layoutIfNeeded()
            }
        }
    }
    
    var askToCell: CardDraftMAAskToCell?
    var askContributionCell: CardDraftMAAskContributionCell?
    var formatCell : CardDraftMAAskFormatCell?
    
    // MARK: - Properties
    
    var askToWhom: AskToWhom {
        get {
            if (askToCell == nil) {
                return AskToWhom.Anyone
            }
            
            return (askToCell?.askToWhom)!
        }
    }
    
    var originalPrizePoint: Int? {
        get {
            return askContributionCell?.originalPrizePoint
        }
    }
    
    var formatString : String?{
        get{
            return formatCell?.txtTitle.text
        }
    }
    
    var joinPrizePoint: Int? {
        get {
            return askContributionCell?.joinPrizePoint
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tbContent.setContentOffset(CGPointMake(tbContent.contentOffset.x, tbContent.contentOffset.y - 1), animated: true)
    }
    
    // MARK: - View Lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        tbHeaderView = UIView()
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        scrollView = tbContent as UIScrollView
        configureView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Methods
    
    func configureView() {
        tbHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 540)
        
        tbContent.rowHeight = UITableViewAutomaticDimension
        tbContent.estimatedRowHeight = Constants.UIConstants.contentCellHeight
        tbContent.separatorStyle = .None
        
        tbContent.tableHeaderView = tbHeaderView
        footerView.layer.cornerRadius = 20
        footerView.layer.shadowRadius = 5
        footerView.layer.shadowOpacity = 0.5
        footerView.layer.shadowOffset = CGSizeZero
        footerView.layer.shadowColor = UIColor.blackColor().CGColor
        footerView.clipsToBounds = true
        
        self.aryTableHeader = ["To", "Format", "Contribution To Prize"]
        
        self.view.setNeedsLayout()
    }
    
    // MARK: - UIScrollViewDelegate
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
    }

}

// MARK: - UITableViewDelegate & UITableViewDataSource

extension CardDraftMAAskVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return aryTableHeader.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.aryTableHeader[section] as? String
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let contentHeight : CGFloat = 48
        
        let headerView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: self.view.frame.width, height: contentHeight)))
        
        let headerContentView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: self.view.frame.width, height: 48)))
        
        headerView.clipsToBounds = false
        let label = UILabel(frame: CGRect(x: 30, y: 0, width: self.view.frame.width-60, height: 48))
        label.text = aryTableHeader[section] as? String
        label.textAlignment = NSTextAlignment.Left
        
        headerView.backgroundColor = UIColor.whiteColor()
        headerContentView.backgroundColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.1)
        
        label.textColor = UIColor.blackColor()
        label.font = Helper.proximaNova("Bold", font: 20)
        
        headerContentView.addSubview(label)
        
        headerView.addSubview(headerContentView)
        
        return headerView
        
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UILabel(frame: CGRect(origin: CGPointZero, size: CGSize(width: self.view.frame.width, height: 1)))
        footerView.backgroundColor = UIColor.whiteColor()
        return footerView
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 48
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        
        let section = indexPath.section
        switch section {
        case 0:
            if (askToCell != nil) {
                return askToCell!
            }
            
            askToCell = tableView.dequeueReusableCellWithIdentifier("CardDraftMAAskToCell") as? CardDraftMAAskToCell
            askToCell?.delegate = self
            
            cell = askToCell!
            break
        case 1:
            formatCell = tableView.dequeueReusableCellWithIdentifier("CardDraftMAAskFormatCell") as? CardDraftMAAskFormatCell
            
            if self.content!.format != ""  &&  self.content!.format != nil{
                formatCell!.txtTitle.text = self.content!.format
            }
            
            cell = formatCell!
            
            break
        case 2:
            if (askContributionCell != nil) {
                return askContributionCell!
            }
            
            askContributionCell = tableView.dequeueReusableCellWithIdentifier("CardDraftMAAskContributionCell") as? CardDraftMAAskContributionCell
            askContributionCell?.delegate = self
            
            if content?.prize_pool != 0 {
                askContributionCell!.tfPrizeOriginal.text = String(content!.prize_pool)
            }
            
            if content!.prize_to_join != 0 {
                askContributionCell!.tfPrizeJoin.text = String(content!.prize_to_join)
                
            }
            
            cell = askContributionCell!
            
            
            
            break
        default:
            break
        }
        
        cell.selectionStyle = .None
        cell.backgroundColor = UIColor.whiteColor()
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Do Nothing
    }
}

extension CardDraftMAAskVC: CardDraftMAAskCellDelegate {
    func cellWasUpdated() {
        tbContent.reloadData()
    }
    
    func cellDidRequestToUpdatePersonList(usernameList: [String]?) {
        
    }
    
    func cellWasUpdatedWithOriginalPoint(point: Int) {
        askDelegate?.cardDraftMAAskVCDidUpdateOriginalPrizePoint(point)
    }
}
