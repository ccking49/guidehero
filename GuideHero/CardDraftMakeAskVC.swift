//
//  CardDraftMakeAskVC.swift
//  GuideHero
//
//  Created by Promising Change on 31/01/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit
import AlamofireImage
import Moya
import AVFoundation

protocol CardDraftMakeAskVCDelegate: class {
    func cardDraftContentDidEnableAsk(content: ContentNode)
}

class CardDraftMakeAskVC: CardViewPagerController {
    
    // MARK: - @IBOutlet Variables for Card View Pager
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var vwDraggableView: PannableCardView!
    @IBOutlet weak var vwHeader: UIView!
    @IBOutlet weak var deckBackground: UIImageView!
    @IBOutlet weak var stackViewActions: UIStackView!
    @IBOutlet weak var cardActionPlay: CardDetailButton!
    @IBOutlet weak var cardActionAsk: CardDetailButton!
    @IBOutlet weak var cardActionGive: CardDetailButton!
    @IBOutlet weak var cardActionPrize: CardDetailButton!
    
    @IBOutlet weak var constraintHeaderTop: NSLayoutConstraint!
    @IBOutlet weak var constraintActionsTop: NSLayoutConstraint!
    @IBOutlet weak var constraintContentBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintSocialTop: NSLayoutConstraint!
    
    @IBOutlet weak var askWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var giveWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var prizeWidthConstraint: NSLayoutConstraint!
    
    // MARK: - @IBOutlet Variables for Card Header
    
//    @IBOutlet weak var backgroundImage: MaskImageView!
    @IBOutlet weak var creatorImage: UIImageView!
    @IBOutlet weak var creatorNameAndDate: UILabel!
//    @IBOutlet weak var cardView: UIView!
//    @IBOutlet weak var imgCard : MaskImageView!
//    @IBOutlet weak var deckTitle: UILabel!
    @IBOutlet weak var cardBackgroundView : CardMaskContainerView!
    @IBOutlet weak var cardView: SubmissionContainerView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentsLabel: UILabel!
    
    @IBOutlet weak var actionView : UIView!
    @IBOutlet weak var imgLine1 : UIImageView!
    
    @IBOutlet weak var btCancel: UIButton!
    @IBOutlet weak var btSave: UIButton!
    
    @IBOutlet weak var containBottomView : UIView!
    
    // MARK: - Variables
    
    weak var delegate: CardDraftMakeAskVCDelegate?
    
    var editingStatus = CardEditingStatus.Normal
    var newChildrenOrdering = [ContentNode]()
    
    var originalHeaderTop: CGFloat?
    var originalActionsTop: CGFloat?
    var originalSocialTop: CGFloat?
    
    var cardDraftPlayVC: CardDraftMAPlayVC?
    var cardDraftAskVC: CardDraftMAAskVC?
    var cardDraftGiveVC: CardDraftMAGiveVC?
    var cardDraftPrizeVC: CardDraftMAPrizeVC?
    
    var tapGestureRecognizer = UITapGestureRecognizer()
    
    var cardActionButtons: [CardDetailButton] = []
    
    
    var isType : Bool = true // true - make ask , false - edit card for ask enabled
    
    var content: ContentNode? {
        
        didSet {
            if isViewLoaded() {
                // Update the Content
                
                cardDraftPlayVC?.content = self.content
                cardDraftAskVC?.content = self.content
                cardDraftGiveVC?.content = self.content
                cardDraftPrizeVC?.content = self.content
            }
        }
    }
    
    var cardId: String? // To load specific deck/card
    var contentIndex: Int = 0
    var onHeaderAction: ((HeaderCellAction) -> Void)!
    
    var showLock = false {
        didSet {
            updateLock()
        }
    }
    
    var moreOptionsView: MoreView?
    
    // MARK: - View Lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        datasource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        datasource = self
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(DraftsViewController.changedTable(_:)),
                                                         name: "changedTable",
                                                         object: nil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func changedTable(notification: NSNotification) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
//        UIApplication.sharedApplication().statusBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        configureView()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        super.viewWillAppear(animated)
        
        UIApplication.sharedApplication().statusBarHidden = true
        
        containerView.bringSubviewToFront(actionView)
        
        if self.content == nil {
            self.fetchAllDecks()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Methods
    
    func configureView() {
        containerView.bounces = false
        containerView.alwaysBounceHorizontal = false
        
        originalHeaderTop = -1000
        originalActionsTop = -1000
        originalSocialTop = -1000
        
        // Card View Pager
        vwHeader.roundTopCorners(20)
        
        let cardActionInfoPlay: CardDetailButtonInfo = CardDetailButtonInfo(Id: 0, title: "Play", image: UIImage(named: "playOff"), selectedImage: UIImage(named: "playOn"), tintColor: UIColor(hexString: "#FE2851"))
        let cardActionInfoAsk: CardDetailButtonInfo = CardDetailButtonInfo(Id: 1, title: "Ask", image: UIImage(named: "requestOff"), selectedImage: UIImage(named: "requestOn"), tintColor: UIColor(hexString: "#FFCD00"))
        let cardActionInfoGive: CardDetailButtonInfo = CardDetailButtonInfo(Id: 2, title: "Give", image: UIImage(named: "offerOff"), selectedImage: UIImage(named: "offerOn"), tintColor: UIColor(hexString: "#BD10E0"))
        let cardActionInfoPrize: CardDetailButtonInfo = CardDetailButtonInfo(Id: 3, title: "Prize", image: UIImage(named: "prizeOff"), selectedImage: UIImage(named: "prizeOn"), tintColor: UIColor(hexString: "#0076FF"))
        
        cardActionPlay.bindItem(cardActionInfoPlay)
        cardActionPlay.delegate = self
        cardActionButtons.append(cardActionPlay)
        cardActionPlay.selectButton()
        
        cardActionAsk.bindItem(cardActionInfoAsk)
        cardActionAsk.delegate = self
        cardActionButtons.append(cardActionAsk)
        
        cardActionGive.bindItem(cardActionInfoGive)
        cardActionGive.delegate = self
        cardActionButtons.append(cardActionGive)
        
        cardActionPrize.bindItem(cardActionInfoPrize)
        cardActionPrize.delegate = self
        cardActionButtons.append(cardActionPrize)
        
        // Ask Enabled
        // Show Ask, Give & Prize sections
        askWidthConstraint.constant = 82
        giveWidthConstraint.constant = 82
        prizeWidthConstraint.constant = 82
        
        self.view.layoutIfNeeded()
        
        // Card View Header
//        imgCard.clipsToBounds = true
//        imgCard.layer.cornerRadius = 5
//
//        print(content?.thumbnailURL)
//        if content?.thumbnailURL != nil {
//            backgroundImage.setImageWithUrlFromCardDetail(NSURL(string: content!.thumbnailURL!))
//            imgCard.setImageWithUrlWhiteBackground(NSURL(string: content!.thumbnailURL!) , rect: content!.maskRect)
//        }else{
//            backgroundImage.setImageWithUrlFromCardDetail(content?.imageURL)
//            imgCard.setImageWithUrlWhiteBackground(content?.imageURL , rect: content!.maskRect)
//        }
//        self.descriptionLabel.text = self.content?.description
//        self.deckTitle.text = self.content?.name
        
        let titleAttributes = [NSForegroundColorAttributeName: UIColor.blackColor(), NSFontAttributeName: Helper.proximaNova("bold", font: 22)]
        let descAttributes = [NSForegroundColorAttributeName: UIColor.blackColor(), NSFontAttributeName: Helper.proximaNova("Regular", font: 15)]
        
        let partTitle = NSMutableAttributedString(string: (self.content?.name)! + "\n\n", attributes: titleAttributes)
        let partDesc = NSMutableAttributedString(string: (self.content?.description)!, attributes: descAttributes)
        
        let combination = NSMutableAttributedString()
        
        combination.appendAttributedString(partTitle)
        combination.appendAttributedString(partDesc)
        
        descriptionLabel.attributedText = combination
        
        cardView.content = self.content
        
        cardBackgroundView.content = self.content
        
        if let creatorImageURL = content?.creatorThumbnailURL {
            self.creatorImage.af_setImageWithURL(creatorImageURL)
        }
        self.creatorNameAndDate.text = content?.formattedCreatorAndDate
        self.creatorNameAndDate.layer.shadowRadius = 0.5
        self.creatorImage.layer.cornerRadius = 10
        
        let likes = content?.likes ?? 0
        likesLabel.text = String(likes)
        let imageName = (self.content!.liked_by_me == false) ? "grey_heart" : "heartFilled"
        self.likeButton.setImage(UIImage(named: imageName), forState: .Normal)
        let comments = content?.comments.count ?? 0
        commentsLabel.text = String(comments)
        
        onHeaderAction = { action in
            self.handleHeaderCellAction(action)
        }
        
//        deckBackground.hidden = self.content!.children.count == 0
//        if (self.content!.children.count == 0) {
//            imgCard.layer.borderColor = UIColor(red: 175.0 / 255, green: 175.0 / 255, blue: 175.0 / 255, alpha: 1.0).CGColor
//            imgCard.layer.borderWidth = 1.0
//        }
//        else {
//            deckTitle.text = self.content!.children[0].name
//            descriptionLabel.text = self.content!.children[0].description
//        }
        
        self.view.setNeedsLayout()
        
        tapGestureRecognizer.addTarget(self, action: #selector(CardDraftEditVC.handleTap(_:)))
        tapGestureRecognizer.delegate = self
        vwDraggableView.addGestureRecognizer(tapGestureRecognizer)
        
        imgLine1.layer.shadowRadius = 2
        imgLine1.layer.shadowOffset = CGSizeZero
        imgLine1.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 0.5).CGColor
        
//        imgCard.layer.shadowRadius = 2
//        imgCard.layer.shadowOffset = CGSizeZero
//        imgCard.layer.shadowColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.5).CGColor
//        
//        backgroundImage.layer.shadowRadius = 2
//        backgroundImage.layer.shadowOffset = CGSizeZero
//        backgroundImage.layer.shadowColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.5).CGColor
        
        btSave.layer.shadowColor = UIColor.blackColor().CGColor
        btSave.layer.shadowOffset = CGSizeMake(0, 1)
        btSave.layer.shadowOpacity = 0.5
        btSave.layer.shadowRadius = 4
        
        btCancel.layer.shadowColor = UIColor.blackColor().CGColor
        btCancel.layer.shadowOffset = CGSizeMake(0, 1)
        btCancel.layer.shadowOpacity = 0.5
        btCancel.layer.shadowRadius = 4
        
    }
    
    func initialize() {
        var childrenSource = self.content!.children
        if editingStatus == CardEditingStatus.Edit {
            childrenSource = newChildrenOrdering
        }
        
        self.showLock = false
        if self.content!.isDeck {
            if !(self.content!.published) {
                if childrenSource[0].isDeck {
                    self.showLock = !childrenSource[0].published
                } else {
                    self.showLock = true
                }
            }
        }
        
        
    }
    
    func updateLock() {
        if self.showLock {
            let lock = UIImageView(frame: CGRectMake(10, 10, 20, 20))
            lock.image = UIImage(named: "lock")
            lock.tag = 333
            cardView.insertSubview(lock, aboveSubview: cardView)
        } else {
            for view in cardView.subviews{
                if view.tag == 333 {
                    view.removeFromSuperview()
                }
            }
        }
    }
    
    private func fetchAllDecks() {
        if cardId == nil {
            return
        }
        self.showProgressHUD()
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.GetCard(cardId: cardId!)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't download decks, please try again later")
                    self.hideProgressHUD()
                    return
                }
                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    self.hideProgressHUD()
                    let dic = JSON["card"] as! [String: AnyObject]
                    self.content = ContentNode(dictionary: dic)
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't parse decks, please try again later")
                    self.hideProgressHUD()
                }
            case let .Failure(error):
                print("can't fetch decks: %@", error)
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't download decks, please try again later")
                self.hideProgressHUD()
            }
        }
        
    }
    
    func handleHeaderCellAction (action: HeaderCellAction) {
        if action == .Loop {
            let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "LoopController") as! LoopController
            vc.content = self.content
            self.presentViewController(vc, animated: true, completion: nil)
        } else if action == .Comment {
            let vc = UIStoryboard.loadViewController(storyboardName: "Comment", viewControllerIdentifier: "CommentsViewController") as! CommentsViewController
            vc.content = content
            self.presentViewController(vc, animated: true, completion: nil)
        } else if action == .Like {
            let liked = content!.liked_by_me
            let provider = MoyaProvider<NetworkService>.createDefaultProvider()
            let target = liked ? NetworkService.UnlikeCard(cardId : content!.id) : NetworkService.LikeCard(cardId : content!.id)
            
            provider.request(target) { result in
                switch result {
                case let .Success(response):
                    if response.statusCode == 200 {
                        self.content!.liked_by_me = !liked
                        self.content!.likes += liked ? -1 : 1
                        
                        // Update View
                        let likeCount = self.content!.likes ?? 0
                        self.likesLabel.text = String(likeCount)
                        let imageName = (self.content!.liked_by_me == false) ? "grey_heart" : "heartFilled"
                        self.likeButton.setImage(UIImage(named: imageName), forState: .Normal)
                    }
                case let .Failure(error):
                    print(error)
                }
            }
        } else {
            
        }
    }
    
    
    func compareChildren(array1: [ContentNode], array2: [ContentNode]) -> Bool {
        for i in 0...array1.count - 1 {
            if array1[i].id != array2[i].id {
                return false
            }
        }
        return true
    }
    
    func save() {
        if (cardDraftPlayVC == nil || cardDraftAskVC == nil || cardDraftGiveVC == nil || cardDraftPrizeVC == nil) {
            return
        }
        
//        if self.isType == true { // make ask
            // Collect information
            let askEnabled = true
        
            var formatString = cardDraftAskVC?.formatString
        
        if formatString == "" || formatString == nil {
            formatString = "1 Submission. No rules."
        }
            var userIDs = [String]()
            var originalPrizePoint = cardDraftAskVC?.originalPrizePoint
        if originalPrizePoint == nil {
                originalPrizePoint = 0
        }
            var joinPrizePoint = cardDraftAskVC?.joinPrizePoint
        
        if joinPrizePoint == nil {
            joinPrizePoint = 0
        }
        
        let total_points = NSUserDefaults.standardUserDefaults().integerForKey("total_points")
        
        if total_points < originalPrizePoint {
        
            self.showSimpleAlert(withTitle: "Error", withMessage: "Not enough points! 😱")
            return
        }
        
            let prizeDistributionFor = cardDraftPrizeVC?.prizeForWhom
            let prizeDistributionRule = cardDraftPrizeVC?.prizeDistributionRule
            let prizeEvaluationStart = cardDraftPrizeVC?.evaluationStartDate
            let prizeEvaluationEnd = cardDraftPrizeVC?.evaluationEndDate
            
            
            // Reflect Changes on the Backend
            self.showProgressHUD()
            
            let provider = MoyaProvider<NetworkService>.createDefaultProvider()
            let target = NetworkService.AddMode(isAskModeEnabled: askEnabled, deckId: self.content!.id, userIds: userIDs, format: formatString!, initialPrize:originalPrizePoint! , prizeToJoin:joinPrizePoint!, distributionRule: prizeDistributionRule, distributionFor: prizeDistributionFor, evaluationStart: prizeEvaluationStart, evaluationEnd: prizeEvaluationEnd)
            provider.request(target) { result in
                switch result {
                case let .Success(response):
                    // Success
                    self.hideProgressHUD()
                    
                    let JSON = try? response.mapJSON() as! [String: AnyObject]
                    
                    if JSON!["deck"] != nil {
                        let dic = JSON!["deck"] as! [String: AnyObject]
                        let newContent = ContentNode(dictionary: dic)
                        // Notify the Content Update.
                        self.delegate?.cardDraftContentDidEnableAsk(newContent)
                        
                        // Dismiss the View
                        self.dismissViewControllerAnimated(true) {
                        }
                    }else{
                        self.showSimpleAlert(withTitle: "Error", withMessage: JSON!["error"] as? String)
                        
                    }
                    
                case .Failure(_):
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't edit deck, please try again later")
                    self.hideProgressHUD()
                }
                
            }
//        }else{ // card edit
//            sdf
//        }
        
    }
    
    
    // MARK: - Actions
    
    @IBAction func didTapOnCancel(sender: AnyObject) {
//        self.content?.ask_enabled = false
        self.dismissViewControllerAnimated(true) {
            
        }
    }
    
    @IBAction func didTapOnSave(sender: AnyObject) {
        save()
    }
    
    @IBAction func didTapOnLike(sender: AnyObject) {
        onHeaderAction(.Like)
    }
    @IBAction func didTapOnComment(sender: AnyObject) {
        onHeaderAction(.Comment)
    }
    
    // MARK: - CardSubPageScrollDelegate Methods
    
    override func subPageScrollViewDidScroll(offset: CGFloat, translation: CGPoint, animated: Bool) {
        super.subPageScrollViewDidScroll(offset, translation: translation, animated: animated)
        
        if (movingFactor != 0 ) {
            return
        }
        
        if (originalHeaderTop == -1000) {
            originalHeaderTop = constraintHeaderTop.constant
        }
        
        if (originalActionsTop == -1000) {
            originalActionsTop = constraintActionsTop.constant
        }
        
        if (originalSocialTop == -1000) {
            originalSocialTop = constraintSocialTop.constant
        }
        
        constraintHeaderTop.constant = originalHeaderTop! - offset
        constraintActionsTop.constant = originalActionsTop! - offset
        constraintSocialTop.constant = originalSocialTop! - offset
        
        if (offset < 0) {
            constraintContentBottom.constant = 30 + offset
        }
        
        if (!animated) {
            self.view.layoutIfNeeded()
        } else {
            UIView.animateWithDuration(0.25, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    override func subPageScrollViewDidBounce(offset: CGFloat) {
        super.subPageScrollViewDidBounce(offset)
        
        constraintContentBottom.constant = 30 + offset
        self.view.layoutIfNeeded()
    }
    
}

// MARK: - CardViewPagerDataSource Methods

extension CardDraftMakeAskVC: CardViewPagerDataSource {
    override func viewControllers(for viewPagerController: CardViewPagerController) -> [CardSubPageBaseVC] {
        var subPages = [CardSubPageBaseVC]()
        
        cardDraftPlayVC = UIStoryboard.loadViewController(storyboardName: "CardDraft", viewControllerIdentifier: "CardDraftMAPlayVC") as? CardDraftMAPlayVC
        cardDraftPlayVC?.delegate = self
        cardDraftPlayVC?.content = self.content
        subPages.append(cardDraftPlayVC!)
        
        cardDraftAskVC = UIStoryboard.loadViewController(storyboardName: "CardDraft", viewControllerIdentifier: "CardDraftMAAskVC") as? CardDraftMAAskVC
        cardDraftAskVC?.delegate = self
        cardDraftAskVC?.askDelegate = self
        cardDraftAskVC?.content = self.content
        subPages.append(cardDraftAskVC!)
        
        cardDraftGiveVC = UIStoryboard.loadViewController(storyboardName: "CardDraft", viewControllerIdentifier: "CardDraftMAGiveVC") as? CardDraftMAGiveVC
        cardDraftGiveVC?.delegate = self
        cardDraftGiveVC?.content = self.content
        subPages.append(cardDraftGiveVC!)
        
        cardDraftPrizeVC = UIStoryboard.loadViewController(storyboardName: "CardDraft", viewControllerIdentifier: "CardDraftMAPrizeVC") as? CardDraftMAPrizeVC
        cardDraftPrizeVC?.delegate = self
        cardDraftPrizeVC?.content = self.content
        subPages.append(cardDraftPrizeVC!)
        
        return subPages
    }
}

// MARK: - CardViewPagerProgressDelegate Methods

extension CardDraftMakeAskVC: CardViewPagerProgressDelegate {
    func updateIndicator(for viewController: CardViewPagerController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool, borderCrossed: Bool, direction: SwipeDirection) {
   
        if (toIndex != -1 && toIndex != cardActionButtons.count) {
            let cardActionButtonFrom = cardActionButtons[fromIndex]
            let cardActionButtonTo = cardActionButtons[max(toIndex, -1)]
            if (fromIndex < toIndex) {
                // Swipe Direction = Left, Transition Direction = Right
                cardActionButtonFrom.transitToRight(withProgressPercentage: progressPercentage)
                cardActionButtonTo.transitFromRight(withProgressPercentage: progressPercentage)
            } else {
                // Swipe Direction = Right, Transition Direction = Left
                cardActionButtonFrom.transitToLeft(withProgressPercentage: progressPercentage)
                cardActionButtonTo.transitFromLeft(withProgressPercentage: progressPercentage)
            }
            
            if (borderCrossed) {
                if (direction == .left) {
                    let cardActionButtonPast = cardActionButtons[max(fromIndex-1, 0)]
                    cardActionButtonPast.deselectButton()
                } else if (direction == .right) {
                    let pastIndex = min(fromIndex+1, cardActionButtons.count-1)
                    let cardActionButtonPast = cardActionButtons[pastIndex]
                    cardActionButtonPast.deselectButton()
                }
            }
        }
        
        if (toIndex < 0 || toIndex >= cardActionButtons.count) {
            let newButton = cardActionButtons[currentIndex]
            newButton.highlightButton()
        } else {
            let oldButton = cardActionButtons[currentIndex != fromIndex ? fromIndex : toIndex]
            let newButton = cardActionButtons[currentIndex]
            oldButton.dehighlightButton()
            newButton.highlightButton()
        }
    }
    
    func updateIndicator(for viewController: CardViewPagerController, fromIndex: Int, toIndex: Int) {
        
    }
}

// MARK: - CardDetailButtonDelegate Methods

extension CardDraftMakeAskVC: CardDetailButtonDelegate {
    func didTapOnCardActionButton(buttonID: Int) {
        guard buttonID != currentIndex else { return }
        
        let oldButton = cardActionButtons[currentIndex]
        let newButton = cardActionButtons[buttonID]
        oldButton.deselectButton()
        newButton.selectButton()
        
        moveToViewController(at: buttonID)
        
    }
}

// MARK: - UIGestureRecognizerDelegate Methods

extension CardDraftMakeAskVC: UIGestureRecognizerDelegate {
    func handleTap(tapGesture: UITapGestureRecognizer) {
        
        if (content!.parentContent != nil) {
            let fullScreenViewController = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "FullScreenViewController") as! FullScreenViewController
            fullScreenViewController.deckIndex = contentIndex
            fullScreenViewController.cardIndex = 0
            fullScreenViewController.content = content!.parentContent
            fullScreenViewController.modalPresentationStyle = .OverFullScreen
            self.presentViewController(fullScreenViewController, animated: true, completion: nil)
            
        }
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if (gestureRecognizer == tapGestureRecognizer) {
            let tapLocation = tapGestureRecognizer.locationInView(vwHeader)
            if (vwHeader.pointInside(tapLocation, withEvent: nil)) {
                
                if self.content?.children.count > 0  && self.content?.ask_enabled == true{
                    
                    if self.content?.children[0].type == .Deck {
                        let vc = UIStoryboard.loadViewController(storyboardName: "CardDraft", viewControllerIdentifier: "CardDraftMakeAskVC") as! CardDraftMakeAskVC
                        vc.delegate = self.delegate
                        vc.content = self.content?.children[0]
                        vc.isType = false
                        self.presentViewController(vc, animated: true, completion: nil)
                    }
                    
                }
                
                
                return true
            }
            
            return false
        }
        
        return false
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if otherGestureRecognizer is UIPanGestureRecognizer || otherGestureRecognizer is UITapGestureRecognizer {
            return true
        }
        
        return false
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

// MARK: - CardDraftMAAskVCDelegate

extension CardDraftMakeAskVC: CardDraftMAAskVCDelegate {
    func cardDraftMAAskVCDidUpdateOriginalPrizePoint(point: Int) {
        cardDraftPrizeVC?.originalPrizePoint = point
    }
}
