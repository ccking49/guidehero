//
//  NetworkService.swift
//  GuideHero
//
//  Created by Abhishek Sengar on 02/09/2016.
//  Copyright © 2016 Twilio. All rights reserved.
//

import Foundation
import Moya

// every API endpoint should be added as an enum here
enum NetworkService {
    case EmailSignIn(email: String)
    case SearchTutors(searchText: String)
    case SearchContent(searchText: String)
    case GetAllDecks  // All cards
    case GetProfileInfo(userId: String)
    case GetAllPoints(userId: String)
    case UsePoints(userId: String, paymentType: String, username: String, email: String)
    case GetPointsFromVenmo(userId: String, username: String, email: String, amount: CGFloat)
    case CreateTextCard(cardTitle: String?, cardDescription: String?, cardText: String, language: String?)
    case GenerateImageUploadParameters(fileName: String)
    case CreateImageCard(title: String, description:String, image: String, image_x: CGFloat, image_y: CGFloat, image_width: CGFloat, image_height: CGFloat, tags: [String], image_scale: Int)
    case EditImageCard(id: String, title:String, description:String, image: String, image_x: CGFloat, image_y: CGFloat, image_width: CGFloat, image_height: CGFloat, tags: [String], image_scale: Int)
    case GenerateVideoUploadParameters(fileName: String)
    case CreateVideoCard(title: String, description:String, video: String, thumbnail_url: String, tags: [String], video_length: Int)
    case EditVideoCard(id: String, title: String, description:String, video: String, thumbnail_url: String, tags: [String])
    case GetCreatedCards // All drafts
    case CreateDeck(title: String, cardIds: [String])
    case DeleteCards(cardIds: [String])
    case CopyCards(cardIds: [String])
    case PublishDecks(cardIds: [String], publish: Bool)
    case EditDeck(deckId: String, cardIds: [String])
    case LikeCard(cardId: String)
    case UnlikeCard(cardId: String)
    case LikeComment(commentId: String)
    case UnlikeComment(commentId: String)
    case MoveCard(cardIds: [String], moveTo: String)
    case AddComment(cardId: String, commentContent: String, commentId: String)
    case GetNotifications
    case EditProfile(firstName: String, lastName: String, username: String, bio: String)
    case GetCard(cardId: String)
    case GetUsernames(searchKey: String)
    case AddMode(isAskModeEnabled: Bool, deckId: String, userIds: [String], format: String, initialPrize: Int, prizeToJoin: Int, distributionRule: String?, distributionFor: Int?, evaluationStart: String?, evaluationEnd: String?)
    case GiveCardToDeck(deckId: String, cardId: String, visibility: String?)
    case JoinAsk(deckId: String, joinPrize: Int)
    case UnjoinAsk(deckId: String)
    case CancelSubmission(cardId: String)
    case EndEvaluationPeriod(deckId: String) // prerequisite call to GetWinners
    case GetWinners(deckId: String)
    case GetContentList(listType : String , user_id : String)
    case EditUserInfo(first_name : String , last_name : String , user_name : String , bio : String)
    case ValidateUserName(user_name : String)
    case FollowAction(user_id : String)
    case UnFollowAction(user_id : String)
    case ChangeOrder(card_id : String , position : Int )
}

extension NetworkService: TargetType {
    
    var baseURL: NSURL {
        return NSURL(string: Constants.ApiConstants.baseApiUrl)!
    }
    var path: String {
        switch self {
        case .EmailSignIn:
            return "auth/signin"
        case .SearchTutors:
            return "search/search_tutors"
        case .SearchContent:
            return "content/get_all_content"
        case .GetAllDecks:
            return "deck/get_all_public_decks"
        case .GetProfileInfo:
            return "account/get_profile"
        case .GetAllPoints:
            return "account/get_points"
        case .UsePoints:
            return "account/use_points"
        case .GetPointsFromVenmo:
            return "account/get_points_from_venmo"
        case .CreateTextCard:
            return "deck/create_text_card"
        case .GenerateImageUploadParameters:
            return "deck/generate_image_upload_params"
        case .CreateImageCard:
            return "deck/create_image_card"
        case .EditImageCard:
            return "deck/edit_image_card"
        case .GenerateVideoUploadParameters:
            return "deck/generate_video_upload_params"
        case .CreateVideoCard:
            return "deck/create_video_card"
        case .EditVideoCard:
            return "deck/edit_video_card"
        case .GetCreatedCards:
            return "deck/get_created_cards"
        case .CreateDeck:
            return "deck/create_deck"
        case .DeleteCards:
            return "deck/delete_cards"
        case .CopyCards:
            return "deck/copy_cards"
        case .PublishDecks:
            return "deck/publish_decks"
        case .EditDeck:
            return "deck/edit_deck"
        case .LikeCard:
            return "deck/like_card"
        case .UnlikeCard:
            return "deck/unlike_card"
        case .LikeComment:
            return "deck/like_comment"
        case .UnlikeComment:
            return "deck/unlike_comment"
        case .MoveCard:
            return "deck/move_card"
        case .AddComment:
            return "deck/add_comment"
        case .GetNotifications:
            return "notification/get_notifications"
        case .EditProfile:
            return "auth/edit_profile"
        case .GetCard:
            return "deck/get_card"
        case .GetUsernames:
            return "account/get_usernames"
        case .AddMode:
            return "deck/add_mode"
        case .GiveCardToDeck:
            return "deck/give_card_to_deck"
        case .JoinAsk:
            return "deck/join_ask"
        case .UnjoinAsk:
            return "deck/unjoin_ask"
        case .CancelSubmission:
            return "deck/revoke_card_from_deck"
        case .EndEvaluationPeriod:
            return "deck/end_evaluation_period"
        case .GetWinners:
            return "deck/get_winners"
        case .GetContentList:
            return "auth/get_user_info"
        case .EditUserInfo:
            return "auth/edit_user"
        case .ValidateUserName:
            return "account/does_user_exist"
        case .FollowAction:
            return "account/follow_user"
        case .UnFollowAction:
            return "account/unfollow_user"
        case .ChangeOrder:
            return "deck/change_order"
        }
    }
    var method: Moya.Method {
        switch self {
        case .EmailSignIn:
            return .POST
        case .SearchTutors:
            return .POST
        case .SearchContent:
            return .GET
        case .GetAllDecks:
            return .GET
        case .GetProfileInfo:
            return .POST
        case .GetAllPoints:
            return .POST
        case .UsePoints:
            return .POST
        case .GetPointsFromVenmo:
            return .POST
        case .CreateTextCard:
            return .POST
        case .GenerateImageUploadParameters:
            return .POST
        case .CreateImageCard:
            return .POST
        case .EditImageCard:
            return .POST
        case .GenerateVideoUploadParameters:
            return .POST
        case .CreateVideoCard:
            return .POST
        case .EditVideoCard:
            return .POST
        case .GetCreatedCards:
            return .GET
        case .CreateDeck:
            return .POST
        case .DeleteCards:
            return .POST
        case .CopyCards:
            return .POST
        case .PublishDecks:
            return .POST
        case .EditDeck:
            return .POST
        case .LikeCard:
            return .POST
        case .UnlikeCard:
            return .POST
        case .LikeComment:
            return .POST
        case .UnlikeComment:
            return .POST
        case .MoveCard:
            return .POST
        case .AddComment:
            return .POST
        case .GetNotifications:
            return .GET
        case .EditProfile:
            return .POST
        case .GetCard:
            return .POST
        case .GetUsernames:
            return .POST
        case .AddMode:
            return .POST
        case .GiveCardToDeck:
            return .POST
        case .JoinAsk:
            return .POST
        case .UnjoinAsk:
            return .POST
        case .CancelSubmission:
            return .POST
        case .EndEvaluationPeriod:
            return .POST
        case .GetWinners:
            return .GET
        case .GetContentList:
            return .POST
        case .EditUserInfo:
            return .POST
        case .ValidateUserName:
            return .POST
        case .FollowAction:
            return .POST
        case .UnFollowAction:
            return .POST
        case .ChangeOrder:
            return .POST
        }
    }
    var parameters: [String: AnyObject]? {
        switch self {
        case .EmailSignIn(let email):
            return  ["email": email]
        case .SearchTutors(let searchText):
            return  ["search": [searchText]]
        case .SearchContent:
            return nil
        case .GetAllDecks:
            return nil
        case .GetProfileInfo(let userId):
            return ["user_id": userId]
        case .GetAllPoints(let userId):
            return ["user_id": userId]
        case .UsePoints(let userId, let paymentType, let username, let email):
            return ["user_id": userId, "payment_type": paymentType, "username": username, "email": email]
        case .GetPointsFromVenmo(let userId, let username, let email, let amount):
            return ["user_id": userId, "username": username, "email": email, "amount": amount]
        case .CreateTextCard(let cardTitle, let cardDescription, let cardText, let language):
            var params = ["card_content": cardText]
            if cardTitle != nil {
                params["card_title"] = cardTitle!
            }
            if cardDescription != nil {
                params["card_description"] = cardDescription!
            }
            if language != nil {
                params["language"] = language!
            }
            return params
        case .GenerateImageUploadParameters(let fileName):
            return ["file_name": fileName]
        case .CreateImageCard(let title, let description, let image, let image_x, let image_y, let image_width, let image_height, let tags, let image_scale):
            return ["card_title": title, "card_description": description, "s3_file_name": image, "image_x": image_x, "image_y": image_y, "image_width": image_width, "image_height": image_height, "tags": tags, "image_scale": image_scale]
            
        case .EditImageCard(let id, let title, let description, let image, let image_x, let image_y, let image_width, let image_height, let tags, let image_scale):
            return ["card_id": id, "card_title": title, "card_description": description, "s3_file_name": image, "image_x": image_x, "image_y": image_y, "image_width": image_width, "image_height": image_height, "tags": tags, "image_scale": image_scale]
            
        case .GenerateVideoUploadParameters(let fileName):
            return ["file_name": fileName]
        case .CreateVideoCard(let title, let description, let video, let thumbnail_url, let tags, let video_length):
            return ["card_title": title, "card_description": description, "s3_file_name": video, "thumbnail_url": thumbnail_url, "tags": tags]
            
        case .EditVideoCard(let id, let title, let description, let video, let thumbnail_url, let tags):
            return ["card_id": id, "card_title": title, "card_description": description, "s3_file_name": video, "thumbnail_url": thumbnail_url, "tags": tags]
            
        case .GetCreatedCards:
            return nil
        case .CreateDeck(let title, let cardIds):
            return ["deck_title": title, "card_ids": cardIds]
        case .DeleteCards(let cardIds):
            return ["card_ids": cardIds]
        case .CopyCards(let cardIds):
            return ["card_ids": cardIds]
        case .PublishDecks(let cardIds, let publish):
            return ["card_ids": cardIds, "publish": publish]
        case .EditDeck(let deckId, let cardIds):
            return ["deck_id": deckId, "card_ids": cardIds]
        case .LikeCard(let cardId):
            return ["card_id": cardId]
        case .UnlikeCard(let cardId):
            return ["card_id": cardId]
        case .LikeComment(let commentId):
            return ["comment_id": commentId]
        case .UnlikeComment(let commentId):
            return ["comment_id": commentId]
        case .MoveCard(let cardIds, let moveTo):
            return ["card_ids": cardIds, "move_to": moveTo]
        case .AddComment(let cardId, let commentContent, let commentId):
            
            if commentId == "" {
                return ["card_id": cardId, "comment_content": commentContent]
            }
            else {
                return ["comment_content": commentContent, "comment_id": commentId]
            }
        case .EditProfile(let firstName, let lastName, let username, let bio):
            return ["first_name": firstName, "last_name": lastName, "username": username, "bio": bio]
        case .GetNotifications:
            return nil
        case .GetCard(let cardId):
            return ["card_id": cardId]
        case .GetUsernames(let searchKey):
            return ["search_key": searchKey]
        case .AddMode(let isAskModeEnabled, let deckId, let userIds, let format, let initialPrize, let prizeToJoin, let distributionRule, let distributionFor, let evaluationStart, let evaluationEnd):
            let params: NSMutableDictionary = ["is_ask_mode_enabled": isAskModeEnabled, "deck_id": deckId, "asked_user_ids": userIds, "format": format, "initial_prize": initialPrize, "prize_to_join": prizeToJoin]
            if distributionRule != nil {
                params["distribution_rule"] = distributionRule
            }
            if distributionFor != 0 {
                params["distribution_for"] = distributionFor
            }
            if evaluationStart != nil {
                params["evaluation_start_dt"] = evaluationStart
            }
            if evaluationEnd != nil {
                params["evaluation_end_dt"] = evaluationEnd
            }
            var dict = [String : AnyObject]()
            for (key, value) in params {
                dict[key as! String] = value
            }
            return dict
//            return ["is_ask_mode_enabled": isAskModeEnabled, "deck_id": deckId, "asked_user_ids": userIds, "format": format, "initial_prize": initialPrize, "prize_to_join": prizeToJoin, "distibution_rule": distributionRule!, "distributtion_for": distributionFor!, "evaluation_start_dt": evaluationStart!, "evaluation_end_dt": evaluationEnd!]
        case .GiveCardToDeck(let deckId, let cardId, let visibility):
            return ["deck_id": deckId, "card_id": cardId, "visibility":visibility ?? Constants.ApiConstants.giveVisibilityAnyone]
        case .JoinAsk(let deckId, let joinPrize):
            return ["deck_id": deckId, "custom_join_prize": joinPrize]
        case .UnjoinAsk(let deckId):
            return ["deck_id": deckId]
        case .CancelSubmission(let cardId):
            return ["card_id": cardId]
        case .EndEvaluationPeriod(let deckId):
            return ["deck_id": deckId]
        case .GetWinners(let deckId):
            return ["deck_id": deckId]
        case .GetContentList(let listType , let user_id):
            return ["fields": listType,
                    "user_id":user_id]
        case .EditUserInfo(let firstname , let lastname , let username , let bio):
            return ["first_name":firstname,
                    "last_name":lastname,
                    "bio":bio,
                    "username":username]
        case .ValidateUserName(let username):
            return ["username":username]
        case .FollowAction(let user_id):
            return ["following_id":user_id]
        case .UnFollowAction(let user_id):
            return ["following_id":user_id]
        case .ChangeOrder(let card_id , let position):
            return ["card_id":card_id , "position":position]
        }
    }
    var sampleData: NSData {
        switch self {
        case .EmailSignIn:
            return "{}".UTF8EncodedData
        case .SearchTutors:
            return "{\"content\": [{\"created_at\": 1472841483, \"creator\": \"Chris Yamamoto\", \"title\": \"5 Japanese Slang Words\", \"url\": \"https://www.youtube.com/watch?v=777dowGCMp8\"}]}".UTF8EncodedData // TODO
        case .SearchContent:
            return "{\"content\": [{\"created_at\": 1472841483, \"creator\": \"Chris Yamamoto\", \"title\": \"5 Japanese Slang Words\", \"url\": \"https://www.youtube.com/watch?v=777dowGCMp8\"}]}".UTF8EncodedData
        case .GetAllDecks:
            return "{ \"content\": [ { \"content\": [ { \"content\": [ { \"content\": [ { \"content\": [ { \"content\": \"\u{304a}\u{306f}\u{3088}\u{3046}\", \"type\": \"text\" }, { \"content\": \"ohayo.mp3\", \"type\": \"audio\" } ], \"type\": \"card\" }, { \"content\": [ { \"content\": \"Good Morning\", \"type\": \"text\" } ], \"type\": \"card\" }, { \"content\": [ { \"content\": \"ohayo.jpg\", \"type\": \"image\" } ], \"type\": \"card\" } ], \"type\": \"set\" }, { \"content\": [ { \"content\": [ { \"content\": \"\u{3053}\u{3093}\u{306b}\u{3061}\u{306f}\", \"type\": \"text\" }, { \"content\": \"konnichiwa.mp3\", \"type\": \"audio\" } ], \"type\": \"card\" }, { \"content\": [ { \"content\": \"Hello\", \"type\": \"text\" } ], \"type\": \"card\" }, { \"content\": [ { \"content\": \"konnichiwa.jpg\", \"type\": \"image\" } ], \"type\": \"card\" } ], \"type\": \"set\" }, { \"content\": [ { \"content\": [ { \"content\": \"\u{3053}\u{3093}\u{3070}\u{3093}\u{306f}\", \"type\": \"text\" }, { \"content\": \"konbanwa.mp3\", \"type\": \"audio\" } ], \"type\": \"card\" }, { \"content\": [ { \"content\": \"Good Evening\", \"type\": \"text\" } ], \"type\": \"card\" }, { \"content\": [ { \"content\": \"konbanwa.jpg\", \"type\": \"image\" } ], \"type\": \"card\" } ], \"type\": \"set\" } ], \"name\": \"Basic Greetings\", \"type\": \"deck\" } ], \"name\": \"Greetings\", \"type\": \"directory\" }, { \"content\": [], \"name\": \"Countries\", \"type\": \"directory\" }, { \"content\": [], \"name\": \"Numbers\", \"type\": \"directory\" } ], \"name\": \"Japanese\", \"type\": \"directory\" }".UTF8EncodedData
        case .GetProfileInfo:
            return "{}".UTF8EncodedData
        case .GetAllPoints:
            return "{}".UTF8EncodedData
        case .UsePoints:
            return "{}".UTF8EncodedData
        case .GetPointsFromVenmo:
            return "{}".UTF8EncodedData
        case .CreateTextCard:
            return "{}".UTF8EncodedData
        case .GenerateImageUploadParameters:
            return "{}".UTF8EncodedData
        case .CreateImageCard:
            return "{}".UTF8EncodedData
        case .EditImageCard:
            return "{}".UTF8EncodedData
        case .GenerateVideoUploadParameters:
            return "{}".UTF8EncodedData
        case .CreateVideoCard:
            return "{}".UTF8EncodedData
        case .EditVideoCard:
            return "{}".UTF8EncodedData
        case .GetCreatedCards:
            return "{}".UTF8EncodedData
        case .CreateDeck:
            return "{}".UTF8EncodedData
        case .DeleteCards:
            return "{}".UTF8EncodedData
        case .CopyCards:
            return "{}".UTF8EncodedData
        case .PublishDecks:
            return "{}".UTF8EncodedData
        case .EditDeck:
            return "{}".UTF8EncodedData
        case .LikeCard:
            return "{}".UTF8EncodedData
        case .UnlikeCard:
            return "{}".UTF8EncodedData
        case .LikeComment:
            return "{}".UTF8EncodedData
        case .UnlikeComment:
            return "{}".UTF8EncodedData
        case .MoveCard:
            return "{}".UTF8EncodedData
        case .AddComment:
            return "{}".UTF8EncodedData
        case .GetNotifications:
            return "{}".UTF8EncodedData
        case .EditProfile:
            return "{}".UTF8EncodedData
        case .GetCard:
            return "{}".UTF8EncodedData
        case .GetUsernames:
            return "{}".UTF8EncodedData
        case .AddMode:
            return "{}".UTF8EncodedData
        case .GiveCardToDeck:
            return "{}".UTF8EncodedData
        case .JoinAsk:
            return "{}".UTF8EncodedData
        case .UnjoinAsk:
            return "{}".UTF8EncodedData
        case .CancelSubmission:
            return "{}".UTF8EncodedData
        case .EndEvaluationPeriod:
            return "{}".UTF8EncodedData
        case .GetWinners:
            return "{}".UTF8EncodedData
        case .GetContentList:
            return "{}".UTF8EncodedData
        case .EditUserInfo:
            return "{}".UTF8EncodedData
        case .ValidateUserName:
            return "{}".UTF8EncodedData
        case .FollowAction:
            return "{}".UTF8EncodedData
        case .UnFollowAction:
            return "{}".UTF8EncodedData
        case .ChangeOrder:
            return "{}".UTF8EncodedData
        }
    }

    var multipartBody: [MultipartFormData]? {
        return nil
    }
}

// MARK: - Helpers

private extension String {
    
    var URLEscapedString: String {
        return stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())!
    }
    var UTF8EncodedData: NSData {
        return dataUsingEncoding(NSUTF8StringEncoding)!
    }
}
