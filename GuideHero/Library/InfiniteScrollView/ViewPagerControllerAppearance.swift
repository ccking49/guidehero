//
//  ViewPagerControllerAppearance.swift
//  GuideHero
//
//  Created by topmobile106 on 12/19/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import Foundation
import UIKit

public struct ViewPagerControllerAppearance {
    
    public init() {}
    
    public var tabMenuHeight : CGFloat = 64.0
    public var headerContentsView : UIView?
    
    // TabMenu
    public var tabMenuAppearance : TabMenuAppearance = TabMenuAppearance()
    
    // ScrollHeaderSupport
    public var scrollViewMinPositionY : CGFloat = 0.0
    public var scrollViewObservingType : ObservingScrollViewType = .none
    public var scrollViewObservingDelay : CGFloat = 0.5
}

public struct TabMenuAppearance {
    
    public init() {}
    
    public var backgroundColor : UIColor = UIColor.blackColor()
    
    public var backgroundImage : UIImage?
    // Title Layout
    public var titleMargin : CGFloat = 12.0
    public var titleTop: CGFloat = 12.0
    public var titleMinWidth : CGFloat = 30.0
    
    // Title Color
    public var defaultTitleColor : UIColor = UIColor.blackColor()
    public var highlightedTitleColor : UIColor = UIColor.whiteColor()
    public var selectedTitleColor : UIColor = UIColor.whiteColor()
    
    // Title Font
    public var defaultTitleFont : UIFont = Helper.proximaNova("SemiBold", font: 18)
    public var highlightedTitleFont : UIFont = Helper.proximaNova("SemiBold", font: 18)
    public var selectedTitleFont : UIFont = Helper.proximaNova("SemiBold", font: 18)
    
    // Selected View
    public var selectedViewBackgroundColor : UIColor = UIColor.redColor()
    public var selectedViewInsets : UIEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
    
    public var backgroundContentsView : UIView?
}
