//
//  TableArrayDataSource.swift
//  GuideHero
//
//  Created by Dino Bartosak on 05/06/16.
//  Copyright © 2016 Dino Bartosak. All rights reserved.
//

import UIKit

class TableArrayDataSource<DataType>: NSObject,
UITableViewDataSource {

    // cell, item, cellIdx
    typealias ConfigureCellBlock = (cell: UITableViewCell, item: DataType, index: Int) -> Void
    typealias AllowsEditingBlock = (index: Int) -> Bool
    typealias CommitEditingBlock = (editingStyle: UITableViewCellEditingStyle, index: Int) -> Void
    
    private var items: [DataType];
    private var cellReuseIdentifier: String
    private var configureBlock: ConfigureCellBlock;
    
    var allowsEditingBlock: AllowsEditingBlock?;
    var commitEditingBlock: CommitEditingBlock?;
    
    init(items: [DataType], cellReuseIdentifier: String, configureBlock: ConfigureCellBlock) {
        self.items = items;
        self.cellReuseIdentifier = cellReuseIdentifier;
        self.configureBlock = configureBlock;
    }
    
    // MARK: Public
    
    func itemsCount() -> Int {
        return items.count;
    }
    
    func itemAtIndex(index: Int) -> DataType {
        return items[index];
    }
    
    func allItems() -> [DataType] {
        return items;
    }
    
    func insertItem(item: DataType, atIndex index: Int) {
        items.insert(item, atIndex: index);
    }
    
    func appendItem(item: DataType) {
        items.append(item);
    }
    
    func removeItemAtIndex(index: Int) {
        items.removeAtIndex(index);
    }
    
    func replaceItem(itemAtIndex index: Int, withItem item: DataType) {
        items.removeAtIndex(index);
        items.insert(item, atIndex: index);
    }
    
    // MARK: Private
    
    private func itemAtIndexPath(indexPath: NSIndexPath) -> DataType {
        return items[indexPath.row];
    }
    
    // MARK: Table View Data Source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let item: DataType = itemAtIndexPath(indexPath);
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellReuseIdentifier, forIndexPath: indexPath);
        
        configureBlock(cell: cell, item: item, index: indexPath.row);
        
        return cell;
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if let allowsEditingBlock = allowsEditingBlock {
            return allowsEditingBlock(index: indexPath.row);
        } else {
            return true;
        }
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if let commitEditingBlock = commitEditingBlock {
            commitEditingBlock(editingStyle: editingStyle, index: indexPath.row);
        }
    }
    
    func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
      let itemToMove = itemAtIndex(fromIndexPath.row)
      removeItemAtIndex(fromIndexPath.row)
      insertItem(itemToMove, atIndex: toIndexPath.row)
    }
}
