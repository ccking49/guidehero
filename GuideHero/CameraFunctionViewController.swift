//
//  CameraFunctionViewController.swift
//  GuideHero
//
//  Created by forever on 1/5/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit

class CameraFunctionViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var panGestureRecognizer: UIPanGestureRecognizer!
    
    var decId = ""
    
    var pageViewController: PageViewController!
    var dismissInteractor: DismissInteractor?
    var parentVC: UIViewController? = nil
    var type: Int = 0
    
    var submittedCallback: ((_: ContentNode!) -> Void)!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        pageController.addTarget(self, action: #selector(CameraFunctionViewController.didChangePageControlValue), forControlEvents: UIControlEvents.ValueChanged)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CameraFunctionViewController.gotoCameraRoll), name: "GotoCameraRollScreen", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CameraFunctionViewController.gotoCamera), name: "GotoCameraScreen", object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().statusBarHidden = true
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.sharedApplication().statusBarHidden = false
    }
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "GotoCameraRollScreen", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "GotoCameraScreen", object: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PageViewControllerSegue", let pViewController = segue.destinationViewController as? PageViewController {
            pageViewController = pViewController
            pageViewController.pageDelegate = self
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let cameraVC = storyBoard.instantiateViewControllerWithIdentifier("VideoCaptureViewController") as! VideoCaptureViewController
            cameraVC.parentVC = self.parentVC
            cameraVC.type = self.type
            
            let cameraRollVC = storyBoard.instantiateViewControllerWithIdentifier("LibraryController") as! LibraryController
            cameraRollVC.parentVC = self.parentVC
            cameraRollVC.type = self.type
            
            cameraVC.panToCloseGestureRecognizer = panGestureRecognizer
            cameraRollVC.panToCloseGestureRecognizer = panGestureRecognizer
            pageViewController.orderedViewControllers = [cameraRollVC, cameraVC]
        }
    }
    
    func gotoCameraRoll() {
        pageViewController?.scrollToPrevViewController()
    }
    
    func gotoCamera() {
        pageViewController?.scrollToNextViewController()
    }
    
    func didChangePageControlValue() {
        pageViewController?.scrollToViewController(index: pageController.currentPage)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CameraFunctionViewController: PageViewControllerDelegate {
    
    func pageViewController(pViewController: PageViewController,
                            didUpdatePageCount count: Int) {
        pageController.numberOfPages = count
    }
    
    func pageViewController(pViewController: PageViewController,
                            didUpdatePageIndex index: Int) {
        pageController.currentPage = index
    }
    
}

// MARK: - Actions
extension CameraFunctionViewController {
    @IBAction func handlePanGesture(sender: UIPanGestureRecognizer) {
        let percentThreshold: CGFloat = 0.3
        
        // convert y-position to downward pull progress (percentage)
        let translation = sender.translationInView(view)
        let verticalMovement = translation.y / view.bounds.height
        let downwardMovement = fmaxf(Float(verticalMovement), 0.0)
        let downwardMovementPercent = fminf(downwardMovement, 1.0)
        let progress = CGFloat(downwardMovementPercent)
        
        guard let dismissInteractor = dismissInteractor else { return }
        
        switch sender.state {
        case .Began:
            dismissInteractor.hasStarted = true
            navigationController!.dismissViewControllerAnimated(true, completion: nil)
        case .Changed:
            dismissInteractor.shouldFinish = progress > percentThreshold
            dismissInteractor.updateInteractiveTransition(progress)
        case .Cancelled:
            dismissInteractor.hasStarted = false
            dismissInteractor.cancelInteractiveTransition()
        case .Ended:
            dismissInteractor.hasStarted = false
            dismissInteractor.shouldFinish ? dismissInteractor.finishInteractiveTransition() : dismissInteractor.cancelInteractiveTransition()
        default:
            break
        }
    }
}
