//
//  Constants.swift
//  GuideHero
//
//  Created by Yohei Oka on 7/24/16.
//  Copyright © 2016 Twilio. All rights reserved.
//

import Foundation

struct Constants {
    static let dev = false // true enables email login
    
    struct ApiConstants {
        #if PRODUCTION
            static let baseApiDomain = "http://ivysaur-staging.us-east-1.elasticbeanstalk.com/"
        
        #else
//            static let baseApiDomain = "http://localhost:5000/"
             static let baseApiDomain = "http://ivysaur-staging.us-east-1.elasticbeanstalk.com/"

//            static let baseApiDomain = "http://ivysaur-staging.us-east-1.elasticbeanstalk.com/"
        #endif
        static let baseApiUrl = baseApiDomain + "api/v1/"
        
        static let giveVisibilityAnyone = "anyone"
        static let giveVisibilityAskers = "only_askers"
        static let prizeDistributionProportionally = "proportionally"
        static let prizeDistributionEvenly = "evenly"
    }


    struct UIConstants {
        static let learnTabIndex = 0
        static let contentHeaderCellHeight = 550 as CGFloat
        static let contentHeaderCellEditingHeight = 600 as CGFloat
        static let contentCellHeight = 120 as CGFloat
        static let detailCardCornerRadius = 20 as CGFloat
        static let pinkColor = UIColor.colorFromRGB(redValue: 255, greenValue: 0, blueValue: 58, alpha: 1)
        static let yellowColor = UIColor.colorFromRGB(redValue: 243, greenValue: 214, blueValue: 33, alpha: 1)
        static let halfYellowColor = UIColor.colorFromRGB(redValue: 243, greenValue: 214, blueValue: 33, alpha: 0.5)
    }
    struct Literals {
        static let contentRefreshNotification   = "contentRefreshNotification"
        static let prizePoolChangedNotification = "prizePoolChangedNotification"
        static let apiDateFormat                = "dd MMM yyyy HH:mm:ss"
        static let localDateFormat              = "H:mm, MMM d, yyyy" // used in prize cells
    }

    // turn off print statemens
    static let showPrintDebug = false
}
