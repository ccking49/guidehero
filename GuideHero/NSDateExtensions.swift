//
//  NSDateExtensions.swift
//  GuideHero
//
//  Created by Justin Holman on 1/31/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import Foundation

extension NSDate {

    func stringWithFormat(format: String) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.PMSymbol = "pm"
        dateFormatter.AMSymbol = "am"
        let dateString = dateFormatter.stringFromDate(self)
        return dateString
    }

    func stringWithFormatInGMT(format: String) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.PMSymbol = "pm"
        dateFormatter.AMSymbol = "am"
        dateFormatter.timeZone = NSTimeZone(name: "UTC")
        let dateString = dateFormatter.stringFromDate(self)
        return dateString
    }
}
