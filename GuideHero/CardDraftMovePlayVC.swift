//
//  CardDetailPlayVC.swift
//  GuideHero
//
//  Created by Promising Change on 23/12/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya

protocol CardDraftMovePlayVCDelegate {
    func onResponseContent(resContent : ContentNode)
    func onMoveContent(resContent : [ContentNode] , nType : Int)
}

class CardDraftMovePlayVC: CardSubPageBaseVC, UITableViewDelegate, UITableViewDataSource ,CardDraftMoveAskEnabledDetailVCDelegate{
    
    // MARK: - Variables
    
    @IBOutlet weak var tbCardList: UITableView!
    @IBOutlet weak var footerView : UIView!
    
    @IBOutlet weak var topView : UIView!
    
    var drafts = [ContentNode]()
    
    var nHeaderHeight : Int = 540
    
    let tbHeaderView: UIView!
    var blankCellHeight: CGFloat!
    
    @IBOutlet weak var vwHeader: UIView!
    
    var cardDetailPlayDelegate : CardDraftMovePlayVCDelegate?
    
    var isType : Int = 2
    
    var lastIndex = -1
    
    
    var nFlag : Int = 0
    var nSubFlag : Int = 0
    
    var content: ContentNode? {
        didSet {
            if isViewLoaded() {
                tbCardList.reloadData()
                self.view.layoutIfNeeded()
            }
        }
    }
    
    var selectedDraft: ContentNode? {
        didSet {
            if isViewLoaded() {
                // Update the Content
                
            }
        }
    }
    
//    var submittedCallback_askEnabled: ((_: ContentNode!) -> Void)!
    var submittedCallback: ((_: [ContentNode]! , _: Int!) -> Void)!
    
    // MARK: - View Lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        tbHeaderView = UIView()
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(nHeaderHeight)
        blankCellHeight = max(UIScreen.mainScreen().bounds.size.height - CGFloat(nHeaderHeight + 50) + 1, 0)
        
        // Do any additional setup after loading the view.
        
        
        scrollView = tbCardList as UIScrollView
        
        configureView()
        
        tbCardList.separatorStyle = .None
        
        if content?.children.count == 1 {
            singleCard = true
        }
        
        refreshData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    // MARK: - Custom Methods
    
    func configureView() {
        tbHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: CGFloat(nHeaderHeight))
        
        //        let cellNib = UINib(nibName: "ContentTableCell", bundle: nil)
        //        tbCardList.registerNib(cellNib, forCellReuseIdentifier: "ContentTableCell")
        
        let cellNib = UINib(nibName: "CardDetailTableViewCell", bundle: nil)
        tbCardList.registerNib(cellNib, forCellReuseIdentifier: "CardDetailTableViewCell")
        
        let blankCellNib = UINib(nibName: "RoundedBlankTableViewCell", bundle: nil)
        tbCardList.registerNib(blankCellNib, forCellReuseIdentifier: "RoundedBlankTableViewCell")
        
        
        let cellHereNib = UINib(nibName: "CardDraftMoveHereCell", bundle: nil)
        tbCardList.registerNib(cellHereNib, forCellReuseIdentifier: "CardDraftMoveHereCell")
        
        
        tbCardList.rowHeight = UITableViewAutomaticDimension
        tbCardList.estimatedRowHeight = Constants.UIConstants.contentCellHeight
        tbCardList.separatorStyle = .SingleLine
        
        tbCardList.tableHeaderView = tbHeaderView
        //        tbCardList.tableFooterView = UIView(frame: CGRect.zero)
        
        footerView.layer.cornerRadius = 20
        footerView.layer.shadowRadius = 5
        footerView.layer.shadowOpacity = 0.7
        footerView.layer.shadowOffset = CGSizeZero
        footerView.layer.shadowColor = UIColor.blackColor().CGColor
        
//        footerView.layer.borderColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.5).CGColor
//        footerView.layer.borderWidth = 0.5
        footerView.clipsToBounds = true
        
        self.view.setNeedsLayout()
        
    }
    
    func refreshData() {
        tbCardList.reloadData()
    }
    
    // MARK: - UITableViewDelegate & UITableViewDataSource
    
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        guard let content = content else { return 0 }
//        
//        if content.children.count == 1 {
//            return 1
//        }
//        
//        return (max(content.children.count, 1)) - 1
//    }
//    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        
////        if content?.children.count == 1 {
////            return 36
////        }
//        
//        return (content?.children.count ?? 0) > 0 ? 216 : blankCellHeight
//    }
//    
//    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if content?.children.count <= 1 {
//            return 0
//        }
//        
//        return 1
//    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let content = content else { return 0 }
        
        
        if self.content?.ask_enabled == true {
            
            if content.children.count == 1 || content.children.count == 0 {
                return 1
            }
            
            return content.children.count-1
            
        }else{
            
            return content.children.count+1
            
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if self.content?.ask_enabled == true {
            if content!.children.count == 1 || content!.children.count == 0{
                return blankCellHeight
            }
            
            return 216
        }else{
            return 216
        }
        
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let separatorView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 1))
        separatorView.backgroundColor = UIColor(colorLiteralRed: 0.84, green: 0.84, blue: 0.84, alpha: 1)
        
        return separatorView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        if self.content?.ask_enabled == true {
            
            if content?.children.count  == 0 || content?.children.count == 1 {
                return tableView.dequeueReusableCellWithIdentifier("Blank")!
            }
            
            let cell = tableView.dequeueReusableCellWithIdentifier("CardDetailTableViewCell", forIndexPath: indexPath) as! CardDetailTableViewCell
            cell.content = self.content!.children[indexPath.row + 1]
            
            cell.imgSeparate.hidden = false
            
            cell.separateView.hidden = true
            cell.imgSeparate.layer.shadowRadius = 2
            
            cell.imgSeparate.layer.shadowOffset = CGSizeZero
            cell.imgSeparate.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
            
            cell.btnProfilePhoto.tag = indexPath.row + 1
            cell.btnProfileName.tag = indexPath.row + 1
            cell.btnProfileName.addTarget(self, action: #selector(CardDraftMovePlayVC.onGotoProfilePage(_:)), forControlEvents: .TouchUpInside)
            cell.btnProfilePhoto.addTarget(self, action: #selector(CardDraftMovePlayVC.onGotoProfilePage(_:)), forControlEvents: .TouchUpInside)
            
            cell.cardView.tag = indexPath.row + 1
            cell.tag = indexPath.row + 1
            cell.imgLock.hidden = true
            
            
            let cellTap = UITapGestureRecognizer(target: self, action: #selector(CardDraftMovePlayVC.openCellDetail))
            cell.addGestureRecognizer(cellTap)
            
            cell.cardView.tag = indexPath.row + 1
            let singleTap = UITapGestureRecognizer(target: self, action: #selector(CardDraftMovePlayVC.showFullScreen))
            cell.cardView.addGestureRecognizer(singleTap)
            
            cell.layer.shadowRadius = 1
            cell.layer.shadowOffset = CGSizeZero
            cell.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
            
            cell.needsRoundBottom = true
            
            
            return cell
            
        }else{
            
            if content?.children.count  == 0 {
                let cell = tableView.dequeueReusableCellWithIdentifier("CardDraftMoveHereCell", forIndexPath: indexPath) as! CardDraftMoveHereCell
                
                cell.borderView.layer.cornerRadius = 7
                cell.borderView.layer.borderColor = UIColor.colorFromRGB(redValue: 254, greenValue: 40, blueValue: 232, alpha: 1.0).CGColor
                cell.borderView.layer.borderWidth = 3
                
                cell.btnHere.addTarget(self, action: #selector(onTapHere(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                return cell
            }
            
            if indexPath.row ==  self.content!.children.count{
                
                let cell = tableView.dequeueReusableCellWithIdentifier("CardDraftMoveHereCell", forIndexPath: indexPath) as! CardDraftMoveHereCell
                
                cell.borderView.layer.cornerRadius = 7
                cell.borderView.layer.borderColor = UIColor.colorFromRGB(redValue: 254, greenValue: 40, blueValue: 232, alpha: 1.0).CGColor
                cell.borderView.layer.borderWidth = 3
                
                cell.btnHere.addTarget(self, action: #selector(onTapHere(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                return cell
                
            }
            
            let cell = tableView.dequeueReusableCellWithIdentifier("CardDetailTableViewCell", forIndexPath: indexPath) as! CardDetailTableViewCell
            cell.content = self.content!.children[indexPath.row]
            
            cell.imgSeparate.hidden = false
            
            cell.separateView.hidden = true
            cell.imgSeparate.layer.shadowRadius = 2
            
            cell.imgSeparate.layer.shadowOffset = CGSizeZero
            cell.imgSeparate.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
            
            cell.btnProfilePhoto.tag = indexPath.row
            cell.btnProfileName.tag = indexPath.row
            cell.btnProfileName.addTarget(self, action: #selector(CardDraftMovePlayVC.onGotoProfilePage(_:)), forControlEvents: .TouchUpInside)
            cell.btnProfilePhoto.addTarget(self, action: #selector(CardDraftMovePlayVC.onGotoProfilePage(_:)), forControlEvents: .TouchUpInside)
            
            cell.cardView.tag = indexPath.row
            cell.tag = indexPath.row
            cell.imgLock.hidden = true
            
            let cellTap = UITapGestureRecognizer(target: self, action: #selector(CardDraftMovePlayVC.openCellDetail))
            cell.addGestureRecognizer(cellTap)
            
            cell.cardView.tag = indexPath.row
            let singleTap = UITapGestureRecognizer(target: self, action: #selector(CardDraftMovePlayVC.showFullScreen))
            cell.cardView.addGestureRecognizer(singleTap)
            
            cell.layer.shadowRadius = 1
            cell.layer.shadowOffset = CGSizeZero
            cell.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
            
            cell.needsRoundBottom = true
            
            
            return cell
        }
        
    }
    
    func onResetDraftData(nContent : ContentNode){
        
        if nContent.children.count != 0 {
            
            var nnIndex = 0
            for itemContent in nContent.children {
                
                if itemContent.id == self.selectedDraft?.id {
                    
                    
                    nContent.children.removeAtIndex(nnIndex)
                    self.nSubFlag = 1
                    
                    
                    break
                }
                
                nnIndex = nnIndex + 1
                
            }
            if self.nSubFlag == 0 {
                for itemContent in nContent.children {
                    if itemContent.children.count != 0 {
                        
                        if self.nSubFlag == 1 {
                            break
                        }else{
                            self.onResetDraftData(itemContent)
                        }
                    }
                }
                
            }
        }
        
    }
    
    @IBAction func onTapHere (sender : AnyObject){
        
//        var nIndex : Int = 0
//        
//        for itemContent in self.drafts {
//            if itemContent.id == self.content?.id {
//                
//                self.showProgressHUD()
//                
//                let provider = MoyaProvider<NetworkService>.createDefaultProvider()
//                
//                var cardIds = [String]()
//                cardIds.append((self.selectedDraft?.id)!)
////                cardIds.append((itemContent.id))
//                
//                provider.request(NetworkService.MoveCard(cardIds: cardIds, moveTo: itemContent.id), completion: { (result) in
//                    switch result {
//                    case let .Success(response):
//                        if response.statusCode != 200 {
//                            self.showSimpleAlert(withTitle: "Error", withMessage: "Can't move draft, please try again later")
//                            self.hideProgressHUD()
//                            return
//                        }
//                        if (try? response.mapJSON() as! [String: AnyObject]) != nil {
//                            
//                            self.hideProgressHUD()
//                            
//                            itemContent.children.append(self.selectedDraft!)
//                            
//                            var nnIndex = 0
//                            
//                            for itemContent in self.drafts {
//                                
//                                if itemContent.id == self.selectedDraft?.id {
//                                    
//                                    self.drafts.removeAtIndex(nnIndex)
//                                    self.nSubFlag = 1
//                                    break
//                                }
//                                if self.nSubFlag == 0 {
//                                    self.onResetDraftData(itemContent)
//                                }else{
//                                    break
//                                }
//                                
//                                
//                                nnIndex = nnIndex + 1
//                            }
//                            
//                            self.nFlag = 1
//                            
//                            self.cardDetailPlayDelegate?.onMoveContent(self.drafts, nType: 1)
//                        }
//                        else {
//                            self.showSimpleAlert(withTitle: "Error", withMessage: "Can't move draft, please try again later")
//                            self.hideProgressHUD()
//                        }
//                    case let .Failure(error):
//                        
//                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't move draft, please try again later")
//                        self.hideProgressHUD()
//                    }
//                    
//                    
//                })
//                
//                
//                break
//            }
//            nIndex = nIndex + 1
//        }
//        
//        if nFlag == 0 {
//            
//            for itemContent in self.drafts {
//                if nFlag == 1 {
//                    break
//                }else{
//                    self.onResetDraftList(itemContent)
//                }
//            }
//            
//        }
        
        self.showProgressHUD()
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        
        var cardIds = [String]()
        cardIds.append((self.selectedDraft?.id)!)
        //                cardIds.append((itemContent.id))
        
        provider.request(NetworkService.MoveCard(cardIds: cardIds, moveTo: self.content!.id), completion: { (result) in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't move draft, please try again later")
                    self.hideProgressHUD()
                    return
                }
                if (try? response.mapJSON() as! [String: AnyObject]) != nil {
                    
                    self.hideProgressHUD()
                    
                    
                    self.cardDetailPlayDelegate?.onMoveContent(self.drafts, nType: 1)
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't move draft, please try again later")
                    self.hideProgressHUD()
                }
            case let .Failure(error):
                
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't move draft, please try again later")
                self.hideProgressHUD()
            }
            
            
        })
        
        
        
    }
    
    func onResetDraftList(nContent : ContentNode){
        
        if nContent.children.count != 0 {
            
            for itemContent in nContent.children {
                
                if itemContent.id == self.content?.id {
                    
                    let provider = MoyaProvider<NetworkService>.createDefaultProvider()
                    var cardIds = [String]()
                    cardIds.append((self.selectedDraft?.id)!)
                    
                    self.showProgressHUD()
                    provider.request(NetworkService.MoveCard(cardIds: cardIds, moveTo: itemContent.id), completion: { (result) in
                        switch result {
                        case let .Success(response):
                            if response.statusCode != 200 {
                                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't move draft, please try again later")
                                self.hideProgressHUD()
                                return
                            }
                            if (try? response.mapJSON() as! [String: AnyObject]) != nil {
                                
                                self.hideProgressHUD()
                                
                                
                                itemContent.children.append(self.selectedDraft!)
                                
                                var nnIndex = 0
                                
                                for itemContent in self.drafts {
                                    
                                    if itemContent.id == self.selectedDraft?.id {
                                        
                                        self.drafts.removeAtIndex(nnIndex)
                                        self.nSubFlag = 1
                                        break
                                    }
                                    if self.nSubFlag == 0 {
                                        self.onResetDraftData(itemContent)
                                    }else{
                                        break
                                    }
                                    
                                    
                                    nnIndex = nnIndex + 1
                                }
                                
                                self.nFlag = 1
                                
                                self.cardDetailPlayDelegate?.onMoveContent(self.drafts, nType: 1)
                            }
                            else {
                                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't move draft, please try again later")
                                self.hideProgressHUD()
                            }
                        case let .Failure(error):
                            
                            self.showSimpleAlert(withTitle: "Error", withMessage: "Can't move draft, please try again later")
                            self.hideProgressHUD()
                        }
                        
                        
                    })
                    
                    
                    break
                }
                
            }
            if self.nFlag == 0 {
                for itemContent in nContent.children {
                    if itemContent.children.count != 0 {
                        
                        if self.nFlag == 1 {
                            break
                        }else{
                            self.onResetDraftList(itemContent)
                        }
                    }
                }
                
            }
        }
        
        
    }
    
    func onGotoProfilePage(sender : UIButton){
        let profileVC = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "TabProfileController") as! TabProfileController
        
        let cData : ContentNode = self.content!.children[sender.tag]
        profileVC.userInfo = cData.creatorInfo
        profileVC.isShow = true
        profileVC.modalPresentationStyle = .OverFullScreen
        self.presentViewController(profileVC, animated: true, completion: nil)
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       
    }
    
    func showFullScreen(gestureRecognizer: UIGestureRecognizer) {
        let index = gestureRecognizer.view!.tag
        
        let fullScreenViewController = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "FullScreenViewController") as! FullScreenViewController
        
        fullScreenViewController.content = content
        fullScreenViewController.deckIndex = index
        
        fullScreenViewController.modalPresentationStyle = .OverFullScreen
        self.presentViewController(fullScreenViewController, animated: true, completion: nil)
        
    }
    
    func openCellDetail(gestureRecognizer: UIGestureRecognizer) {
        let nIndex = gestureRecognizer.view!.tag
        let indexPath = NSIndexPath(forRow: nIndex, inSection: 0)
        
        let vc = UIStoryboard.loadViewController(storyboardName: "CardDraft", viewControllerIdentifier: "CardDraftMoveAskEnabledDetailVC") as! CardDraftMoveAskEnabledDetailVC
        vc.content = self.content?.children[nIndex]
        vc.drafts = self.drafts
        vc.selectedDraft = self.selectedDraft
        vc.delegate = self
        vc.submittedCallback = {rContent , nType in
            self.cardDetailPlayDelegate?.onMoveContent(rContent, nType: 1)
        }
        
        presentViewController(vc, animated: true, completion: nil)
        self.tbCardList.deselectRowAtIndexPath(indexPath, animated: true)
        
    }
    
    // MARK: - UIScrollViewDelegate
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
    }
    func cardDraftDetailContentWasUpdated(content: ContentNode) {
        // Update previously selected draft with new content, from Make an Ask or Edit.
        if (lastIndex != -1) {
            self.drafts[lastIndex] = content
        }
    }
    func contentReloadData(){
        self.tbCardList.reloadData()
    }

}
// MARK: - CardDraftMoveAskEnabledDetailVCDelegate
