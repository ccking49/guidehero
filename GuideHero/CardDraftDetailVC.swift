//
//  CardDraftDetailVC.swift
//  GuideHero
//
//  Created by Promising Change on 31/01/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit
import AlamofireImage
import Moya
import AVFoundation

protocol CardDraftDetailVCDelegate: class {
    func cardDraftDetailContentWasUpdated(content: ContentNode)
    func contentReloadData()
}

class CardDraftDetailVC: CardViewPagerController, CardDraftDetailPlayVCDelegate {
    
    // MARK: - @IBOutlet Variables for Card View Pager
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var vwDraggableView: PannableCardView!
    @IBOutlet weak var vwHeader: UIView!
//    @IBOutlet weak var deckBackground: UIImageView!
    @IBOutlet weak var stackViewActions: UIStackView!
    @IBOutlet weak var cardActionPlay: CardDetailButton!
    @IBOutlet weak var cardActionAsk: CardDetailButton!
    @IBOutlet weak var cardActionGive: CardDetailButton!
    @IBOutlet weak var cardActionPrize: CardDetailButton!
    
    @IBOutlet weak var constraintHeaderTop: NSLayoutConstraint!
    @IBOutlet weak var constraintActionsTop: NSLayoutConstraint!
    @IBOutlet weak var constraintContentBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintSocialTop: NSLayoutConstraint!
    
    @IBOutlet weak var askWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var giveWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var prizeWidthConstraint: NSLayoutConstraint!
    
    // MARK: - @IBOutlet Variables for Card Header
    
//    @IBOutlet weak var backgroundImage: MaskImageView!
    @IBOutlet weak var creatorImage: UIImageView!
    @IBOutlet weak var creatorNameAndDate: UILabel!
//    @IBOutlet weak var cardView: UIView!
//    @IBOutlet weak var imgCard : MaskImageView!
    
    //    @IBOutlet weak var imgCard : MaskImageView!
    @IBOutlet weak var cardBackgroundView : CardMaskContainerView!
    @IBOutlet weak var cardView: SubmissionContainerView!
    
//    @IBOutlet weak var deckTitle: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentsLabel: UILabel!
    
    @IBOutlet weak var actionView : UIView!
    @IBOutlet weak var imgLine1 : UIImageView!
    
    @IBOutlet weak var containBottomView : UIView!
    // MARK: - Variables
    
    weak var delegate: CardDraftDetailVCDelegate?
    
    var editingStatus = CardEditingStatus.Normal
    var newChildrenOrdering = [ContentNode]()
    
    var originalHeaderTop: CGFloat?
    var originalActionsTop: CGFloat?
    var originalSocialTop: CGFloat?
    
    var cardDetailPlayVC: CardDraftDetailPlayVC?
    var cardDetailAskVC: CardDetailAskVC?
    var cardDetailGiveVC: CardDetailGiveVC?
    var cardDetailPrizeVC: CardDetailPrizeVC?
    
    var panGestureRecognizer = UIPanGestureRecognizer()
    var tapGestureRecognizer = UITapGestureRecognizer()
    
    var cardActionButtons: [CardDetailButton] = []
    
    var drafts = [ContentNode]()
    
    var submittedCallback: ((_: [ContentNode]! , _: Int!) -> Void)!
    
    var content: ContentNode? {
        didSet {
            if isViewLoaded() {
                // Update the Content
                
                cardDetailPlayVC?.content = self.content
                cardDetailAskVC?.content = self.content
                cardDetailGiveVC?.content = self.content
                cardDetailPrizeVC?.content = self.content
                
                reconfigureView()
            }
        }
    }
    
    var cardId: String? // To load specific deck/card
    var contentIndex: Int = 0
    var onHeaderAction: ((HeaderCellAction) -> Void)!
    
    var showLock = false {
        didSet {
            updateLock()
        }
    }
    
    var moreOptionsView: MoreView?
    
    // MARK: - View Lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        datasource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        datasource = self
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(DraftsViewController.changedTable(_:)),
                                                         name: "changedTable",
                                                         object: nil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func changedTable(notification: NSNotification) {
        self.dismissViewControllerAnimated(true) { 
            self.delegate?.contentReloadData()
        }
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        
        
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureView()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        UIApplication.sharedApplication().statusBarHidden = true
        
        super.viewWillAppear(animated)
        
//        self.vwMain.backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.7)

        
        containerView.bringSubviewToFront(actionView)
        reconfigureView()
        
        if self.content == nil {
            self.fetchAllDecks()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Methods
    
    func configureView() {
        
        
        containBottomView.layer.cornerRadius = 20
        containBottomView.layer.shadowRadius = 5
        containBottomView.layer.shadowOpacity = 0.5
        containBottomView.layer.shadowOffset = CGSizeZero
        containBottomView.layer.shadowColor = UIColor.blackColor().CGColor
        containBottomView.clipsToBounds = true
        
        
        
        originalHeaderTop = -1000
        originalActionsTop = -1000
        originalSocialTop = -1000
        
        // Card View Pager
        vwHeader.roundTopCorners(20)
        
        let cardActionInfoPlay: CardDetailButtonInfo = CardDetailButtonInfo(Id: 0, title: "Play", image: UIImage(named: "playOff"), selectedImage: UIImage(named: "playOn"), tintColor: UIColor(hexString: "#FE2851"))
        let cardActionInfoAsk: CardDetailButtonInfo = CardDetailButtonInfo(Id: 1, title: "Ask", image: UIImage(named: "requestOff"), selectedImage: UIImage(named: "requestOn"), tintColor: UIColor(hexString: "#FFCD00"))
        let cardActionInfoGive: CardDetailButtonInfo = CardDetailButtonInfo(Id: 2, title: "Give", image: UIImage(named: "offerOff"), selectedImage: UIImage(named: "offerOn"), tintColor: UIColor(hexString: "#BD10E0"))
        let cardActionInfoPrize: CardDetailButtonInfo = CardDetailButtonInfo(Id: 3, title: "Prize", image: UIImage(named: "prizeOff"), selectedImage: UIImage(named: "prizeOn"), tintColor: UIColor(hexString: "#0076FF"))
        
        cardActionPlay.bindItem(cardActionInfoPlay)
        cardActionPlay.delegate = self
        cardActionButtons.append(cardActionPlay)
        cardActionPlay.selectButton()
        
        cardActionAsk.bindItem(cardActionInfoAsk)
        cardActionAsk.delegate = self
        cardActionButtons.append(cardActionAsk)
        
        cardActionGive.bindItem(cardActionInfoGive)
        cardActionGive.delegate = self
        cardActionButtons.append(cardActionGive)
        
        cardActionPrize.bindItem(cardActionInfoPrize)
        cardActionPrize.delegate = self
        cardActionButtons.append(cardActionPrize)
        
        // Card View Header
//        imgCard.clipsToBounds = true
//        imgCard.layer.cornerRadius = 5
//        
//        print(content?.thumbnailURL)
//        if content?.thumbnailURL != nil {
//            backgroundImage.setImageWithUrlFromCardDetail(NSURL(string: content!.thumbnailURL!))
//            imgCard.setImageWithUrlWhiteBackground(NSURL(string: content!.thumbnailURL!) , rect: content!.maskRect)
//        }else{
//            backgroundImage.setImageWithUrlFromCardDetail(content?.imageURL)
//            imgCard.setImageWithUrlWhiteBackground(content?.imageURL , rect: content!.maskRect)
//        }
//        self.descriptionLabel.text = self.content?.description
//        self.deckTitle.text = self.content?.name
        
        let titleAttributes = [NSForegroundColorAttributeName: UIColor.blackColor(), NSFontAttributeName: Helper.proximaNova("bold", font: 22)]
        let descAttributes = [NSForegroundColorAttributeName: UIColor.blackColor(), NSFontAttributeName: Helper.proximaNova("Regular", font: 15)]
        
        let partTitle = NSMutableAttributedString(string: (self.content?.name)! + "\n\n", attributes: titleAttributes)
        let partDesc = NSMutableAttributedString(string: (self.content?.description)!, attributes: descAttributes)
        
        let combination = NSMutableAttributedString()
        
        combination.appendAttributedString(partTitle)
        combination.appendAttributedString(partDesc)
        
        descriptionLabel.attributedText = combination
        
        if let creatorImageURL = content?.creatorThumbnailURL {
            self.creatorImage.af_setImageWithURL(creatorImageURL)
        }
        self.creatorNameAndDate.text = content?.formattedCreatorAndDate
        self.creatorNameAndDate.layer.shadowRadius = 0.5
        self.creatorImage.layer.cornerRadius = 10
        
        let likes = content?.likes ?? 0
        likesLabel.text = String(likes)
        let imageName = (self.content!.liked_by_me == false) ? "grey_heart" : "heartFilled"
        self.likeButton.setImage(UIImage(named: imageName), forState: .Normal)
        
        
        let comments = content?.comments.count ?? 0
        var count = comments
        
        for comment in (content?.comments)! {
            count = count + comment.sub_comments.count
        }
        commentsLabel.text = String(count)
        
        onHeaderAction = { action in
            self.handleHeaderCellAction(action)
        }
        
        cardView.content = self.content
        
        cardBackgroundView.content = self.content
        // Enable Swipe Away to Close in the Edit
        vwDraggableView.delegate = self
        
//        deckBackground.hidden = self.content!.children.count == 0
//        if (self.content!.children.count == 0) {
//            imgCard.layer.borderColor = UIColor(red: 175.0 / 255, green: 175.0 / 255, blue: 175.0 / 255, alpha: 1.0).CGColor
//            imgCard.layer.borderWidth = 1.0
//        }
//        else {
//            deckTitle.text = self.content!.children[0].name
//            descriptionLabel.text = self.content!.children[0].description
//        }
        
        self.view.setNeedsLayout()
        
        panGestureRecognizer.addTarget(self, action: #selector(CardDraftDetailVC.handlePan(_:)))
        panGestureRecognizer.delegate = self
        vwDraggableView.addGestureRecognizer(panGestureRecognizer)
        
        tapGestureRecognizer.addTarget(self, action: #selector(CardDraftDetailVC.handleTap(_:)))
        tapGestureRecognizer.delegate = self
        vwDraggableView.addGestureRecognizer(tapGestureRecognizer)
        
        imgLine1.layer.shadowRadius = 2
        imgLine1.layer.shadowOffset = CGSizeZero
        imgLine1.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 0.5).CGColor
        
//        imgCard.layer.shadowRadius = 2
//        imgCard.layer.shadowOffset = CGSizeZero
//        imgCard.layer.shadowColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.5).CGColor
//        
//        backgroundImage.layer.shadowRadius = 2
//        backgroundImage.layer.shadowOffset = CGSizeZero
//        backgroundImage.layer.shadowColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.5).CGColor
        
    }
    
    func reconfigureView() {
        if (self.content?.ask_enabled == true) {
            // Ask Enabled
            // Show Ask, Give & Prize sections
            askWidthConstraint.constant = 82
            giveWidthConstraint.constant = 82
            prizeWidthConstraint.constant = 82
        } else {
            // Ask Disabled
            // Hide Ask, Give & Prize sections
            askWidthConstraint.constant = 0
            giveWidthConstraint.constant = 0
            prizeWidthConstraint.constant = 0
        }
        
        // Reload View Pages
        reloadViewPager()
        
        self.view.layoutIfNeeded()
    }
    
    func initialize() {
        var childrenSource = self.content!.children
        if editingStatus == CardEditingStatus.Edit {
            childrenSource = newChildrenOrdering
        }
        
        self.showLock = false
        if self.content!.isDeck {
            if !(self.content!.published) {
                if childrenSource[0].isDeck {
                    self.showLock = !childrenSource[0].published
                } else {
                    self.showLock = true
                }
            }
        }
    }
    
    func updateLock() {
        if self.showLock {
            let lock = UIImageView(frame: CGRectMake(10, 10, 20, 20))
            lock.image = UIImage(named: "lock")
            lock.tag = 333
            cardView.insertSubview(lock, aboveSubview: cardView)
        } else {
            for view in cardView.subviews{
                if view.tag == 333 {
                    view.removeFromSuperview()
                }
            }
        }
    }
    
    private func fetchAllDecks() {
        if cardId == nil {
            return
        }
        self.showProgressHUD()
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.GetCard(cardId: cardId!)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't download decks, please try again later")
                    self.hideProgressHUD()
                    return
                }
                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    self.hideProgressHUD()
                    let dic = JSON["card"] as! [String: AnyObject]
                    self.content = ContentNode(dictionary: dic)
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't parse decks, please try again later")
                    self.hideProgressHUD()
                }
            case let .Failure(error):
                print("can't fetch decks: %@", error)
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't download decks, please try again later")
                self.hideProgressHUD()
            }
        }
        
    }
    
    func handleHeaderCellAction (action: HeaderCellAction) {
        if action == .Loop {
            let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "LoopController") as! LoopController
            vc.content = self.content
            self.presentViewController(vc, animated: true, completion: nil)
        } else if action == .Comment {
            let vc = UIStoryboard.loadViewController(storyboardName: "Comment", viewControllerIdentifier: "CommentsViewController") as! CommentsViewController
            vc.content = content
            vc.prevDraftVC = self
            vc.prevMainVC = nil
            
            self.presentViewController(vc, animated: true, completion: nil)
        } else if action == .Like {
            let liked = content!.liked_by_me
            let provider = MoyaProvider<NetworkService>.createDefaultProvider()
            let target = liked ? NetworkService.UnlikeCard(cardId : content!.id) : NetworkService.LikeCard(cardId : content!.id)
            
            provider.request(target) { result in
                switch result {
                case let .Success(response):
                    if response.statusCode == 200 {
                        self.content!.liked_by_me = !liked
                        self.content!.likes += liked ? -1 : 1
                        
                        // Update View
                        let likeCount = self.content!.likes ?? 0
                        self.likesLabel.text = String(likeCount)
                        let imageName = (self.content!.liked_by_me == false) ? "grey_heart" : "heartFilled"
                        self.likeButton.setImage(UIImage(named: imageName), forState: .Normal)
                    }
                case let .Failure(error):
                    print(error)
                }
            }
        } else {
            
        }
    }
    
    func compareChildren(array1: [ContentNode], array2: [ContentNode]) -> Bool {
        for i in 0...array1.count - 1 {
            if array1[i].id != array2[i].id {
                return false
            }
        }
        return true
    }
    
    func showMoreOptions(){
        
        guard let popView = NSBundle.mainBundle().loadNibNamed("MoreView", owner: self, options: nil)?[0] as? MoreView else {
            return
        }
        
        popView.mainView.clipsToBounds = true
        popView.mainView.layer.borderColor = UIColor.colorFromRGB(redValue: 155/255.0, greenValue: 155/255.0, blueValue: 155/255.0, alpha: 0.4).CGColor
        popView.mainView.layer.borderWidth = 1
        
        popView.mainView.roundTopCorners(20)
        
        popView.constrantY.constant = -450
        popView.delegate = self
        
        if self.content?.ask_enabled == true {
            popView.moveHeight.constant = 0
            popView.totalHeight.constant = 400
        }else{
            popView.moveHeight.constant = 50
            popView.totalHeight.constant = 450
        }
        
        self.view.addSubview(popView)
        
        popView.hidden = false
        
        UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            
            popView.constrantY.constant = 0
            self.view.layoutIfNeeded()
            
        }) { (finished) in
            
            self.moreOptionsView = popView
        }
    }
    
    func hideMoreOptions() {
        if (moreOptionsView == nil) {
            return
        }
        
        self.moreOptionsView!.hideMoreView()
    }
    
    func onMoveTo(){
        
        if (self.content == nil) {
            self.hideMoreOptions()
            return
        }
        
        // Check if Ask Enabled
        
        if (self.content?.ask_enabled == true) {
            // No Need to Make an Ask again
            self.hideMoreOptions()
            return
        }
        
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("CardDraftMoveVC") as! CardDraftMoveVC
        
        vc.selectedDraft = self.content
        
        for itemContent in self.drafts {
            
            if itemContent.id != self.content?.id {
                vc.drafts.append(itemContent)
            }
            
        }
        
        vc.submittedCallback = {rContent ,nType in
            
            self.dismissViewControllerAnimated(false, completion: { 
                self.submittedCallback(rContent , nType)
            })
            
        }
        presentViewController(vc, animated: true, completion: nil)
        
        // Hide More Options in the background
        self.hideMoreOptions()
    }

    
    func onMakeAsk(){
        
        if (self.content == nil) {
            self.hideMoreOptions()
            return
        }
        
        // Check if Ask Enabled
        
        if (self.content?.ask_enabled == true) {
            // No Need to Make an Ask again
            self.hideMoreOptions()
            return
        }
 
        
        // Show "Make an Ask" View
        let vc = UIStoryboard.loadViewController(storyboardName: "CardDraft", viewControllerIdentifier: "CardDraftMakeAskVC") as! CardDraftMakeAskVC
        vc.delegate = self
        vc.content = self.content
        vc.isType = true
        self.presentViewController(vc, animated: true, completion: nil)
        
        // Hide More Options in the background
        self.hideMoreOptions()
    }
    
    func onPublishAction(){
        self.showProgressHUD()
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.PublishDecks(cardIds: [self.content!.id], publish: !self.content!.published)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't publish/unpublish deck, please try again later")
                    self.hideProgressHUD()
                    self.hideMoreOptions()
                }
                else if let JSON = try? response.mapJSON() as! [String: String] {
                    if JSON["result"] == "success" {
                        self.content!.published = !self.content!.published
                        //                        self.reloadData()
                        self.showSuccessHUD()
                        self.hideMoreOptions()
                        self.dismissViewControllerAnimated(true) {
                            self.delegate?.contentReloadData()
                        }
                    }
                    else {
                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't publish/unpublish deck, please try again later")
                        self.hideProgressHUD()
                        self.hideMoreOptions()
                    }
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't publish/unpublish deck, please try again later")
                    self.hideProgressHUD()
                    self.hideMoreOptions()
                }
            case .Failure(_):
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't publish/unpublish deck, please try again later")
                self.hideProgressHUD()
                self.hideMoreOptions()
            }
        }
    }
    
    func onEditAction() {
        self.hideMoreOptions()
        if (self.content == nil) {
            return
        }
        
        if self.content?.ask_enabled == true {
            // Show "Make an Ask" View
            let vc = UIStoryboard.loadViewController(storyboardName: "CardDraft", viewControllerIdentifier: "CardDraftMakeAskVC") as! CardDraftMakeAskVC
            vc.delegate = self
            vc.content = self.content
            vc.isType = false
            self.presentViewController(vc, animated: true, completion: nil)
            
            // Hide More Options in the background
            self.hideMoreOptions()
            
            return
        }
        
        editingStatus = CardEditingStatus.Edit
        
        newChildrenOrdering = []
        for child in self.content!.children {
            newChildrenOrdering.append(child)
        }
        
        if self.content!.type == .Video {
            self.editVideoDraft()
        }
        else if self.content!.type == .Image {
            self.editImageDraft()
        }
        else {
            // Other Card Type, e.g. Deck
            self.hideMoreOptions()
        }
    }
    
    func editImageDraft() {
        guard let content = content else { return }
        
        self.showProgressHUD()
        
        let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "ImageCardCreationController") as! ImageCardCreationController
        
        vc.cardName             = content.name
        vc.descStr              = content.description
        vc.cardId               = content.id
        
        vc.prevViewController   = self
        
        let imageUrl            = NSURL(string: content.content!)
        let request             = NSMutableURLRequest(URL: imageUrl!)
        let imageCache          = GuideHeroImageCache.sharedInstance.getCache()
        let cachedImage         = imageCache.imageForRequest(request)
        
        if cachedImage != nil {
            vc.image = cachedImage
            self.hideProgressHUD()
            self.presentViewController(vc, animated: true, completion: nil)
        } else {
            ImageDownloader.defaultInstance.downloadImage(URLRequest: request) { response in
                if response.result.isSuccess {
                    let image = response.result.value
                    imageCache.addImage(image!, forRequest: request)
                    
                    self.hideProgressHUD()
                    self.presentViewController(vc, animated: true, completion: nil)
                }
                else {
                    print(response.result.description)
                    self.hideProgressHUD()
                }
            }
        }
    }
    
    func editVideoDraft() {
        guard let content = content else { return }
        self.showProgressHUD()
        
        let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "VideoCardCreationViewController") as! VideoCardCreationViewController
        
        vc.videoUrl             = content.videoURL
        vc.cardName             = content.name
        vc.descStr              = content.description
        vc.cardId               = content.id
        
        vc.prevViewController   = self
        
        self.hideProgressHUD()
        
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    
    // MARK: - Actions
    
    @IBAction func didTapOnClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) {
            self.delegate?.contentReloadData()
        }
    }
    
    @IBAction func didTapOnMore(sender: AnyObject) {
        showMoreOptions()
    }
    
    @IBAction func clickedDelete(sender: AnyObject) {
        //        if let list = tableView.indexPathsForSelectedRows {
        //            let alertController2 = UIAlertController(title: "Are you sure you want to continue with the deletion?", message: nil, preferredStyle: .Alert)
        //            let cancelAction2 = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
        //                // ...
        //            }
        //            let okayAction2 = UIAlertAction(title: "Ok", style: .Default) { (action) in
        //                // API call
        //
        //                var cardIds = [String]()
        //
        //                var deleteDeck = false
        //                if list.count == self.content.children.count {
        //                    deleteDeck = true
        //                    cardIds.append(self.content.id)
        //                }
        //
        //                for index in list {
        //                    cardIds.append(self.content.children[index.row].id)
        //                }
        //
        //                self.showProgressHUD()
        //                let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        //                provider.request(.DeleteCards(cardIds: cardIds)) { result in
        //                    switch result {
        //                    case let .Success(response):
        //                        if response.statusCode != 200 {
        //                            self.showSimpleAlert(withTitle: "Error", withMessage: "Can't delete content, please try again later")
        //                            self.hideProgressHUD()
        //                        }
        //                        else if let JSON = try? response.mapJSON() as! [String: String] {
        //                            if JSON["result"] == "success" {
        //                                self.showSuccessHUD()
        //                                if (deleteDeck) {
        //                                    self.navigationController?.popViewControllerAnimated(true)
        //                                } else {
        //                                    var newChildren:[ContentNode] = []
        //
        //                                    for child in self.content.children {
        //                                        if !cardIds.contains(child.id) {
        //                                            newChildren.append(child)
        //                                        }
        //                                    }
        //                                    self.content.children = newChildren
        //                                    self.reloadData()
        //                                    self.exitSelect()
        //                                }
        //                            }
        //                            else {
        //                                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't delete content, please try again later")
        //                                self.hideProgressHUD()
        //                            }
        //                        }
        //                        else {
        //                            self.showSimpleAlert(withTitle: "Error", withMessage: "Can't delete content, please try again later")
        //                            self.hideProgressHUD()
        //                        }
        //                    case .Failure(_):
        //                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't delete content, please try again later")
        //                        self.hideProgressHUD()
        //                    }
        //                }
        //            }
        //            alertController2.addAction(cancelAction2)
        //            alertController2.addAction(okayAction2)
        //            self.presentViewController(alertController2, animated: true){}
        //        }
    }
    
    @IBAction func clickedMove(sender: UIBarButtonItem) {
        
    }
    
    func clickedCopy() {
        self.hideMoreOptions()
        var cardIds = [String]()
        cardIds.append((self.content?.id)!)
        self.showProgressHUD()
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.CopyCards(cardIds: cardIds)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't copy content, please try again later")
                    self.hideProgressHUD()
                }
                else if let JSON = try? response.mapJSON() as! [String: String] {
                    if JSON["result"] == "success" {
                        self.hideProgressHUD()
                        self.dismissViewControllerAnimated(false, completion: {
                            self.submittedCallback(self.drafts , 1)
                        })
                    }
                    else {
                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't copy content, please try again later")
                        self.hideProgressHUD()
                    }
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't copy content, please try again later")
                    self.hideProgressHUD()
                }
            case .Failure(_):
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't copy content, please try again later")
                self.hideProgressHUD()
            }
        }
        
    }
    
    @IBAction func didTapOnLike(sender: AnyObject) {
        onHeaderAction(.Like)
    }
    @IBAction func didTapOnComment(sender: AnyObject) {
        onHeaderAction(.Comment)
    }
    
    // MARK: - CardSubPageScrollDelegate Methods
    
    override func subPageScrollViewDidScroll(offset: CGFloat, translation: CGPoint, animated: Bool) {
        super.subPageScrollViewDidScroll(offset, translation: translation, animated: animated)
        
        if (movingFactor != 0 ) {
            return
        }
        
        if (originalHeaderTop == -1000) {
            originalHeaderTop = constraintHeaderTop.constant
        }
        
        if (originalActionsTop == -1000) {
            originalActionsTop = constraintActionsTop.constant
        }
        
        if (originalSocialTop == -1000) {
            originalSocialTop = constraintSocialTop.constant
        }
        
        constraintHeaderTop.constant = originalHeaderTop! - offset
        constraintActionsTop.constant = originalActionsTop! - offset
        constraintSocialTop.constant = originalSocialTop! - offset
        
        if (!animated) {
            self.view.layoutIfNeeded()
        } else {
            UIView.animateWithDuration(0.25, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    override func subPageScrollViewDidBounce(offset: CGFloat) {
        super.subPageScrollViewDidBounce(offset)
        
        constraintContentBottom.constant = 30 + offset
        self.view.layoutIfNeeded()
    }
    
    func onResponseContent(resContent : [ContentNode]! , nType : Int){
        self.dismissViewControllerAnimated(false, completion: {
            self.submittedCallback(resContent , nType)
        })
    }
    
    
}

// MARK: - CardViewPagerDataSource Methods

extension CardDraftDetailVC: CardViewPagerDataSource {
    override func viewControllers(for viewPagerController: CardViewPagerController) -> [CardSubPageBaseVC] {
        var subPages = [CardSubPageBaseVC]()
        
        cardDetailPlayVC = UIStoryboard.loadViewController(storyboardName: "CardDraft", viewControllerIdentifier: "CardDraftDetailPlayVC") as? CardDraftDetailPlayVC
        cardDetailPlayVC?.delegate = self
        cardDetailPlayVC?.content = self.content
        cardDetailPlayVC?.cardDetailPlayDelegate = self

        cardDetailPlayVC?.drafts = self.drafts
        
        subPages.append(cardDetailPlayVC!)
        
        if (content?.ask_enabled == true) {
            cardDetailAskVC = UIStoryboard.loadViewController(storyboardName: "CardDetail", viewControllerIdentifier: "CardDetailAskVC") as? CardDetailAskVC
            cardDetailAskVC?.delegate = self
            cardDetailAskVC?.content = self.content
            subPages.append(cardDetailAskVC!)
        }
        
        if (content?.ask_enabled == true) {
            cardDetailGiveVC = UIStoryboard.loadViewController(storyboardName: "CardDetail", viewControllerIdentifier: "CardDetailGiveVC") as? CardDetailGiveVC
            cardDetailGiveVC?.delegate = self
            cardDetailGiveVC?.content = self.content
            subPages.append(cardDetailGiveVC!)
        }
        
        if (content?.ask_enabled == true) {
            cardDetailPrizeVC = UIStoryboard.loadViewController(storyboardName: "CardDetail", viewControllerIdentifier: "CardDetailPrizeVC") as? CardDetailPrizeVC
            cardDetailPrizeVC?.delegate = self
            cardDetailPrizeVC?.content = self.content
            subPages.append(cardDetailPrizeVC!)
        }
        
        return subPages
    }
}

// MARK: - CardViewPagerProgressDelegate Methods

extension CardDraftDetailVC: CardViewPagerProgressDelegate {
    func updateIndicator(for viewController: CardViewPagerController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool, borderCrossed: Bool, direction: SwipeDirection) {
        //        let changeFlag = borderCrossed ? "---------" : ""
        //        print("Border crossed: " + changeFlag)
        
        if (toIndex != -1 && toIndex != cardActionButtons.count) {
            let cardActionButtonFrom = cardActionButtons[fromIndex]
            let cardActionButtonTo = cardActionButtons[max(toIndex, -1)]
            if (fromIndex < toIndex) {
                // Swipe Direction = Left, Transition Direction = Right
                cardActionButtonFrom.transitToRight(withProgressPercentage: progressPercentage)
                cardActionButtonTo.transitFromRight(withProgressPercentage: progressPercentage)
            } else {
                // Swipe Direction = Right, Transition Direction = Left
                cardActionButtonFrom.transitToLeft(withProgressPercentage: progressPercentage)
                cardActionButtonTo.transitFromLeft(withProgressPercentage: progressPercentage)
            }
            
            if (borderCrossed) {
                if (direction == .left) {
                    let cardActionButtonPast = cardActionButtons[max(fromIndex-1, 0)]
                    cardActionButtonPast.deselectButton()
                } else if (direction == .right) {
                    let pastIndex = min(fromIndex+1, cardActionButtons.count-1)
                    let cardActionButtonPast = cardActionButtons[pastIndex]
                    cardActionButtonPast.deselectButton()
                }
            }
        }
        
        if (toIndex < 0 || toIndex >= cardActionButtons.count) {
            let newButton = cardActionButtons[currentIndex]
            newButton.highlightButton()
        } else {
            let oldButton = cardActionButtons[currentIndex != fromIndex ? fromIndex : toIndex]
            let newButton = cardActionButtons[currentIndex]
            oldButton.dehighlightButton()
            newButton.highlightButton()
        }
    }
    
    func updateIndicator(for viewController: CardViewPagerController, fromIndex: Int, toIndex: Int) {
        
    }
}

// MARK: - CardDetailButtonDelegate Methods

extension CardDraftDetailVC: CardDetailButtonDelegate {
    func didTapOnCardActionButton(buttonID: Int) {
        guard buttonID != currentIndex else { return }
        
        let oldButton = cardActionButtons[currentIndex]
        let newButton = cardActionButtons[buttonID]
        oldButton.deselectButton()
        newButton.selectButton()
        
        moveToViewController(at: buttonID)
        
    }
}

// MARK: - UIGestureRecognizerDelegate Methods

extension CardDraftDetailVC: UIGestureRecognizerDelegate {
    func handleTap(tapGesture: UITapGestureRecognizer) {
        
        if (content!.parentContent != nil) {
            let fullScreenViewController = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "FullScreenViewController") as! FullScreenViewController
            fullScreenViewController.deckIndex = contentIndex
            fullScreenViewController.cardIndex = 0
            fullScreenViewController.content = content!.parentContent
            fullScreenViewController.modalPresentationStyle = .OverFullScreen
            self.presentViewController(fullScreenViewController, animated: true, completion: nil)
            
        }
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if (gestureRecognizer == tapGestureRecognizer) {
            let tapLocation = tapGestureRecognizer.locationInView(vwHeader)
            if (vwHeader.pointInside(tapLocation, withEvent: nil)) {
                
                if self.content?.children.count > 0 && self.content?.ask_enabled == true{
                    
                    if self.content?.children[0].type == .Deck {
                        let vc = UIStoryboard.loadViewController(storyboardName: "CardDraft", viewControllerIdentifier: "CardDraftDetailVC") as! CardDraftDetailVC
                        
                        vc.delegate = self.delegate
                        
                        vc.content = self.content?.children[0]
                        
                        vc.drafts = self.drafts
                        
                        vc.submittedCallback = {rContent , nType in
                            self.dismissViewControllerAnimated(false, completion: {
                                self.submittedCallback(rContent , nType)
                            })
                        }
                        
                        presentViewController(vc, animated: true, completion: nil)
                    }
                    
                }
                return true
            }
            
            return false
        }
        
        let velocity = panGestureRecognizer.velocityInView(self.vwDraggableView)
        
        let pannableToLeft = (currentPageIndex == 0 && fabs(velocity.x) >= fabs(velocity.y) && velocity.x > 0)
        let pannableToRight = (currentPageIndex == (viewControllers.count - 1) && fabs(velocity.x) >= fabs(velocity.y) && velocity.x < 0)
        
        let currentSubPage = viewControllers[currentPageIndex]
        let pannableFromTop = currentSubPage.contentOffsetY <= 0 && fabs(velocity.x) <= fabs(velocity.y) && velocity.y > 0
        let pannableFromBottom = currentSubPage.touchedBottom && fabs(velocity.x) <= fabs(velocity.y) && velocity.y < 0
        
        let touchLocation = panGestureRecognizer.locationInView(vwHeader)
        let pannableInsideHeader = vwHeader.pointInside(touchLocation, withEvent: nil) && fabs(velocity.x) >= fabs(velocity.y)
        
        if (pannableToLeft || pannableToRight || pannableFromTop || pannableFromBottom || pannableInsideHeader) {
            return true
        }
        
        return false
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if otherGestureRecognizer is UIPanGestureRecognizer || otherGestureRecognizer is UITapGestureRecognizer {
            return true
        }
        
        return false
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func handlePan(panGesture: UIPanGestureRecognizer){
        
        vwDraggableView.handlePanGesture(panGestureRecognizer)
        
    }
}

// MARK: - PannableCardViewDelegate Methods

extension CardDraftDetailVC: PannableCardViewDelegate {
    func cardSwipedAway() {
        self.dismissViewControllerAnimated(true) {
            self.delegate?.contentReloadData()
        }
    }
    
    func cardWasDragged(dragPercentage: CGFloat) {
        self.vwMain.backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.7 * (1 - Float(dragPercentage)))
    }
    
    func cardStartedDismissal(duration: NSTimeInterval, dragPercentage: Float) {
        var percentage = dragPercentage
        UIView.animateWithDuration(duration) {
            percentage = 1
            self.vwMain.backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.7 * (1 - percentage))
        }
    }
    
    func resetCardPosition(){
        
    }
    
}

// MARK: - MoreDelegate Methods

extension CardDraftDetailVC: MoreDelegate {
    func onSelectButton(nIndex : Int){
        switch nIndex {
        case 0://Publish
            self.onPublishAction()
            break
        case 1://Make an Ask
            self.onMakeAsk()
            break
        case 2://Move...
            self.onMoveTo()
            break
        case 3://Reorder
            break
        case 4://Save Copy
            self.clickedCopy()
            break
        case 5://Edit
            self.onEditAction()
            break
        case 6://Delete
            break
        case 7://Play Fullscreen
            break
        case 8://Show tags
            break
        default:
            break
        }
    }
}


// MARK: - CardDraftMakeAskVCDelegate

extension CardDraftDetailVC: CardDraftMakeAskVCDelegate {
    func cardDraftContentDidEnableAsk(content: ContentNode) {
        // Update the content with new content, possible ask enabled.
        self.content = content
        
        // Notify the Draft view of the Content Update.
        self.delegate?.cardDraftDetailContentWasUpdated(content)
    }
}

