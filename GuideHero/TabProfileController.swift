//
//  TabProfileController.swift
//  GuideHero
//
//  Created by Yohei Oka on 8/28/16.
//  Copyright © 2016 Twilio. All rights reserved.
//

import UIKit
import Moya


class TabProfileController: BasePostLoginController,
 UITableViewDelegate, UITableViewDataSource , UIGestureRecognizerDelegate , ProfileFollowersDelegate,ProfileFollowingsDelegate{
    
    @IBOutlet var profilePictureView: UIImageView!
    @IBOutlet var profileAvataView : UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var avatarName : UILabel!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblBio: UILabel!
//    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var followWidth : NSLayoutConstraint!
    @IBOutlet weak var followHeight : NSLayoutConstraint!
    
    @IBOutlet weak var lblFollowers: UILabel!
    @IBOutlet weak var lblFollowing: UILabel!
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var lblShareCount: UILabel!
    @IBOutlet weak var lblPoint: UILabel!
    
    @IBOutlet weak var scrollViewBottomConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var followViewHeight : NSLayoutConstraint!
    @IBOutlet weak var publishViewHeight : NSLayoutConstraint!
    @IBOutlet weak var optionViewHeight : NSLayoutConstraint!
    
    @IBOutlet weak var containView : UIView!
    
    @IBOutlet weak var scrollContainHeight : NSLayoutConstraint!
    
    @IBOutlet weak var wholeView : UIView!
    
    @IBOutlet weak var imgBackground : UIImageView!
    
    @IBOutlet weak var publishView : UIView!
    @IBOutlet weak var followView : UIView!
    
    var isShow : Bool = false
    
    var userInfo: UserNode!
    var publishedDecks = [ContentNode]()
    var responseJSON :[String: AnyObject] = [:]
    
    @IBOutlet var nameTextView: UILabel!
    
    @IBOutlet weak var btnSearch : UIButton!
    @IBOutlet weak var btnClose : UIButton!
    
    @IBOutlet weak var btnMore : UIButton!
    
    @IBOutlet weak var bottomView : UIView!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var btnSave : UIButton!
    
    @IBOutlet weak var btnPublish : UIButton!
    @IBOutlet weak var btnDraft : UIButton!
    @IBOutlet weak var btnAsks : UIButton!
    @IBOutlet weak var btnGives : UIButton!
    @IBOutlet weak var btnPrizes : UIButton!
    @IBOutlet weak var btnLikes : UIButton!
    @IBOutlet weak var btnShare : UIButton!
    
    @IBOutlet weak var btnFacebook : UIButton!
    @IBOutlet weak var btnHarvard : UIButton!
    @IBOutlet weak var btnLikeIcon : UIButton!
    @IBOutlet weak var btnShareIcon : UIButton!
    @IBOutlet weak var lblHarvard : UILabel!
    
    
    
    var txtUserName : UITextField!
    var txtFirstName : UITextField!
    var txtLastName : UITextField!
    var txtBio : UITextField!
    
    var backup_firstname : String!
    var backup_lastname : String!
    
    var pan = UIPanGestureRecognizer()
    var panelOpened = true
    
    var startPos = CGPointZero
    var startCenter = CGPointZero
    
    var isRotating = false
    
    var nameGesture = UITapGestureRecognizer()
    
    var userNameGesture = UITapGestureRecognizer()
    
    var bioGesture = UITapGestureRecognizer()
    
    var followingGesture = UITapGestureRecognizer()
    
    var followersGesture = UITapGestureRecognizer()
    
    var following_list = [UserNode]()
    
    var followers_list = [UserNode]()
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        
        nameGesture.addTarget(self, action: #selector(TabProfileController.nameTap(_:)))
        nameGesture.delegate = self
        lblFullName.addGestureRecognizer(nameGesture)
        
        userNameGesture.addTarget(self, action: #selector(TabProfileController.userNameTap(_:)))
        userNameGesture.delegate = self
        avatarName.addGestureRecognizer(userNameGesture)
        
        bioGesture.addTarget(self, action: #selector(TabProfileController.bioTap(_:)))
        bioGesture.delegate = self
        lblBio.addGestureRecognizer(bioGesture)
        
        followingGesture.addTarget(self, action: #selector(TabProfileController.followingTap(_:)))
        followingGesture.delegate = self
        lblFollowing.addGestureRecognizer(followingGesture)
        
        followersGesture.addTarget(self, action: #selector(TabProfileController.followersTap(_:)))
        followersGesture.delegate = self
        lblFollowers.addGestureRecognizer(followersGesture)

        nameGesture.enabled = false
        userNameGesture.enabled = false
        bioGesture.enabled = false
        
        UIApplication.sharedApplication().statusBarHidden = true
        
        btnSave.layer.shadowColor = UIColor.blackColor().CGColor
        btnSave.layer.shadowOffset = CGSizeMake(0, 1)
        btnSave.layer.shadowOpacity = 0.5
        btnSave.layer.shadowRadius = 4
        
        btnCancel.layer.shadowColor = UIColor.blackColor().CGColor
        btnCancel.layer.shadowOffset = CGSizeMake(0, 1)
        btnCancel.layer.shadowOpacity = 0.5
        btnCancel.layer.shadowRadius = 4
        
        if self.isShow == false {
            self.btnMore.enabled = true
        }else{
            self.btnMore.enabled = false
        }
        bottomView.hidden = true
        onDisableEditButtons()
        
//        self.imgBackground.backgroundColor = UIColor.blackColor()
        self.imgBackground.alpha = 0.5
        self.imgBackground.clipsToBounds = false
        self.imgBackground.layer.cornerRadius = 0
        self.imgBackground.layer.shadowRadius = 0
        
        if isShow == true {
            onSetupPanel()
        }
        
        
        let cellNib = UINib(nibName: "CardDetailTableViewCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: "CardDetailTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.scrollEnabled = false
        tableView.hidden = true
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(TabProfileController.onPoints))
        singleTap.numberOfTapsRequired = 1
        self.lblPoint.userInteractionEnabled = true
        self.lblPoint.addGestureRecognizer(singleTap)
        
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewDidAppear(animated: Bool) {
        if (userInfo != nil) {
            if self.userInfo.user_id == (Session.sharedSession.user?.user_id)! {
           
                self.followViewHeight.constant = 0
                self.publishViewHeight.constant = 48
                self.optionViewHeight.constant = 158
                
                self.publishView.hidden = false
                self.followView.hidden = true
                
            }else{
                self.followViewHeight.constant = 55
                self.publishViewHeight.constant = 0
                optionViewHeight.constant = 110
                self.publishView.hidden = true
                self.followView.hidden = false
            }
        }else{
            
            
            self.publishView.hidden = false
            self.followViewHeight.constant = 0
            self.publishViewHeight.constant = 48
            self.optionViewHeight.constant = 158
            
        }
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        self.containView.setNeedsLayout()
        self.containView.layoutIfNeeded()
        
        UIApplication.sharedApplication().statusBarHidden = true
    }
    
    override func viewWillAppear(animated: Bool) {
        
        UIApplication.sharedApplication().statusBarHidden = true
        
        if self.isShow {
            btnClose.hidden = false
            btnSearch.hidden = true
        }else{
            btnClose.hidden = true
            btnSearch.hidden = false
        }
        reloadData()
        // Hide nav bar when from EditDeckViewController publishing/unpublishing deck
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        
    }
    
    func reloadData() {
        
        var user_id : String!
        
        if (userInfo != nil) {
            user_id = self.userInfo.user_id
        }else{
            user_id = (Session.sharedSession.user?.user_id)!
        }
        self.showProgressHUD()
        
        
                
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        
        provider.request(.GetProfileInfo(userId: user_id)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.hideProgressHUD()
                    return
                }
                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    self.responseJSON = JSON
                    self.userInfo = UserNode.init(dictionary:JSON["user_info"] as! [String: AnyObject])
                    
//                    let userState = dictionary:JSON["stats"]
                    print(self.userInfo)
                    print(JSON)
                    if self.userInfo != nil {
                        self.profilePictureView.af_setImageWithURL(NSURL(string: self.userInfo.thumbnail_url)!)
                        
                        self.profilePictureView.layer.cornerRadius = (self.profilePictureView.frame.size.height - 4) / 2
                        self.profilePictureView.layer.borderColor = UIColor.whiteColor().CGColor
                        self.profilePictureView.layer.borderWidth = 3
                        self.profilePictureView.layer.masksToBounds = true
                        
                        self.profileAvataView.af_setImageWithURL(NSURL(string: self.userInfo.thumbnail_url)!)
                        self.profileAvataView.layer.cornerRadius = (self.profileAvataView.frame.size.height - 2) / 2
//                        self.profileAvataView.layer.borderColor = UIColor.whiteColor().CGColor
//                        self.profileAvataView.layer.borderWidth = 1
                        self.profileAvataView.layer.masksToBounds = true
                        
                        self.avatarName.text = self.userInfo.username
                        self.lblFullName.text = String(format:"%@ %@", self.userInfo.first_name, self.userInfo.last_name)
                        self.lblBio.text = self.userInfo.bio
//                        self.lblDescription.text = self.userInfo.source
                        
                        self.backup_firstname = self.userInfo.first_name
                        self.backup_lastname = self.userInfo.last_name
                    }
                    
                    if self.userInfo.user_id == (Session.sharedSession.user?.user_id)! {
                       
                        self.followViewHeight.constant = 0
                        self.publishViewHeight.constant = 48
                        self.optionViewHeight.constant = 158
                        self.publishView.hidden = false
                        self.followView.hidden = true
                    }else{
                        self.followViewHeight.constant = 55
                        self.publishViewHeight.constant = 0
                        self.optionViewHeight.constant = 110
                        self.publishView.hidden = true
                        self.followView.hidden = false
                    }
                    
                    self.followers_list = []
                    self.following_list = []
                    
                    if JSON["followers"] != nil {
                        for followersDict in (JSON["followers"] as? [[String : AnyObject]])! {
                            self.followers_list.append(UserNode(dictionary: followersDict))
                        }
                    }
                    
                    if JSON["followings"] != nil {
                        for followingDict in (JSON["followings"] as? [[String : AnyObject]])! {
                            self.following_list.append(UserNode(dictionary: followingDict))
                        }
                    }
                    
                    let temp = JSON["stats"] as![String: AnyObject]
                    self.userInfo.followers_count = temp["total_followers"] as! Int
                    
                    self.lblFollowers.text = String(format:"%d Followers", self.userInfo.followers_count)
                    
                    self.userInfo.following_count = temp["total_followings"] as! Int
                    
                    self.lblFollowing.text = String(format: "%d Following", self.userInfo.following_count)
                    
                    let likeCount = temp["total_likes"] as! Int
                    self.lblLikeCount.text = String(format: "%d", likeCount)
                    let shareCount = temp["total_shared"] as! Int
                    self.lblShareCount.text = String(format: "%d", shareCount)
//                    let totalPoint = temp["total_silver_points"] as! Int
                    
                    let totalPoint = temp["total_points"]!["gold"] as! Int
                    self.userInfo.current_point = temp["total_points"]!["silver"] as! Int
                    if self.userInfo.isRelation {
                        self.btnFollow.setImage(UIImage(named: "unfollowing"), forState: .Normal)
                        self.followWidth.constant = 145
                        self.followHeight.constant = 40
                        
                    }else{
                        
                        self.btnFollow.setImage(UIImage(named: "follow"), forState: .Normal)
                        self.followWidth.constant = 120
                        self.followHeight.constant = 40
                    }
                    
                    let formatter = NSNumberFormatter()
                    formatter.numberStyle = .DecimalStyle
                    formatter.maximumFractionDigits = 0;
                    self.lblPoint.text = formatter.stringFromNumber(totalPoint)
                    
                    if self.userInfo.user_id == Session.sharedSession.user?.user_id {
                        NSUserDefaults.standardUserDefaults().setInteger(self.userInfo.current_point, forKey: "total_points")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                    
                                        
                    self.publishedDecks = []
                    for deckDict in (JSON["cards"] as? [[String : AnyObject]])! {
                        self.publishedDecks.append(ContentNode(dictionary: deckDict))
                    }
                    
                    self.scrollContainHeight.constant = 0
                    
//                    self.tableView.reloadData()
                    
                    
                    
                    self.hideProgressHUD()
                }
                else {
                    self.hideProgressHUD()
                }
            case .Failure(_):
                self.hideProgressHUD()
            }
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func onSearch(sender: AnyObject) {
        
    }
    
    func onPoints(gestureRecognizer: UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("PointViewController") as! PointViewController
        vc.userInfo = self.userInfo
        
        vc.modalPresentationStyle = .OverFullScreen
        
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func onFollowAction(){
        
        self.showProgressHUD()
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.FollowAction(user_id: self.userInfo.user_id)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Please try again later")
                    self.hideProgressHUD()
                    return
                }
                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    
                    self.userInfo.followers_count = self.userInfo.followers_count + 1
                    self.lblFollowers.text = String(format:"%d Followers", self.userInfo.followers_count)
                    
                    self.followers_list.append(Session.sharedSession.user!)
                    self.hideProgressHUD()
                    
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Please try again later")
                    self.hideProgressHUD()
                }
            case let .Failure(error):
                
                self.showSimpleAlert(withTitle: "Error", withMessage: "Please try again later")
                self.hideProgressHUD()
            }
        }
        
    }
    
    func onUnFollowAction(){
        self.showProgressHUD()
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.UnFollowAction(user_id: self.userInfo.user_id)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Please try again later")
                    self.hideProgressHUD()
                    return
                }
                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    
                    self.userInfo.followers_count = self.userInfo.followers_count - 1
                    self.lblFollowers.text = String(format:"%d Followers", self.userInfo.followers_count)
                    
                    var nIndex : Int = 0
                    
                    for userDict in self.followers_list {
                        if userDict.user_id == Session.sharedSession.user?.user_id {
                            
                            self.followers_list.removeAtIndex(nIndex)
                            break
                        }
                        
                        nIndex = nIndex + 1
                    }
                    
                    self.hideProgressHUD()
                    
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Please try again later")
                    self.hideProgressHUD()
                }
            case let .Failure(error):
                
                self.showSimpleAlert(withTitle: "Error", withMessage: "Please try again later")
                self.hideProgressHUD()
            }
        }
    }
    
    @IBAction func onFollow(sender: AnyObject) {
        
        if self.userInfo.isRelation {
            
            self.btnFollow.setImage(UIImage(named: "follow"), forState: .Normal)
            
            self.followWidth.constant = 120
            self.followHeight.constant = 40
            
            onUnFollowAction()
            
        }else{
            
            self.btnFollow.setImage(UIImage(named: "unfollowing"), forState: .Normal)
            self.followWidth.constant = 145
            self.followHeight.constant = 40
            onFollowAction()
        }
        
        self.userInfo.isRelation = !self.userInfo.isRelation
        
    }
    
    @IBAction func onPublished(sender: AnyObject) {
        let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "ProfilePublishedVC") as! ProfilePublishedVC
        
        vc.modalPresentationStyle = .OverFullScreen
        
        vc.userInfo = self.userInfo
        
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    @IBAction func onDrafts(sender: AnyObject) {
//        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("DraftsViewController") as! DraftsViewController
        let vc = UIStoryboard.loadViewController(storyboardName: "CardDraft", viewControllerIdentifier: "CardDraftVC") as! CardDraftVC
        
        vc.modalPresentationStyle = .OverFullScreen
        
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    @IBAction func onPrizes(sender : AnyObject){
        let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "ProfilePrizesVC") as! ProfilePrizesVC
        
        vc.modalPresentationStyle = .OverFullScreen
        
        vc.userInfo = self.userInfo
        
        self.presentViewController(vc, animated: true, completion: nil)
    }
    @IBAction func onLiked(sender: AnyObject) {
        let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "ProfileLikesVC") as! ProfileLikesVC
        
        vc.modalPresentationStyle = .OverFullScreen
        
        vc.userInfo = self.userInfo
        
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    @IBAction func onAsks(sender: AnyObject) {
        
        let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "ProfileAsksVC") as! ProfileAsksVC
        
        vc.modalPresentationStyle = .OverFullScreen
        
//        var asksList = [ContentNode]()
//        
//        for askData in self.publishedDecks {
//            print(askData)
//            asksList.append(askData)
//        }
//        
//        vc.listData = asksList
        
        vc.userInfo = self.userInfo
        
        self.presentViewController(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func onGives(sender: AnyObject) {
        
        let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "ProfileGivesVC") as! ProfileGivesVC
        
        vc.modalPresentationStyle = .OverFullScreen
        
//        var givesList = [ContentNode]()
//        
//        for givesData in self.publishedDecks {
//            print(givesData)
//            givesList.append(givesData)
//        }
//        
//        vc.listData = givesList
        
        vc.userInfo = self.userInfo
        
        self.presentViewController(vc, animated: true, completion: nil)
        
    }
    
    // MARK: - UITableView
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(self.publishedDecks.count)
        return self.publishedDecks.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCellWithIdentifier("FeedTableViewCell", forIndexPath: indexPath) as! FeedTableViewCell
//        
//        cell.content = publishedDecks[indexPath.row]
//        
//        cell.cardView.tag = indexPath.row
//        cell.btnLike.tag = indexPath.row
//        cell.imgLock.hidden = true
//        
////        let singleTap = UITapGestureRecognizer(target: self, action: #selector(TrendingViewController.showFullScreen))
////        cell.cardView.addGestureRecognizer(singleTap)
//        
//        cell.setNeedsLayout()
//        cell.layoutIfNeeded()
        let cell = tableView.dequeueReusableCellWithIdentifier("CardDetailTableViewCell", forIndexPath: indexPath) as! CardDetailTableViewCell
        cell.content = publishedDecks[indexPath.row]
        
        cell.imgSeparate.hidden = false
        
        cell.separateView.hidden = true
        cell.imgSeparate.layer.shadowRadius = 2
        
        cell.imgSeparate.layer.shadowOffset = CGSizeZero
        cell.imgSeparate.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 0.5).CGColor
        
        
        cell.btnProfileName.tag = indexPath.row
        cell.btnProfilePhoto.tag = indexPath.row
        cell.btnProfileName.addTarget(self, action: #selector(TabProfileController.onGotoProfilePage(_:)), forControlEvents: .TouchUpInside)
        cell.btnProfilePhoto.addTarget(self, action: #selector(TabProfileController.onGotoProfilePage(_:)), forControlEvents: .TouchUpInside)
        
        
        
        cell.cardView.tag = indexPath.row
        cell.tag = indexPath.row
        cell.imgLock.hidden = true
        
        
//        if self.isType == 1 || self.isType == 0 {
//            let childTap = UITapGestureRecognizer(target: self, action: #selector(CardDetailPlayVC.showChildScreen))
//            cell.addGestureRecognizer(childTap)
//        }else{
//            let cellTap = UITapGestureRecognizer(target: self, action: #selector(CardDetailPlayVC.openCellDetail))
//            cell.addGestureRecognizer(cellTap)
//        }
//        
//        cell.cardView.tag = indexPath.row
//        let singleTap = UITapGestureRecognizer(target: self, action: #selector(CardDetailPlayVC.showFullScreen))
//        cell.cardView.addGestureRecognizer(singleTap)
        
        cell.layer.shadowRadius = 1
        cell.layer.shadowOffset = CGSizeZero
        cell.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
        
        cell.needsRoundBottom = true
        
        cell.setNeedsLayout()
        cell.layoutIfNeeded()

        return cell
    }
    func onGotoProfilePage(sender : UIButton){
        let profileVC = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "TabProfileController") as! TabProfileController
        
        let cData : ContentNode = publishedDecks[sender.tag]
        profileVC.userInfo = cData.creatorInfo
        
        profileVC.modalPresentationStyle = .OverFullScreen
        profileVC.isShow = true
        self.presentViewController(profileVC, animated: true, completion: nil)
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
//        let cell = tableView.cellForRowAtIndexPath(indexPath) as! FeedTableViewCell
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("CardDetailViewController") as! CardDetailViewController
        vc.content = self.publishedDecks[indexPath.row]
        vc.showNavOnClose = true
        tabBarController!.presentViewController(vc, animated: true, completion: nil)
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 216
    }

    func scrollViewDidScroll(scrollView: UIScrollView) {
        
//        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
//            //reach bottom
//            self.scrollViewBottomConstraints.constant = 49
//        }
//        
//        if (scrollView.contentOffset.y < 0){
//            //reach top
//            self.scrollViewBottomConstraints.constant = 0
//        }
//        
//        if (scrollView.contentOffset.y > 0 && scrollView.contentOffset.y < (scrollView.contentSize.height - scrollView.frame.size.height)){
//            //not top and not bottom
//            self.scrollViewBottomConstraints.constant = 49
//        }
//        
//        scrollView.setNeedsLayout()
//        scrollView.layoutIfNeeded()
        
    }
    
    @IBAction func onSearch(){
        
    }
    @IBAction func onClose(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    // MARK: - UIPanGestureRecognizer
    
    
    func onSetupPanel(){
        pan.addTarget(self, action: #selector(TabProfileController.handlePan(_:)))
        pan.delegate = self
        self.wholeView.addGestureRecognizer(pan)
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if otherGestureRecognizer is UIPanGestureRecognizer {
            return true
        }
        
        return false
    }
    
    func handlePan(panGesture: UIPanGestureRecognizer){
        
        let factor: CGFloat = 2
        let angleLimit = CGFloat(M_PI_4)
        
        switch panGesture.state {
        case .Began:
            
            startPos = panGesture.locationInView(self.wholeView.superview)
            startCenter = self.wholeView.center
            print(startPos)
            let velocity = panGesture.velocityInView(self.wholeView.superview)
            
            isRotating = fabs(velocity.x) > fabs(velocity.y)
            
        case .Changed:
            
            if isRotating {
                
                let pos = panGesture.locationInView(self.wholeView?.superview)
                
                let rotationAngle = atan((pos.x - startPos.x) / (self.wholeView.bounds.size.height * factor - pos.y))
                var direction:CGFloat = 1
                if pos.x != startPos.x {
                    direction = (startPos.x - pos.x) / fabs(pos.x - startPos.x)
                }
                imgBackground.alpha = 0.7 - fabs(rotationAngle)
                
                var transform = CGAffineTransformMakeTranslation(self.wholeView.bounds.size.width * 0.5 * direction, self.wholeView.bounds.size.height * (factor - 0.5))
                transform = CGAffineTransformRotate(transform, rotationAngle)
                transform = CGAffineTransformTranslate(transform, -self.wholeView.bounds.size.width * 0.5 * direction, -self.wholeView.bounds.size.height * (factor - 0.5))
                self.wholeView.transform = transform;
                
            } else {
                let pos = panGesture.locationInView(self.view?.superview)
                self.wholeView.center = CGPointMake(startCenter.x, startCenter.y + pos.y - startPos.y)
            }
            
        case .Ended:
            
            if isRotating {
                let pos = panGesture.locationInView(self.wholeView?.superview)
                let velocity = panGesture.velocityInView(self.wholeView?.superview)
                var startDirection = 1
                if pos.x != startPos.x {
                    startDirection = Int(round((pos.x - startPos.x) / fabs(pos.x - startPos.x)))
                }
                var velocityDirection = -startDirection
                if velocity.x != 0 {
                    velocityDirection = Int(round(velocity.x / fabs(velocity.x)))
                }
                if startDirection == velocityDirection { // should dismiss
                    UIView.animateWithDuration(0.3, animations: {
                        
                        var transform = CGAffineTransformMakeTranslation(-self.wholeView.bounds.size.width * 0.5 * CGFloat(startDirection), self.wholeView.bounds.size.height * (factor - 0.5))
                        transform = CGAffineTransformRotate(transform, angleLimit * CGFloat(startDirection))
                        transform = CGAffineTransformTranslate(transform, self.wholeView.bounds.size.width * 0.5 * CGFloat(startDirection), -self.wholeView.bounds.size.height * (factor - 0.5))
                        
                        self.wholeView.transform = transform
                        self.imgBackground.alpha = 0
                        
                        }, completion: { completed in
                            self.dismissViewControllerAnimated(false, completion: nil)
                    })
                } else {
                    UIView.animateWithDuration(0.2, animations: {
                        self.wholeView.transform = CGAffineTransformIdentity
                        self.imgBackground.alpha = 0.7
                        }, completion: { completed in
                    })
                }
                
            } else {
                let velocity = panGesture.velocityInView(self.view?.superview)
                if velocity.y > 0 { // should dismiss
                    UIView.animateWithDuration(0.2, animations: {
                        self.wholeView.center = CGPointMake(self.startCenter.x, self.wholeView.bounds.size.height * 1.5)
                        self.imgBackground.alpha = 0
                        }, completion: { completed in
                            self.dismissViewControllerAnimated(false, completion: nil)
                    })
                } else {
                    UIView.animateWithDuration(0.2, animations: {
                        self.wholeView.center = self.startCenter
                        }, completion: { completed in
                    })
                }
            }
            
            
        default:
            break
        }
    }
    //////////////////////
    
    @IBAction func nameTap(gesture : UITapGestureRecognizer){
        let editPromt = UIAlertController(title: "Name", message: "Yup, that’s you! 😎", preferredStyle: UIAlertControllerStyle.Alert)
        
        editPromt.addTextFieldWithConfigurationHandler(configurationFirstNameTextField)
        editPromt.addTextFieldWithConfigurationHandler(configurationLastNameTextField)
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Default) { (action) in
            
        }
        
        let saveAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            
            if self.txtFirstName.text == "" {
                
                self.backup_firstname = ""
            }else{
                self.backup_firstname = self.txtFirstName.text
            }
            
            if self.txtLastName.text == ""{
                self.backup_lastname = ""
            }else{
                self.backup_lastname = self.txtLastName.text
                
            }
            
            if self.backup_firstname == "" && self.backup_lastname == "" {
                
                let alert = UIAlertController(title: "No name?", message: String(format:"Gotta have something, right? 😅"), preferredStyle: UIAlertControllerStyle.Alert)
                
                let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action) in
                }
                alert.addAction(cancelAction)
                self.presentViewController(alert, animated: true, completion: nil)
                
                self.lblFullName.text = "Full Name"
            }else{
                self.lblFullName.text = String(format:"%@ %@", self.backup_firstname, self.backup_lastname)
            }
        }
        editPromt.addAction(cancelAction)
        editPromt.addAction(saveAction)
        presentViewController(editPromt, animated: true, completion: nil)
    }
    
    func configurationUserNameTextField(textField: UITextField!)
    {
        textField.text = avatarName.text
        self.txtUserName = textField
    }
    func configurationLastNameTextField(textField: UITextField!)
    {
        if lblFullName.text == "Full Name" {
            textField.text = ""
        }else{
            
            textField.text = self.backup_lastname
        }
        textField.placeholder = "Last name"
        
        self.txtLastName = textField
    }
    
    func configurationFirstNameTextField(textField: UITextField!)
    {
        if lblFullName.text == "Full Name" {
            textField.text = ""
        }else{
            
            textField.text = self.backup_firstname
        }
        textField.placeholder = "First name"
        
        self.txtFirstName = textField
    }
    func configurationBioTextField(textField: UITextField!)
    {
        if lblBio.text == "Bio" {
            textField.text = ""
        }else{
            textField.text = lblBio.text
        }
        
        self.txtBio = textField
    }
    @IBAction func userNameTap(gesture : UITapGestureRecognizer){
        
        let editPromt = UIAlertController(title: "username", message: "Make it unique! 😙", preferredStyle: UIAlertControllerStyle.Alert)
        
        editPromt.addTextFieldWithConfigurationHandler(configurationUserNameTextField)
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Default) { (action) in
            
        }
        
        let saveAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            
            if self.txtUserName.text == "" {
                let alert = UIAlertController(title: "Oops.", message: String(format:"You need a username! 😉💫"), preferredStyle: UIAlertControllerStyle.Alert)
                
                let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action) in
                    // ...
                    self.userNameTap(gesture)
                }
                alert.addAction(cancelAction)
                self.presentViewController(alert, animated: true, completion: nil)
            }else{
                //request the web api to check the username validation
                
                self.onValidateUserName(self.txtUserName.text! , gesture: gesture)
            }
            
        }
        editPromt.addAction(cancelAction)
        editPromt.addAction(saveAction)
        presentViewController(editPromt, animated: true, completion: nil)
    }
    
    func onValidateUserName(userName : String , gesture : UITapGestureRecognizer){
        
        if userName == self.userInfo.username {
            return
        }
        self.showProgressHUD()
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        
        provider.request(.ValidateUserName(user_name: userName)) { (result) in
            switch result {
            case let .Success(response):
                print(response.description)
                print(response.debugDescription)
                
                if response.statusCode != 200 {
                    self.hideProgressHUD()
                    
                    return
                }
                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    self.responseJSON = JSON
                    
                    self.hideProgressHUD()
                    
                    let flag : Int = JSON["result"] as! Int
                    
                    if flag == 1 {
                        let alert = UIAlertController(title: "Oh snap!", message: String(format:"That username is taken! 😭"), preferredStyle: UIAlertControllerStyle.Alert)
                        
                        let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action) in
                            
                            self.userNameTap(gesture)
                        }
                        alert.addAction(cancelAction)
                        self.presentViewController(alert, animated: true, completion: nil)
                    }else{
                        self.avatarName.text = userName
                    }
                    
                    
                }
                else {
                    self.hideProgressHUD()
                }
            case .Failure(_):
                self.hideProgressHUD()
            }
        }
    
    }
    
    func followingTap(gesture : UITapGestureRecognizer){
        let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "ProfileFollowingVC") as! ProfileFollowingVC
        
        vc.modalPresentationStyle = .OverFullScreen
        
        vc.userInfo = self.userInfo
        vc.listData = self.following_list
        vc.delegate = self
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func followersTap(gesture : UITapGestureRecognizer){
        let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "ProfileFollowersVC") as! ProfileFollowersVC
        
        vc.modalPresentationStyle = .OverFullScreen
        
        vc.userInfo = self.userInfo
        vc.listData = self.followers_list
        vc.delegate = self
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    @IBAction func bioTap(gesture : UITapGestureRecognizer){
        let editPromt = UIAlertController(title: "Bio", message: "Tell me something cool about you! 🤓", preferredStyle: UIAlertControllerStyle.Alert)
        
        editPromt.addTextFieldWithConfigurationHandler(configurationBioTextField)
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Default) { (action) in
            
        }
        
        let saveAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            
            if self.txtBio.text != "" {
                
                self.lblBio.text = self.txtBio.text
            }else{
                self.lblBio.text = "Bio"
            }
            
        }
        editPromt.addAction(cancelAction)
        editPromt.addAction(saveAction)
        presentViewController(editPromt, animated: true, completion: nil)
    }
    
    
    func onShowNotice(){
        let alert = UIAlertController(title: nil, message: "Tap on a field to edit! 😉", preferredStyle: UIAlertControllerStyle.Alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action) in
            // ...
            
            self.onDisablePopularButtons()
            self.onEnableEditButtons()
            self.tabBarController?.tabBar.hidden = true
            self.bottomView.hidden = false
            
            if self.lblFullName.text == "" {
                self.lblFullName.text = "Full Name"
                
                self.backup_lastname = ""
                self.backup_firstname = ""
            }else{
                self.backup_firstname = self.userInfo.first_name
                self.backup_lastname = self.userInfo.last_name
            }
            
            if self.lblBio.text == "" {
                self.lblBio.text = "Bio"
            }
            
        }
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func clickedOnMenu(sender: AnyObject) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            // ...
        }
        alertController.addAction(cancelAction)
        
        let editProfile = UIAlertAction(title: "📝 Edit Profile", style: .Default) { (action) in
            //            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("EditProfileViewController") as! EditProfileViewController
            //            vc.JSON = self.responseJSON
            //            self.presentViewController(vc, animated: true, completion: nil)
            
            self.onShowNotice()
        }
        alertController.addAction(editProfile)
        
        let logoutAction = UIAlertAction(title: "👋 Log Out", style: .Default) { (action) in
            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.removeObjectForKey("guidehero.authtoken")
            userDefaults.synchronize()
            self.dismissVC()
        }
        alertController.addAction(logoutAction)
        
        self.presentViewController(alertController, animated: true) {
            
        }
    }
    
    func onDisableEditButtons(){
        
        nameGesture.enabled = false
        userNameGesture.enabled = false
        bioGesture.enabled = false
    }
    func onEnableEditButtons(){
        
        nameGesture.enabled = true
        userNameGesture.enabled = true
        bioGesture.enabled = true
    }
    
    @IBAction func onEditCancel (sender : AnyObject){
        
        self.avatarName.text = self.userInfo.username
        self.lblFullName.text = String(format:"%@ %@", self.userInfo.first_name, self.userInfo.last_name)
        self.lblBio.text = self.userInfo.bio
        
        tabBarController?.tabBar.hidden = false
        bottomView.hidden = true
        onDisableEditButtons()
        onEnablePopularButtons()
        
    }
    @IBAction func onEditSave (sender : AnyObject){
//        tabBarController?.tabBar.hidden = false
//        bottomView.hidden = true
//        onDisableEditButtons()
//        onEnablePopularButtons()
        
        self.onRequestEditProfile()
        
    }
    
    func onRequestEditProfile(){
        
        let request_bio : String
        if lblBio.text == "Bio" {
            request_bio = ""
        }else{
            request_bio = lblBio.text!
        }
        
        let request_firstName : String = self.backup_firstname
        let request_lastName : String = self.backup_lastname
        let request_username : String = avatarName.text!
        
        self.showProgressHUD()
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        
        provider.request(.EditUserInfo(first_name: request_firstName, last_name: request_lastName, user_name: request_username, bio: request_bio)) { (result) in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.hideProgressHUD()
                    return
                }
                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    self.responseJSON = JSON
                    self.userInfo = UserNode.init(dictionary:JSON )
                    
                    self.avatarName.text = self.userInfo.username
                    self.lblFullName.text = String(format:"%@ %@", self.userInfo.first_name, self.userInfo.last_name)
                    self.lblBio.text = self.userInfo.bio
                    
                    self.hideProgressHUD()
                }
                else {
                    self.hideProgressHUD()
                }
            case .Failure(_):
                self.hideProgressHUD()
            }
            
        }
        
    }
    
    func onEnablePopularButtons(){
        self.btnPublish.enabled = true
        self.btnDraft.enabled = true
        self.btnAsks.enabled = true
        self.btnGives.enabled = true
        self.btnPrizes.enabled = true
        self.btnLikes.enabled = true
        self.btnShare.enabled = true
        self.btnMore.enabled = true
        
        self.btnFacebook.enabled = true
        self.btnHarvard.enabled = true
        self.btnLikeIcon.enabled = true
        self.btnShareIcon.enabled = true
        
        self.lblHarvard.alpha = 1
        self.lblFollowers.alpha = 1
        self.lblFollowing.alpha = 1
        self.lblLikeCount.alpha = 1
        self.lblShareCount.alpha = 1
        self.lblPoint.alpha = 1
    }
    
    func onDisablePopularButtons(){
        self.btnMore.enabled = false
        self.btnPublish.enabled = false
        self.btnDraft.enabled = false
        self.btnAsks.enabled = false
        self.btnGives.enabled = false
        self.btnPrizes.enabled = false
        self.btnLikes.enabled = false
        self.btnShare.enabled = false
        
        self.btnFacebook.enabled = false
        self.btnHarvard.enabled = false
        self.btnLikeIcon.enabled = false
        self.btnShareIcon.enabled = false
        
        self.lblHarvard.alpha = 0.5
        self.lblFollowers.alpha = 0.5
        self.lblFollowing.alpha = 0.5
        self.lblLikeCount.alpha = 0.5
        self.lblShareCount.alpha = 0.5
        self.lblPoint.alpha = 0.5
    }
    
    func onFollowers_Follow(userData : UserNode) {
        
        if self.userInfo.user_id == Session.sharedSession.user?.user_id {
            self.userInfo.following_count = self.userInfo.following_count + 1
            self.following_list.append(userData)
            
        }else{
            
            userData.isRelation = true
        }
        
        
        if self.userInfo != nil {
            self.lblFollowing.text = String(format:"%d Followers", self.userInfo.following_count)
            
            self.lblFollowers.text = String(format:"%d Followers", self.userInfo.followers_count)
        }
        
    }
    func onFollowers_UnFollow(userData : UserNode) {
        
        if self.userInfo.user_id == Session.sharedSession.user?.user_id {
            
            self.userInfo.following_count = self.userInfo.following_count - 1
            
            var index : Int = 0
            for followersDict in self.following_list {
                if followersDict.user_id == userData.user_id {
                    
                    break
                }
                index = index + 1
            }
            
            self.following_list.removeAtIndex(index)
            
            
        }else{
            
            userData.isRelation = false
        }
        
        
        if self.userInfo != nil {
            self.lblFollowing.text = String(format:"%d Followers", self.userInfo.following_count)
            
            self.lblFollowers.text = String(format:"%d Followers", self.userInfo.followers_count)
        }
    }
    
    func onFollowing_Follow(userData : UserNode) {
        if self.userInfo.user_id == Session.sharedSession.user?.user_id {
            self.userInfo.following_count = self.userInfo.following_count + 1
            userData.isRelation = true
            self.following_list.append(userData)
            
        }else{
            userData.isRelation = true
        }
        
        
        if self.userInfo != nil {
            self.lblFollowing.text = String(format:"%d Followers", self.userInfo.following_count)
            
            self.lblFollowers.text = String(format:"%d Followers", self.userInfo.followers_count)
        }
    }
    func onFollowing_UnFollow(userData : UserNode) {
        if self.userInfo.user_id == Session.sharedSession.user?.user_id {
            
            self.userInfo.following_count = self.userInfo.following_count - 1
            
            var index : Int = 0
            for followersDict in self.following_list {
                if followersDict.user_id == userData.user_id {
                    
                    break
                }
                index = index + 1
            }
            
            self.following_list.removeAtIndex(index)
            
            
        }else{
            
            userData.isRelation = false
        }
        
        
        if self.userInfo != nil {
            self.lblFollowing.text = String(format:"%d Followers", self.userInfo.following_count)
            
            self.lblFollowers.text = String(format:"%d Followers", self.userInfo.followers_count)
        }
    }
    
    
}
