//
//  PrizeContributionPopViewController.swift
//  GuideHero
//
//  Created by HC on 11/24/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya

class PrizeContributionPopViewController: UIViewController {

    @IBOutlet var popupView: UIView!
    @IBOutlet var prizeLabel: UILabel!
    @IBOutlet var joinButton: UIButton!
    
    var content: ContentNode!
    var joinPrize: Int! {
        didSet {
            prizeLabel.text = "\(joinPrize) pt"
        }
    }
    var joinedCallback: ((_: ContentNode!) -> Void)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popupView.layer.cornerRadius = 10
        joinPrize = content.prize_to_join
        joinButton.roundView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onCancel(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func onJoinAsk(sender: UIButton) {
        showProgressHUD()
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(NetworkService.JoinAsk(deckId: self.content!.id, joinPrize: joinPrize), completion: { (result) in
            let JSON = Helper.validateResponse(result, sender: self)
            if JSON != nil {
                let rContent = ContentNode.init(dictionary: JSON!["deck"] as! [String: AnyObject])
                self.joinedCallback(rContent)
                self.content = rContent
                // do reload
//                var asked_users = self.content.asked_users
//                asked_users.append(Session.sharedSession.user!)
//                self.content.asked_users = asked_users
//                self.onJoinAsk()
            }
            sender.enabled = true
        })
    }

    @IBAction func onCustomAmount(sender: UIButton) {
        var textField = UITextField()
        let alert = UIAlertController(title: "Use Custom Amount!", message: "(Enter minimum amount or more ✍️)", preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler { (tField) in
            textField = tField
            textField.keyboardType = .NumberPad
        }
        
        let send = UIAlertAction(title: "Send", style: .Default) { (action) in
            let customPrize = Int(textField.text ?? "0")
            if customPrize! < self.joinPrize {
                self.showSimpleAlert(withTitle: "", withMessage: "Custom amount must not be less than join amount");
            } else {
                self.joinPrize = customPrize
            }
        }
        alert.addAction(send)
        
        let cancel = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in }
        alert.addAction(cancel)
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func onViewTapped(sender: UITapGestureRecognizer) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
