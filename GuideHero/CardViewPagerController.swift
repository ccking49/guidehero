//
//  CardViewPagerController.swift
//  GuideHero
//
//  Created by Promising Change on 03/01/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import Foundation

public protocol CardViewPagerProgressDelegate : class {
    
    func updateIndicator(for viewController: CardViewPagerController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool, borderCrossed: Bool, direction: SwipeDirection)
    func updateIndicator(for viewController: CardViewPagerController, fromIndex: Int, toIndex: Int)
}

public protocol CardViewPagerDataSource: class {
    
    func viewControllers(for viewPagerController: CardViewPagerController) -> [CardSubPageBaseVC]
}

public protocol CardSubPageScrollDelegate: class {
    func subPageScrollViewDidScroll(offset:CGFloat, translation:CGPoint, animated: Bool)
    func subPageScrollViewDidBounce(offset:CGFloat)
}

public class CardViewPagerController: UIViewController, UIScrollViewDelegate, CardSubPageScrollDelegate {
    
    @IBOutlet lazy public var containerView: UIScrollView! = { [unowned self] in
        let containerView = UIScrollView(frame: CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)))
        containerView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        return containerView
        }()
    
    public weak var datasource: CardViewPagerDataSource?
    
    public private(set) var viewControllers = [CardSubPageBaseVC]()
    public private(set) var currentIndex = 0
    public private(set) var currentPageIndex = 0
    
    public var pageWidth: CGFloat {
        return containerView.bounds.width
    }
    
    public var scrollPercentage: CGFloat {
        if swipeDirection != .right {
            let module = fmod(containerView.contentOffset.x, pageWidth)
            return module == 0.0 ? 1.0 : module / pageWidth
        }
        return 1 - fmod(containerView.contentOffset.x >= 0 ? containerView.contentOffset.x : pageWidth + containerView.contentOffset.x, pageWidth) / pageWidth
    }
    
    public var swipeDirection: SwipeDirection {
        if containerView.contentOffset.x > lastContentOffset {
            return .left
        }
        else if containerView.contentOffset.x < lastContentOffset {
            return .right
        }
        return .none
    }
    
    var isDraggable: Bool = false
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let containerViewAux = containerView ?? {
            let containerView = UIScrollView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height))
            containerView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
            return containerView
            }()
        containerView = containerViewAux
        if containerView.superview == nil {
            view.addSubview(containerView)
        }
        containerView.bounces = true
        containerView.alwaysBounceHorizontal = true
        containerView.alwaysBounceVertical = false
        containerView.scrollsToTop = false
        containerView.delegate = self
        containerView.showsVerticalScrollIndicator = false
        containerView.showsHorizontalScrollIndicator = false
        containerView.pagingEnabled = true
        reloadViewControllers()
    }
    
    override public func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        isViewAppearing = true
    }
    
    override public func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        lastSize = containerView.bounds.size
        updateIfNeeded()
        isViewAppearing = false
    }
    
    override public func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        updateIfNeeded()
    }
    
    public func moveToViewController(at index: Int, animated: Bool = true) {
        guard isViewLoaded() == true && view.window != nil else {
            currentIndex = index
            currentPageIndex = index
            return
        }
        
        isJumping = true
        if animated && abs(currentIndex - index) > 1 {
            var tmpViewControllers = viewControllers
            let currentChildVC = viewControllers[currentIndex]
            let fromIndex = currentIndex < index ? index - 1 : index + 1
            let fromChildVC = viewControllers[fromIndex]
            tmpViewControllers[currentIndex] = fromChildVC
            tmpViewControllers[fromIndex] = currentChildVC
            pagerChildViewControllersForScrolling = tmpViewControllers
            
            isJumpingReset = true
            containerView.setContentOffset(CGPoint(x: pageOffsetForChild(at: fromIndex), y: 0), animated: false)
            (navigationController?.view ?? view)?.userInteractionEnabled = !animated
            isJumpingReset = false
            containerView.setContentOffset(CGPoint(x: pageOffsetForChild(at: index), y: 0), animated: true)
        }
        else {
            (navigationController?.view ?? view)?.userInteractionEnabled = !animated
            
            print(containerView.contentOffset.y)
            containerView.setContentOffset(CGPoint(x: pageOffsetForChild(at: index), y: 0), animated: animated)

        }
    }
    
    public func moveTo(viewController: CardSubPageBaseVC, animated: Bool = true) {
        moveToViewController(at: viewControllers.indexOf(viewController)!, animated: animated)
    }
    
    // MARK: - ViewPagerDataSource
    
    public func viewControllers(for viewPagerController: CardViewPagerController) -> [CardSubPageBaseVC] {
        assertionFailure("Sub-class must implement the CardViewPagerDataSource viewControllers(for:) method")
        return []
    }
    
    // MARK: - Helpers
    
    public func updateIfNeeded() {
        if isViewLoaded() && !(CGSizeEqualToSize(lastSize, containerView.bounds.size)) {
            updateContent()
        }
    }
    
    public func canMoveTo(index: Int) -> Bool {
        return currentIndex != index && viewControllers.count > index
    }
    
    public func pageOffsetForChild(at index: Int) -> CGFloat {
        return CGFloat(index) * containerView.bounds.width
    }
    
    public func offsetForChild(at index: Int) -> CGFloat{
        return (CGFloat(index) * containerView.bounds.width) + ((containerView.bounds.width - view.bounds.width) * 0.5)
    }
    
    public func offsetForChild(viewController: CardSubPageBaseVC) throws -> CGFloat{
        guard let index = viewControllers.indexOf(viewController) else {
            throw CardViewPagerControllerError.viewControllerNotContainedInViewPager
        }
        return offsetForChild(at: index)
    }
    
    public func pageFor(contentOffset contentOffset: CGFloat) -> Int {
        let result = virtualPageFor(contentOffset: contentOffset)
        return pageFor(virtualPage: result)
    }
    
    public func pageIndexFor(contentOffset contentOffset: CGFloat) -> Int {
        return Int(contentOffset / pageWidth)
    }
    
    public func virtualPageFor(contentOffset contentOffset: CGFloat) -> Int {
        return Int((contentOffset + 1.5 * pageWidth) / pageWidth) - 1
    }
    
    public func pageFor(virtualPage virtualPage: Int) -> Int{
        if virtualPage < 0 {
            return 0
        }
        if virtualPage > viewControllers.count - 1 {
            return viewControllers.count - 1
        }
        return virtualPage
    }
    
    public func updateContent() {
        if lastSize.width != containerView.bounds.size.width {
            lastSize = containerView.bounds.size
            containerView.contentOffset = CGPoint(x: pageOffsetForChild(at: currentIndex), y: 0)
        }
        lastSize = containerView.bounds.size
        
        let pagerViewControllers = pagerChildViewControllersForScrolling ?? viewControllers
        containerView.contentSize = CGSize(width: containerView.bounds.width * CGFloat(pagerViewControllers.count), height: containerView.contentSize.height)
        
        for (index, childController) in pagerViewControllers.enumerate() {
            let pageOffsetForChild = self.pageOffsetForChild(at: index)
            if fabs(containerView.contentOffset.x - pageOffsetForChild) < containerView.bounds.width {
                if let _ = childController.parentViewController {
                    childController.view.frame = CGRect(x: offsetForChild(at: index), y: 0, width: view.bounds.width, height: containerView.bounds.height)
//                    childController.view.frame = CGRect(x: offsetForChild(at: index), y: 0, width: view.bounds.width, height: childController.view.frame.height)
//                    childController.view.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
                }
                else {
                    addChildViewController(childController)
                    childController.beginAppearanceTransition(true, animated: false)
                    childController.view.frame = CGRect(x: offsetForChild(at: index), y: 0, width: view.bounds.width, height: containerView.bounds.height)
//                    childController.view.frame = CGRect(x: offsetForChild(at: index), y: 0, width: view.bounds.width, height: childController.view.frame.height)
//                    childController.view.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
                    containerView.addSubview(childController.view)
                    childController.didMoveToParentViewController(self)
                    childController.endAppearanceTransition()
                }
                
                if ((movingFactor == 1 && currentPageIndex != index && !isJumpingReset) || (isJumping && !isJumpingReset && currentPageIndex != index && movingFactor == 2)) {
//                    print("Started Moving from VC: \(currentPageIndex) to VC: \(index)")
                    
                    let currentController = pagerViewControllers[currentPageIndex]
                    let currentTop = currentController.contentOffsetY
                    let nextTop = childController.contentOffsetY
                    
                    if (currentTop > nextTop) {
                        ///
                        ///     ======== |
                        ///     ======== |
                        ///     ======== |-> ========
                        ///     ======== |   ========
                        ///
//                        print("High -> Low")
                        childController.layoutView()
                        childController.scrollToOffsetY(currentTop)
                    } else if (currentTop < nextTop) {
                        ///
                        ///              |   ========
                        ///              |   ========
                        ///     ======== |-> ========
                        ///     ======== |   ========
                        ///
//                        print("Low -> High")
                        childController.layoutView()
                        childController.scrollToOffsetY(currentTop)
                    }
                }
            }
            else {
                if let _ = childController.parentViewController {
                    childController.willMoveToParentViewController(nil)
                    childController.beginAppearanceTransition(false, animated: false)
                    childController.view.removeFromSuperview()
                    childController.removeFromParentViewController()
                    childController.endAppearanceTransition()
                }
            }
        }
        
        let oldCurrentIndex = currentIndex
        let virtualPage = virtualPageFor(contentOffset: containerView.contentOffset.x)
        let newCurrentIndex = pageFor(virtualPage: virtualPage)
        currentIndex = newCurrentIndex
        let changeCurrentIndex = newCurrentIndex != oldCurrentIndex
        
        let oldCurrentPageIndex = currentPageIndex
        let newCurrentPageIndex = pageIndexFor(contentOffset: containerView.contentOffset.x)
        currentPageIndex = newCurrentPageIndex
        let changeCurrentPageIndex = newCurrentPageIndex != oldCurrentPageIndex
        
        if let progressiveDelegate = self as? CardViewPagerProgressDelegate {
            
            let (fromIndex, toIndex, scrollPercentage) = progressiveIndicatorData(virtualPage)
            if (!isJumping) {
                progressiveDelegate.updateIndicator(for: self, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: scrollPercentage, indexWasChanged: changeCurrentIndex, borderCrossed: changeCurrentPageIndex, direction: swipeDirection)
            } else {
                progressiveDelegate.updateIndicator(for: self, fromIndex: fromIndex, toIndex: toIndex)
            }
        }
    }
    
    public func reloadViewPager() {
        guard isViewLoaded() else { return }
        for childController in viewControllers {
            if let _ = childController.parentViewController {
                childController.view.removeFromSuperview()
                childController.willMoveToParentViewController(nil)
                childController.removeFromParentViewController()
            }
        }
        reloadViewControllers()
        containerView.contentSize = CGSize(width: containerView.bounds.width * CGFloat(viewControllers.count), height: containerView.contentSize.height)
        if currentIndex >= viewControllers.count {
            currentIndex = viewControllers.count - 1
        }
        containerView.contentOffset = CGPoint(x: pageOffsetForChild(at: currentIndex), y: 0)
        updateContent()
    }
    
    // MARK: - UIScrollViewDelegate
    
    public func scrollViewDidScroll(scrollView: UIScrollView) {
        if containerView == scrollView {
            if (scrollView.contentOffset.x < 0) {
                // Scroll past the left edge of the container view
//                print("Scrolling past the left edge")
                isDraggable = true
            } else if (scrollView.contentOffset.x > (containerView.bounds.size.width * CGFloat(viewControllers.count))) {
                // Scroll past the right edge of the container view
//                print("Scrolling past the right edge")
                isDraggable = true
            } else {
                // Scroll inside the container view
                isDraggable = false
                if movingFactor == 0 {
                    movingFactor = 1
                } else {
                    movingFactor += 1
                }
                
                updateContent()
            }
        }
    }
    
    public func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        if containerView == scrollView {
            lastPageNumber = pageFor(contentOffset: scrollView.contentOffset.x)
            lastContentOffset = scrollView.contentOffset.x
        }
    }
    
    public func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
    }
    
    public func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        movingFactor = 0
        let currentSubPage = viewControllers[currentPageIndex]
        currentSubPage.scrollRefresh()
    }
    
    public func scrollViewDidEndScrollingAnimation(scrollView: UIScrollView) {
        if containerView == scrollView {
            pagerChildViewControllersForScrolling = nil
            isJumping = false
            movingFactor = 0
            (navigationController?.view ?? view)?.userInteractionEnabled = true
            updateContent()
            let currentSubPage = viewControllers[currentPageIndex]
            currentSubPage.scrollRefresh()
        }
    }
    
    // MARK: - SubPageScrollDelegate
    
    public func subPageScrollViewDidScroll(offset: CGFloat, translation: CGPoint, animated: Bool) {
        isDraggable = false
    }
    
    public func subPageScrollViewDidBounce(offset:CGFloat) {
//        print("Scrolling past the bottom edge")
        isDraggable = true
    }
    
    // MARK: - Orientation
    
    override public func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        isViewRotating = true
        pageBeforeRotate = currentIndex
        coordinator.animateAlongsideTransition(nil) { [weak self] _ in
            guard let me = self else { return }
            me.isViewRotating = false
            me.currentIndex = me.pageBeforeRotate
            me.updateIfNeeded()
        }
        
    }
    
    // MARK: Private
    
    private func progressiveIndicatorData(virtualPage: Int) -> (Int, Int, CGFloat) {
        let count = viewControllers.count
        var fromIndex = currentIndex
        var toIndex = currentIndex
        let direction = swipeDirection
        
        if direction == .left {
            if virtualPage > count - 1 {
                fromIndex = count - 1
                toIndex = count
            }
            else {
                if self.scrollPercentage >= 0.5 {
                    fromIndex = max(toIndex - 1, 0)
                }
                else {
                    toIndex = fromIndex + 1
                }
            }
        }
        else if direction == .right {
            if virtualPage < 0 {
                fromIndex = 0
                toIndex = -1
            }
            else {
                if self.scrollPercentage > 0.5 {
                    fromIndex = min(toIndex + 1, count - 1)
                }
                else {
                    toIndex = fromIndex - 1
                }
            }
        }
        let scrollPercentage = self.scrollPercentage
        return (fromIndex, toIndex, scrollPercentage)
    }
    
    private func reloadViewControllers(){
        guard let dataSource = datasource else {
            fatalError("dataSource must not be nil")
        }
        viewControllers = dataSource.viewControllers(for: self)
        // viewControllers
        guard viewControllers.count != 0 else {
            fatalError("viewControllers(for:) should provide at least one child view controller")
        }
        
    }
    
    private var pagerChildViewControllersForScrolling : [CardSubPageBaseVC]?
    private var lastPageNumber = 0
    private var lastContentOffset: CGFloat = 0.0
    private var pageBeforeRotate = 0
    private var lastSize = CGSize(width: 0, height: 0)
    internal var isViewRotating = false
    internal var isViewAppearing = false
    internal var isJumping = false
    internal var isJumpingReset = false
    internal var movingFactor = 0
    
    
}
