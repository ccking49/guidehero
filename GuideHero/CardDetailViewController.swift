//
//  CardDetailViewController.swift
//  GuideHero
//
//  Created by Ascom on 11/2/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AlamofireImage
import Moya

class CardDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var creatorImage: UIImageView!
    @IBOutlet var creatorNameAndDate: UILabel!
    @IBOutlet var dotsButton: UIButton!
    @IBOutlet var containerView: UIView!
    
    var contentHeaderCell: ContentHeaderCell!
    var askCell: AskCell!
    var giveCell: GiveCell!
    var prizeCell: PrizeCell!
    
    var content: ContentNode? {
        didSet {
            if isViewLoaded() {
//                updateHeader()
                tableView.reloadData()
                
            }
        }
    }
    var contentIndex: Int = 0
    
    var cardId: String? // To load specific deck/card
    var showNavOnClose: Bool = false
    var showNav: Bool = false
    var currentMode: HeaderCellAction = .Play
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setupUI()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CardDetailViewController.refreshData), name:Constants.Literals.contentRefreshNotification, object: nil)
        
//        containerView.layer.cornerRadius = 10
//        containerView.layer.borderColor = UIColor.whiteColor().CGColor
//        containerView.layer.borderWidth = 1
//        tableView.backgroundColor = UIColor.blackColor()
//        let wrapperView = tableView.subviews.first as UIView!
//        wrapperView.layer.cornerRadius = 20
//        wrapperView.layer.borderColor = UIColor.whiteColor().CGColor
//        wrapperView.layer.borderWidth = 1
//        wrapperView.clipsToBounds = true
        
//        tableView.layer.cornerRadius = 20.0;
//        tableView.layer.masksToBounds = true;
//        tableView.clipsToBounds = true;
//        for subLayer in tableView.layer.sublayers! {
//            subLayer.cornerRadius = 20
//            subLayer.masksToBounds = true
//        }
        
        let askCellNib = UINib(nibName: "AskCell", bundle: nil)
        tableView.registerNib(askCellNib, forCellReuseIdentifier: "AskCell")
        let prizeCellNib = UINib(nibName: "PrizeCell", bundle: nil)
        tableView.registerNib(prizeCellNib, forCellReuseIdentifier: "PrizeCell")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if self.content == nil {
            self.fetchAllDecks()
        }
        dotsButton.hidden = !(navigationController?.viewControllers.first?.isKindOfClass(TabProfileController) ?? false)
        navigationController?.setNavigationBarHidden(!showNav, animated: false)
//        tabBarController!.tabBar.hidden = false
    }
    
    // MARK: - Private methods
    
    private func fetchAllDecks() {
        if cardId == nil {
            return
        }
        self.showProgressHUD()
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.GetCard(cardId: cardId!)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't download decks, please try again later")
                    self.hideProgressHUD()
                    return
                }
                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    self.hideProgressHUD()
                    let dic = JSON["card"] as! [String: AnyObject]
                    self.content = ContentNode(dictionary: dic)
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't parse decks, please try again later")
                    self.hideProgressHUD()
                }
            case let .Failure(error):
                print("can't fetch decks: %@", error)
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't download decks, please try again later")
                self.hideProgressHUD()
            }
        }
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if content == nil {
            return 0
        }
        if currentMode == .Play {
            tableView.separatorStyle = .SingleLine
            return content!.children.count + 1
        } else if currentMode == .Ask || currentMode == .Give || currentMode == .Prize{
            tableView.separatorStyle = .None
            return 2
        }
        return 1
    }
    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        if indexPath.row == 0 {
//            return Constants.UIConstants.contentHeaderCellHeight
//        }
//        return Constants.UIConstants.contentCellHeight
//    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let rowsNum = tableView.numberOfRowsInSection(indexPath.section)
        if (indexPath.row == 0) { //(content?.type != ContentType.Deck) ||
            if contentHeaderCell != nil {
                return contentHeaderCell
            }
            let cell = tableView.dequeueReusableCellWithIdentifier("ContentHeaderCell", forIndexPath: indexPath) as! ContentHeaderCell
            cell.content = content
            cell.onAction = { action in
                self.handleHeaderCellAction(action)
            }
            
            cell.cardView.tag = indexPath.row
            let singleTap = UITapGestureRecognizer(target: self, action: #selector(CardDetailViewController.showFullScreen))
            cell.cardView.addGestureRecognizer(singleTap)
            
            cell.selectionStyle = .None
            contentHeaderCell = cell
            cell.needsRoundBottom = rowsNum == 1
            return cell
        }
        
        if currentMode == .Ask {
            if askCell != nil {
                return askCell
            }
            let cell = tableView.dequeueReusableCellWithIdentifier("AskCell", forIndexPath: indexPath) as! AskCell
            cell.content = content
            cell.onJoinAsk = {
                if self.content!.isJoined {
                    self.showProgressHUD()
                    let provider = MoyaProvider<NetworkService>.createDefaultProvider()
                    provider.request(NetworkService.UnjoinAsk(deckId: self.content!.id), completion: { (result) in
                        let JSON = Helper.validateResponse(result, sender: self)
                        if JSON != nil {
                            let rContent = ContentNode.init(dictionary: JSON!["deck"] as! [String: AnyObject])
                            self.content = rContent
                            cell.content = rContent
                            self.tableView.reloadData()
                        } else {
                            self.showSimpleAlert(withTitle: "", withMessage: "Unjoin failed")
                        }
                        
                    })
                } else {
                    
                    let total_points = NSUserDefaults.standardUserDefaults().integerForKey("total_points")
                    
                    if total_points < self.content?.prize_to_join {
                        self.showSimpleAlert(withTitle: "Error", withMessage: "Not enough points! 😱")
                        return
                    }
                    
                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("PrizeContributionPopViewController") as! PrizeContributionPopViewController
                    vc.content = self.content
                    vc.joinedCallback = { rContent in
                        self.content = rContent
                        cell.content = rContent
                        self.tableView.reloadData()
                        vc.dismissViewControllerAnimated(true, completion: nil)
                        
                        NSUserDefaults.standardUserDefaults().setInteger((total_points - (self.content?.prize_to_join)!), forKey: "total_points")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                    
                    self.presentViewController(vc, animated: true, completion: nil)
                    // User joined ask. Reload table
                    //                tableView.reloadData()
                }
            }
            askCell = cell
            cell.needsRoundBottom = true
            return cell
        } else if currentMode == .Give {
            if giveCell != nil {
                return giveCell
            }
            let cell = tableView.dequeueReusableCellWithIdentifier("GiveCell", forIndexPath: indexPath) as! GiveCell
            cell.content = self.content!
            cell.onSubmit = {
                if self.content?.isGiver == 0 {
                    
                    let alertController = UIAlertController(title: "Select your submission", message: nil, preferredStyle: .ActionSheet)
                    
                    let draftAction = UIAlertAction(title: "Choose From Drafts", style: .Default) { (action) in
                        self.chooseFromDraft()
                    }
                    alertController.addAction(draftAction)
                    
                    let createAction = UIAlertAction(title: "Create New Card", style: .Default) { (action) in
                        self.createNewCard()
                    }
                    alertController.addAction(createAction)
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
                    }
                    alertController.addAction(cancelAction)
                    
                    self.presentViewController(alertController, animated: true) {
                    }
                    
                    
                } else {
                    
                    // Cancel Submission
                    let gives = self.content?.myGives
                    if gives?.count == 0 {return}
                    let cardId = gives?.first?.id
                    let provider = MoyaProvider<NetworkService>.createDefaultProvider()
                    provider.request(NetworkService.CancelSubmission(cardId: cardId!), completion: { (result) in
                        let JSON = Helper.validateResponse(result, sender: self)
                        if JSON != nil {
                            let rContent = ContentNode(dictionary: JSON!["deck"] as! [String: AnyObject])
                            self.content = rContent
                            self.giveCell.content = rContent
                            self.tableView.reloadData()
                        } else {
                            self.showSimpleAlert(withTitle: "", withMessage: "Failed to submit card.")
                        }
                    })
                }
            }
            giveCell = cell
            cell.needsRoundBottom = true
            return cell
        } else if currentMode == .Prize {
            if prizeCell != nil {
                return prizeCell
            }
            let cell = tableView.dequeueReusableCellWithIdentifier("PrizeCell", forIndexPath: indexPath) as! PrizeCell
            cell.content = self.content
            cell.reloadTable = {
                self.reloadModeCell()
            }
            prizeCell = cell
            cell.needsRoundBottom = true
            return cell
        }
        let cell = tableView.dequeueReusableCellWithIdentifier("ContentTableCell", forIndexPath: indexPath) as! ContentTableCell
        cell.content = self.content!.children[indexPath.row-1]
        
        cell.thumbnailView.tag = indexPath.row - 1
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(CardDetailViewController.showFullScreen))
        cell.thumbnailView.addGestureRecognizer(singleTap)
        cell.needsRoundBottom = rowsNum == indexPath.row+1
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0 {
            return // This is header cell
        }
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if !(cell?.isKindOfClass(ContentTableCell))! {
            return // Other mode cells
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let vc = storyboard?.instantiateViewControllerWithIdentifier("CardDetailViewController") as! CardDetailViewController
        vc.content = content?.children[indexPath.row-1]
        self.presentViewController(vc, animated: true, completion: nil)
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Private methods
    
//    private func updateHeader() {
//        if let creatorImageURL = self.content?.creatorThumbnailURL {
//            self.creatorImage.af_setImageWithURL(creatorImageURL)
//        }
//        self.creatorNameAndDate.text = self.content?.formattedCreatorAndDate
//
//    }
    
    private func setupUI() {
        let cellNib = UINib(nibName: "ContentTableCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: "ContentTableCell")
        
        let headerCellNib = UINib(nibName: "ContentHeaderCell", bundle: nil)
        tableView.registerNib(headerCellNib, forCellReuseIdentifier: "ContentHeaderCell")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = Constants.UIConstants.contentCellHeight
        
//        tableView.tableHeaderView!.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
//        self.creatorNameAndDate.layer.shadowRadius = 0.5
//        self.creatorImage.layer.cornerRadius = 10
        
//        updateHeader()
    }
    
    func refreshData() {
        tableView.reloadData()
    }
    func reloadModeCell () {
        if currentMode != .Play {
            let indexPath = NSIndexPath(forRow: 1, inSection: 0)
            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Bottom)
        }
    }
    func handleHeaderCellAction (action: HeaderCellAction) {
        if action == .Loop {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LoopController") as! LoopController
            vc.content = self.content
            self.presentViewController(vc, animated: true, completion: nil)
        } else if action == .Comment {
//            let vc = storyboard?.instantiateViewControllerWithIdentifier("CommentsViewController") as! CommentsViewController
            let vc = UIStoryboard.loadViewController(storyboardName: "Comment", viewControllerIdentifier: "CommentsViewController") as! CommentsViewController
            vc.content = content
            tabBarController!.presentViewController(vc, animated: true, completion: nil)
        } else if action == .Like {
            let liked = content!.liked_by_me
            let provider = MoyaProvider<NetworkService>.createDefaultProvider()
            let target = liked ? NetworkService.UnlikeCard(cardId : content!.id) : NetworkService.LikeCard(cardId : content!.id)
            
            provider.request(target) { result in
                switch result {
                case let .Success(response):
                    if response.statusCode == 200 {
                        self.content!.liked_by_me = !liked
                        self.content!.likes += liked ? -1 : 1
                        self.tableView.reloadData()
                    }
                case let .Failure(error):
                    print(error)
                }
            }
        } else {
            currentMode = action
            tableView.reloadData()
        }
    }
    
    
    // MARK: - Actions
    
    @IBAction func onBack(sender: AnyObject) {
//        navigationController?.popViewControllerAnimated(true)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onDotsButton(sender: AnyObject) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            // ...
        }
        alertController.addAction(cancelAction)
        
        let unpublishAction = UIAlertAction(title: "Unpublish", style: .Default) { (action) in
            let provider = MoyaProvider<NetworkService>.createDefaultProvider()
            provider.request(.PublishDecks(cardIds: [self.content!.id], publish: false)) { result in
                let JSON = Helper.validateResponse(result, sender: self)
                if JSON != nil {
                    self.navigationController?.popToRootViewControllerAnimated(true)
                }
            }
        }
        alertController.addAction(unpublishAction)
        
        self.presentViewController(alertController, animated: true) {
        }
    }
    
    ////////////////////////////////
    
    func showFullScreen(gestureRecognizer: UIGestureRecognizer) {
        if (content!.parentContent != nil) {
            let fullScreenViewController = storyboard!.instantiateViewControllerWithIdentifier("FullScreenViewController") as! FullScreenViewController
            fullScreenViewController.deckIndex = contentIndex
            fullScreenViewController.cardIndex = gestureRecognizer.view!.tag
            fullScreenViewController.content = content!.parentContent
            fullScreenViewController.modalPresentationStyle = .OverFullScreen
            navigationController?.presentViewController(fullScreenViewController, animated: true, completion: nil)
            
        }
    }
    
    ////////////////////////////////
    
    
    func chooseFromDraft() {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SubmitNav") as! SubmitCardNavigationController
        vc.content = self.content
        
        vc.submittedCallback = { rContent in
            // Card submitted here
            self.content = rContent
            self.giveCell.content = rContent
            self.tableView.reloadData()
            vc.dismissViewControllerAnimated(true, completion: nil)
        }
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func createNewCard() {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SubmitNav") as! SubmitCardNavigationController
        vc.content = self.content
        
        vc.submittedCallback = { rContent in
            // Card submitted here
            self.content = rContent
            self.giveCell.content = rContent
            self.tableView.reloadData()
            vc.dismissViewControllerAnimated(true, completion: nil)
        }
        self.presentViewController(vc, animated: true, completion: nil)

    }

}
