//
//  HeaderTableCell.swift
//  GuideHero
//
//  Created by Yohei Oka on 10/20/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AlamofireImage


class HeaderTableCell: BaseTableViewCell {
    
    // MARK: - Properties
    
    var content: ContentNode! {
        didSet {
            self.updateUI()
        }
    }
    

    @IBOutlet var title: UILabel!
    @IBOutlet var details: UILabel!
    @IBOutlet var cardView: CardContainerView!
    @IBOutlet var backgroundImage: MaskImageView!
    var showLock = false

    
    // MARK: - View lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    // MARK: - Private methods
    
    private func updateUI() {
        
        self.title.text = self.content.name
        self.details.text = self.content.description
        self.cardView.content = content
        backgroundImage.setImageWithUrl(content!.imageURL, rect: content!.maskRect)
    }
    
    func updateLock() {
        if self.showLock {
            let lock = UIImageView(frame: CGRectMake(10, 10, 20, 20))
            lock.image = UIImage(named: "lock")
            lock.tag = 333
            cardView.insertSubview(lock, aboveSubview: cardView)
        } else {
            for view in cardView.subviews{
                if view.tag == 333 {
                    view.removeFromSuperview()
                }
            }
        }
    }
}
