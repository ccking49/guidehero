//
//  CardDetailAskVC.swift
//  GuideHero
//
//  Created by Promising Change on 23/12/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AlamofireImage
import Moya

class CardDetailAskVC: CardSubPageBaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Variables
    
    @IBOutlet weak var tbContent: UITableView!
    
    @IBOutlet weak var footerView : UIView!
    
    var aryTableHeader :NSMutableArray = []
    
    let tbHeaderView: UIView!
    
    var content: ContentNode? {
        didSet {
            if isViewLoaded() {
                tbContent.reloadData()
                self.view.layoutIfNeeded()
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        tbContent.setContentOffset(CGPointMake(tbContent.contentOffset.x, tbContent.contentOffset.y - 1), animated: true)
    }
    // MARK: - View Lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        tbHeaderView = UIView()
        
        super.init(coder: aDecoder)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        scrollView = tbContent as UIScrollView
        
        configureView()
        refreshData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Methods
    
    func configureView() {
        tbHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 540)
        
//        let askerNib = UINib(nibName: "AskerCell", bundle: nil)
//        tbContent.registerNib(askerNib, forCellReuseIdentifier: "AskerCell")
//        
//        let joinNib = UINib(nibName: "AskJoinCell", bundle: nil)
//        tbContent.registerNib(joinNib, forCellReuseIdentifier: "AskJoinCell")
//        
//        let defaultNib = UINib(nibName: "AskDefaultCell", bundle: nil)
//        tbContent.registerNib(defaultNib, forCellReuseIdentifier: "AskDefaultCell")
        
        tbContent.rowHeight = UITableViewAutomaticDimension
        tbContent.estimatedRowHeight = Constants.UIConstants.contentCellHeight
        tbContent.separatorStyle = .None
        
        tbContent.tableHeaderView = tbHeaderView
//        tbContent.tableFooterView = UIView(frame: CGRect.zero)
        
        footerView.layer.cornerRadius = 20
        footerView.layer.shadowRadius = 5
        footerView.layer.shadowOpacity = 0.5
        footerView.layer.shadowOffset = CGSizeZero
        footerView.layer.shadowColor = UIColor.blackColor().CGColor
        footerView.clipsToBounds = true
        
        self.aryTableHeader = ["To", "Format", "Contribution To Prize", "Askers"]
        
        
        self.view.setNeedsLayout()
    }
    
    func refreshData() {
        tbContent.reloadData()
        self.view.layoutIfNeeded()
    }
    
    // MARK: - UITableViewDelegate & UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return aryTableHeader.count
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.aryTableHeader[section] as? String
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var contentHeight : CGFloat = 48
        
        if section == 3 {
            contentHeight = 58
        }
        
        let headerView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: self.view.frame.width, height: contentHeight)))
        
        let headerContentView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: self.view.frame.width, height: 48)))
        
        headerView.clipsToBounds = false
        let label = UILabel(frame: CGRect(x: 30, y: 0, width: self.view.frame.width-60, height: 48))
        label.text = aryTableHeader[section] as? String
        label.textAlignment = NSTextAlignment.Left
        
        headerView.backgroundColor = UIColor.whiteColor()
        headerContentView.backgroundColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.1)
        
        label.textColor = UIColor.blackColor()
        label.font = Helper.proximaNova("Bold", font: 20)
        
        headerContentView.addSubview(label)
        
        headerView.addSubview(headerContentView)
        
        return headerView
        
    }
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UILabel(frame: CGRect(origin: CGPointZero, size: CGSize(width: self.view.frame.width, height: 1)))
        footerView.backgroundColor = UIColor.whiteColor()
        return footerView
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 3 {
            return 58
        }
        return 48
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 || indexPath.section == 1{
            return 51
        } else if indexPath.section == 2 {
            return 135
        } else if indexPath.section == 3 {
            return 30
        }
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            if self.content?.asked_users.count == 0 {
                return 1
            }
            return (self.content?.asked_users.count)!
        }else if section == 1 {
            return 1
        }else if section == 2 {
            return 1
        }else if section == 3 {
            return content!.joined_users.count
        }
        return 1;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var tbCell : UITableViewCell!
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("AskerCell", forIndexPath: indexPath) as! AskerCell
            
            cell.imgAvatar.hidden = true
            cell.lblTime.hidden = true
            cell.avatarWidth.constant = 0
            
            if self.content?.asked_users.count == 0 {
                cell.userView.hidden = true
                cell.NoticeView.hidden = false
                
            }else{
                cell.userView.hidden = false
                cell.NoticeView.hidden = true
                
                cell.content = content
                
                let user = content!.asked_users[indexPath.row]
                cell.imgAvatar.clipsToBounds = true
                cell.imgAvatar.layer.cornerRadius = cell.imgAvatar.frame.size.width/2.0
                cell.imgAvatar.af_setImageWithURL(NSURL(string: user.thumbnail_url)!)
                cell.lblName.text = user.username
                cell.lblTime.text = "• 1h"
                cell.lblAccountType.hidden = true
            }
            
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            
            tbCell = cell
            
        }else if indexPath.section == 1 {
            
            let cell = tableView.dequeueReusableCellWithIdentifier("AskDefaultCell", forIndexPath: indexPath) as! AskDefaultCell
            
            cell.lblTitle.text = self.content!.format
            
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            
            tbCell = cell
            
            
        }else if indexPath.section == 2 {
            
            let cell = tableView.dequeueReusableCellWithIdentifier("AskJoinCell", forIndexPath: indexPath) as! AskJoinCell
            cell.content = content
            cell.lblOriginalPoint.text = String(content!.prize_pool)
            cell.lblJoinPoint.text = String(content!.prize_to_join)
            
            cell.btnJoinUs.roundView()
            
            let joinButtonTitle = content!.isJoined ? "Unjoin The Ask" : "Join The Ask"
            cell.btnJoinUs.setTitle(joinButtonTitle, forState: .Normal)
            cell.btnJoinUs.enabled = !content!.isAskCreator
            
            
            cell.onJoinAsk = {
                if self.content!.isJoined {
                    self.showProgressHUD()
                    let provider = MoyaProvider<NetworkService>.createDefaultProvider()
                    provider.request(NetworkService.UnjoinAsk(deckId: self.content!.id), completion: { (result) in
                        let JSON = Helper.validateResponse(result, sender: self)
                        if JSON != nil {
                            let rContent = ContentNode.init(dictionary: JSON!["deck"] as! [String: AnyObject])
                            self.content = rContent
                            cell.content = rContent
                            self.tbContent.reloadData()
                        } else {
                            self.showSimpleAlert(withTitle: "", withMessage: "Unjoin failed")
                        }
                        
                    })
                } else {
                    
                    let total_points = NSUserDefaults.standardUserDefaults().integerForKey("total_points")
                    
                    if total_points < self.content?.prize_to_join {
                        self.showSimpleAlert(withTitle: "Error", withMessage: "Not enough points! 😱")
                        return
                    }
                    let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "PrizeContributionPopViewController") as! PrizeContributionPopViewController
                    vc.content = self.content
                    vc.joinedCallback = { rContent in
                        self.content = rContent
                        cell.content = rContent
                        self.tbContent.reloadData()
                        vc.dismissViewControllerAnimated(true, completion: nil)
                        
                        NSUserDefaults.standardUserDefaults().setInteger((total_points - (self.content?.prize_to_join)!), forKey: "total_points")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                    
                    self.presentViewController(vc, animated: true, completion: nil)
                }
            }
            
            
            tbCell = cell
            
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("AskerCell", forIndexPath: indexPath) as! AskerCell
            cell.content = content
            
            cell.imgAvatar.clipsToBounds = true
            cell.imgAvatar.layer.cornerRadius = cell.imgAvatar.frame.size.width/2.0
            
            cell.userView.hidden = false
            cell.NoticeView.hidden = true
            
            let user = content!.joined_users[indexPath.row]
            cell.imgAvatar.af_setImageWithURL(NSURL(string: user.thumbnail_url)!)
            cell.lblName.text = user.username
            if Session.sharedSession.user?.user_id == user.user_id {
                cell.lblAccountType.text = "(Original Asker)"
            }else{
                cell.lblAccountType.text = ""
            }
            cell.lblTime.text = "• 1h"
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            
            
            tbCell = cell
            
        }
       
        
        return tbCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Do Nothing
    }

    // MARK: - UIScrollViewDelegate
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
    }
    
}
