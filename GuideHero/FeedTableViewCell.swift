//
//  FeedTableViewCell.swift
//  GuideHero
//
//  Created by forever on 12/9/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya

protocol FeedTableViewCellDelegate {
    func onUIUpdate()
}

class FeedTableViewCell: BaseTableViewCell {

    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPostDate: UILabel!
    @IBOutlet weak var lblUserBio: UILabel!
    @IBOutlet weak var cardView: CardContainerView!
    @IBOutlet weak var lblContentTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var requestImage: UIImageView!
    @IBOutlet weak var lblRequestCount: UILabel!
    @IBOutlet weak var prizeImage: UIImageView!
    @IBOutlet weak var lblPrize: UILabel!
    @IBOutlet weak var navArrow: UIImageView!
    
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var lblAskTitle: UILabel!
    @IBOutlet weak var imgAskUser1: UIImageView!
    @IBOutlet weak var imgAskUser2: UIImageView!
    
    @IBOutlet weak var imgLock: UIImageView!
    @IBOutlet weak var lblTime : UILabel!
    
    @IBOutlet weak var giveImage: UIImageView!
    
    @IBOutlet weak var separateView : UIView!
    
    @IBOutlet weak var btnProfilePhoto : UIButton!
    @IBOutlet weak var btnProfileName : UIButton!
    @IBOutlet weak var btnCheckMark : UIButton!

    @IBOutlet weak var timeDisplayView: UIView! {
        didSet {
            timeDisplayView.backgroundColor = UIColor.timeDisplayBackgroundColor
            timeDisplayView.alpha = 0.8
        }
    }

    @IBOutlet weak var timeDisplayLabel: UILabel! {
        didSet {
            timeDisplayLabel.text = "00:00"
            timeDisplayLabel.textColor = UIColor.whiteColor()
            timeDisplayLabel.font = UIFont(appFont: .standard, size: 15)
        }
    }
    
    var delegate: FeedTableViewCellDelegate?
    
    var content: ContentNode! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.layer.shadowRadius = 1
        self.layer.shadowOffset = CGSizeZero
        self.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
        
        cardView.layer.shadowRadius = 1
        cardView.layer.shadowOffset = CGSizeZero
        cardView.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
        
        userProfileImage.layer.cornerRadius = userProfileImage.frame.size.width / 2
        userProfileImage.layer.masksToBounds = true
        
        imgAskUser1.layer.cornerRadius = imgAskUser1.frame.size.width / 2
        imgAskUser1.layer.masksToBounds = true
        imgAskUser2.layer.cornerRadius = imgAskUser1.frame.size.width / 2
        imgAskUser2.layer.masksToBounds = true
        
    }

    private func updateUI() {

        self.lblContentTitle.text = self.content.name
        self.lblDescription.text = self.content.description
        self.lblLikeCount.text = String(self.content.likes)
        self.lblRequestCount.text = String(self.content.prize_to_join)
        self.lblPrize.text = String(self.content.prize_pool)
        self.lblFullName.text = String(format: "%@ %@", (self.content.creatorInfo?.first_name)!, (self.content.creatorInfo?.last_name)!)
        self.lblUserBio.text = self.content.creatorInfo?.bio
        self.lblUserName.text = self.content.creatorInfo?.username
        self.lblPostDate.text = self.content.formattedCreationDate
        
        self.requestImage.hidden = !self.content.ask_enabled
        self.lblRequestCount.hidden = !self.content.ask_enabled
        
        if self.content.isJoined {
    
            self.btnCheckMark.hidden = false
        }else{
            self.btnCheckMark.hidden = true
        }
        self.prizeImage.hidden = !self.content.ask_enabled
        self.lblPrize.hidden = !self.content.ask_enabled
        self.navArrow.hidden = !self.content.ask_enabled
        self.lblAskTitle.hidden = true
        self.lblTime.hidden = !self.content.ask_enabled
        self.imgAskUser1.hidden = true
        self.imgAskUser2.hidden = true
        
        if self.content.ask_enabled == true {
            self.lblAskTitle.hidden = false
            self.lblAskTitle.text = "Anyone"
            self.imgAskUser1.hidden = true
            self.imgAskUser2.hidden = true
            
        } else {
            if self.content.joined_users.count > 1 {
                self.imgAskUser1.hidden = false
                self.imgAskUser2.hidden = false
                
                var nIndex: Int = 0
                for user in self.content.joined_users {
                    if nIndex == 0 {
                        self.imgAskUser1.af_setImageWithURL(NSURL(string: user.thumbnail_url)!)
                    } else if nIndex == 1{
                        self.imgAskUser2.af_setImageWithURL(NSURL(string: user.thumbnail_url)!)
                        break
                    }
                    nIndex += 1
                }
                
            } else if self.content.joined_users.count == 1 {
//                self.lblAskTitle.hidden = true
                self.imgAskUser1.hidden = false
                self.imgAskUser2.hidden = true
                
                for user in self.content.joined_users {
                    self.imgAskUser1.af_setImageWithURL(NSURL(string: user.thumbnail_url)!)
                    break
                }
                
            } else {
//                self.lblAskTitle.hidden = false
                self.imgAskUser1.hidden = true
                self.imgAskUser2.hidden = true
//                self.lblAskTitle.text = "Anyone"
            }
        }

        
        let endDate = self.content.evaluation_end_dt
        
        if endDate == nil || endDate <= NSDate() {
            lblTime.text = "Ended"
        } else {
            let diff = endDate!.difference(NSDate(), unitFlags: NSCalendarUnit(rawValue: UInt.max))
            let hourDiff = -(diff.hour)
            let minDiff = -(diff.minute)
            lblTime.text = "\(hourDiff)h \(minDiff)m"
        }
        
        if self.content.ask_enabled == true {
            self.imgLock.hidden = true
        } else {
            self.imgLock.hidden = false
        }
        
        if self.content.isGiver == 2 {
            giveImage.hidden = true//false
        }
        else {
            giveImage.hidden = true
        }
        
        cardView.content = content
        
        if let userProfileImageURL = self.content.creatorThumbnailURL {
            self.userProfileImage.af_setImageWithURL(userProfileImageURL)
        } else {
            self.userProfileImage.image = UIImage(named: "me")
        }
        
        let imageName = content.liked_by_me ? "heartFilled" : "heart"
        btnLike.setImage(UIImage(named: imageName), forState: .Normal)
        lblLikeCount.text = String(content.likes)

        self.timeDisplayLabel.text = "\(self.content.deckLength)"

        self.cardView.bringSubviewToFront(self.timeDisplayView)
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onLike(sender: AnyObject) {
        let liked = self.content!.liked_by_me
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        let target = liked ? NetworkService.UnlikeCard(cardId : self.content!.id) : NetworkService.LikeCard(cardId : self.content!.id)
        
        provider.request(target) { result in
            switch result {
            case let .Success(response):
                print(response)
                if response.statusCode == 200 {
                    self.content!.liked_by_me = !liked
                    self.content!.likes += liked ? -1 : 1
                    let imageName = self.content.liked_by_me ? "heartFilled" : "heart"
                    self.btnLike.setImage(UIImage(named: imageName), forState: .Normal)
                    self.lblLikeCount.text = String(self.content.likes)
                    if (self.delegate != nil) {
                        self.delegate!.onUIUpdate()
                    }
                }
            case let .Failure(error):
                print(error)
            }
        }
    }
    
    
    @IBAction func onMore(sender: AnyObject) {
        
    }
    
}
