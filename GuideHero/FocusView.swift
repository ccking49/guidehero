//
//  FocusView.swift
//  GuideHero
//
//  Created by Star on 12/30/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

class FocusView: UIView {
    
    lazy var strokeColor = UIColor(colorLiteralRed: 1, green: 0.7, blue: 0, alpha: 1)

    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        
        self.strokeColor.set()
        
        let frame = rect
        
//        let outerRectPath = UIBezierPath(ovalInRect: CGRectInset(frame, 1, 1))
//        outerRectPath.lineWidth = 1
//        outerRectPath.stroke()
        
        let outerRectPath = UIBezierPath(rect: CGRectInset(frame, 1, 1))
        outerRectPath.lineWidth = 1
        outerRectPath.stroke()
        
        let line1 = UIBezierPath()
        line1.moveToPoint(CGPointMake(CGRectGetMidX(frame), CGRectGetMinY(frame)))
        line1.addLineToPoint(CGPointMake(CGRectGetMidX(frame), CGRectGetMinY(frame) + 5))
        line1.lineWidth = 1;
        line1.stroke()
        
        let line2 = UIBezierPath()
        line2.moveToPoint(CGPointMake(CGRectGetMaxX(frame), CGRectGetMidY(frame)))
        line2.addLineToPoint(CGPointMake(CGRectGetMaxX(frame) - 5, CGRectGetMidY(frame)))
        line2.lineWidth = 1;
        line2.stroke()
        
        let line3 = UIBezierPath()
        line3.moveToPoint(CGPointMake(CGRectGetMidX(frame), CGRectGetMaxY(frame)))
        line3.addLineToPoint(CGPointMake(CGRectGetMidX(frame), CGRectGetMaxY(frame) - 5))
        line3.lineWidth = 1;
        line3.stroke()
        
        let line4 = UIBezierPath()
        line4.moveToPoint(CGPointMake(CGRectGetMinX(frame), CGRectGetMidY(frame)))
        line4.addLineToPoint(CGPointMake(CGRectGetMinX(frame) + 5, CGRectGetMidY(frame)))
        line4.lineWidth = 1;
        line4.stroke()
    }

}
