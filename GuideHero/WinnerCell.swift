//
//  WinnerCell.swift
//  GuideHero
//
//  Created by HC on 12/14/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

class WinnerCell: BaseTableViewCell {

    @IBOutlet var rankLabel: UILabel!
    @IBOutlet var likesLabel: UILabel!
    @IBOutlet var prizeLabel: UILabel!
    @IBOutlet var winnerLabel: UILabel!
    @IBOutlet var winnerImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        winnerImage.roundView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populateWinnerWithUser(user: UserNode, indexPath: NSIndexPath) {
        emptyLabels()
        winnerImage.af_setImageWithURL(NSURL(string: user.thumbnail_url)!)
        winnerLabel.text = user.username
        prizeLabel.text = String(user.contribution)
        rankLabel.text = String(indexPath.row+1)
        likesLabel.text = String(87 - 11 * indexPath.row)
    }
    
    func emptyLabels () {
        rankLabel.text = ""
        likesLabel.text = ""
        prizeLabel.text = ""
        winnerLabel.text = ""
    }
}
