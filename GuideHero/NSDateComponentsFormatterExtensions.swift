//
//  NSDateComponentsFormatterExtensions.swift
//  GuideHero
//
//  Created by Ignacio Rodrigo on 10/17/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import Foundation

extension NSDateComponentsFormatter {

    @nonobjc static let sharedFormatter = NSDateComponentsFormatter.createSharedFormatter()

    static func createSharedFormatter() -> NSDateComponentsFormatter {
        let formatter = NSDateComponentsFormatter()
        formatter.unitsStyle = .Abbreviated
        formatter.allowedUnits = [.Year, .Month, .WeekOfMonth, .Day, .Hour, .Minute]
        formatter.maximumUnitCount = 1
        formatter.zeroFormattingBehavior = .DropAll
        return formatter
    }

}
