//
//  CardDraftMakeAskVC.swift
//  GuideHero
//
//  Created by Promising Change on 31/01/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit
import Moya

class CardDraftMoveVC: UIViewController,
UITableViewDelegate, UITableViewDataSource  , UIGestureRecognizerDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView : UIView!
    @IBOutlet var extraToolbar: UIToolbar!
    
    @IBOutlet var closeButton : UIButton!
    
    @IBOutlet var footerView : UIView!
    @IBOutlet var footerBottomView : UIView!
    @IBOutlet var whoolView : UIView!
    @IBOutlet weak var imgBackground : UIImageView!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var btnCancel : UIButton!
    
    var lastIndex = -1
    
    var nSubFlag : Int = 0
    
    var drafts = [ContentNode]()
    
    var selectedDraft: ContentNode? {
        didSet {
            if isViewLoaded() {
                // Update the Content
                
            }
        }
    }
    
    var pan = UIPanGestureRecognizer()
    var isRotating = false
    var startPos = CGPointZero
    var startCenter = CGPointZero
    
    var submittedCallback: ((_: [ContentNode]! , _:Int!) -> Void)!
    
    
    // MARK: UIViewController Lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(DraftsViewController.changedTable(_:)),
                                                         name: "changedTable",
                                                         object: nil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cellNib = UINib(nibName: "DraftCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: "DraftCell")
        
        let cellHereNib = UINib(nibName: "CardDraftMoveHereCell", bundle: nil)
        tableView.registerNib(cellHereNib, forCellReuseIdentifier: "CardDraftMoveHereCell")
        
        
        btnCancel.layer.shadowColor = UIColor.blackColor().CGColor
        btnCancel.layer.shadowOffset = CGSizeMake(0, 1)
        btnCancel.layer.shadowOpacity = 0.5
        btnCancel.layer.shadowRadius = 4
        
//        onSetupPanel()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        
        footerBottomView.layer.cornerRadius = 20
        footerBottomView.layer.shadowRadius = 5
        footerBottomView.layer.shadowOpacity = 0.5
        footerBottomView.layer.shadowOffset = CGSizeZero
        footerBottomView.layer.shadowColor = UIColor.blackColor().CGColor
        footerBottomView.clipsToBounds = true
        
        if (self.drafts.count) * 216 < Int(UIScreen.mainScreen().bounds.size.height - self.headerView.frame.size.height){
            self.footerView.frame.size = CGSizeMake(UIScreen.mainScreen().bounds.size.width, (UIScreen.mainScreen().bounds.size.height - self.headerView.frame.size.height)-CGFloat((self.drafts.count) * 216))
        }else{
            self.footerView.frame.size = CGSizeMake(0, 50)
        }
        
        tableView.allowsMultipleSelection = false
        extraToolbar.hidden = true
        
        if Session.sharedSession.user != nil {
            self.imgProfile.af_setImageWithURL(NSURL(string: (Session.sharedSession.user?.thumbnail_url)!)!)
            self.imgProfile.layer.cornerRadius = (self.imgProfile.frame.size.height - 2) / 2
            
            self.imgProfile.layer.masksToBounds = true
            self.lblUsername.text = Session.sharedSession.user?.username
        }
        
        
        self.showSimpleAlert(withTitle: "Warning", withMessage: "Choose a location and tap on   “👇 Here 👇” to move the item!")
    }
    
    override func viewWillDisappear(animated: Bool) {
        resetSelection()
        //        UIApplication.sharedApplication().statusBarHidden = false
        super.viewWillDisappear(animated);
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().statusBarHidden = true
        
    }
    
    // MARK: Content Load
    func changedTable(notification: NSNotification) {
        
    }
    
    
    // MARK: - Cell Selection
    
    
    // MARK: - Button Press
    
    
    func resetSelection() {
        
        if let indexPaths = tableView.indexPathsForSelectedRows {
            for indexPath in indexPaths {
                tableView.deselectRowAtIndexPath(indexPath, animated: true)
            }
        }
        tableView.allowsMultipleSelection = false
        //        navigationController?.navigationBar.barTintColor = UIColor.blackColor()
        
        extraToolbar.hidden = true
        
        closeButton.hidden = false
        
    }
    
    
    
    @IBAction func onPressBack(sender:AnyObject){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK: - UITableView
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 216
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.drafts.count+1 ?? 1
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("CardDraftMoveHereCell", forIndexPath: indexPath) as! CardDraftMoveHereCell
            
            cell.borderView.layer.cornerRadius = 7
            cell.borderView.layer.borderColor = UIColor.colorFromRGB(redValue: 254, greenValue: 40, blueValue: 232, alpha: 1.0).CGColor
            cell.borderView.layer.borderWidth = 3
            
            cell.btnHere.addTarget(self, action: #selector(onTapHere(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            return cell
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("CardDraftCell", forIndexPath: indexPath) as! CardDraftCell
            
            cell.content = self.drafts[indexPath.row-1]
            
            return cell
        }
        
    }
    func onResetDraftData(nContent : ContentNode){
        
        if nContent.children.count != 0 {
            
            var nnIndex = 0
            for itemContent in nContent.children {
                
                if itemContent.id == self.selectedDraft?.id {
                    
                    
                    nContent.children.removeAtIndex(nnIndex)
                    self.nSubFlag = 1
                    
                    
                    break
                }
                
                nnIndex = nnIndex + 1
                
            }
            if self.nSubFlag == 0 {
                for itemContent in nContent.children {
                    if itemContent.children.count != 0 {
                        
                        if self.nSubFlag == 1 {
                            break
                        }else{
                            self.onResetDraftData(itemContent)
                        }
                    }
                }
                
            }
        }
        
    }
    @IBAction func onTapHere (sender : AnyObject){
        
        self.showProgressHUD()
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()

        provider.request(NetworkService.ChangeOrder(card_id: (self.selectedDraft?.id)!, position: 0), completion: { (result) in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't move draft, please try again later")
                    self.hideProgressHUD()
                    return
                }
                if (try? response.mapJSON() as! [String: AnyObject]) != nil {
                    
                    self.hideProgressHUD()
                    
                    
//                    var nIndex : Int = 0
//                    
//                    for itemContent in self.drafts {
//                        
//                        if itemContent.id == self.selectedDraft?.id {
//                            self.drafts.removeAtIndex(nIndex)
//                            break
//                        }
//                        nIndex = nIndex + 1
//                    }
                    var nnIndex = 0
                    
                    for itemContent in self.drafts {
                        
                        if itemContent.id == self.selectedDraft?.id {
                            
                            self.drafts.removeAtIndex(nnIndex)
                            self.nSubFlag = 1
                            break
                        }
                        if self.nSubFlag == 0 {
                            self.onResetDraftData(itemContent)
                        }else{
                            break
                        }
                        
                        
                        nnIndex = nnIndex + 1
                    }
                    
                    self.drafts.insert(self.selectedDraft!, atIndex: 0)
                    
                    self.dismissViewControllerAnimated(true) {
                        self.submittedCallback(self.drafts , 0)
                    }
                    
                    
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't move draft, please try again later")
                    self.hideProgressHUD()
                }
            case let .Failure(error):
                
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't move draft, please try again later")
                self.hideProgressHUD()
            }
            
            
        })
//        var nIndex : Int = 0
//
//        for itemContent in self.drafts {
//
//            if itemContent.id == self.selectedDraft?.id {
//
//
//                self.showProgressHUD()
//
//                let provider = MoyaProvider<NetworkService>.createDefaultProvider()
//                var cardIds = [String]()
//                cardIds.append((self.selectedDraft?.id)!)
//                
//                provider.request(NetworkService.ChangeOrder(card_id: (self.selectedDraft?.id)!, position: 0), completion: { (result) in
//                    switch result {
//                    case let .Success(response):
//                        if response.statusCode != 200 {
//                            self.showSimpleAlert(withTitle: "Error", withMessage: "Can't move draft, please try again later")
//                            self.hideProgressHUD()
//                            return
//                        }
//                        if (try? response.mapJSON() as! [String: AnyObject]) != nil {
//                            
//                            self.hideProgressHUD()
//                            
//                            self.drafts.removeAtIndex(nIndex)
//                            self.drafts.insert(self.selectedDraft!, atIndex: 0)
//                            
//                            self.dismissViewControllerAnimated(true) {
//                                self.submittedCallback(self.drafts , 0)
//                            }
//                            
//                            
//                        }
//                        else {
//                            self.showSimpleAlert(withTitle: "Error", withMessage: "Can't move draft, please try again later")
//                            self.hideProgressHUD()
//                        }
//                    case let .Failure(error):
//                        
//                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't move draft, please try again later")
//                        self.hideProgressHUD()
//                    }
//
//                    
//                })
//                
//                break
//            }
//            nIndex = nIndex + 1
//        }
        
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        lastIndex = indexPath.row-1
        
        let vc = UIStoryboard.loadViewController(storyboardName: "CardDraft", viewControllerIdentifier: "CardDraftMoveAskEnabledDetailVC") as! CardDraftMoveAskEnabledDetailVC
        
        vc.content = self.drafts[indexPath.row-1]
        
        vc.drafts = self.drafts
        vc.delegate = self
        vc.selectedDraft = self.selectedDraft
        vc.submittedCallback = {rContent , nType in
            self.dismissViewControllerAnimated(true) {
                self.submittedCallback(rContent , nType)
            }
        }
        
        presentViewController(vc, animated: true, completion: nil)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func onSetupPanel(){
        
        pan.addTarget(self, action: #selector(PointViewController.handlePan(_:)))
        pan.delegate = self
        whoolView.addGestureRecognizer(pan)
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        let velocity = pan.velocityInView(self.tableView?.superview)
        
        if fabs(velocity.x) >= fabs(velocity.y) || (tableView.contentOffset.y <= 0 && velocity.y > 0) {
            return true
        }
        
        return false
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if otherGestureRecognizer is UIPanGestureRecognizer {
            return true
        }
        
        return false
    }
    
    func handlePan(panGesture: UIPanGestureRecognizer){
        
        let factor: CGFloat = 2
        let angleLimit = CGFloat(M_PI_4)
        
        switch panGesture.state {
        case .Began:
            
            startPos = panGesture.locationInView(self.whoolView.superview)
            startCenter = self.whoolView.center
            print(startPos)
            let velocity = panGesture.velocityInView(self.whoolView.superview)
            
            isRotating = fabs(velocity.x) > fabs(velocity.y)
            
        case .Changed:
            
            if isRotating {
                
                let pos = panGesture.locationInView(self.view?.superview)
                
                let rotationAngle = atan((pos.x - startPos.x) / (self.view.bounds.size.height * factor - pos.y))
                var direction:CGFloat = 1
                if pos.x != startPos.x {
                    direction = (startPos.x - pos.x) / fabs(pos.x - startPos.x)
                }
                imgBackground.alpha = 0.5 - fabs(rotationAngle)
                
                var transform = CGAffineTransformMakeTranslation(self.view.bounds.size.width * 0.5 * direction, self.view.bounds.size.height * (factor - 0.5))
                transform = CGAffineTransformRotate(transform, rotationAngle)
                transform = CGAffineTransformTranslate(transform, -self.view.bounds.size.width * 0.5 * direction, -self.view.bounds.size.height * (factor - 0.5))
                self.whoolView.transform = transform;
                
            } else {
                let pos = panGesture.locationInView(self.view?.superview)
                self.whoolView.center = CGPointMake(startCenter.x, startCenter.y + pos.y - startPos.y)
            }
            
        case .Ended:
            
            if isRotating {
                let pos = panGesture.locationInView(self.whoolView?.superview)
                let velocity = panGesture.velocityInView(self.whoolView?.superview)
                var startDirection = 1
                if pos.x != startPos.x {
                    startDirection = Int(round((pos.x - startPos.x) / fabs(pos.x - startPos.x)))
                }
                var velocityDirection = -startDirection
                if velocity.x != 0 {
                    velocityDirection = Int(round(velocity.x / fabs(velocity.x)))
                }
                if startDirection == velocityDirection { // should dismiss
                    UIView.animateWithDuration(0.3, animations: {
                        
                        var transform = CGAffineTransformMakeTranslation(-self.whoolView.bounds.size.width * 0.5 * CGFloat(startDirection), self.whoolView.bounds.size.height * (factor - 0.5))
                        transform = CGAffineTransformRotate(transform, angleLimit * CGFloat(startDirection))
                        transform = CGAffineTransformTranslate(transform, self.whoolView.bounds.size.width * 0.5 * CGFloat(startDirection), -self.whoolView.bounds.size.height * (factor - 0.5))
                        
                        self.whoolView.transform = transform
                        self.imgBackground.alpha = 0
                        
                        }, completion: { completed in
                            self.dismissViewControllerAnimated(false, completion: nil)
                    })
                } else {
                    UIView.animateWithDuration(0.2, animations: {
                        self.whoolView.transform = CGAffineTransformIdentity
                        self.imgBackground.alpha = 0.5
                        }, completion: { completed in
                    })
                }
                
            } else {
                let velocity = panGesture.velocityInView(self.whoolView?.superview)
                if velocity.y > 0 { // should dismiss
                    UIView.animateWithDuration(0.2, animations: {
                        self.whoolView.center = CGPointMake(self.startCenter.x, self.whoolView.bounds.size.height * 1.5)
                        self.imgBackground.alpha = 0
                        }, completion: { completed in
                            self.dismissViewControllerAnimated(false, completion: nil)
                    })
                } else {
                    UIView.animateWithDuration(0.2, animations: {
                        self.whoolView.center = self.startCenter
                        }, completion: { completed in
                    })
                }
            }
            
            
        default:
            break
        }
    }
    
    @IBAction func onMore(sender:AnyObject){
        
    }
    
    @IBAction func onCancel(sender : AnyObject){
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

// MARK: - CardDraftMoveAskEnabledDetailVCDelegate

extension CardDraftMoveVC: CardDraftMoveAskEnabledDetailVCDelegate {
    func cardDraftDetailContentWasUpdated(content: ContentNode) {
        // Update previously selected draft with new content, from Make an Ask or Edit.
        if (lastIndex != -1) {
            self.drafts[lastIndex] = content
        }
    }
    func contentReloadData(){
        self.tableView.reloadData()
    }
}

