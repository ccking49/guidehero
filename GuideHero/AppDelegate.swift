//
//  AppDelegate.swift
//  GuideHero
//
//  Created by Kevin Whinnery on 12/16/15.
//  Copyright © 2015 Twilio. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Alamofire
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        
        return FBSDKApplicationDelegate.sharedInstance().application(
            application,
            openURL: url,
            sourceApplication: sourceApplication,
            annotation: annotation) ||
            GIDSignIn.sharedInstance().handleURL(url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        if (configureError != nil) {
        }

        FBSDKApplicationDelegate.sharedInstance().application(
            application, didFinishLaunchingWithOptions: launchOptions)
        
        GlobalAppearance.setAppearance()
        
        registerForPushNotifications(application)

        // NOTE: this is a workaround for downloading amazon S3 images with AlamofireImage.
        // Amazon S3 is returning a non-standard content-type.
        Request.addAcceptableImageContentTypes(["binary/octet-stream"])
        
        IQKeyboardManager.sharedManager().enable = true
        
        //UITabBar appearance
//        UITabBar.appearance().translucent = false
//        UITabBar.appearance().barTintColor = UIColor.redColor()
//        UITabBar.appearance().tintColor = UIColor.whiteColor()
        
        return true
    }
    
    func registerForPushNotifications(application: UIApplication) {
        let notificationSettings = UIUserNotificationSettings(
            forTypes: [.Badge, .Sound, .Alert], categories: nil)
        application.registerUserNotificationSettings(notificationSettings)
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != .None {
            application.registerForRemoteNotifications()
        }
    }
    
    // push notification delegate methods
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let trimEnds = deviceToken.description.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "<>"))
        let cleanToken = trimEnds.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch)
        
        let prefs = NSUserDefaults.standardUserDefaults()
        prefs.setValue(cleanToken, forKey: "guidehero.devicetoken")
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.removeObjectForKey("guidehero.devicetoken")
        userDefaults.synchronize()
    }
        
    func application(application: UIApplication,  didReceiveRemoteNotification userInfo: [NSObject : AnyObject],  fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        let notification = NSNotification(name:"my_push", object: nil)
        NSNotificationCenter.defaultCenter().postNotification(notification)
        if application.applicationState == .Inactive || application.applicationState == .Background {
            for i in 1...5 {
                let notification = UILocalNotification()
                notification.applicationIconBadgeNumber = 0
                notification.category = "Incoming Call"
                notification.alertBody = "Incoming Call"
                notification.hasAction = false
                notification.soundName = "callingsingle.caf"
                notification.fireDate = NSDate(timeIntervalSinceNow: Double(i) * 2.5)
                UIApplication.sharedApplication().scheduleLocalNotification(notification)
            }
            NSThread.sleepForTimeInterval(1)
        }
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
        UIApplication.sharedApplication().cancelAllLocalNotifications()
    }

}

