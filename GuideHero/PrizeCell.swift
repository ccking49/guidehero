//
//  PrizeCell.swift
//  GuideHero
//
//  Created by HC on 11/26/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya


class PrizeCell: BaseTableViewCell, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var sponsorsButton: UIButton!
    @IBOutlet var winnersButton: UIButton!
    @IBOutlet var prizeLabel: UILabel!
    @IBOutlet var forLabel: UILabel!
    @IBOutlet var distributeLabel: UILabel!
    @IBOutlet var startTimeLabel: UILabel!
    @IBOutlet var endTimeLabel: UILabel!
    @IBOutlet var leftTimeLabel: UILabel!
    
    @IBOutlet var sponsorsView: UIView!
    @IBOutlet var sponsorsTable: UITableView!
    
    @IBOutlet var winnersView: UIView!
    @IBOutlet var winnersTable: UITableView!
    
    
    var content: ContentNode! {
        didSet {
            updateUI()
        }
    }
    
    var reloadTable: (() -> Void)!
    var showingSponsors = false
    var showingWinners = false
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        sponsorsButton.roundView()
        winnersButton.roundView()
        
        let cellNib = UINib(nibName: "UserBasicCell", bundle: nil)
        sponsorsTable.registerNib(cellNib, forCellReuseIdentifier: "UserBasicCell")
        sponsorsTable.delegate = self
        sponsorsTable.dataSource = self
        
        let cellNib1 = UINib(nibName: "WinnerCell", bundle: nil)
        winnersTable.registerNib(cellNib1, forCellReuseIdentifier: "WinnerCell")
        winnersTable.delegate = self
        winnersTable.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateUI () {
        prizeLabel.text = "\(content.prize_pool) pts"
        if content.distribution_for == nil {
            forLabel.hidden = true
        } else {
            let forNum = content.distribution_for!
            forLabel.text = "Top \(forNum) Gives ranked by number of Likes*."
        }
        if content.distribution_rule == Constants.ApiConstants.prizeDistributionEvenly {
            distributeLabel.text = "Split evenly"
        } else {
            distributeLabel.text = "Split proportionally to %Likes*."
        }
        
        let startDate = content.evaluation_start_dt
        startTimeLabel.text = startDate?.stringWithFormat(Constants.Literals.localDateFormat)
        let endDate = content.evaluation_end_dt
        endTimeLabel.text = endDate?.stringWithFormat(Constants.Literals.localDateFormat)
        if endDate == nil || endDate <= NSDate() {
            leftTimeLabel.text = "Ended"
        } else {
            let diff = endDate!.difference(NSDate(), unitFlags: NSCalendarUnit(rawValue: UInt.max))
            let hourDiff = -(diff.hour)
            let minDiff = -(diff.minute)
            leftTimeLabel.text = "\(hourDiff)h \(minDiff)m Left"
        }
        
        sponsorsTable.reloadData()
    }
    
    @IBAction func onSeeSponsors(sender: UIButton) {
        showingSponsors = !showingSponsors
        sponsorsView.hidden = !showingSponsors
        reloadTable()
    }

    @IBAction func onSeeWinners(sender: AnyObject) {
        showingWinners = !showingWinners
        winnersView.hidden = !showingWinners
        reloadTable()
        
        
        /*
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        
        if content.evaluation_period_status == .Done {
            provider.request(.GetWinners(deckId: self.content.id), completion: { (result) in
                let JSON1 = Helper.validateResponse(result)
                if JSON1 != nil {
                    print(JSON1)
                }
            })
        } else { //if content.evaluation_period_status == .Open
            provider.request(.EndEvaluationPeriod(deckId: content.id), completion: { (result) in
                let JSON = Helper.validateResponse(result)
                //                if JSON != nil {
                provider.request(.GetWinners(deckId: self.content.id), completion: { (result) in
                    let JSON1 = Helper.validateResponse(result)
                    if JSON1 != nil {
                        print(JSON1)
                    }
                })
                //                }
            })
        }*/
        
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.isEqual(sponsorsTable) {
            let count = content.joined_users.count
            return count
        } else {
            let count = content.joined_users.count
            return count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView.isEqual(sponsorsTable) {
            let cell = tableView.dequeueReusableCellWithIdentifier("UserBasicCell") as! UserBasicCell
            let user = content.joined_users[indexPath.row]
            cell.populateContributionWithUser(user)
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("WinnerCell") as! WinnerCell
            let user = content.joined_users[indexPath.row]
            cell.populateWinnerWithUser(user, indexPath: indexPath)
            return cell
        }
    }
}
