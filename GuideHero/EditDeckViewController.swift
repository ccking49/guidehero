
//
//  EditDeckViewController.swift
//  GuideHero
//
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AlamofireImage
import Moya
import AVFoundation

enum EditingStatus: String {
    case Select = "select"
    case Edit = "edit"
    case Normal = "normal"
}


class EditDeckViewController: CardViewPagerController, CardViewPagerDataSource, CardViewPagerProgressDelegate, CardDetailButtonDelegate, UIGestureRecognizerDelegate, PannableCardViewDelegate , MoreDelegate  {
    
    // MARK: - Variables for Card View Pager
    
    
    @IBOutlet weak var vwMain: UIView!
    
    @IBOutlet weak var vwDraggableView: PannableCardView!
    
    @IBOutlet weak var vwHeader: UIView!
    @IBOutlet weak var deckBackground: UIImageView!
    @IBOutlet weak var stackViewActions: UIStackView!
    @IBOutlet weak var cardActionPlay: CardDetailButton!
    @IBOutlet weak var cardActionAsk: CardDetailButton!
    @IBOutlet weak var cardActionGive: CardDetailButton!
    @IBOutlet weak var cardActionPrize: CardDetailButton!
    
    @IBOutlet weak var constraintHeaderTop: NSLayoutConstraint!
    @IBOutlet weak var constraintActionsTop: NSLayoutConstraint!
    @IBOutlet weak var constraintContentBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintSocialTop: NSLayoutConstraint!
    
    @IBOutlet weak var askWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var giveWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var prizeWidthConstraint: NSLayoutConstraint!
    
    var editingStatus = EditingStatus.Normal
    var newChildrenOrdering = [ContentNode]()
    
    var originalHeaderTop: CGFloat?
    var originalActionsTop: CGFloat?
    var originalSocialTop: CGFloat?
    
    var cardDetailPlayVC: CardDetailPlayVC?
    var cardDetailAskVC: CardDetailAskVC?
    var cardDetailGiveVC: CardDetailGiveVC?
    var cardDetailPrizeVC: CardDetailPrizeVC?
    
    var panGestureRecognizer = UIPanGestureRecognizer()
    var tapGestureRecognizer = UITapGestureRecognizer()
    
    // MARK: - Variables for Card Header
    
    //    @IBOutlet weak var backgroundImage: MaskImageView!
    @IBOutlet weak var backgroundImage: MaskImageView!
    @IBOutlet weak var creatorImage: UIImageView!
    @IBOutlet weak var creatorNameAndDate: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var imgCard : MaskImageView!
    @IBOutlet weak var deckTitle: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentsLabel: UILabel!
    
    @IBOutlet weak var actionView : UIView!
    
    @IBOutlet weak var imgLine1 : UIImageView!
    //    @IBOutlet weak var imgLine2 : UIImageView!
    
    @IBOutlet weak var editTopView: UIView!
    
    
    var cardActionButtons: [CardDetailButton] = []
    
    var content: ContentNode? {
        
        didSet {
            if isViewLoaded() {
                // Update the Content
                
                cardDetailPlayVC?.content = self.content
                cardDetailAskVC?.content = self.content
                cardDetailGiveVC?.content = self.content
                cardDetailPrizeVC?.content = self.content
            }
            
            
            
        }
    }
    
    var cardId: String? // To load specific deck/card
    var contentIndex: Int = 0
    var onHeaderAction: ((HeaderCellAction) -> Void)!
    
    // MARK: - View Lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        datasource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        datasource = self
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.sharedApplication().statusBarHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        configureView()
        
        panGestureRecognizer.addTarget(self, action: #selector(CardDetailPageVC.handlePan(_:)))
        panGestureRecognizer.delegate = self
        vwDraggableView.addGestureRecognizer(panGestureRecognizer)
        
        tapGestureRecognizer.addTarget(self, action: #selector(CardDetailPageVC.handleTap(_:)))
        tapGestureRecognizer.delegate = self
        vwDraggableView.addGestureRecognizer(tapGestureRecognizer)
        
        imgLine1.layer.shadowRadius = 2
        //        imgLine1.layer.shadowOpacity = 0.5
        imgLine1.layer.shadowOffset = CGSizeZero
        imgLine1.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 0.5).CGColor
        
        //        imgLine2.layer.shadowRadius = 2
        ////        imgLine2.layer.shadowOpacity = 0.5
        //        imgLine2.layer.shadowOffset = CGSizeZero
        //        imgLine2.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 0.5).CGColor
        
        imgCard.layer.shadowRadius = 2
        //        imgCard.layer.shadowOpacity = 0.5
        imgCard.layer.shadowOffset = CGSizeZero
        imgCard.layer.shadowColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.5).CGColor
        
        backgroundImage.layer.shadowRadius = 2
        //        backgroundImage.layer.shadowOpacity = 0.5
        backgroundImage.layer.shadowOffset = CGSizeZero
        backgroundImage.layer.shadowColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.5).CGColor
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        super.viewWillAppear(animated)
        
        UIApplication.sharedApplication().statusBarHidden = true
        
        containerView.bringSubviewToFront(actionView)
        
        if self.content == nil {
            self.fetchAllDecks()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Methods
    
    func configureView() {
        originalHeaderTop = -1000
        originalActionsTop = -1000
        originalSocialTop = -1000
        
        // Card View Pager
        vwHeader.roundTopCorners(20)
        
        let cardActionInfoPlay: CardDetailButtonInfo = CardDetailButtonInfo(Id: 0, title: "Play", image: UIImage(named: "playOff"), selectedImage: UIImage(named: "playOn"), tintColor: UIColor(hexString: "#FE2851"))
        var cardActionInfoAsk: CardDetailButtonInfo = CardDetailButtonInfo(Id: 1, title: "Ask", image: UIImage(named: "requestOff"), selectedImage: UIImage(named: "requestOn"), tintColor: UIColor(hexString: "#FFCD00"))
        var cardActionInfoGive: CardDetailButtonInfo = CardDetailButtonInfo(Id: 2, title: "Give", image: UIImage(named: "offerOff"), selectedImage: UIImage(named: "offerOn"), tintColor: UIColor(hexString: "#BD10E0"))
        var cardActionInfoPrize: CardDetailButtonInfo = CardDetailButtonInfo(Id: 3, title: "Prize", image: UIImage(named: "prizeOff"), selectedImage: UIImage(named: "prizeOn"), tintColor: UIColor(hexString: "#0076FF"))
        
        cardActionPlay.bindItem(cardActionInfoPlay)
        cardActionPlay.delegate = self
        cardActionButtons.append(cardActionPlay)
        cardActionPlay.selectButton()
        
        cardActionInfoAsk.Id = cardActionButtons.count
        if (content?.ask_enabled == true) {
            cardActionAsk.bindItem(cardActionInfoAsk)
            cardActionAsk.delegate = self
            cardActionButtons.append(cardActionAsk)
        }
        else {
            //            cardActionAsk.hidden = true
            askWidthConstraint.constant = 0
        }
        
        cardActionInfoGive.Id = cardActionButtons.count
        if (content?.ask_enabled == true) {
            cardActionGive.bindItem(cardActionInfoGive)
            cardActionGive.delegate = self
            cardActionButtons.append(cardActionGive)
        }
        else {
            //            cardActionAsk.hidden = true
            giveWidthConstraint.constant = 0
        }
        
        cardActionInfoPrize.Id = cardActionButtons.count
        if (content?.ask_enabled == true) {
            cardActionInfoPrize.Id = cardActionButtons.count
            cardActionPrize.bindItem(cardActionInfoPrize)
            cardActionPrize.delegate = self
            cardActionButtons.append(cardActionPrize)
        }
        else {
            //            cardActionPrize.hidden = true
            prizeWidthConstraint.constant = 0
        }
        
        view.layoutIfNeeded()
        
        // Card View Header
        imgCard.clipsToBounds = true
        imgCard.layer.cornerRadius = 5
        
        
        
        //        cardView.isInList = false
        //        cardView.content = self.content
        print(content?.thumbnailURL)
        if content?.thumbnailURL != nil {
            backgroundImage.setImageWithUrlFromCardDetail(NSURL(string: content!.thumbnailURL!))
            imgCard.setImageWithUrlWhiteBackground(NSURL(string: content!.thumbnailURL!) , rect: content!.maskRect)
        }else{
            backgroundImage.setImageWithUrlFromCardDetail(content?.imageURL)
            imgCard.setImageWithUrlWhiteBackground(content?.imageURL , rect: content!.maskRect)
        }
        self.descriptionLabel.text = self.content?.description
        self.deckTitle.text = self.content?.name
        
        if let creatorImageURL = content?.creatorThumbnailURL {
            self.creatorImage.af_setImageWithURL(creatorImageURL)
        }
        self.creatorNameAndDate.text = content?.formattedCreatorAndDate
        self.creatorNameAndDate.layer.shadowRadius = 0.5
        self.creatorImage.layer.cornerRadius = 10
        
        let likes = content?.likes ?? 0
        likesLabel.text = String(likes)
        let imageName = (self.content!.liked_by_me == false) ? "grey_heart" : "heartFilled"
        self.likeButton.setImage(UIImage(named: imageName), forState: .Normal)
        let comments = content?.comments.count ?? 0
        commentsLabel.text = String(comments)
        
        onHeaderAction = { action in
            self.handleHeaderCellAction(action)
        }
        
        vwDraggableView.delegate = self
        
        deckBackground.hidden = self.content!.children.count == 0
        if (self.content!.children.count == 0) {
            imgCard.layer.borderColor = UIColor(red: 175.0 / 255, green: 175.0 / 255, blue: 175.0 / 255, alpha: 1.0).CGColor
            imgCard.layer.borderWidth = 1.0
        }
        else {
            deckTitle.text = self.content!.children[0].name
            descriptionLabel.text = self.content!.children[0].description
        }
        
        self.view.setNeedsLayout()
    }
    
    // MARK: - Private methods
    
    private func fetchAllDecks() {
        if cardId == nil {
            return
        }
        self.showProgressHUD()
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.GetCard(cardId: cardId!)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't download decks, please try again later")
                    self.hideProgressHUD()
                    return
                }
                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    self.hideProgressHUD()
                    let dic = JSON["card"] as! [String: AnyObject]
                    self.content = ContentNode(dictionary: dic)
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't parse decks, please try again later")
                    self.hideProgressHUD()
                }
            case let .Failure(error):
                print("can't fetch decks: %@", error)
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't download decks, please try again later")
                self.hideProgressHUD()
            }
        }
        
    }
    
    func handleHeaderCellAction (action: HeaderCellAction) {
        if action == .Loop {
            let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "LoopController") as! LoopController
            vc.content = self.content
            self.presentViewController(vc, animated: true, completion: nil)
        } else if action == .Comment {
            let vc = UIStoryboard.loadViewController(storyboardName: "Comment", viewControllerIdentifier: "CommentsViewController") as! CommentsViewController
            vc.content = content
            self.presentViewController(vc, animated: true, completion: nil)
        } else if action == .Like {
            let liked = content!.liked_by_me
            let provider = MoyaProvider<NetworkService>.createDefaultProvider()
            let target = liked ? NetworkService.UnlikeCard(cardId : content!.id) : NetworkService.LikeCard(cardId : content!.id)
            
            provider.request(target) { result in
                switch result {
                case let .Success(response):
                    if response.statusCode == 200 {
                        self.content!.liked_by_me = !liked
                        self.content!.likes += liked ? -1 : 1
                        
                        // Update View
                        let likeCount = self.content!.likes ?? 0
                        self.likesLabel.text = String(likeCount)
                        let imageName = (self.content!.liked_by_me == false) ? "grey_heart" : "heartFilled"
                        self.likeButton.setImage(UIImage(named: imageName), forState: .Normal)
                    }
                case let .Failure(error):
                    print(error)
                }
            }
        } else {
            //            currentMode = action
            //            tableView.reloadData()
        }
    }
    
    
    // MARK: - Actions
    
    @IBAction func didTapOnClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) {
            
        }
    }
    
    @IBAction func didTapOnMore(sender: AnyObject) {
        
        onShowAlert()
        /*
         
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            // ...
        }
        alertController.addAction(cancelAction)
        
        if self.content!.type == ContentType.Deck {
            let editAction = UIAlertAction(title: "Edit deck", style: .Default) { (action) in
                self.enterEdit()
            }
            alertController.addAction(editAction)
            
            var title = "Publish Deck"
            if content!.published {
                title = "Unpublish Deck"
            }
            let publishAction = UIAlertAction(title: title, style: .Default) { (action) in
                
                self.showProgressHUD()
                let provider = MoyaProvider<NetworkService>.createDefaultProvider()
                provider.request(.PublishDecks(cardIds: [self.content!.id], publish: !self.content!.published)) { result in
                    switch result {
                    case let .Success(response):
                        if response.statusCode != 200 {
                            self.showSimpleAlert(withTitle: "Error", withMessage: "Can't publish/unpublish deck, please try again later")
                            self.hideProgressHUD()
                        }
                        else if let JSON = try? response.mapJSON() as! [String: String] {
                            if JSON["result"] == "success" {
                                self.content!.published = !self.content!.published
                                self.reloadData()
                                self.showSuccessHUD()
                                self.navigationController!.popToRootViewControllerAnimated(true)
                            }
                            else {
                                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't publish/unpublish deck, please try again later")
                                self.hideProgressHUD()
                            }
                        }
                        else {
                            self.showSimpleAlert(withTitle: "Error", withMessage: "Can't publish/unpublish deck, please try again later")
                            self.hideProgressHUD()
                        }
                    case .Failure(_):
                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't publish/unpublish deck, please try again later")
                        self.hideProgressHUD()
                    }
                }
            }
            alertController.addAction(publishAction)
        }
        self.presentViewController(alertController, animated: true) {
            
        }
        
        */
    }
    
    func reloadData() {
//        self.tableView.reloadData()
    }
    
    @IBAction func onCancel(sender: AnyObject) {
        exitEdit()
    }
    
    @IBAction func onDone(sender: AnyObject) {
//        let askEnabled = contentHeaderCell!.askEnabled
//        var formatString: String
//        var ids = [String]()
//        var initialPrize: Int = 0
//        var prizeToJoin: Int = 0
//        formatString = askEditCell?.format ?? ""
//        if askEditCell != nil {
//            if !askEditCell!.isAnyone {
//                ids = askEditCell!.getInputtedUserIDs()
//                //            if ids.count == 0 {
//                //                return // No specific person
//                //            }
//            }
//            initialPrize = askEditCell!.originalPrize
//            prizeToJoin = askEditCell!.joinPrize
//        } else {
//            
//        }
//        
//        var distributionRule: String?
//        var distributionFor: Int?
//        var evaluationStart: String?
//        var evaluationEnd: String?
//        if prizeEditCell != nil {
//            distributionRule = prizeEditCell?.distributionRule
//            distributionFor = prizeEditCell?.distributionFor
//            evaluationStart = prizeEditCell?.evaluationStart
//            evaluationEnd = prizeEditCell?.evaluationEnd
//        }
//        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
//        provider.request(.AddMode(isAskModeEnabled: askEnabled, deckId: content.id, userIds: ids, format: formatString, initialPrize:initialPrize , prizeToJoin:prizeToJoin, distributionRule: distributionRule, distributionFor: distributionFor, evaluationStart: evaluationStart, evaluationEnd: evaluationEnd)) { (result) in
//            let JSON = Helper.validateResponse(result)
//            self.goToDraftsView()
//        }
//        
//        
//        
//        self.showProgressHUD()
//        if compareChildren(content!.children, array2: newChildrenOrdering) {
//            self.showSuccessHUD()
//            exitEdit()
//            return
//        }
//        
//        var cardIds = [String]()
//        for child in newChildrenOrdering {
//            cardIds.append(child.id)
//        }
//
//        
//        
//        //        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
//        provider.request(.EditDeck(deckId: self.content!.id, cardIds: cardIds)) { result in
//            switch result {
//            case let .Success(response):
//                if response.statusCode != 200 {
//                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't edit deck, please try again later")
//                    self.hideProgressHUD()
//                }
//                else if let JSON = try? response.mapJSON() as! [String: String] {
//                    if JSON["result"] == "success" {
//                        self.content!.children = self.newChildrenOrdering
//                        self.exitEdit()
//                        self.showSuccessHUD()
//                    }
//                    else {
//                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't edit deck, please try again later")
//                        self.hideProgressHUD()
//                    }
//                }
//                else {
//                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't edit deck, please try again later")
//                    self.hideProgressHUD()
//                }
//            case .Failure(_):
//                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't edit deck, please try again later")
//                self.hideProgressHUD()
//            }
//        }
    }
    
    func enterEdit() {
//        editTopView.hidden = false
//        editingStatus = EditingStatus.Edit
//        if currentMode == .Play {
//            self.tableView.editing = true
//        }
//        newChildrenOrdering = []
//        for child in self.content.children {
//            newChildrenOrdering.append(child)
//        }
//        
//        contentHeaderCell!.isDraftEditing = true
//        tableView.reloadData()
        
        

    }
    
    func exitEdit() {
//        editTopView.hidden = true
//        editingStatus = EditingStatus.Normal
//        //        navigationController?.navigationBar.barTintColor = UIColor.blackColor()
//        self.tableView.editing = false
//        newChildrenOrdering = []
//        contentHeaderCell!.isDraftEditing = false
//        tableView.reloadData()
//        //        self.navigationItem.leftBarButtonItem  = customBackButton
//        //        self.navigationItem.rightBarButtonItems = [customMoreButton, customSelectButton]
    }
    
    func enterSelect() {
//        selectionTopView.hidden = false
//        tableView.allowsMultipleSelection = true
//        editingStatus = EditingStatus.Select
//        selectedTitleLabel.text = "0 Selected"
//        //        self.title = "0 Selected"
//        //        self.navigationItem.rightBarButtonItems = []
//        //        navigationController?.navigationBar.barTintColor = UIColor.colorFromRGB(redValue: 251, greenValue: 45, blueValue: 85, alpha: 1)
//        extraToolbar.hidden = false
//        tabBarController?.tabBar.hidden = true
    }
    
    func exitSelect() {
//        selectionTopView.hidden = true
//        tableView.allowsMultipleSelection = true
//        editingStatus = EditingStatus.Normal
//        //        self.title = ""
//        if let indexPaths = tableView.indexPathsForSelectedRows {
//            for indexPath in indexPaths {
//                tableView.deselectRowAtIndexPath(indexPath, animated: true)
//            }
//        }
//        //        self.navigationItem.rightBarButtonItems = [customMoreButton, customSelectButton]
//        //        navigationController?.navigationBar.barTintColor = UIColor.blackColor()
//        extraToolbar.hidden = true
//        tabBarController?.tabBar.hidden = false
    }
    
    func compareChildren(array1: [ContentNode], array2: [ContentNode]) -> Bool {
        for i in 0...array1.count - 1 {
            if array1[i].id != array2[i].id {
                return false
            }
        }
        return true
    }
    
    @IBAction func clickedDelete(sender: AnyObject) {
//        if let list = tableView.indexPathsForSelectedRows {
//            let alertController2 = UIAlertController(title: "Are you sure you want to continue with the deletion?", message: nil, preferredStyle: .Alert)
//            let cancelAction2 = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
//                // ...
//            }
//            let okayAction2 = UIAlertAction(title: "Ok", style: .Default) { (action) in
//                // API call
//                
//                var cardIds = [String]()
//                
//                var deleteDeck = false
//                if list.count == self.content.children.count {
//                    deleteDeck = true
//                    cardIds.append(self.content.id)
//                }
//                
//                for index in list {
//                    cardIds.append(self.content.children[index.row].id)
//                }
//                
//                self.showProgressHUD()
//                let provider = MoyaProvider<NetworkService>.createDefaultProvider()
//                provider.request(.DeleteCards(cardIds: cardIds)) { result in
//                    switch result {
//                    case let .Success(response):
//                        if response.statusCode != 200 {
//                            self.showSimpleAlert(withTitle: "Error", withMessage: "Can't delete content, please try again later")
//                            self.hideProgressHUD()
//                        }
//                        else if let JSON = try? response.mapJSON() as! [String: String] {
//                            if JSON["result"] == "success" {
//                                self.showSuccessHUD()
//                                if (deleteDeck) {
//                                    self.navigationController?.popViewControllerAnimated(true)
//                                } else {
//                                    var newChildren:[ContentNode] = []
//                                    
//                                    for child in self.content.children {
//                                        if !cardIds.contains(child.id) {
//                                            newChildren.append(child)
//                                        }
//                                    }
//                                    self.content.children = newChildren
//                                    self.reloadData()
//                                    self.exitSelect()
//                                }
//                            }
//                            else {
//                                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't delete content, please try again later")
//                                self.hideProgressHUD()
//                            }
//                        }
//                        else {
//                            self.showSimpleAlert(withTitle: "Error", withMessage: "Can't delete content, please try again later")
//                            self.hideProgressHUD()
//                        }
//                    case .Failure(_):
//                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't delete content, please try again later")
//                        self.hideProgressHUD()
//                    }
//                }
//            }
//            alertController2.addAction(cancelAction2)
//            alertController2.addAction(okayAction2)
//            self.presentViewController(alertController2, animated: true){}
//        }
    }
    
    @IBAction func clickedMove(sender: UIBarButtonItem) {
//        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("MoveToNav") as! MoveCardNavigationController
//        let cardIds = NSMutableArray()
//        for indexPath in tableView.indexPathsForSelectedRows! {
//            cardIds.addObject(content.children[indexPath.row].id)
//        }
//        
//        vc.cardIds = cardIds
//        //        vc.drafts = drafts
//        presentViewController(vc, animated: true, completion: nil)
    }
    
    @IBAction func clickedCopy(sender: AnyObject) {
//        if let list = tableView.indexPathsForSelectedRows {
//            var cardIds = [String]()
//            
//            for index in list {
//                cardIds.append(self.content.children[index.row].id)
//            }
//            
//            self.showProgressHUD()
//            let provider = MoyaProvider<NetworkService>.createDefaultProvider()
//            provider.request(.CopyCards(cardIds: cardIds)) { result in
//                switch result {
//                case let .Success(response):
//                    if response.statusCode != 200 {
//                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't copy content, please try again later")
//                        self.hideProgressHUD()
//                    }
//                    else if let JSON = try? response.mapJSON() as! [String: String] {
//                        if JSON["result"] == "success" {
//                            self.showSuccessHUD()
//                            self.exitSelect()
//                        }
//                        else {
//                            self.showSimpleAlert(withTitle: "Error", withMessage: "Can't copy content, please try again later")
//                            self.hideProgressHUD()
//                        }
//                    }
//                    else {
//                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't copy content, please try again later")
//                        self.hideProgressHUD()
//                    }
//                case .Failure(_):
//                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't copy content, please try again later")
//                    self.hideProgressHUD()
//                }
//            }
//        }
    }
    
    
    @IBAction func didTapOnLike(sender: AnyObject) {
        onHeaderAction(.Like)
    }
    @IBAction func didTapOnComment(sender: AnyObject) {
        onHeaderAction(.Comment)
    }
    
    // MARK: - CardViewPagerDataSource
    
    
    override func viewControllers(for viewPagerController: CardViewPagerController) -> [CardSubPageBaseVC] {
        var subPages = [CardSubPageBaseVC]()
        
        cardDetailPlayVC = UIStoryboard.loadViewController(storyboardName: "CardDetail", viewControllerIdentifier: "CardDetailPlayVC") as? CardDetailPlayVC
        cardDetailPlayVC?.delegate = self
        cardDetailPlayVC?.content = self.content
        subPages.append(cardDetailPlayVC!)
        
        if (content?.ask_enabled == true) {
            cardDetailAskVC = UIStoryboard.loadViewController(storyboardName: "CardDetail", viewControllerIdentifier: "CardDetailAskVC") as? CardDetailAskVC
            cardDetailAskVC?.delegate = self
            cardDetailAskVC?.content = self.content
            subPages.append(cardDetailAskVC!)
        }
        
        if (content?.ask_enabled == true) {
            cardDetailGiveVC = UIStoryboard.loadViewController(storyboardName: "CardDetail", viewControllerIdentifier: "CardDetailGiveVC") as? CardDetailGiveVC
            cardDetailGiveVC?.delegate = self
            cardDetailGiveVC?.content = self.content
            subPages.append(cardDetailGiveVC!)
        }
        
        if (content?.ask_enabled == true) {
            cardDetailPrizeVC = UIStoryboard.loadViewController(storyboardName: "CardDetail", viewControllerIdentifier: "CardDetailPrizeVC") as? CardDetailPrizeVC
            cardDetailPrizeVC?.delegate = self
            cardDetailPrizeVC?.content = self.content
            subPages.append(cardDetailPrizeVC!)
        }
        
        return subPages
    }
    
    // MARK: - CardViewPagerProgressDelegate
    
    func updateIndicator(for viewController: CardViewPagerController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool, borderCrossed: Bool, direction: SwipeDirection) {
        //        let changeFlag = borderCrossed ? "---------" : ""
        //        print("Border crossed: " + changeFlag)
        
        if (toIndex != -1 && toIndex != cardActionButtons.count) {
            let cardActionButtonFrom = cardActionButtons[fromIndex]
            let cardActionButtonTo = cardActionButtons[max(toIndex, -1)]
            if (fromIndex < toIndex) {
                // Swipe Direction = Left, Transition Direction = Right
                cardActionButtonFrom.transitToRight(withProgressPercentage: progressPercentage)
                cardActionButtonTo.transitFromRight(withProgressPercentage: progressPercentage)
            } else {
                // Swipe Direction = Right, Transition Direction = Left
                cardActionButtonFrom.transitToLeft(withProgressPercentage: progressPercentage)
                cardActionButtonTo.transitFromLeft(withProgressPercentage: progressPercentage)
            }
            
            if (borderCrossed) {
                if (direction == .left) {
                    let cardActionButtonPast = cardActionButtons[max(fromIndex-1, 0)]
                    cardActionButtonPast.deselectButton()
                } else if (direction == .right) {
                    let pastIndex = min(fromIndex+1, cardActionButtons.count-1)
                    let cardActionButtonPast = cardActionButtons[pastIndex]
                    cardActionButtonPast.deselectButton()
                }
            }
        }
        
        if (toIndex < 0 || toIndex >= cardActionButtons.count) {
            let newButton = cardActionButtons[currentIndex]
            newButton.highlightButton()
        } else {
            let oldButton = cardActionButtons[currentIndex != fromIndex ? fromIndex : toIndex]
            let newButton = cardActionButtons[currentIndex]
            oldButton.dehighlightButton()
            newButton.highlightButton()
        }
    }
    
    func updateIndicator(for viewController: CardViewPagerController, fromIndex: Int, toIndex: Int) {
        
    }
    
    // MARK: - CardDetailButtonDelegate
    
    func didTapOnCardActionButton(buttonID: Int) {
        guard buttonID != currentIndex else { return }
        
        let oldButton = cardActionButtons[currentIndex]
        let newButton = cardActionButtons[buttonID]
        oldButton.deselectButton()
        newButton.selectButton()
        
        moveToViewController(at: buttonID)
        
    }
    
    // MARK: - SubPageScrollViewDidScrollDelegate
    
    override func subPageScrollViewDidScroll(offset: CGFloat, translation: CGPoint, animated: Bool) {
        super.subPageScrollViewDidScroll(offset, translation: translation, animated: animated)
        
        if (movingFactor != 0 ) {
            return
        }
        
        if (originalHeaderTop == -1000) {
            originalHeaderTop = constraintHeaderTop.constant
        }
        
        if (originalActionsTop == -1000) {
            originalActionsTop = constraintActionsTop.constant
        }
        
        if (originalSocialTop == -1000) {
            originalSocialTop = constraintSocialTop.constant
        }
        
        constraintHeaderTop.constant = originalHeaderTop! - offset
        constraintActionsTop.constant = originalActionsTop! - offset
        constraintSocialTop.constant = originalSocialTop! - offset
        
        if (!animated) {
            self.view.layoutIfNeeded()
        } else {
            UIView.animateWithDuration(0.25, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    override func subPageScrollViewDidBounce(offset: CGFloat) {
        super.subPageScrollViewDidBounce(offset)
        
        constraintContentBottom.constant = 30 + offset
        self.view.layoutIfNeeded()
    }
    
    // MARK: - UITapGestureRecognizer
    
    func handleTap(tapGesture: UITapGestureRecognizer) {
        
        if (content!.parentContent != nil) {
            let fullScreenViewController = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "FullScreenViewController") as! FullScreenViewController
            fullScreenViewController.deckIndex = contentIndex
            fullScreenViewController.cardIndex = 0
            fullScreenViewController.content = content!.parentContent
            fullScreenViewController.modalPresentationStyle = .OverFullScreen
            self.presentViewController(fullScreenViewController, animated: true, completion: nil)
            
        }
    }
    
    // MARK: - UIPanGestureRecognizer
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if (gestureRecognizer == tapGestureRecognizer) {
            let tapLocation = tapGestureRecognizer.locationInView(cardView)
            if (cardView.pointInside(tapLocation, withEvent: nil)) {
                return true
            }
            
            return false
        }
        
        let velocity = panGestureRecognizer.velocityInView(self.vwDraggableView)
        
        let pannableToLeft = (currentPageIndex == 0 && fabs(velocity.x) >= fabs(velocity.y) && velocity.x > 0)
        let pannableToRight = (currentPageIndex == (viewControllers.count - 1) && fabs(velocity.x) >= fabs(velocity.y) && velocity.x < 0)
        
        let currentSubPage = viewControllers[currentPageIndex]
        let pannableFromTop = currentSubPage.contentOffsetY <= 0 && fabs(velocity.x) <= fabs(velocity.y) && velocity.y > 0
        let pannableFromBottom = currentSubPage.touchedBottom && fabs(velocity.x) <= fabs(velocity.y) && velocity.y < 0
        
        let touchLocation = panGestureRecognizer.locationInView(vwHeader)
        let pannableInsideHeader = vwHeader.pointInside(touchLocation, withEvent: nil) && fabs(velocity.x) >= fabs(velocity.y)
        //        let insideHeader = pannableInsideHeader ? "Inside Header" : "Outside Header"
        //        print(insideHeader)
        
        if (pannableToLeft || pannableToRight || pannableFromTop || pannableFromBottom || pannableInsideHeader) {
            return true
        }
        
        return false
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if otherGestureRecognizer is UIPanGestureRecognizer || otherGestureRecognizer is UITapGestureRecognizer {
            return true
        }
        
        return false
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func handlePan(panGesture: UIPanGestureRecognizer){
        
        vwDraggableView.handlePanGesture(panGestureRecognizer)
        
    }
    
    func generateThumnail(url : NSURL, fromTime:Float64) -> UIImage? {
        
        let asset = AVAsset(URL: url)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        assetImgGenerate.requestedTimeToleranceAfter = kCMTimeZero;
        assetImgGenerate.requestedTimeToleranceBefore = kCMTimeZero;
        
        let time        : CMTime = CMTimeMakeWithSeconds(fromTime, 600)
        var img         : CGImageRef!
        do{
            img = try assetImgGenerate.copyCGImageAtTime(time, actualTime: nil)
            
            
        }catch{
            
        }
        if img != nil {
            let frameImg    : UIImage = UIImage(CGImage: img)
            return frameImg
        } else {
            return nil
        }
        
    }
    
    // MARK: - PannableCardViewDelegate
    
    func cardSwipedAway() {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    func resetCardPosition(){
        
    }
    func cardWasDragged(dragPercentage: CGFloat) {
        self.vwMain.backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.7 * (1 - Float(dragPercentage)))
    }
    
    func cardStartedDismissal(duration: NSTimeInterval, dragPercentage: Float) {
        var percentage = dragPercentage
        UIView.animateWithDuration(duration) {
            percentage = 1
            self.vwMain.backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.7 * (1 - percentage))
        }
    }
    
    func onShowAlert(){
        
        guard let popView = NSBundle.mainBundle().loadNibNamed("MoreView", owner: self, options: nil)?[0] as? MoreView else {
            return
        }
        
        popView.mainView.clipsToBounds = true
        popView.mainView.layer.borderColor = UIColor.colorFromRGB(redValue: 155/255.0, greenValue: 155/255.0, blueValue: 155/255.0, alpha: 0.4).CGColor
        popView.mainView.layer.borderWidth = 1
        
        popView.mainView.roundTopCorners(20)
        
        popView.constrantY.constant = -450
        popView.delegate = self
        self.view.addSubview(popView)
        
        popView.hidden = false
        
        UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            
            popView.constrantY.constant = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        
        
    }
    
    func onSelectButton(nIndex : Int){
        switch nIndex {
        case 0://Publish
            self.onPublishAction()
            break
        case 1://Make an Ask
            self.onMakeAsk()
            break
        case 2://Move...
            break
        case 3://Reorder
            break
        case 4://Save Copy
            break
        case 5://Edit
            self.onEditAction()
            break
        case 6://Delete
            break
        case 7://Play Fullscreen
            break
        case 8://Show tags
            break
        default:
            break
        }
    }
    
    func onMakeAsk(){
        self.showProgressHUD()
//        if compareChildren(content!.children, array2: newChildrenOrdering) {
//            self.showSuccessHUD()
//            exitEdit()
//            return
//        }
        
        var cardIds = [String]()
        for child in newChildrenOrdering {
            cardIds.append(child.id)
        }
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        
        provider.request(.EditDeck(deckId: self.content!.id, cardIds: cardIds)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't edit deck, please try again later")
                    self.hideProgressHUD()
                }
                else if let JSON = try? response.mapJSON() as! [String: String] {
                    if JSON["result"] == "success" {
                        self.content!.children = self.newChildrenOrdering
                        self.exitEdit()
                        self.showSuccessHUD()
                    }
                    else {
                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't edit deck, please try again later")
                        self.hideProgressHUD()
                    }
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't edit deck, please try again later")
                    self.hideProgressHUD()
                }
            case .Failure(_):
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't edit deck, please try again later")
                self.hideProgressHUD()
            }
        }
    }
    func onPublishAction(){
        self.showProgressHUD()
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.PublishDecks(cardIds: [self.content!.id], publish: !self.content!.published)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't publish/unpublish deck, please try again later")
                    self.hideProgressHUD()
                }
                else if let JSON = try? response.mapJSON() as! [String: String] {
                    if JSON["result"] == "success" {
                        self.content!.published = !self.content!.published
                        self.reloadData()
                        self.showSuccessHUD()
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }
                    else {
                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't publish/unpublish deck, please try again later")
                        self.hideProgressHUD()
                    }
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't publish/unpublish deck, please try again later")
                    self.hideProgressHUD()
                }
            case .Failure(_):
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't publish/unpublish deck, please try again later")
                self.hideProgressHUD()
            }
        }
    }
    
    func onEditAction() {
        guard let content = content else { return }
        
        if content.type == .Video {
            self.editVideoDraft()
        }
        else if content.type == .Image {
            self.editImageDraft()
        }
        else {
            // other process
        }
    }
    
    func editImageDraft() {
        guard let content = content else { return }
        self.showProgressHUD()
        
        let vc = storyboard?.instantiateViewControllerWithIdentifier("ImageCardCreationController") as! ImageCardCreationController
        
        vc.cardName             = content.name
        vc.descStr              = content.description
        vc.cardId               = content.id
        vc.prevViewController   = self
        
        let imageUrl            = NSURL(string: content.content!)
        let request             = NSMutableURLRequest(URL: imageUrl!)
        let imageCache          = GuideHeroImageCache.sharedInstance.getCache()
        let cachedImage         = imageCache.imageForRequest(request)
        
        if cachedImage != nil {
            vc.image = cachedImage
            self.hideProgressHUD()
            self.presentViewController(vc, animated: true, completion: nil)
        } else {
            ImageDownloader.defaultInstance.downloadImage(URLRequest: request) { response in
                if response.result.isSuccess {
                    let image = response.result.value
                    imageCache.addImage(image!, forRequest: request)
                    
                    self.hideProgressHUD()
                    self.presentViewController(vc, animated: true, completion: nil)
                }
                else {
                    print(response.result.description)
                    self.hideProgressHUD()
                }
            }
        }
    }
    
    func editVideoDraft() {
        guard let content = content else { return }
        self.showProgressHUD()
        
        let vc = storyboard?.instantiateViewControllerWithIdentifier("VideoCardCreationViewController") as! VideoCardCreationViewController
        
        vc.videoUrl             = content.contentURL
        vc.cardName             = content.name
        vc.descStr              = content.description
        vc.cardId               = content.id

        vc.prevViewController   = self
        
        self.hideProgressHUD()
        
        self.presentViewController(vc, animated: true, completion: nil)
    }
    

}
