//
//  MoveCardViewController.swift
//  GuideHero
//
//  Created by Ascom on 10/29/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya

class MoveCardViewController: UIViewController {
    
    @IBOutlet var thumbImage: UIImageView!
    @IBOutlet var thumbsView: UIView!
    @IBOutlet var thumbImages: [MaskImageView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let nav = navigationController as! MoveCardNavigationController
        nav.hereButton.enabled = false
        nav.hereButton.alpha = 0.5
        
        let drafts = Helper.drafts
        if drafts.count > 3 {
            var index = 0
            for content in drafts {
                thumbImages[index].setImageWithContent(content)
                thumbImages[index].layer.cornerRadius = 5
                index += 1
                if index == 4 {
                    break
                }
            }
            thumbsView.layer.cornerRadius = 10
            thumbImage.hidden = true
        } else if drafts.count > 0 {
            thumbImage.layer.cornerRadius = 10
            let content = Helper.drafts.first
            thumbImage.af_setImageWithURL(content!.imageURL ?? NSURL())
            self.thumbsView.hidden = true
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        let nav = navigationController as! MoveCardNavigationController
        nav.hereButton.enabled = true
        nav.hereButton.alpha = 1
    }
}
