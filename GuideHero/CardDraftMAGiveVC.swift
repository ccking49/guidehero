//
//  CardDraftMAGiveVC.swift
//  GuideHero
//
//  Created by Promising Change on 31/01/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit

class CardDraftMAGiveVC: CardSubPageBaseVC {
    
    // MARK: - @IBOutlet Variables
    
    @IBOutlet weak var tbContent: UITableView!
    @IBOutlet weak var footerView : UIView!

    // MARK: - Variables
    
    let tbHeaderView: UIView!
    var aryTableHeader: [String]
    let empowerString: String = "Go empower some people! 😉🙌"
    let inspireString: String = "Still waiting! 💫 Be the first one! ☝️"
    
    var content: ContentNode? {
        didSet {
            if isViewLoaded() {
                tbContent.reloadData()
                self.view.layoutIfNeeded()
            }
        }
    }
    
    // MARK: - View Lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        tbHeaderView = UIView()
        aryTableHeader = ["Your Give", "Givers"]
        
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        tbContent.setContentOffset(CGPointMake(tbContent.contentOffset.x, tbContent.contentOffset.y - 1), animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        scrollView = tbContent as UIScrollView
        
        configureView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Methods
    
    func configureView() {
        tbHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 540)
        
        let contentNib = UINib(nibName: "ContentTableCell", bundle: nil)
        tbContent.registerNib(contentNib, forCellReuseIdentifier: "ContentTableCell")
        let basicNib = UINib(nibName: "UserBasicCell", bundle: nil)
        tbContent.registerNib(basicNib, forCellReuseIdentifier: "UserBasicCell")
        
        let cellNib = UINib(nibName: "CardDetailTableViewCell", bundle: nil)
        tbContent.registerNib(cellNib, forCellReuseIdentifier: "CardDetailTableViewCell")
        
        tbContent.rowHeight = UITableViewAutomaticDimension
        tbContent.estimatedRowHeight = Constants.UIConstants.contentCellHeight
        tbContent.separatorStyle = .None
        
        tbContent.tableHeaderView = tbHeaderView
        
        footerView.layer.cornerRadius = 20
        footerView.layer.shadowRadius = 5
        footerView.layer.shadowOpacity = 0.5
        footerView.layer.shadowOffset = CGSizeZero
        footerView.layer.shadowColor = UIColor.blackColor().CGColor
        footerView.clipsToBounds = true
        
        self.view.setNeedsLayout()
    }
    
    // MARK: - UIScrollViewDelegate
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
    }

}

// MARK: - UITableViewDelegate & UITableViewDataSource

extension CardDraftMAGiveVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return self.aryTableHeader.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.aryTableHeader[section] as String
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let contentHeight: CGFloat = 48
        
        let headerView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: self.tbContent.bounds.width, height: contentHeight)))
        let headerContentView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: self.tbContent.bounds.width, height: 48)))
        
        let label = UILabel(frame: CGRect(x: 30, y: 0, width: self.tbContent.frame.width - 60, height: 48))
        label.text = aryTableHeader[section] as String
        label.textAlignment = NSTextAlignment.Left
        label.textColor = UIColor.blackColor()
        label.font = Helper.proximaNova("Bold", font: 20)
        
        headerView.clipsToBounds = false
        headerView.backgroundColor = UIColor.whiteColor()
        headerContentView.backgroundColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.1)
        
        headerContentView.addSubview(label)
        headerView.addSubview(headerContentView)
        
        return headerView
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 49
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if self.content!.myGives.count > 0 {
                
                return 2
                
            }else{
                return 2
            }
            
            
        case 1:
            
            if self.content!.givers.count == 0 {
                return 1
            }else{
                return self.content!.givers.count+1
            }
            
        default:
            return 0
        }
    }
    
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let section = indexPath.section
        let row = indexPath.row
        
        if (section == 0) {
            
            if self.content!.myGives.count != 0 {
                
                // Your Give
                if (row == 1) {
                    // Submit cell
                    return 70
                } else {
                    // Give cell
                    return 216
                }
            }else{
                // Your Give
                if (row == 0) {
                    // Empower cell
                    return 44
                } else {
                    // Submit cell
                    return 70
                }
            }
            
        } else if (section == 1) {
            // Givers
            
            if self.content!.givers.count == 0 {
                return 64
            }else{
                
                if (row == 0) {
                    return 12.5
                }else{
                    return 30
                }
                
            }
            
        }
        
        return 0
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        
        var tbCell : UITableViewCell!
        
        if (section == 0) {
            
            if self.content!.myGives.count != 0 {
                
                // Your Give
                if (row == 1) {
                    // Submit cell
                    let cell = tableView.dequeueReusableCellWithIdentifier("DraftGiveSubmitCell", forIndexPath: indexPath) as! DraftGiveSubmitCell
                    
                    cell.btSubmit.roundView()
                    
                    let isGiver = self.content!.isGiver
                    cell.btSubmit.enabled = true
                    if isGiver == 2 {
                        cell.btSubmit.setTitle("Edit Submission", forState: .Normal)
                    } else if isGiver == 1 {
                        cell.btSubmit.setTitle("Edit Submission", forState: .Normal)
                    } else {
                        cell.btSubmit.setTitle("Submit", forState: .Normal)
                    }
                    
                    cell.setNeedsLayout()
                    cell.layoutIfNeeded()
                    
                    tbCell = cell
                } else{
                    
                    let cell = tableView.dequeueReusableCellWithIdentifier("CardDetailTableViewCell", forIndexPath: indexPath) as! CardDetailTableViewCell
                    
                    cell.content = self.content!.myGives[row]
                    
                    if self.content!.myGives.count > 1 {
                        cell.cardView.backView1.hidden = false
                        cell.cardView.backView2.hidden = false
                    }else{
                        cell.cardView.backView1.hidden = true
                        cell.cardView.backView2.hidden = true
                    }
                    
                    cell.btnProfilePhoto.tag = row
                    cell.btnProfileName.tag = row
                    
                    cell.imgSeparate.hidden = false
                    
                    cell.separateView.hidden = true
                    cell.imgSeparate.layer.shadowRadius = 2
                    
                    cell.imgSeparate.layer.shadowOffset = CGSizeZero
                    cell.imgSeparate.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
                    
//                    cell.cardView.tag = indexPath.row
//                    let singleTap = UITapGestureRecognizer(target: self, action: #selector(CardDetailPlayVC.showFullScreen))
//                    cell.cardView.addGestureRecognizer(singleTap)
                    
                    cell.layer.shadowRadius = 1
                    cell.layer.shadowOffset = CGSizeZero
                    cell.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
                    
                    cell.giveImage.hidden = false
                    
                    cell.tag = indexPath.row
                    
                    cell.userProfileImage.alpha = 0.5
                    cell.lblFullName.alpha = 0.5
                    cell.lblUserName.alpha = 0.5
                    cell.lblPostDate.alpha = 0.5
                    cell.lblUserBio.alpha = 0.5
                    cell.cardView.alpha = 0.5
                    cell.lblContentTitle.alpha = 0.5
                    cell.lblDescription.alpha = 0.5
                    cell.lblLikeCount.alpha = 0.5
                    cell.giveImage.alpha = 0.5
                    cell.imgLock.alpha = 0.5
                    
                    cell.btnLike.enabled = false
//                    let childTap = UITapGestureRecognizer(target: self, action: #selector(CardDetailGiveVC.showChildScreen))
//                    cell.addGestureRecognizer(childTap)
                    
                    tbCell = cell
                }
                
            }else{
                // Your Give
                if (row == 0) {
                    // Empower cell
                    let cell = tableView.dequeueReusableCellWithIdentifier("DraftGiveSubHeaderCell", forIndexPath: indexPath) as! DraftGiveSubHeaderCell
                    
                    cell.lbTitle.text = empowerString
                    
                    cell.setNeedsLayout()
                    cell.layoutIfNeeded()
                    
                    tbCell = cell
                } else {
                    // Submit cell
                    let cell = tableView.dequeueReusableCellWithIdentifier("DraftGiveSubmitCell", forIndexPath: indexPath) as! DraftGiveSubmitCell
                    
                    cell.btSubmit.roundView()
                    
                    let isGiver = self.content!.isGiver
                    cell.btSubmit.enabled = true
                    if isGiver == 2 {
                        cell.btSubmit.setTitle("Edit Submission", forState: .Normal)
                        //            cell.btSubmit.enabled = false
                    } else if isGiver == 1 {
                        cell.btSubmit.setTitle("Edit Submission", forState: .Normal)
                    } else {
                        cell.btSubmit.setTitle("Submit", forState: .Normal)
                    }
                    
                    cell.setNeedsLayout()
                    cell.layoutIfNeeded()
                    
                    tbCell = cell
                }
            }
            
        } else if (section == 1) {
            // Givers
            
            if self.content?.givers.count == 0 {
                // Inspire cell
                let cell = tableView.dequeueReusableCellWithIdentifier("DraftGiveSubHeaderCell", forIndexPath: indexPath) as! DraftGiveSubHeaderCell
                
                cell.lbTitle.text = inspireString
                
                cell.setNeedsLayout()
                cell.layoutIfNeeded()
                
                tbCell = cell
            }else{
                // Giver cell
                
                if (row == 0) {
                    return tableView.dequeueReusableCellWithIdentifier("Blank")!
                }else{
                    let cell = tableView.dequeueReusableCellWithIdentifier("UserBasicCell") as! UserBasicCell
                    let user = self.content!.givers[row-1]
                    cell.populateWithUser(user)
                    
                    cell.avatarImage.alpha = 0.5
                    cell.nameLabel.alpha = 0.5
                    cell.timeLabel.alpha = 0.5
                    
                    
                    cell.setNeedsLayout()
                    cell.layoutIfNeeded()
                    
                    tbCell = cell
                }
            }
            
        }
        
        return tbCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Do Nothing
    }
}
