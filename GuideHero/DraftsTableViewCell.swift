//
//  DraftsTableViewCell.swift
//  GuideHero
//
//  Created by Dino Bartosak on 12/10/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

class DraftsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cardContentView: UIView!
    @IBOutlet weak var imageContentImageView: UIImageView!
    @IBOutlet weak var textContentLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var creationDetailsLabel: UILabel!
    
    var viewModel: DraftsTableCellViewModel? {
        didSet {
            if let viewModel = viewModel {
                hideAllCardContentViews()
                
                switch viewModel.cellType {
                case .Image:
                    imageContentImageView.hidden = false
                    if let deckImageURL = viewModel.deckImageURL {
                        imageContentImageView.af_setImageWithURL(deckImageURL, placeholderImage: UIImage(named: "photo_placeholder"));
                        
                        print("deck URL")
                        print(deckImageURL)
                    }
                case .Text:
                    textContentLabel.hidden = false
                    textContentLabel.text = viewModel.deckText
                }
                
                if let authorImageURL = viewModel.authorImageURL {
                    authorImageView.af_setImageWithURL(authorImageURL, placeholderImage: UIImage(named: "photo_placeholder"));
                }
                titleLabel.text = viewModel.title
                
                descriptionLabel.text = viewModel.cardDescription
                creationDetailsLabel.text = viewModel.creationDetails
            }
        }
    }
    
    // MARK: UITableViewCell Override
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cardContentView.layer.cornerRadius = 20;
        authorImageView.layer.cornerRadius = authorImageView.bounds.size.width/2.0;
        
        cardContentView.layer.masksToBounds = true
        authorImageView.layer.masksToBounds = true
        
        hideAllCardContentViews()
    }
    
    // MARK: Appearance
    
    private func hideAllCardContentViews() {
        imageContentImageView.hidden = true
        textContentLabel.hidden = true
    }
}
