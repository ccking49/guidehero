//
//  NotificationNode.swift
//  GuideHero
//
//  Created by Ascom on 11/1/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

public class NotificationNode: CustomDebugStringConvertible {

    var card_id: String
    var content: String? // can be nil or anything from text, image URL, video ID, etc...
    var thumbnail_url: String
    var createdAt: Int // unix timestamp
    
    
    // MARK: - Calculated properties
    
    public var debugDescription: String {
        get {
            let dic: [String: String] = [
                "card_id": self.card_id,
                "content": self.content ?? "None",
                "thumbnail_url": self.thumbnail_url
            ]
            return dic.debugDescription
        }
    }
    
    var formattedCreationDate: String {
        get {
            let creationDate = NSDate(timeIntervalSince1970: NSTimeInterval(self.createdAt))
            return NSDateComponentsFormatter.sharedFormatter.stringFromDate(creationDate, toDate: NSDate()) ?? ""
        }
    }
    
    
    
    // MARK: - Object lifecycle
    
    init(dictionary: [String: AnyObject]) {
        self.card_id = dictionary["card_id"] as! String
        self.content = dictionary["content"] as? String
        self.thumbnail_url = dictionary["thumbnail_url"] as! String
        self.createdAt = dictionary["created_at"] as! Int
    }
    
}
