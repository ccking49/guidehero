//
//  ImageBrowserCollectionViewCell.swift
//  GuideHero
//
//  Created by Dino Bartosak on 11/10/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

class ImageBrowserCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var photoURL: NSURL? {
        didSet {
            if let photoURL = photoURL {
                imageView.af_setImageWithURL(photoURL, placeholderImage: UIImage(named: "photo_placeholder"));
            }
        }
    }
    
    // MARK: UICollectionViewCell
    
    override func awakeFromNib() {
        super.awakeFromNib();
        
        self.layer.cornerRadius = 5.0;
    }
}
