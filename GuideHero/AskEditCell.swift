//
//  AskEditCell.swift
//  GuideHero
//
//  Created by HC on 11/15/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya
import KMPlaceholderTextView


class AskEditCell: BaseTableViewCell, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UITextFieldDelegate {

    @IBOutlet var anyoneSelect: UIImageView!
    @IBOutlet var specificSelect: UIImageView!
    @IBOutlet var usersTextView: KMPlaceholderTextView!
    @IBOutlet var formatText: UITextField!
    @IBOutlet var usersTable: UITableView!
    @IBOutlet var usersTableHeight: NSLayoutConstraint!
    @IBOutlet var originalPrizeText: UITextField!
    @IBOutlet var joinPrizeText: UITextField!
    
    
    var format: String? {
        get {
            return self.formatText.text
        }
    }
    var userIds: [String] {
        get {
            return getInputtedUserIDs()
        }
    }
    var originalPrize: Int {
        get {
            return Int(originalPrizeText.text!) ?? 0
        }
    }
    var joinPrize: Int {
        get {
            return Int(joinPrizeText.text!) ?? 0
        }
    }
    
    var content: ContentNode! {
        didSet {
            updateUI()
        }
    }
    var isAnyone: Bool = true
    var usernames = [[String: AnyObject]]()
    var totalUsernames = [[String: AnyObject]]()
    var currentWordIndex: Int = 0
//    var contentHeaderCell: ContentHeaderCell
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        usersTextView.placeholder = "Input usernames..."
        usersTable.layer.borderColor = UIColor.lightGrayColor().CGColor
        usersTable.layer.borderWidth = 1
        usersTable.delegate = self
        usersTable.dataSource = self
        usersTextView.delegate = self
        originalPrizeText.delegate = self
        originalPrizeText.addTarget(self, action: #selector(AskEditCell.originalPrizeTextChanged(_:)), forControlEvents: .EditingChanged)
        joinPrizeText.addTarget(self, action: #selector(AskEditCell.originalPrizeTextChanged(_:)), forControlEvents: .EditingChanged)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: - @IBAction
    
    @IBAction func onSelectAnyone(sender: UIButton) {
        isAnyone = true
        updateSelection()
    }
    @IBAction func onSelectSpecific(sender: AnyObject) {
        isAnyone = false
        updateSelection()
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = usernames.count < 3 ? usernames.count : 2
        let height = 44 * CGFloat(count)
        usersTableHeight.constant = height
        return count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UsernameCell")
        cell?.textLabel?.text = usernames[indexPath.row]["username"] as? String
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        usersTableHeight.constant = 0
        let word = usernames[indexPath.row]["username"] as! String
        var words = usersTextView.text.componentsSeparatedByString(" ")
        let keyword = words[currentWordIndex]
        
        
        if keyword.hasPrefix("@") {
            words[currentWordIndex] = "@" + word
        }
        let totalString = words.joinWithSeparator(" ")
        usersTextView.text = totalString
        usernames = []
        tableView.reloadData()
    }
    
    // MARK: - UITextView
    
    func textViewDidChange(textView: UITextView) {
        let selectedRange = textView.selectedRange
        let beginning = textView.beginningOfDocument
        let start = textView.positionFromPosition(beginning, offset: selectedRange.location)
        let end = textView.positionFromPosition(start!, offset: selectedRange.length)
        //        let textRange = textView.tokenizer.rangeEnclosingPosition(end!, withGranularity: .Word, inDirection: 1)
        //        let wordTyped = textView.textInRange(textRange) ?? ""
        let wordsInSentence = textView.text.componentsSeparatedByString(" ")
        
        
        var indexInSavedArray = 0;
        for string in wordsInSentence {
            let nsText = textView.text as NSString
            let range = nsText.rangeOfString(string)
            if selectedRange.location >= range.location && selectedRange.location <= (range.location + range.length) {
                if string.hasPrefix("@") {
                    currentWordIndex = indexInSavedArray
                    loadUsernamesWithWord(string)
                } else {
                    
                }
            }
            indexInSavedArray += 1
        }
        
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        //Ensure we're not at the start of the text field and we are inserting text
        if range.location > 0 && text.characters.count > 0 {
            let whitespace = NSCharacterSet.whitespaceCharacterSet()
            let utf16text = text.utf16
            let utf16textViewText = textView.text.utf16
            
            let startIndex = utf16text.startIndex
            let locationIndex = utf16textViewText.startIndex.advancedBy(range.location - 1)
            
            //Check if a space follows a space
            if whitespace.characterIsMember(utf16text[startIndex]) && whitespace.characterIsMember(utf16textViewText[locationIndex])
            {
                //Manually replace the space with your own space, programmatically
                textView.text = (textView.text as NSString).stringByReplacingCharactersInRange(range, withString: " ")
                
                //Make sure you update the text caret to reflect the programmatic change to the text view
                textView.selectedRange = NSMakeRange(range.location + 1, 0)
                
                //Tell UIKit not to insert its space, because you've just inserted your own
                return false
            }
        }
        return true
    }

    
    // MARK: - UITextField
    
    func originalPrizeTextChanged (tField: UITextField) {
        let prize = Int(tField.text!) ?? 0;
        tField.text = String(prize)
        if tField.isEqual(originalPrizeText) {
            NSNotificationCenter.defaultCenter().postNotificationName(Constants.Literals.prizePoolChangedNotification, object: originalPrizeText.text)
        }
    }
    
    // MARK: - Function
    
    func updateUI () {
        formatText.text = content.format
        isAnyone = content.isAnyone
        originalPrizeText.text = String(content.prize_pool)
        joinPrizeText.text = String(content.prize_to_join)
        if !isAnyone {
            var askers = ""
            for asked_user in content.asked_users {
                let userDict = ["username":asked_user.username, "user_id":asked_user.user_id]
                totalUsernames.append(userDict)
                askers += "@" + asked_user.username + " "
            }
            usersTextView.text = askers
        }
        
        updateSelection()
    }
    
    func updateSelection () {
        let onImage = UIImage(named: "selectOn")
        let offImage = UIImage(named: "selectOff")
        anyoneSelect.image = isAnyone ? onImage : offImage
        specificSelect.image = isAnyone ? offImage : onImage
        usersTextView.hidden = isAnyone
    }
    
    func saveInfo (enabled: Bool) {
        
    }
    
    func loadUsernamesWithWord(word: String) {
        var string = word
        if word.hasPrefix("@") {
            string = word.substringFromIndex(word.startIndex.advancedBy(1))
        }
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.GetUsernames(searchKey: string)) { result in
            let JSON = Helper.validateResponse(result)
            if JSON != nil {
//                self.usernames = JSON!["usernames"] as! [String]
                self.usernames = JSON!["users"] as? [[String: AnyObject]] ?? []
                self.totalUsernames.appendContentsOf(self.usernames)
                self.usersTable.reloadData()
            }
        }
    }
    
    func getInputtedUserIDs () -> [String] {
        let wordsInSentence = usersTextView.text.componentsSeparatedByString(" ")
        
        var userIds = [String]()
        for string in wordsInSentence {
            if string.hasPrefix("@") {
                let username = string.substringFromIndex(string.startIndex.advancedBy(1))
                let userId = userIDOfUsername(username)
                if userId.length > 0 {
                    userIds.append(userId)
                }
            }
        }
        return Array(Set(userIds)) // remove redundant
    }
    
    func userIDOfUsername(username: String) -> String {
        for user in totalUsernames {
            let uname = user["username"] as! String
            if uname == username {
                return user["user_id"] as! String
            }
        }
        return ""
    }
}
