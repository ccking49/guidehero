//
//  ImageCardCreationController.swift
//  GuideHero
//
//  Created by Ignacio Rodrigo on 9/30/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit


class ImageCardCreationController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UIScrollViewDelegate {

    // MARK: - Properties

    @IBOutlet var imageViewContainer: UIView!
    @IBOutlet var imageScrollView: UIScrollView!
    @IBOutlet weak var wholeView : UIView!
    
    @IBOutlet var imageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var imageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet var imageViewXConstraint: NSLayoutConstraint!
    @IBOutlet var imageViewYConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cardTitle: UITextField!
    @IBOutlet weak var cardDescription: UITextView!
    @IBOutlet weak var descriptionHint: UILabel!
    @IBOutlet weak var cardTags: UITextView!
    @IBOutlet weak var tagsView: UIView!
    @IBOutlet weak var tagButton: UIButton!
    @IBOutlet weak var imgBackground : UIImageView!
    @IBOutlet weak var tagsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var editButton: UIButton!
    
    var tagArray = [String]()
    
    var keyboardFlag : Bool = false
    
    var prevViewController: UIViewController?

    var image: UIImage?
    var cardName: String? = nil
    var descStr: String? = nil
    var cardId: String? = nil
    
    var imageView: UIImageView? = nil
    var imageContainer: UIView? = nil
    
    var screenHeight = UIScreen.mainScreen().bounds.height
    var screenWidth = UIScreen.mainScreen().bounds.width
    
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        wholeView.layer.cornerRadius = 20
        wholeView.layer.shadowRadius = 5
        wholeView.layer.shadowOpacity = 0.5
        wholeView.layer.shadowOffset = CGSizeZero
        wholeView.layer.shadowColor = UIColor.blackColor().CGColor
        wholeView.clipsToBounds = true
        
        tagsViewHeightConstraint.constant = 50.0+6.0+50
        cardTags.hidden = true
        
        cardTags.delegate = self
        cardDescription.delegate = self
        
        editButton.hidden = true
        
        self.setupImageView()
        if cardName != nil {
            cardTitle.text = cardName
            editButton.hidden = false
        }
        if descStr != nil {
            cardDescription.text = descStr
            if descStr == "" {
                descriptionHint.hidden = false
            }
            else {
                descriptionHint.hidden = true
            }
        }
//        cardNameTextField.text = cardName
    }

    // MARK: - Actions

    override func viewWillAppear(animated: Bool){
        super.viewWillAppear(animated)
        
        UIApplication.sharedApplication().statusBarHidden = true
        
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        
        deregisterFromKeyboardNotifications()
        
    }
    
    
    @IBAction func back(sender: AnyObject) {
        self.clearData()
        if self.navigationController != nil {
            self.navigationController?.popViewControllerAnimated(true)
        }
        else {
            self.dismissViewControllerAnimated(false, completion: nil)
        }
    }

    @IBAction func actionEdit(sender: UIButton) {
        if let navCtrl = storyboard!.instantiateViewControllerWithIdentifier("CameraFunctionNavViewController") as? UINavigationController {
            let camFuncVC = navCtrl.viewControllers.first as! CameraFunctionViewController
        
            camFuncVC.parentVC  = self
            camFuncVC.type      = 1
        
            presentViewController(navCtrl, animated: true, completion: nil)
        }
    }
    
    @IBAction func done(sender: AnyObject) {
        // validate card name
        if cardTitle.text == nil || cardTitle.text!.length == 0  {
            self.showSimpleAlert(withTitle: "Error", withMessage: "Please add a card name")
            return
        }
        
        let rect: CGRect = self.imageViewContainer.convertRect(self.imageViewContainer.bounds, toView: self.imageView)
        
        let finalCardVC = storyboard!.instantiateViewControllerWithIdentifier("FinalCardViewController") as! FinalCardViewController
        
        finalCardVC.image                   = self.image
        finalCardVC.cardName                = self.cardTitle.text!
        finalCardVC.cropImage               = self.crop(self.image!, cropRect: rect)
        
        finalCardVC.descriptionText         = self.cardDescription.text
        finalCardVC.cardTags                = self.tagArray
        finalCardVC.rect                    = rect
        finalCardVC.prevViewController      = self
        
        finalCardVC.cardId                  = self.cardId
        
        presentViewController(finalCardVC, animated: true, completion: nil)
        
        // Calculate mask data
//        let zoomScale = imageScrollView.zoomScale
    }

    // MARK: - Text field delegate

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - UIScrollView
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.imageContainer
    }
    func scrollViewDidEndZooming(scrollView: UIScrollView, withView view: UIView?, atScale scale: CGFloat) {
//        self.adjustFrameToCenter()
    }
    
    // MARK: - Image view

    func setupImageView() {
        self.view.layoutIfNeeded()
        
//        var imageViewFrame = CGRect.zero
//        imageViewFrame.size = image!.size
        
        let imageSize = image!.size
        let greater = max(imageSize.width, imageSize.height)
        let containerSize = CGSize(width: greater*3, height: greater*3)
        self.imageScrollView.contentSize = containerSize
        
        self.imageContainer?.removeFromSuperview()
        
        self.imageContainer = UIView(frame: CGRect(origin: CGPointZero, size: containerSize))
        self.imageScrollView.addSubview(self.imageContainer!)
        
        self.imageView?.removeFromSuperview()
        
        self.imageView = UIImageView(image: image)
        self.imageView?.frame = CGRect(origin: CGPointZero, size: imageSize)
        self.imageView?.center = (self.imageContainer?.center)!
        self.imageContainer?.addSubview(self.imageView!)
        
        self.setMaxMinZoomScalesForCurrentBounds()
//        self.adjustFrameToCenter()
        let containerFrame = self.imageContainer?.frame
        self.imageScrollView.contentOffset = CGPoint(x: CGRectGetWidth(containerFrame!)/3, y: CGRectGetHeight(containerFrame!)/3)
        
        // Add background view with shadow.
        // Needs a separate view because the image view container 
        // requires clipsToBounds = true for the cornerRadius.

        // TODO: define this background view in the storyboard and use autolayout.
        self.imageViewContainer.layer.cornerRadius = 10
        self.imageViewContainer.layer.shadowRadius = 3
        self.imageViewContainer.layer.shadowOffset = CGSizeZero
        self.imageViewContainer.layer.shadowColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.5).CGColor
        self.imageViewContainer.clipsToBounds = true
        self.imageViewContainer.layer.borderWidth = 0.5
        self.imageViewContainer.layer.borderColor = UIColor.colorFromRGB(redValue: 155/255.0, greenValue: 155/255.0, blueValue: 155/255.0, alpha: 0.5).CGColor

    }
    
    func setMaxMinZoomScalesForCurrentBounds() {
        // calculate min/max zoomscale
        let bounds = self.imageScrollView.bounds
        let imageSize = image!.size
        let xScale = bounds.width / imageSize.width    // the scale needed to perfectly fit the image width-wise
        let yScale = bounds.height / imageSize.height   // the scale needed to perfectly fit the image height-wise
        
        // fill width if the image and phone are both portrait or both landscape; otherwise take smaller scale
//        let imagePortrait = imageSize.height > imageSize.width
//        let phonePortrait = bounds.height >= bounds.width
//        var minScale = (imagePortrait == phonePortrait) ? xScale : min(xScale, yScale)
        var minScale = min(xScale, yScale)
        var maxScale = 20 * minScale
        maxScale = 10
        // don't let minScale exceed maxScale. (If the image is smaller than the screen, we don't want to force it to be zoomed.)
        if minScale > maxScale {
            minScale = maxScale
        }
        
        self.imageScrollView.minimumZoomScale = minScale / 10
        self.imageScrollView.maximumZoomScale = maxScale
        self.imageScrollView.setZoomScale(minScale, animated: false)
    }
    /*
    func adjustFrameToCenter() {
        
        guard self.imageContainer != nil else {
            return
        }
        
        var frameToCenter = self.imageContainer!.frame
        let imageFrame = imageView!.frame
        let bounds = self.imageScrollView.bounds
        // center horizontally
        if imageFrame.size.width < bounds.width {
            frameToCenter.origin.x = (bounds.width - frameToCenter.size.width) / 2
        }
        else {
            frameToCenter.origin.x = 0
        }
        
        // center vertically
        if imageFrame.size.height < bounds.height {
            frameToCenter.origin.y = (bounds.height - frameToCenter.size.height) / 2
        }
        else {
            frameToCenter.origin.y = 0
        }
        
        self.imageContainer!.frame = frameToCenter
    }*/

    // MARK: - Private methods

    private func clearData() {
//        self.cardDescription.text = nil
//        self.cardNameTextField.text = nil
    }
    
    func crop(image:UIImage, cropRect:CGRect) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(cropRect.size, false, image.scale)
        let origin = CGPoint(x: cropRect.origin.x * CGFloat(-1), y: cropRect.origin.y * CGFloat(-1))
        image.drawAtPoint(origin)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        
        return result
    }
    
    func registerForKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWasShown), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillBeHidden), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWillBeHidden(notification: NSNotification) {
        
        keyboardFlag = false
    }
    
    func keyboardWasShown(notification: NSNotification) {
        
        keyboardFlag = true
    }
    
    //  Tags
    
    @IBAction func onTagEdit(sender: AnyObject) {
        for subView in tagsView.subviews {
            subView.removeFromSuperview()
        }
        
        cardTags.hidden = false
        cardTags.becomeFirstResponder()
        tagsView.hidden = true
        tagButton.hidden = true
        tagsViewHeightConstraint.constant = 75.0+6.0+50
        self.view.layoutIfNeeded()
    }
    
    func makeTagViews() {
        var x = 25
        var y = 15
        for tag in self.tagArray {
            let width = Int(widthForView(tag, font: UIFont(name: "ProximaNova-Semibold", size: 18.0)!, height: 29))
            if (x + width + 25 > Int(screenWidth) - 15) {
                x = 25
                y = y + 39
            }
            let label = UILabel(frame: CGRect(x: x, y: y, width: width + 25, height: 29))
            label.text = tag
            label.textAlignment = NSTextAlignment.Center
            label.backgroundColor = UIColor.whiteColor()
            label.textColor = UIColor(red: 155.0 / 255.0, green: 155.0 / 255.0, blue: 155.0 / 255.0, alpha: 1.0)
            label.layer.cornerRadius = 14
            label.layer.borderColor = UIColor(red: 155.0 / 255.0, green: 155.0 / 255.0, blue: 155.0 / 255.0, alpha: 1.0).CGColor
            label.layer.borderWidth = 1.0
            label.clipsToBounds = true
            tagsView.addSubview(label)
            x = x + width + 35
        }
        
        tagsViewHeightConstraint.constant = CGFloat(y) + 39 + 50.0 + 50
        self.view.layoutIfNeeded()
    }
    
    func widthForView(text: String, font: UIFont, height: CGFloat) -> CGFloat{
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.max, height: height))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        if (text == "") {
            label.text = " "
        }
        else {
            label.text = text
        }
        
        label.sizeToFit()
        return label.frame.width
    }
    
    //keyboard 
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if (textField.tag == 3) {
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            if (newLength > 5) {
                return false
            }
            
            
            
            return true
        }
        else {
            return true
        }
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if (textView.tag == 0) {
            descriptionHint.hidden = true
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if (textView.tag == 0) {
            if (textView.text == "") {
                descriptionHint.hidden = false
            }
        }
        else {
            self.tagArray.removeAll()
            if (textView.text != "") {
                self.tagArray = textView.text.componentsSeparatedByString(",")
                for (index, tag) in self.tagArray.enumerate() {
                    tagArray[index] = tag.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                }
                makeTagViews()
            }
            else {
                tagsViewHeightConstraint.constant = 50.0+6.0+50
            }
            cardTags.hidden = true
            tagsView.hidden = false
            tagButton.hidden = false
        }
    }
    
}
