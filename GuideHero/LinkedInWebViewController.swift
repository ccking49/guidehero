//
//  LinkedInWebViewController.swift
//  GuideHero
//
//  Created by Yohei Oka on 7/30/16.
//  Copyright © 2016 Twilio. All rights reserved.
//

import UIKit

class LinkedInWebViewController: BasePreLoginController, UIWebViewDelegate {
    
    let baseUrl = Constants.ApiConstants.baseApiUrl + "auth/"
    
    @IBOutlet var spinner: UIActivityIndicatorView!
    // MARK: IBOutlet Properties
    
    @IBOutlet var webView: UIWebView!
    
    // MARK: Constants
    
    let linkedInKey = "77g08zsgvg6nkj"
    
    let linkedInSecret = "M0exxCcj0LOYnj8G"
    
    let authorizationEndPoint = "https://www.linkedin.com/uas/oauth2/authorization"
    
    let accessTokenEndPoint = "https://www.linkedin.com/uas/oauth2/accessToken"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        webView.delegate = self
        
        startAuthorization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: IBAction Function
    
    
    @IBAction func dismiss(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func startAuthorization() {
        // Specify the response type which should always be "code".
        let responseType = "code"
        
        // Set the redirect URL. Adding the percent escape characthers is necessary.
        let redirectURL = "http://linkedin.com".stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet())!
        
        // Create a random string based on the time interval (it will be in the form linkedin12345679).
        let state = "linkedin\(Int(NSDate().timeIntervalSince1970))"
        
        // Set preferred scope.
        let scope = "r_basicprofile%20r_emailaddress"
        
        // Create the authorization URL string.
        var authorizationURL = "\(authorizationEndPoint)?"
        authorizationURL += "response_type=\(responseType)&"
        authorizationURL += "client_id=\(linkedInKey)&"
        authorizationURL += "redirect_uri=\(redirectURL)&"
        authorizationURL += "state=\(state)&"
        authorizationURL += "scope=\(scope)"
        
        // Create a URL request and load it in the web view.
        let request = NSURLRequest(URL: NSURL(string: authorizationURL)!)
        webView.loadRequest(request)
        
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let url = request.URL!
        
        if url.host == "linkedin.com" {
            if url.absoluteString!.rangeOfString("code") != nil {
                // Extract the authorization code.
                let urlParts = url.absoluteString!.componentsSeparatedByString("?")
                let code = urlParts[1].componentsSeparatedByString("=")[1]
                requestForAccessToken(code)
            }
            webView.stopLoading()
        }
        
        return true
    }
    
    func requestForAccessToken(authorizationCode: String) {
        let grantType = "authorization_code"
        
        let redirectURL = "http://linkedin.com".stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet())!
        
        // Set the POST parameters.
        var postParams = "grant_type=\(grantType)&"
        postParams += "code=\(authorizationCode)&"
        postParams += "redirect_uri=\(redirectURL)&"
        postParams += "client_id=\(linkedInKey)&"
        postParams += "client_secret=\(linkedInSecret)"
        
        // Convert the POST parameters into a NSData object.
        let postData = postParams.dataUsingEncoding(NSUTF8StringEncoding)
        
        // Initialize a mutable URL request object using the access token endpoint URL string.
        let request = NSMutableURLRequest(URL: NSURL(string: accessTokenEndPoint)!)
        
        // Indicate that we're about to make a POST request.
        request.HTTPMethod = "POST"
        
        // Set the HTTP body using the postData object created above.
        request.HTTPBody = postData
        
        // Add the required HTTP header field.
        request.addValue("application/x-www-form-urlencoded;", forHTTPHeaderField: "Content-Type")
        
        // Initialize a NSURLSession object.
        let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        
        // Make the request.
        let task: NSURLSessionDataTask = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
            // Get the HTTP status code of the request.
            let statusCode = (response as! NSHTTPURLResponse).statusCode
            
            if statusCode == 200 {
                // Convert the received JSON data into a dictionary.
                do {
                    let dataDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)
                    
                    let accessToken = dataDictionary["access_token"] as! String
                    self.signIn(accessToken)
                }
                catch {
                    print("Could not convert JSON data into a dictionary.")
                }
            }
        }
        
        task.resume()
    }
    
    func signIn(accessToken: String) {
        spinner.startAnimating()
        let params = ["access_token": accessToken] as Dictionary<String, String>
        let request = NSMutableURLRequest(URL: NSURL(string:baseUrl + "linkedin_signin")!)
        let session = NSURLSession(configuration: NSURLSessionConfiguration.ephemeralSessionConfiguration())
        request.HTTPMethod = "POST"
        
        do {
            request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
        } catch {
            self.displayAlert("Can't login", message: "Please try again")
            spinner.stopAnimating()
            return
        }
        
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            guard error == nil else {
                self.dispatchStopActivityIndicatory(self.spinner)
                self.dispatchAlert("Can't login", message: "Please try again")
                return
            }
            
            let json: NSDictionary?
            do {
                json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as? NSDictionary
            } catch {
                // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                self.dispatchStopActivityIndicatory(self.spinner)
                self.dispatchAlert("Can't login", message: "Please try again")
                return
            }
            
            if let parseJSON = json {
                // check if auth_token is returned
                if let authToken = parseJSON.valueForKeyPath("auth_token") as! String! {
                    self.dispatchStopActivityIndicatory(self.spinner)
                    
                    let prefs = NSUserDefaults.standardUserDefaults()
                    prefs.setValue(authToken, forKey: "guidehero.authtoken")
                    self.setAuthDataAndLogin(parseJSON)
                    
//                    self.dismissVC()
                    return
                }
            }
            self.dispatchStopActivityIndicatory(self.spinner)
            self.dispatchAlert("Can't login", message: "Please try again")
        })
        task.resume()
        
    
    }
    
    @IBAction func clickExit(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        spinner.startAnimating()
    }
    
    func webViewDidFinishLoad(webView: UIWebView){
        spinner.stopAnimating()
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        spinner.stopAnimating()
    }
}
