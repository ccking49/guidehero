//
//  SubmitCardViewController.swift
//  GuideHero
//
//  Created by HC on 11/16/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya

class SubmitCardViewController: UIViewController , UITableViewDelegate, UITableViewDataSource ,DraftTableViewCellDelegate , SubmissionDelegate {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var mainView : UIView!
    @IBOutlet var headerView : UIView!
    
    @IBOutlet var footerView : UIView!
    @IBOutlet var footerBottomView : UIView!
    
    @IBOutlet var cancelButton : UIButton!
    @IBOutlet var removeButton : UIButton!
    @IBOutlet var submitButton : UIButton!
    
//    @IBOutlet var constantY : NSLayoutConstraint!
//    @IBOutlet var tapGesture : UITapGestureRecognizer!
    
    @IBOutlet var frontView : UIView!
    
    var isType : Int! // true:submit false:remove
    var contentId: String = ""
    var cardIds: NSArray!
    var currentContent: ContentNode! // selected content within nav
    var content: ContentNode!
    
    var drafts = [ContentNode]()
    
    var submittedCallback: ((_: ContentNode!) -> Void)!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        
//        if isType == true {
//            submitButton.hidden = false
//            removeButton.hidden = true
//        }else{
//            submitButton.hidden = true
//            removeButton.hidden = false
//        }
        submitButton.layer.shadowColor = UIColor.blackColor().CGColor
        submitButton.layer.shadowOffset = CGSizeMake(0, 1)
        submitButton.layer.shadowOpacity = 0.5
        submitButton.layer.shadowRadius = 4
        
        removeButton.layer.shadowColor = UIColor.blackColor().CGColor
        removeButton.layer.shadowOffset = CGSizeMake(0, 1)
        removeButton.layer.shadowOpacity = 0.5
        removeButton.layer.shadowRadius = 4
        
        cancelButton.layer.shadowColor = UIColor.blackColor().CGColor
        cancelButton.layer.shadowOffset = CGSizeMake(0, 1)
        cancelButton.layer.shadowOpacity = 0.5
        cancelButton.layer.shadowRadius = 4

        
        let cellNib = UINib(nibName: "ContentTableCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: "ContentTableCell")
        
        footerBottomView.layer.cornerRadius = 20
        footerBottomView.layer.shadowRadius = 5
        footerBottomView.layer.shadowOpacity = 0.5
        footerBottomView.layer.shadowOffset = CGSizeZero
        footerBottomView.layer.shadowColor = UIColor.blackColor().CGColor
        footerBottomView.clipsToBounds = true
        
        footerView.frame.size = CGSizeMake(UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height)
        
        loadDrafts()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
//        navigationController?.setNavigationBarHidden(true, animated: false)
        UIApplication.sharedApplication().statusBarHidden = true
        
        frontView.hidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
//        UIApplication.sharedApplication().statusBarHidden = false
//        frontView.hidden = false
    }
    
    func loadDrafts () {
        drafts = Helper.drafts
        if drafts.count > 0 {
            self.updateTable()
        } else {
            self.showProgressHUD()
            let provider = MoyaProvider<NetworkService>.createDefaultProvider()
            provider.request(.GetCreatedCards) { result in
                switch result {
                case let .Success(response):
                    if response.statusCode != 200 {
                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't get drafts, please try again later")
                        self.hideProgressHUD()
                        return
                    }
                    if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                        self.hideProgressHUD()
                        for draftDict in (JSON["created"] as? [[String : AnyObject]])! {
                            self.drafts.append(ContentNode(dictionary: draftDict))
                        }
                        Helper.drafts = self.drafts
                        self.updateTable()
                    }
                    else {
                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't get drafts, please try again later")
                        self.hideProgressHUD()
                    }
                case let .Failure(error):
                    print("Can't get drafts: %@", error)
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't get drafts, please try again later")
                    self.hideProgressHUD()
                }
            }
        }
    }
    
    // MARK: Content Load
    
    private func updateTable() {
        drafts = Helper.drafts
        
        if (self.drafts.count) * 216 < Int(UIScreen.mainScreen().bounds.size.height - self.headerView.frame.size.height){
            self.footerView.frame.size = CGSizeMake(UIScreen.mainScreen().bounds.size.width, (UIScreen.mainScreen().bounds.size.height - self.headerView.frame.size.height)-CGFloat((self.drafts.count) * 216))
        }else{
            self.footerView.frame.size = CGSizeMake(0, 50)
        }
        
        self.tableView.reloadData()
    }
    
    
    // MARK: - UITableView
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 216
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.drafts.count ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("DraftCell", forIndexPath: indexPath) as! DraftCell
        
        cell.delegate = self
        cell.content = self.drafts[indexPath.row]
        
        cell.showLock = false
        if !self.drafts[indexPath.row].published {
            cell.showLock = true
        }
        cell.updateLock()
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let cardDetailVC = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "SubmissionDetailViewController") as! SubmissionDetailViewController
        cardDetailVC.content = self.drafts[indexPath.row]
        cardDetailVC.isType = self.isType
        
        cardDetailVC.delegate = self
        
        cardDetailVC.contentIndex = indexPath.row
        cardDetailVC.currentContent = self.content
        cardDetailVC.submittedCallback = { rContent in
            // Card submitted here
            
            self.dismissViewControllerAnimated(false, completion: {
                self.submittedCallback(rContent)
            })

        }
        cardDetailVC.modalPresentationStyle = .OverFullScreen
        cardDetailVC.isType = 0
//        cardDetailVC.isType = self.isType®
        self.navigationController?.presentViewController(cardDetailVC, animated: true, completion: nil)
//        self.navigationController?.pushViewController(cardDetailVC, animated: true)
        
    }
    
    func onUIUpdate() {
        updateTable()
    }
    
    @IBAction func onCancelSubmissionAction(sender : AnyObject){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func onSubmissionAction(sender : AnyObject){
        self.showSimpleAlert(withTitle: "", withMessage: "Choose an item! 😉💫")
    }
    @IBAction func onRemoveAction(sender : AnyObject){
        
    }
    
//    @IBAction func onTapGesture (gesture : UITapGestureRecognizer){
//        
//        
//        UIView.animateWithDuration(0.2, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
//            self.constantY.constant = 0
//            self.view.layoutIfNeeded()
//            }) { (true) in
//                self.tapGesture.enabled = false
//        }
//    }
    
    func onHideFrontBackground(){
        frontView.hidden = true
    }
    
    func onShowFrontBackground(){
        frontView.hidden = false
    }
}
