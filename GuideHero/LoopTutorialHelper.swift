//
//  LoopTutorialHelper.swift
//  GuideHero
//
//  Created by Ignacio Rodrigo on 10/27/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import Foundation

class LoopTutorialHelper {

    // Check if the loop tutorial should be automatically displayed.
    // Loop tutorial should be automatically displayed the first time the user goes into loop mode.
    static var shouldDisplayLoopTutorial: Bool {
        get {
            return !NSUserDefaults.standardUserDefaults().boolForKey("LoopTutorialCompleted")
        }
    }

    // Notify this helper class that the loop tutorial has been displayed.
    // Calling this method will prevent the loop tutorial to be automatically displayed in the future.
    class func didDisplayLoopTutorial() {
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "LoopTutorialCompleted")
    }
}
