//
//  Album.swift
//  Camera
//
//  Created by Vitor Oliveira on 2/24/16.
//
//

import UIKit

class AlbumCell: UICollectionViewCell {
    
    @IBOutlet weak var albumPhoto: UIImageView!
    @IBOutlet weak var timeLabel: UILabel! {
        didSet {
            timeLabel.hidden = true
        }
    }
    
    func setImage(image: UIImage){
        self.albumPhoto.image = image
    }
    
}
