//
//  SubmitCardViewController.swift
//  GuideHero
//
//  Created by HC on 11/16/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya


class SubmitCardRootViewController: UIViewController {

    @IBOutlet var thumbImage: UIImageView!
    @IBOutlet var thumbsView: UIView!
    @IBOutlet var thumbImages: [MaskImageView]!
    
    var drafts = [ContentNode]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadDrafts()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
//        let nav = navigationController as! SubmitCardNavigationController
//        nav.submitButton.enabled = false
//        nav.submitButton.alpha = 0.5
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
    }

    func setupThumbnails () {
        if drafts.count > 3 {
            var index = 0
            for content in drafts {
                thumbImages[index].setImageWithContent(content)
                thumbImages[index].layer.cornerRadius = 5
                index += 1
                if index == 4 {
                    break
                }
            }
            thumbsView.layer.cornerRadius = 10
            thumbImage.hidden = true
        } else if drafts.count > 0 {
            thumbImage.layer.cornerRadius = 10
            let content = Helper.drafts.first
            thumbImage.af_setImageWithURL(content!.imageURL ?? NSURL())
            self.thumbsView.hidden = true
        }
    }
    
    func loadDrafts () {
        drafts = Helper.drafts
        if drafts.count > 0 {
            setupThumbnails()
        } else {
            self.showProgressHUD()
            let provider = MoyaProvider<NetworkService>.createDefaultProvider()
            provider.request(.GetCreatedCards) { result in
                switch result {
                case let .Success(response):
                    if response.statusCode != 200 {
                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't get drafts, please try again later")
                        self.hideProgressHUD()
                        return
                    }
                    if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                        self.hideProgressHUD()
                        for draftDict in (JSON["created"] as? [[String : AnyObject]])! {
                            self.drafts.append(ContentNode(dictionary: draftDict))
                        }
                        Helper.drafts = self.drafts
                        self.setupThumbnails()
//                        self.tableView.reloadData()
//                        self.resetSelection()
                    }
                    else {
                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't get drafts, please try again later")
                        self.hideProgressHUD()
                    }
                case let .Failure(error):
                    print("Can't get drafts: %@", error)
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't get drafts, please try again later")
                    self.hideProgressHUD()
                }
            }
        }
    }
}
