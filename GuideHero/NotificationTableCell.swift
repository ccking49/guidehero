//
//  NotificationTableCell.swift
//  GuideHero
//
//  Created by Ascom on 11/1/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

class NotificationTableCell: UITableViewCell {

    @IBOutlet var thumbnail: UIImageView!
    @IBOutlet var contentLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    
    var notification: NotificationNode! {
        didSet {
            updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    private func updateUI() {
        self.contentView .layoutIfNeeded()
//        self.nameLabel.text = self.comment.name
        self.contentLabel.text = notification.content
        self.timeLabel.text = notification.formattedCreationDate
        thumbnail.af_setImageWithURL(NSURL(string: notification.thumbnail_url)!)
        thumbnail.layer.cornerRadius = CGRectGetHeight(thumbnail.frame)/2
    }
}
