//
//  ImageCameraRollViewController.swift
//  GuideHero
//
//  Created by Dino Bartosak on 10/10/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import MobileCoreServices

class CameraRollViewController: UIViewController,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addImagePickerController()
    }
    
    private func addImagePickerController() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .PhotoLibrary
        imagePicker.allowsEditing = false
        imagePicker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
        imagePicker.view.translatesAutoresizingMaskIntoConstraints = false
        imagePicker.willMoveToParentViewController(self)
        addChildViewController(imagePicker)
        self.view.addSubview(imagePicker.view)
        
        // add layout constraints
        imagePicker.view.fillSuperview()
        
        imagePicker.didMoveToParentViewController(self)
    }
    
    // MARK: UIImagePickerControllerDelegate
   
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {

        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if (Session.sharedSession.user!.tier == "admin") {
                showCardCreationWithImage(image)
            }
            else {
                showSimpleAlert(withTitle: "Guide Hero", withMessage: "Sorry! Video uploads only.")
            }
        }
        else if let videoURL = info["UIImagePickerControllerMediaURL"] as? NSURL {
            let videoCardCreationViewController = storyboard!.instantiateViewControllerWithIdentifier("VideoCardCreationViewController") as! VideoCardCreationViewController
            videoCardCreationViewController.videoUrl = videoURL
            videoCardCreationViewController.prevViewController = self
            navigationController!.pushViewController(videoCardCreationViewController, animated: true)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.tabBarController?.selectedIndex = Constants.UIConstants.learnTabIndex
    }
 
    // MARK: - Navigation
    
    func showCardCreationWithImage(image: UIImage?) {
        self.performSegueWithIdentifier("ShowImageCardCreationController", sender: image)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? ImageCardCreationController {
            vc.image = sender as? UIImage
        }
    }

}
