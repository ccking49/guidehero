//
//  AskEditCell.swift
//  GuideHero
//
//  Created by HC on 11/15/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya
import KMPlaceholderTextView


class PrizeEvaluationCell: BaseTableViewCell, UITextViewDelegate, UITextFieldDelegate {

    @IBOutlet var lblStartTime : UILabel!
    @IBOutlet var lblEndTime : UILabel!
    @IBOutlet var lblLeftTime : UILabel!
    
//    var contentHeaderCell: ContentHeaderCell
    var content: ContentNode! {
        didSet {
            updateUI()
        }
    }
    
    func updateUI() {
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: - @IBAction
    
    
}
