//
//  UIColorExtensions.swift
//  GuideHero
//
//  Created by Justin Holman on 1/27/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {

    // progress bar that is full or the current one
    class var progressBar: UIColor {
        return UIColor.whiteColor()
    }

    // progress bar that is empty or the next one
    class var progressBarCurrent: UIColor {
        return UIColor.whiteColor().colorWithAlphaComponent(0.5)
    }

    class var timeDisplayBackgroundColor: UIColor {
        return UIColor(red:0.50, green:0.50, blue:0.50, alpha:1.00)
    }

    class func colorFromRGB(redValue redValue: CGFloat, greenValue: CGFloat, blueValue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: redValue/255.0, green: greenValue/255.0, blue: blueValue/255.0, alpha: alpha)
    }

    /// Creates a UIColor object for the given rgb value in a Hex color value format. For example:
    ///
    ///     let color = UIColor(rgb: 0x2196F3)
    ///     let colorWithAlpha = UIColor(rgb: 0x2196F3, alpha: 0.5)
    ///
    /// - parameter rgb:   color value as Int. To be specified as hex literal like 0xff00ff
    /// - parameter alpha: optional alpha value (default 1.0)
    ///
    /// - returns: UIColor object

    convenience init(rgb: Int, alpha: CGFloat = 1.0) {
        let r = CGFloat((rgb & 0xff0000) >> 16) / 255
        let g = CGFloat((rgb & 0x00ff00) >>  8) / 255
        let b = CGFloat((rgb & 0x0000ff)      ) / 255

        self.init(red: r, green: g, blue: b, alpha: alpha)
    }

    /// Creates a UIColor object for the given HEX string. For example:
    ///
    ///     let color = UIColor(hexString: "#2196F3")
    ///     let colorWithAlpha = UIColor(hexString: "#CC2196F3)
    ///
    /// - parameter hexString:   color hex string as String. To be specified as hex literal like "#2196F3"
    ///
    /// - returns: UIColor object

    convenience init?(hexString: String) {
        // Swift 2.0
        var hexSanitized = hexString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        hexSanitized = hexSanitized.stringByReplacingOccurrencesOfString("#", withString: "")
        // Swift 3.0
        // var hexSanitized = hexString.trimmingCharacters(in: .whitespacesAndNewlines)
        // hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")

        let r, g, b, a: CGFloat
        let length = hexSanitized.characters.count

        var hexNumber: UInt64 = 0

        // Swift 2.0
        guard NSScanner(string: hexSanitized).scanHexLongLong(&hexNumber) else { return nil }
        // Swift 3.0
        // guard Scanner(string: hexSanitized).scanHexInt64(&hexNumber) else { return nil }

        if (length == 8) {
            a = CGFloat((hexNumber & 0xff000000) >> 24) / 255
            r = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
            g = CGFloat((hexNumber & 0x0000ff00) >>  8) / 255
            b = CGFloat((hexNumber & 0x000000ff)      ) / 255
        } else if (length == 6) {
            r = CGFloat((hexNumber & 0xff0000) >> 16) / 255
            g = CGFloat((hexNumber & 0x00ff00) >>  8) / 255
            b = CGFloat((hexNumber & 0x0000ff)      ) / 255
            a = 1.0
        } else {
            return nil
        }

        self.init(red: r, green: g, blue: b, alpha: a)
    }
}
