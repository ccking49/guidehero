//
//  MaskImageView.swift
//  GuideHero
//
//  Created by Ascom on 10/27/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AlamofireImage


public class MaskImageView: UIImageView {
    
    required public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func setImageWithContent(content: ContentNode) {
        let rect = CGRect(x: content.image_x, y: content.image_y, width: content.image_width, height: content.image_height)
        if let url = content.imageURL {
            setImageWithUrl(url, rect: rect)
        }
    }
    
    public func setImageWithUrl(url: NSURL?, rect: CGRect) {
        if url==nil {
            return
        }
        contentMode = UIViewContentMode.ScaleAspectFit
        
        let imageDownloader = af_imageDownloader ?? UIImageView.af_sharedImageDownloader
        let request = NSMutableURLRequest(URL: url!)

        let imageCache = GuideHeroImageCache.sharedInstance.getCache()
        let cachedImage = imageCache.imageForRequest(request)
        if cachedImage != nil {
            self.image = cachedImage
            self.backgroundColor = UIColor.blackColor()
            return
        }

        imageDownloader.downloadImage(URLRequest: request) { response in
            if response.result.isSuccess {
                var img = response.result.value
                if (rect.width>0) && (rect.height>0) {
                    img = img?.cropInRect(rect)
                }
                self.image = img
                imageCache.addImage(img!, forRequest: request)
                self.backgroundColor = UIColor.blackColor()
            }
        }
    }
    public func setImageWithUrlMaskBackground(url: NSURL?, rect: CGRect) {
//        if url==nil {
//            return
//        }
//        contentMode = UIViewContentMode.ScaleAspectFill
//        
//        let imageDownloader = af_imageDownloader ?? UIImageView.af_sharedImageDownloader
//        let request = NSMutableURLRequest(URL: url!)
//        
//        let imageCache = GuideHeroImageCache.sharedInstance.getCache()
//        let cachedImage = imageCache.imageForRequest(request)
//        if cachedImage != nil {
//            self.image = cachedImage
//            self.backgroundColor = UIColor.clearColor()
//            return
//        }
//        
//        imageDownloader.downloadImage(URLRequest: request) { response in
//            if response.result.isSuccess {
//                var img = response.result.value
//                if (rect.width>0) && (rect.height>0) {
//                    img = img?.cropInRect(rect)
//                }
//                self.image = img
//                imageCache.addImage(img!, forRequest: request)
//                self.backgroundColor = UIColor.whiteColor()
//            }
//        }
        if url==nil {
            return
        }
        contentMode = UIViewContentMode.ScaleAspectFill
        
        let imageDownloader = af_imageDownloader ?? UIImageView.af_sharedImageDownloader
        let request = NSMutableURLRequest(URL: url!)
        
        //        let imageCache = GuideHeroImageCache.sharedInstance.getCache()
        //        let cachedImage = imageCache.imageForRequest(request)
        //        if cachedImage != nil {
        //            self.image = cachedImage
        //            self.backgroundColor = UIColor.clearColor()
        //            return
        //        }
        
        imageDownloader.downloadImage(URLRequest: request) { response in
            if response.result.isSuccess {
                let imgData = response.result.value
                
                if let img = imgData {
                    let ratio = self.bounds.size.width / self.bounds.size.height
                    let ratioImage = img.size.width / img.size.height
                    
                    if ratio > ratioImage {
                        self.layer.contentsRect = CGRect(x: 0, y: 0, width: 1, height: ratio * ratioImage)
                    } else {
                        self.layer.contentsRect = CGRect(x: 0, y: 0, width: 1, height: 1)
                    }
                }
                self.image = imgData
                //                imageCache.addImage(img!, forRequest: request)
                self.backgroundColor = UIColor.whiteColor()
            }
        }
    }
    
    public func setImageWithUrlWhiteBackground(url: NSURL?, rect: CGRect) {
        if url==nil {
            return
        }
        contentMode = UIViewContentMode.ScaleAspectFit
        
        let imageDownloader = af_imageDownloader ?? UIImageView.af_sharedImageDownloader
        let request = NSMutableURLRequest(URL: url!)
        
        let imageCache = GuideHeroImageCache.sharedInstance.getCache()
        let cachedImage = imageCache.imageForRequest(request)
        if cachedImage != nil {
            self.image = cachedImage
            self.backgroundColor = UIColor.whiteColor()
            return
        }
        
        imageDownloader.downloadImage(URLRequest: request) { response in
            if response.result.isSuccess {
                var img = response.result.value
                if (rect.width>0) && (rect.height>0) {
                    img = img?.cropInRect(rect)
                }
                self.image = img
                imageCache.addImage(img!, forRequest: request)
                self.backgroundColor = UIColor.whiteColor()
            }
        }
    }
    
    
    public func setImageWithUrlFromCardDetail(url: NSURL?) {
        if url==nil {
            return
        }
        contentMode = UIViewContentMode.ScaleAspectFill
//        layer.contentsRect = CGRectMake(<#T##x: CGFloat##CGFloat#>, <#T##y: CGFloat##CGFloat#>, <#T##width: CGFloat##CGFloat#>, <#T##height: CGFloat##CGFloat#>)
        
        let imageDownloader = af_imageDownloader ?? UIImageView.af_sharedImageDownloader
        let request = NSMutableURLRequest(URL: url!)
        
        let imageCache = GuideHeroImageCache.sharedInstance.getCache()
        let cachedImage = imageCache.imageForRequest(request)
        if cachedImage != nil {
            self.image = cachedImage
            self.backgroundColor = UIColor.whiteColor()
            self.alpha = 0.9
            return
        }
        
        imageDownloader.downloadImage(URLRequest: request) { response in
            if response.result.isSuccess {
                let img = response.result.value
                self.image = img
                imageCache.addImage(img!, forRequest: request)
                self.backgroundColor = UIColor.clearColor()
                self.alpha = 0.9
            }
        }
    }
    
}
