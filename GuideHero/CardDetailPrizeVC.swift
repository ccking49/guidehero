//
//  CardDetailPrizeVC.swift
//  GuideHero
//
//  Created by Promising Change on 23/12/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AlamofireImage
import Moya

class CardDetailPrizeVC: CardSubPageBaseVC, UITableViewDelegate, UITableViewDataSource  {

    // MARK: - Variables
    
    @IBOutlet weak var tbContent: UITableView!
    
    @IBOutlet weak var footerView : UIView!
    
    var aryTableHeader1 :NSMutableArray = []
    var aryTableHeader2 :NSMutableArray = []
    
    var isWinnerFlag : Bool = false
    var winnerSyncCount : Int = 11
    
    var isSponsorsFlag : Bool = false
    var sponsorSyncCount : Int = 11
    
    let tbHeaderView: UIView!
    
    var content: ContentNode? {
        didSet {
            if isViewLoaded() {
                tbContent.reloadData()
                self.view.layoutIfNeeded()
            }
        }
    }
    
    // MARK: - View Lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        tbHeaderView = UIView()
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        scrollView = tbContent as UIScrollView
        
        configureView()
        refreshData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        tbContent.setContentOffset(CGPointMake(tbContent.contentOffset.x, tbContent.contentOffset.y - 1), animated: true)
    }
    // MARK: - Custom Methods
    
    func configureView() {
        
        tbHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 540)
        
        let cellNib = UINib(nibName: "PrizeCell", bundle: nil)
        tbContent.registerNib(cellNib, forCellReuseIdentifier: "PrizeCell")
        tbContent.rowHeight = UITableViewAutomaticDimension
        tbContent.estimatedRowHeight = Constants.UIConstants.contentCellHeight
        tbContent.separatorStyle = .None
        
        tbContent.tableHeaderView = tbHeaderView
//        tbContent.tableFooterView = UIView(frame: CGRect.zero)
        
        footerView.layer.cornerRadius = 20
        footerView.layer.shadowRadius = 5
        footerView.layer.shadowOpacity = 0.5
        footerView.layer.shadowOffset = CGSizeZero
        footerView.layer.shadowColor = UIColor.blackColor().CGColor
        footerView.clipsToBounds = true
        
        self.aryTableHeader1 = ["Prize", "For", "Distribution", "Evaluation" , "Winners" , "Sponsors"]
        self.aryTableHeader2 = ["Prize", "For", "Distribution", "Evaluation" , "Sponsors"]
        
        
        if content!.joined_users.count > 10 {
            isWinnerFlag = true
        }else{
            isWinnerFlag = false
            if content!.joined_users.count != 0 {
                winnerSyncCount = content!.joined_users.count+1
            }else{
                winnerSyncCount = 0
            }
            
        }
        
        if content!.joined_users.count > 10 {
            isSponsorsFlag = true
        }else{
            isSponsorsFlag = false
            
            if content?.joined_users.count != 0 {
                sponsorSyncCount = (content?.joined_users.count)!+1
            }else{
                sponsorSyncCount = 0
            }
            
        }
////     for test
//        isWinnerFlag = true
//        isSponsorsFlag = true
//        winnerSyncCount = 11
//        sponsorSyncCount = 11
//        
        ////////
        self.view.setNeedsLayout()
    }
    
    func refreshData() {
        tbContent.reloadData()
        self.view.layoutIfNeeded()
    }
    
    // MARK: - UITableViewDelegate & UITableViewDataSource
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var contentHeight : CGFloat = 48
        
        if section == 3 {
            contentHeight = 58
        }
        
        let headerView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: self.view.frame.width, height: contentHeight)))
        
        let headerContentView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: self.view.frame.width, height: 48)))
        
        headerView.clipsToBounds = false
        let label = UILabel(frame: CGRect(x: 30, y: 0, width: self.view.frame.width-60, height: 48))
        
        
        let endDate = content!.evaluation_end_dt
        
        if endDate == nil || endDate <= NSDate() {
            label.text = aryTableHeader1[section] as? String
        } else {
            label.text = aryTableHeader2[section] as? String
        }
        
        
        label.textAlignment = NSTextAlignment.Left
        
        headerView.backgroundColor = UIColor.whiteColor()
        headerContentView.backgroundColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.1)
        
        label.textColor = UIColor.blackColor()
        label.font = Helper.proximaNova("Bold", font: 20)
        
        headerContentView.addSubview(label)
        
        headerView.addSubview(headerContentView)
        
        return headerView
        
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        let endDate = content!.evaluation_end_dt
        
        if endDate == nil || endDate <= NSDate() {
            return aryTableHeader1.count
        } else {
            return aryTableHeader2.count
        }
        
        
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let endDate = content!.evaluation_end_dt
        
        if endDate == nil || endDate <= NSDate() {
            return self.aryTableHeader1[section] as? String
        } else {
            return self.aryTableHeader2[section] as? String
        }
        
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let endDate = content!.evaluation_end_dt
        
        if endDate == nil || endDate <= NSDate() {
            if section == 0 || section == 1 || section == 2 || section == 3 {
                return 1
            }else if section == 4{
                
                return winnerSyncCount+Int(isWinnerFlag)
            }else{
                
                return sponsorSyncCount+Int(isSponsorsFlag)
            }
        } else {
            if section == 0 || section == 1 || section == 2 || section == 3 {
                return 1
            }else{
                
                return sponsorSyncCount+Int(isSponsorsFlag)
            }
        }
        
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        let endDate = content!.evaluation_end_dt
        
        if endDate == nil || endDate <= NSDate() {
            if section == 4 {
                if winnerSyncCount + Int(isWinnerFlag) == 0 {
                    return 48
                }
                return 58
            }else if section == 5{
                return 58
            }
            return 48
            
        }else{
            if section == 4 {
                
                return 58
            }
            return 48
            
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 70
        }else if indexPath.section == 1 || indexPath.section == 2{
            return 69
        }else if indexPath.section == 3{
            
            let endDate = content!.evaluation_end_dt
            
            if endDate == nil || endDate <= NSDate() {
                return 77
            } else {
                return 129
            }
            
        }else if indexPath.section == 4{
            
            let endDate = content!.evaluation_end_dt
            
            if endDate == nil || endDate <= NSDate() {
                
                if isWinnerFlag {
                    if indexPath.row == winnerSyncCount {
                        return 44
                    }else{
                        return 37
                    }
                }else{
                    
                    if indexPath.row == winnerSyncCount-1 {
                        return 57
                    }
                    return 37
                }
                
            }else{
                if isSponsorsFlag {
                    if indexPath.row == sponsorSyncCount {
                        return 44
                    }else{
                        return 37
                    }
                }else{
                    return 37
                }
            }
            
            
        }else if indexPath.section == 5{
            if isSponsorsFlag {
                if indexPath.row == sponsorSyncCount {
                    return 44
                }else{
                    return 37
                }
            }else{
                return 37
            }
            
        }
        return 1
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var tbCell : UITableViewCell!
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("PrizeDefaultCell", forIndexPath: indexPath) as! PrizeDefaultCell
            
            cell.lblPrice.text = "\(content!.prize_pool)"
            
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            
            tbCell = cell
            
        }else if indexPath.section == 1{
            
            let cell = tableView.dequeueReusableCellWithIdentifier("PrizeDistributionCell", forIndexPath: indexPath) as! PrizeDistributionCell
            
            if content!.distribution_for == nil {
                cell.lblTitle.text = "Top \(0) Gives ranked by number of Likes*."
            } else {
                let forNum = content!.distribution_for!
                cell.lblTitle.text = "Top \(forNum) Gives ranked by number of Likes*."
            }
            cell.lblDesc.text = "(*Likes by Askers.)"
            
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            
            tbCell = cell
            
        }else if (indexPath.section == 2){
            let cell = tableView.dequeueReusableCellWithIdentifier("PrizeDistributionCell", forIndexPath: indexPath) as! PrizeDistributionCell
            
            
            if content!.distribution_rule == Constants.ApiConstants.prizeDistributionEvenly {
                cell.lblTitle.text = "Split evenly"
            } else {
                cell.lblTitle.text = "Split proportionally to %Likes*."
            }
            
            cell.lblDesc.text = "(*Likes by Askers.)"
            
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            
            tbCell = cell
        
        }else if indexPath.section == 3 {
            
            
            let cell = tableView.dequeueReusableCellWithIdentifier("PrizeEvaluationCell", forIndexPath: indexPath) as! PrizeEvaluationCell
            cell.content = content
            
            let startDate = content!.evaluation_start_dt
            cell.lblStartTime.text = startDate?.stringWithFormat(Constants.Literals.localDateFormat)
            let endDate = content!.evaluation_end_dt
            cell.lblEndTime.text = endDate?.stringWithFormat(Constants.Literals.localDateFormat)
            if endDate == nil || endDate <= NSDate() {
                cell.lblLeftTime.text = "Ended"
                cell.lblLeftTime.hidden = true
            } else {
                cell.lblLeftTime.hidden = false
                let diff = endDate!.difference(NSDate(), unitFlags: NSCalendarUnit(rawValue: UInt.max))
                let hourDiff = -(diff.hour)
                let minDiff = -(diff.minute)
                cell.lblLeftTime.text = "\(hourDiff)h \(minDiff)m Left"
            }
            tbCell = cell
            
        }else if indexPath.section == 4{
            
            let endDate = content!.evaluation_end_dt
            
            if endDate == nil || endDate <= NSDate() {
                
                if isWinnerFlag {
                    if indexPath.row == winnerSyncCount {
                        let cell = tableView.dequeueReusableCellWithIdentifier("SeeMoreCell", forIndexPath: indexPath) as! SeeMoreCell
                        
                        cell.btnSeeMore.addTarget(self, action: #selector(CardDetailPrizeVC.onSeeMoreWinners(_:)), forControlEvents:.TouchUpInside )
                        
                        tbCell = cell
                    }else{
                        let cell = tableView.dequeueReusableCellWithIdentifier("PrizeWinnersCell", forIndexPath: indexPath) as! PrizeWinnersCell
                        
                        if indexPath.row == 0 {
                            cell.titleView.hidden = false
                            cell.rankView.hidden = true
                        }else{
                            cell.rankView.hidden = false
                            cell.titleView.hidden = true
                            
                            let user = content!.joined_users[indexPath.row-1]
                            cell.imgPhoto.clipsToBounds = true
                            cell.imgPhoto.layer.cornerRadius = cell.imgPhoto.frame.size.width/2.0
                            cell.imgPhoto.af_setImageWithURL(NSURL(string: user.thumbnail_url)!)
                            cell.lblName.text = user.username
                            cell.lblPoint.text = String(user.contribution)
                            cell.lblRank.text = String(indexPath.row)
                            cell.lblLikers.text = String(87 - 11 * (indexPath.row-1))
                            
                        }
                        
                        
                        tbCell = cell
                    }
                }else{
                    
                    let cell = tableView.dequeueReusableCellWithIdentifier("PrizeWinnersCell", forIndexPath: indexPath) as! PrizeWinnersCell
                    
                    if indexPath.row == 0 {
                        cell.titleView.hidden = false
                        cell.rankView.hidden = true
                    }else{
                        cell.rankView.hidden = false
                        cell.titleView.hidden = true
                        
                        let user = content!.joined_users[indexPath.row-1]
                        cell.imgPhoto.clipsToBounds = true
                        cell.imgPhoto.layer.cornerRadius = cell.imgPhoto.frame.size.width/2.0
                        cell.imgPhoto.af_setImageWithURL(NSURL(string: user.thumbnail_url)!)
                        cell.lblName.text = user.username
                        cell.lblPoint.text = String(user.contribution)
                        cell.lblRank.text = String(indexPath.row)
                        cell.lblLikers.text = String(87 - 11 * (indexPath.row-1))
                        
                    }
                    
                    
                    tbCell = cell
                    
                }
                
            }else{
                if isSponsorsFlag {
                    if indexPath.row == sponsorSyncCount {
                        let cell = tableView.dequeueReusableCellWithIdentifier("SeeMoreCell", forIndexPath: indexPath) as! SeeMoreCell
                        
                        cell.btnSeeMore.addTarget(self, action: #selector(CardDetailPrizeVC.onSeeMoreSponsors(_:)), forControlEvents:.TouchUpInside )
                        
                        tbCell = cell
                    }else{
                        let cell = tableView.dequeueReusableCellWithIdentifier("PrizeSponsorsCell", forIndexPath: indexPath) as! PrizeSponsorsCell
                        
                        if indexPath.row == 0 {
                            cell.titleView.hidden = false
                            cell.rankView.hidden = true
                        }else{
                            cell.rankView.hidden = false
                            cell.titleView.hidden = true
                            
                            let user = content!.joined_users[indexPath.row-1]
                            cell.imgPhoto.clipsToBounds = true
                            cell.imgPhoto.layer.cornerRadius = cell.imgPhoto.frame.size.width/2.0
                            cell.imgPhoto.af_setImageWithURL(NSURL(string: user.thumbnail_url)!)
                            cell.lblName.text = user.username
                            cell.lblPoint.text = String(user.contribution)
                            
                        }
                        
                        tbCell = cell
                    }
                }else{
                    let cell = tableView.dequeueReusableCellWithIdentifier("PrizeSponsorsCell", forIndexPath: indexPath) as! PrizeSponsorsCell
                    
                    if indexPath.row == 0 {
                        cell.titleView.hidden = false
                        cell.rankView.hidden = true
                    }else{
                        cell.rankView.hidden = false
                        cell.titleView.hidden = true
                        
                        let user = content!.joined_users[indexPath.row-1]
                        cell.imgPhoto.clipsToBounds = true
                        cell.imgPhoto.layer.cornerRadius = cell.imgPhoto.frame.size.width/2.0
                        cell.imgPhoto.af_setImageWithURL(NSURL(string: user.thumbnail_url)!)
                        cell.lblName.text = user.username
                        cell.lblPoint.text = String(user.contribution)
                        
                    }
                    
                    tbCell = cell
                }
            }
            
            
            
        
        }else{
            
            if isSponsorsFlag {
                if indexPath.row == sponsorSyncCount {
                    let cell = tableView.dequeueReusableCellWithIdentifier("SeeMoreCell", forIndexPath: indexPath) as! SeeMoreCell
                    
                    cell.btnSeeMore.addTarget(self, action: #selector(CardDetailPrizeVC.onSeeMoreSponsors(_:)), forControlEvents:.TouchUpInside )
                    
                    tbCell = cell
                }else{
                    let cell = tableView.dequeueReusableCellWithIdentifier("PrizeSponsorsCell", forIndexPath: indexPath) as! PrizeSponsorsCell
                    
                    if indexPath.row == 0 {
                        cell.titleView.hidden = false
                        cell.rankView.hidden = true
                    }else{
                        cell.rankView.hidden = false
                        cell.titleView.hidden = true
                        
                        let user = content!.joined_users[indexPath.row-1]
                        cell.imgPhoto.clipsToBounds = true
                        cell.imgPhoto.layer.cornerRadius = cell.imgPhoto.frame.size.width/2.0
                        cell.imgPhoto.af_setImageWithURL(NSURL(string: user.thumbnail_url)!)
                        cell.lblName.text = user.username
                        cell.lblPoint.text = String(user.contribution)
                        
                    }
                    
                    tbCell = cell
                }
            }else{
                let cell = tableView.dequeueReusableCellWithIdentifier("PrizeSponsorsCell", forIndexPath: indexPath) as! PrizeSponsorsCell
                
                if indexPath.row == 0 {
                    cell.titleView.hidden = false
                    cell.rankView.hidden = true
                }else{
                    cell.rankView.hidden = false
                    cell.titleView.hidden = true
                    
                    let user = content!.joined_users[indexPath.row-1]
                    cell.imgPhoto.clipsToBounds = true
                    cell.imgPhoto.layer.cornerRadius = cell.imgPhoto.frame.size.width/2.0
                    cell.imgPhoto.af_setImageWithURL(NSURL(string: user.thumbnail_url)!)
                    cell.lblName.text = user.username
                    cell.lblPoint.text = String(user.contribution)
                    
                }
                
                tbCell = cell
            }
            
            
        }
        
        
        return tbCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Do Nothing
    }
    
    // MARK: - UIScrollViewDelegate
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
    }
    
    func onSeeMoreSponsors(sender: UIButton!){
        
        if content?.joined_users.count > sponsorSyncCount + 10 {
            sponsorSyncCount += 10
            isSponsorsFlag = true
        }else{
            sponsorSyncCount = (content?.joined_users.count)!
            isSponsorsFlag = false
        }
        
//        //for test
//        sponsorSyncCount += 10
//        isSponsorsFlag = true
//        ///////
        
        tbContent.reloadData()
    }
    
    func onSeeMoreWinners(sender: UIButton!){
     
        if content?.joined_users.count > winnerSyncCount + 10 {
            winnerSyncCount += 10
            isWinnerFlag = true
        }else{
            winnerSyncCount = (content?.joined_users.count)!
            isWinnerFlag = false
        }
        
//        //for test
//        winnerSyncCount += 10
//        isWinnerFlag = false
//        /////
        
        tbContent.reloadData()
    }
}
