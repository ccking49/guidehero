//
//  UIStoryboardExtensions.swift
//  GuideHero
//
//  Created by Justin Holman on 1/31/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import Foundation

extension UIStoryboard {

    enum AppStoryboards: String {
        case accountManagement = "AccountManagement"
        case camera = "Camera"
        case profile = "Profile"
    }

    convenience init(appStoryboard: AppStoryboards) {
        self.init(name: appStoryboard.rawValue, bundle: nil)
    }
    
}
