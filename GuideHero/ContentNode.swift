//
//  ContentItem.swift
//  GuideHero
//
//  Created by Ignacio Rodrigo on 9/15/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

enum ContentType: String {
    case Deck = "deck"
    case Text = "text"
    case Image = "image"
    case Video = "video"
    case Web = "web"
}

enum EvaluationPeriodStatus: String{
    case Done = "done"
    case Open = "open"
    case Close = "close"
}

public class ContentNode: CustomDebugStringConvertible {

    // MARK: - Stored properties

    var id: String
    var type: ContentType
    var name: String
    var description: String?
    var content: String? // can be nil or anything from text, image URL, video ID, etc...
    var thumbnailURL: String?
    var language: String?
    var format: String?
    var pronunciation: Bool
    var ask_enabled: Bool
    var asked_users: [UserNode]
    var joined_users: [UserNode]
    var givers: [UserNode] // Users who Gave
    var creator: String
    var creatorFullName: String
    var creatorBio: String
    var creatorInfo: UserNode?
    var creatorThumbnail: String
    var isAnswer: Bool
    var tags: [String]
    
    var followers: Int
    var following: Bool
    var createdAt: Int // unix timestamp
    var updatedAt: Int // unix timestamp
    var children: [ContentNode]
    var comments: [CommentNode]
    
    var published: Bool
    var likes: Int
    var liked_by_me: Bool
    var image_x: CGFloat
    var image_y: CGFloat
    var image_width: CGFloat
    var image_height: CGFloat
    var image_scale: Int
    
    var prize_pool: Int
    var prize_to_join: Int
    var distribution_for: Int?
    var distribution_rule: String
    var evaluation_end_dt: NSDate?
    var evaluation_start_dt: NSDate?
    var evaluation_period_status: EvaluationPeriodStatus

    // determines how long the content is
    var contentLength: Float = 0.5
    
    weak var parentContent: ContentNode?

    // MARK: - Calculated properties

    public var debugDescription: String {
        get {
            let dic: [String: String] = [
                "type": self.type.rawValue,
                "content": self.content ?? "None",
                "name": self.name,
                "creator": self.creator,
                "creator_full_name": self.creatorFullName,
                "creator_bio": self.creatorBio,
                "children": self.children.debugDescription
            ]
            return dic.debugDescription
        }
    }

    var contentURL: NSURL? {
        get {
            if let string = self.content {
                return NSURL(string: string)
            }
            else {
                return nil
            }
        }
    }

    var formattedCreator: String {
        get {
            return "by \(self.creator)"
        }
    }

    var formattedCreatorAndDate: String {
        get {
            return "\(self.creator) · \(self.formattedCreationDate)"
        }
    }

    var formattedCreationDate: String {
        get {
            let creationDate = NSDate(timeIntervalSince1970: NSTimeInterval(self.createdAt))
            return NSDateComponentsFormatter.sharedFormatter.stringFromDate(creationDate, toDate: NSDate()) ?? ""
        }
    }

    var creatorThumbnailURL: NSURL? {
        get {
            return NSURL(string: self.creatorThumbnail)
        }
    }
    
    var isDeck: Bool {
        return self.type == ContentType.Deck
    }
    
    var maskRect: CGRect {
        return CGRect(x: image_x, y: image_y, width: image_width, height: image_height)
    }
    
    var isAnyone: Bool {
        if asked_users.count == 0 {
            return true
        }
        for user in asked_users {
            if user.user_id == "_anyone_" {
                return true
            }
        }
        return false
    }

    var isJoined: Bool {
        for user in joined_users {
            if Session.sharedSession.user?.user_id == user.user_id {
                return true
            }
        }
        return false
    }

    var isAskCreator: Bool {
        for user in joined_users {
            if Session.sharedSession.user?.user_id == user.user_id && user.ask_creator {
                return true
            }
        }
        return false
    }

    var isGiver: Int { // 2: Creator, 1: Giver, 0: Not giver
        for user in givers {
            if Session.sharedSession.user?.user_id == user.user_id {
                if user.username == creator {
                    return 2
                }
                return 1
            }
        }
        return 0
    }

    var deckLength: Float {
        if isDeck {
            return self.children.reduce(0) {$0 + $1.contentLength}
        }

        return 0
    }
    
    
    // Cards I Gave
    var myGives: [ContentNode] {
        var gives = [ContentNode]()
        for card in children {
            if card.creator == Session.sharedSession.user?.username && card.isAnswer {
                gives.append(card)
            }
        }
        return gives
    }
    
    // Get an image URL for the content.
    // Works if the content is an image, or a deck containing an image.
    var imageURL: NSURL? {
        get {
            switch self.type {
            case .Image:
                return self.contentURL
            case .Deck:
                return self.deckImageURL
            default:
                return nil
            }
        }
    }
    
    var videoURL: NSURL? {
        get {
            switch self.type {
            case .Video:
                return self.contentURL
            default:
                return nil
            }
        }
    }

    // Get the image URL from the first image card in the deck, if any.
    private var deckImageURL: NSURL? {
        for child in self.children {
            if child.type == .Image {
                return child.contentURL
            }
            else if child.type == .Deck {
                return child.deckImageURL
            }
        }
        return nil
    }

    // Get the thumbnail image from the first text card in the deck, if any.
    private var deckImage: UIImage? {
        get {
            if self.type != .Deck {
                return nil
            }
            
            for child in self.children {
                if child.type == .Text {
                    let cardView = CardView.loadFromNib() as! CardView
                    cardView.content = child
                    cardView.layoutIfNeeded()
                    return cardView.snapshotImage()
                }
                else if child.type == .Deck {
                    return child.deckImage
                }
            }
            return nil
        }
    }

    // MARK: - Object lifecycle

    init(dictionary: [String: AnyObject]) {
        self.id = dictionary["id"] as! String
        self.type = ContentType(rawValue: dictionary["type"] as! String)!
        self.name = dictionary["name"] as! String
        self.description = dictionary["description"] as? String
        self.content = dictionary["content"] as? String
        self.thumbnailURL = dictionary["sub_content"] as? String
        self.language = dictionary["language"] as? String
        self.format = dictionary["format"] as? String
        self.pronunciation = dictionary["pronunciation"] as? Bool ?? false
        self.ask_enabled = dictionary["ask_enabled"] as? Bool ?? false
        self.creator = dictionary["creator"] as! String
        self.creatorFullName = dictionary["creator_full_name"] as! String
        self.creatorBio = dictionary["creator_bio"] as! String

        if dictionary["creator_info"] != nil {
            self.creatorInfo = UserNode(dictionary:dictionary["creator_info"] as! [String: AnyObject])
        } else {
            self.creatorInfo = nil
        }
        
        self.isAnswer = dictionary["is_answer"] as? Bool ?? false
        
        if dictionary["tags"] != nil {
            self.tags = dictionary["tags"] as! [String]
        } else {
            self.tags = []
        }
        
        self.creatorThumbnail = dictionary["creator_thumbnail"] as! String
        self.followers = dictionary["followers"] as? Int ?? 0 // TODO: remove hardcoded value when API is updated
        self.following = dictionary["following"] as? Bool ?? false // TODO: remove hardcoded value when API is updated
        self.prize_pool = dictionary["original_prize_pool"] as? Int ?? 0
        self.prize_to_join = dictionary["prize_to_join"] as? Int ?? 0
        self.createdAt = dictionary["created_at"] as! Int
        self.updatedAt = dictionary["updated_at"] as! Int
        self.published = dictionary["published"] as? Bool ?? false
        self.likes = dictionary["likes"] as? Int ?? 0
        self.liked_by_me = dictionary["liked_by_me"] as? Bool ?? false
        self.image_x = dictionary["image_x"] as? CGFloat ?? 0
        self.image_y = dictionary["image_y"] as? CGFloat ?? 0
        self.image_width = dictionary["image_width"] as? CGFloat ?? 0
        self.image_height = dictionary["image_height"] as? CGFloat ?? 0
        self.image_scale = dictionary["image_scale"] as? Int ?? 0
        self.distribution_for = dictionary["distribution_for"] as? Int
        self.distribution_rule = dictionary["distribution_rule"] as? String ?? Constants.ApiConstants.prizeDistributionProportionally
        
        let endDateString = dictionary["evaluation_end_dt"] as? String
        self.evaluation_end_dt = endDateString?.toDateInGMT(Constants.Literals.apiDateFormat)
        let startDateString = dictionary["evaluation_start_dt"] as? String
        self.evaluation_start_dt = startDateString?.toDateInGMT(Constants.Literals.apiDateFormat)
//        let evaluationStatus = dictionary["evaluation_period_status"] as? EvaluationPeriodStatus ?? .Close
        let status = dictionary["evaluation_period_status"] as? String ?? "close"
        let evaluationStatus = EvaluationPeriodStatus(rawValue: status)!
        self.evaluation_period_status = evaluationStatus

        // data back could be "<null?"
        if let videoLength = dictionary["video_length"] {
            if let converted = videoLength as? Float {
                self.contentLength = converted
            }
        }
        
        self.children = []
        self.comments = []
        self.asked_users = []
        self.joined_users = []
        self.givers = []
        
        self.parseChildContent(dictionary)
        self.parseComments(dictionary)
        self.parseAskedUsers(dictionary)
        self.parseJoinedUsers(dictionary)
        self.parseGivers(dictionary)
    }

    private func parseChildContent(dictionary: [String: AnyObject]) {
        for child in (dictionary["children"] as? [[String: AnyObject]] ?? []) {
            let childContent = ContentNode(dictionary: child)
            childContent.parentContent = self
            self.children.append(childContent)
        }
    }
    
    private func parseComments(dictionary: [String: AnyObject]) {
        for comment in (dictionary["comments"] as? [[String: AnyObject]] ?? []) {
            let commentContent = CommentNode(dictionary: comment)
            commentContent.parentContent = self
            commentContent.parentComment = nil
            
            for sub_comment in (dictionary["comments"]!["sub_comments"] as? [[String: AnyObject]] ?? []) {
                let sub_commentContent = CommentNode(dictionary: sub_comment)
                sub_commentContent.parentContent = self
                sub_commentContent.parentComment = commentContent
                commentContent.sub_comments.append(sub_commentContent)
            }
            
            self.comments.append(commentContent)
        }
    }
    private func parseAskedUsers(dictionary: [String: AnyObject]) {
        for user in (dictionary["asked_users"] as? [[String: AnyObject]] ?? []) {
            let userContent = UserNode(dictionary: user)
            self.asked_users.append(userContent)
        }
    }

    private func parseJoinedUsers(dictionary: [String: AnyObject]) {
        for user in (dictionary["joined_users"] as? [[String: AnyObject]] ?? []) {
            let userContent = UserNode(dictionary: user)
            self.joined_users.append(userContent)
        }
    }

    private func parseGivers(dictionary: [String: AnyObject]) {
        for user in (dictionary["givers"] as? [[String: AnyObject]] ?? []) {
            let userContent = UserNode(dictionary: user)
            self.givers.append(userContent)
        }
    }

    func getTextCardSnapshot() -> UIImage {
        // create label
        let label = UITextView()
        label.text = self.content
        label.textAlignment = .Center
        label.sizeToFit()
        // create white square background view
        let size = max(label.bounds.width, label.bounds.height) + 10
        let view = UIView(frame: CGRectMake(0, 0, size, size))
        view.backgroundColor = UIColor.whiteColor()
        view.addSubview(label)
        label.center = view.center
        // return a snaphot of the view
        return view.snapshotImage()
    }
    
    func setThumbnailImage(thumbnailView: CardContainerView) {
        switch (type) {
            case .Image :
                thumbnailView.imageView.setImageWithUrl(imageURL!, rect: maskRect)
                return
            case .Text:
                // return a snaphot of the view
                thumbnailView.imageView.image = getTextCardSnapshot()
                return
            case .Deck:
                self.children[0].setThumbnailImage(thumbnailView)
                return
            default:
                return
        }
    }
    
    
    func setThumbnailImage(targetView: MaskImageView, containerView: UIImageView, isDeck: Bool) {
        switch (type) {
            case .Image :
                targetView.setImageWithUrl(imageURL!, rect: maskRect)
                targetView.layer.borderWidth = 1
                targetView.layer.borderColor = UIColor.grayColor().CGColor
                
                if isDeck {
                    containerView.layer.borderWidth = 1
                    containerView.layer.borderColor = UIColor.grayColor().CGColor
                    containerView.layer.cornerRadius = 10
                    containerView.hidden = false;
                } else {
                    containerView.hidden = true;
                }
                return
            case .Text:
                // return a snaphot of the view
                targetView.image = getTextCardSnapshot()
                targetView.layer.borderWidth = 1
                targetView.layer.borderColor = UIColor.grayColor().CGColor
                
                if isDeck {
                    containerView.layer.borderWidth = 1
                    containerView.layer.borderColor = UIColor.grayColor().CGColor
                    containerView.layer.cornerRadius = 10
                    containerView.hidden = false;
                } else {
                    containerView.hidden = true;
                }
                return
            case .Deck:
                self.children[0].setThumbnailImage(targetView, containerView: containerView, isDeck: isDeck)
                return
            default:
                return
        }
    }
    
    func setBlurImage(backgroundImage: MaskImageView, contentImage: MaskImageView, blur: BlurFilter) {
        switch (type) {
        case .Image :
            backgroundImage.setImageWithUrl(imageURL!, rect: maskRect)
            contentImage.setImageWithUrl(imageURL!, rect: maskRect)
            return
        case .Text:
            let image = getTextCardSnapshot()
            backgroundImage.image = blur.filter(image)
            contentImage.image = image
            contentImage.layer.borderWidth = 1
            contentImage.layer.borderColor = UIColor.grayColor().CGColor
            
            return
        case .Deck:
            self.children[0].setBlurImage(backgroundImage, contentImage: contentImage, blur: blur)
            return
        default:
            return
        }
    }
}
