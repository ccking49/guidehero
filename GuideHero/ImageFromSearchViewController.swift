//
//  ImageFromSearchViewController.swift
//  GuideHero
//
//  Created by Dino Bartosak on 10/10/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

// TODO: rename this class and refactor accordingly
class ImageFromSearchViewController: UIViewController, UITextFieldDelegate, ImageBrowserCollectionViewControllerDelegate {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var imageButton: UIButton!
    @IBOutlet var videoButton: UIButton!
    @IBOutlet var pageButton: UIButton!
    
    // MARK: - Button actions
    
    @IBAction func cancel(sender: AnyObject) {
        textField.text = nil
        self.tabBarController?.selectedIndex = Constants.UIConstants.learnTabIndex
    }
    
    @IBAction func search(sender: AnyObject) {
        handleInputText();
    }
    
    @IBAction func imageCard(sender: UIButton) {
        self.selectCardButton(sender)
        self.textField.placeholder = "Search an image or enter image URL"
        self.searchButton.hidden = false
    }

    @IBAction func videoCard(sender: UIButton) {
        self.selectCardButton(sender)
        self.textField.placeholder = "Link a video by URL"
        self.searchButton.hidden = true
    }

    @IBAction func pageCard(sender: UIButton) {
        self.selectCardButton(sender)
        self.textField.placeholder = "Link a Webpage by URL"
        self.searchButton.hidden = true
    }

    private func selectCardButton(selectedButton: UIButton) {
        for button in [imageButton, videoButton, pageButton] {
            button.selected = (button == selectedButton)
        }
    }

    // MARK: UITextFieldDelegate

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        handleInputText();
        return true;
    }
    
    // MARK: Private
    
    private func handleInputText() {
        textField.resignFirstResponder();
        
        guard let searchText = textField.text else {
            return;
        };
        
        self.showProgressHUD()
        
        let searchUrl: NSURL? = NSURL(string: searchText);
        if searchUrl != nil && searchUrl!.isValidURL() {
            downloadPhotoAtURL(searchText, completion: { (image, error) in
                self.hideProgressHUD();
                
                if let image = image {
                    self.showCardCreationWithImage(image);
                } else {
                    self.showSimpleAlert(withTitle: "Photo search error", withMessage: "Could not download photo");
                }
            });
        } else {
            searchPhotosByText(searchText, completion: { (photos, error) in
                self.hideProgressHUD();
                
                if error != nil {
                    print("show search error alert")
                    self.showSimpleAlert(withTitle: "Photo search error", withMessage: error?.localizedDescription);
                } else if photos.count > 0 {
                    self.showPhotoSearchBrowser(withPhotoURLs: photos, title: searchText);
                } else {
                    self.showSimpleAlert(withTitle: "Photo search error", withMessage: "No photos found");
                }
            })
        }
    }
    
    private func downloadPhotoAtURL(urlString: String, completion: (image: UIImage?, error: NSError?) -> Void) {
        Alamofire.request(.GET, urlString).responseData { response in
            switch response.result {
            case .Success(let data):
                if let image = UIImage(data: data) {
                    completion(image: image, error: nil);
                } else {
                    completion(image: nil, error: NSError(domain: "com.guidehero", code: 0, userInfo: [NSLocalizedDescriptionKey : "photo error"]));
                }
                break

            case .Failure(let error):
                completion(image: nil, error: error);
                break
            }
        }
    }
    
    private func searchPhotosByText(searchText: String, completion: (photos: [NSURL], error: NSError?) -> Void) {
        let searchUrl = Constants.ApiConstants.baseApiUrl + "deck/get_image_search_urls"
        let request = Alamofire.request(.POST, searchUrl, parameters: ["search_key" : searchText], encoding: .JSON, headers: nil);
        request.responseJSON { (response) in
            switch response.result {
            case .Success(let JSON):
                let responseJSON: [String:AnyObject] = JSON as? [String:AnyObject] ?? [:]
                let photoURLs: [String] = responseJSON["urls"] as? [String] ?? []
                var photos: [NSURL] = []
                for photoURLString in photoURLs {
                    if let photoURL = NSURL(string: photoURLString) {
                        photos.append(photoURL);
                    }
                }
                completion(photos: photos, error: nil);
                
            case .Failure(let error):
                completion(photos: [], error: error);
            }
        }
    }
    
    // MARK: - Navigation
    
    private func showPhotoSearchBrowser(withPhotoURLs photoURLs: [NSURL], title: String) {
        let sender = ["photos" : photoURLs,
                      "title" : title]
        self.performSegueWithIdentifier("ShowImageBrowserController", sender: sender)
    }
    
    private func showCardCreationWithImage(image: UIImage?) {
        let vc = storyboard?.instantiateViewControllerWithIdentifier("ImageCardCreationController") as! ImageCardCreationController
        vc.image = image
        vc.cardName = textField.text
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? ImageCardCreationController {
            vc.image = sender as? UIImage
        } else if let vc = segue.destinationViewController as? ImageBrowserCollectionViewController {
            vc.delegate = self;
            
            let senderMap = sender as! [String : AnyObject]
            vc.photos = senderMap["photos"] as? [NSURL];
            vc.title = senderMap["title"] as? String;
        }
    }
    
    // MARK: ImageBrowserCollectionViewControllerDelegate
    
    func imageBrowserCollectionViewController(controller: ImageBrowserCollectionViewController, didSelectPhotoWithURL photoURL: NSURL) {
        self.showProgressHUD()
        downloadPhotoAtURL(photoURL.absoluteString!) { (image, error) in
            self.hideProgressHUD()
            if let image = image {
                self.showCardCreationWithImage(image);
            } else {
                self.showSimpleAlert(withTitle: "Photo download", withMessage: error?.localizedDescription);
            }
        }
    }
}

private extension NSURL {
    func isValidURL() -> Bool {
        return host != nil && scheme != nil
            && (scheme!.caseInsensitiveCompare("http") == .OrderedSame ||
                scheme!.caseInsensitiveCompare("https") == .OrderedSame);
    }
}
