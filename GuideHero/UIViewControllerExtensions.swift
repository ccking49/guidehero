//
//  UIViewControllerExtensions.swift
//  GuideHero
//
//  Created by Abhishek Sengar on 03/09/2016.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import Foundation
import PKHUD


extension UIViewController {

    // MARK: - Alerts
    
    func showSimpleAlert(withTitle title: String?, withMessage message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(OKAction)
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.presentViewController(alertController, animated: true, completion: nil)
        })
    }

    // MARK: - Progress indicator

    func showProgressHUD() {
        HUD.show(.Progress)
    }

    func hideProgressHUD() {
        HUD.hide()
    }

    func showSuccessHUD() {
        HUD.flash(.Success, delay: 1.0)
    }
    
    func showSuccessHUDWithCompletion(completion: (() -> Void)? = nil) {
        HUD.flash(.Success, delay: 0.3) { (finished) in
            completion?()
        }
    }

    func showErrorHUD() {
        HUD.flash(.Error, delay: 1.0)
    }
}
