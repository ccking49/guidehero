//
//  AskEditCell.swift
//  GuideHero
//
//  Created by HC on 11/15/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya
import KMPlaceholderTextView


class AskerCell: BaseTableViewCell, UITextViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var imgAvatar : UIImageView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var lblAccountType : UILabel!
    @IBOutlet weak var userView : UIView!
    @IBOutlet weak var NoticeView : UIView!
    @IBOutlet weak var avatarWidth: NSLayoutConstraint!
    
    var content: ContentNode! {
        didSet {
            updateUI()
        }
    }
    
    func updateUI() {
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: - @IBAction
    
    
}
