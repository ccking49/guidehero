//
//  NSLayoutConstraintExtensions.swift
//  GuideHero
//
//  Created by Ignacio Rodrigo on 10/6/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {

    func setMultiplier(multiplier: CGFloat) -> NSLayoutConstraint {

        // create and setup new constraint with new multiplier
        let newConstraint = NSLayoutConstraint(
            item: firstItem,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)

        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier

        // activate new constraint and deactivate the old one
        self.active = false
        newConstraint.active = true

        return newConstraint
    }

}
