//
//  AskEditCell.swift
//  GuideHero
//
//  Created by HC on 11/15/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya
import KMPlaceholderTextView

class FollowCell: BaseTableViewCell {

    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var lblFullName : UILabel!
    @IBOutlet weak var lblUserName : UILabel!
    @IBOutlet weak var lblBio : UILabel!
    @IBOutlet weak var btnRelation : UIButton!
    
    var userNode: UserNode! {
        didSet {
            updateUI()
        }
    }
    
    private func updateUI() {
        
        self.layer.shadowRadius = 1
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSizeZero
        self.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 0.5).CGColor
        
        imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2
        imgProfile.layer.masksToBounds = true
        if self.userNode.thumbnail_url != "" {
            self.imgProfile.af_setImageWithURL(NSURL(string: self.userNode.thumbnail_url)!)
        } else {
            self.imgProfile.image = UIImage(named: "me")
        }
        
        lblFullName.text = self.userNode.first_name + " " + self.userNode.last_name
        lblUserName.text = self.userNode.username
        lblBio.text = self.userNode.bio
        
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
