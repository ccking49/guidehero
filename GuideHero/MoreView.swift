//
//  CardView.swift
//  GuideHero
//
//  Created by Abhishek Sengar on 10/09/2016.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
protocol MoreDelegate {
    func onSelectButton(nIndex : Int)
}
public class MoreView: UIView {
    
    // MARK: - @IBOutlet Variables

    @IBOutlet weak var subView : UIView!
    @IBOutlet weak var mainView : UIView!
    @IBOutlet weak var tapGesture : UITapGestureRecognizer!
    @IBOutlet weak var constrantY : NSLayoutConstraint!
    @IBOutlet weak var moveHeight : NSLayoutConstraint!
    @IBOutlet weak var totalHeight : NSLayoutConstraint!
    // MARK: - Variables
    
    var delegate : MoreDelegate?
    
    // MARK: - View Lifecycle
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
    }
    
    // MARK: - Actions
    
    @IBAction func onTap (gesture : UITapGestureRecognizer){
        hideMoreView()
    }
    
    @IBAction func onSelectOption(sender: UIButton){
        
        var tag : Int
        tag = sender.tag
        
        self.delegate?.onSelectButton(tag)
        
    }
    
    // MARK: - Private Methods
    
    func onInit(){
        
        mainView.clipsToBounds = true
        mainView.layer.borderColor = UIColor.colorFromRGB(redValue: 155/255.0, greenValue: 155/255.0, blueValue: 155/255.0, alpha: 0.5).CGColor
        mainView.layer.borderWidth = 1
        
        mainView.roundTopCorners(20)
    }
    
    // MARK: - Public Methods
    
    public func hideMoreView() {
        UIView.animateWithDuration(0.5, delay: 0.0, options: [], animations: {
            
            self.constrantY.constant = -450
            self.layoutIfNeeded()
            
        }) { (true) in
            self.removeFromSuperview()
        }
    }
}
