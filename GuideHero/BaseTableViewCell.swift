//
//  BaseTableViewCell.swift
//  GuideHero
//
//  Created by HC on 12/17/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

    var needsRoundBottom: Bool?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        if needsRoundBottom == true {
            roundBottomCorners(Constants.UIConstants.detailCardCornerRadius)
        }
    }
}
