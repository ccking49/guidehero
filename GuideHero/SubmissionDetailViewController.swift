//
//  CardDetailPageVC.swift
//  GuideHero
//
//  Created by Promising Change on 23/12/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AlamofireImage
import Moya
import AVFoundation
protocol SubmissionDelegate {
    func onHideFrontBackground()
    func onShowFrontBackground()
}
class SubmissionDetailViewController: CardViewPagerController, CardViewPagerDataSource, CardViewPagerProgressDelegate, CardDetailButtonDelegate, UIGestureRecognizerDelegate, PannableCardViewDelegate ,CardDetailPlayVCDelegate {
    
    // MARK: - Variables for Card View Pager
    var delegate: SubmissionDelegate?
    
    @IBOutlet weak var vwMain: UIView!
    
    @IBOutlet weak var vwDraggableView: PannableCardView!
    
    @IBOutlet weak var vwHeader: UIView!
    @IBOutlet weak var stackViewActions: UIStackView!
    @IBOutlet weak var cardActionPlay: CardDetailButton!
    @IBOutlet weak var cardActionAsk: CardDetailButton!
    @IBOutlet weak var cardActionGive: CardDetailButton!
    @IBOutlet weak var cardActionPrize: CardDetailButton!
    
    @IBOutlet weak var constraintHeaderTop: NSLayoutConstraint!
    @IBOutlet weak var constraintActionsTop: NSLayoutConstraint!
    @IBOutlet weak var constraintContentBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintSocialTop: NSLayoutConstraint!
    
    @IBOutlet weak var askWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var giveWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var prizeWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var cancelButton : UIButton!
    @IBOutlet var submitButton : UIButton!
    @IBOutlet var removeButton : UIButton!
    @IBOutlet var bottomView : UIView!
    
    var originalHeaderTop: CGFloat?
    var originalActionsTop: CGFloat?
    var originalSocialTop: CGFloat?
    
    var cardDetailPlayVC: CardDetailPlayVC?
    var cardDetailAskVC: CardDetailAskVC?
    var cardDetailGiveVC: CardDetailGiveVC?
    var cardDetailPrizeVC: CardDetailPrizeVC?
    
    var panGestureRecognizer = UIPanGestureRecognizer()
    var tapGestureRecognizer = UITapGestureRecognizer()
    
    // MARK: - Variables for Card Header
    
//    @IBOutlet weak var backgroundImage: MaskImageView!
    @IBOutlet weak var creatorImage: UIImageView!
    @IBOutlet weak var creatorNameAndDate: UILabel!
    @IBOutlet weak var cardView: SubmissionContainerView!
//    @IBOutlet weak var imgCard : MaskImageView!
    @IBOutlet weak var cardBackgroundView : CardMaskContainerView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentsLabel: UILabel!
    
    @IBOutlet weak var actionView : UIView!
    
    @IBOutlet weak var imgLine1 : UIImageView!
//    @IBOutlet weak var imgLine2 : UIImageView!
    
    @IBOutlet weak var lockButton : UIButton!
    
    @IBOutlet weak var containBottomView : UIView!
    
    @IBOutlet weak var backgroundView : UIView!
    var cardActionButtons: [CardDetailButton] = []
    
    var submittedCallback: ((_: ContentNode!) -> Void)!
    
    var isType : Int! // 0:submit 1:remove 2: else

    var isBackType : Int! = 0
//    var isChild : Bool! = false
    
    var content: ContentNode? {
        
        didSet {
            if isViewLoaded() {
                // Update the Content
                
                cardDetailPlayVC?.content = self.content
                cardDetailAskVC?.content = self.content
                cardDetailGiveVC?.content = self.content
                cardDetailPrizeVC?.content = self.content
            }
            
        }
    }
    
    var cardId: String? // To load specific deck/card
    var contentIndex: Int = 0
    var onHeaderAction: ((HeaderCellAction) -> Void)!
    var currentContent: ContentNode!
    
    // MARK: - View Lifecycle
    
    override public init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        datasource = self
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        datasource = self
    }
    override public func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
//        UIApplication.sharedApplication().statusBarHidden = false
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        
        
        configureView()
        
        panGestureRecognizer.addTarget(self, action: #selector(CardDetailPageVC.handlePan(_:)))
        panGestureRecognizer.delegate = self
        vwDraggableView.addGestureRecognizer(panGestureRecognizer)
        
        tapGestureRecognizer.addTarget(self, action: #selector(CardDetailPageVC.handleTap(_:)))
        tapGestureRecognizer.delegate = self
        vwDraggableView.addGestureRecognizer(tapGestureRecognizer)
        
        imgLine1.layer.shadowRadius = 2
//        imgLine1.layer.shadowOpacity = 0.5
        imgLine1.layer.shadowOffset = CGSizeZero
        imgLine1.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 0.5).CGColor

//        imgCard.layer.shadowRadius = 2
////        imgCard.layer.shadowOpacity = 0.5
//        imgCard.layer.shadowOffset = CGSizeZero
//        imgCard.layer.shadowColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.5).CGColor
        
//        backgroundImage.layer.shadowRadius = 2
////        backgroundImage.layer.shadowOpacity = 0.5
//        backgroundImage.layer.shadowOffset = CGSizeZero
//        backgroundImage.layer.shadowColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.5).CGColor
        
        
    }
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        self.delegate?.onShowFrontBackground()
        
        if self.isType == 1 {
            backgroundView.hidden = false
        }else{
            backgroundView.hidden = true
        }
        
        self.vwMain.backgroundColor = UIColor.clearColor()
    }
    override public func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        super.viewWillAppear(animated)
        
        
        
        UIApplication.sharedApplication().statusBarHidden = true
        
        containerView.bringSubviewToFront(actionView)
//        containerView.pagingEnabled = false
//        containerView.scrollEnabled = false
        
        if self.content == nil {
            self.fetchAllDecks()
        }
    }
    
    
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Methods
    
    func configureView() {
        
        
        containBottomView.layer.cornerRadius = 20
        containBottomView.layer.shadowRadius = 5
        containBottomView.layer.shadowOpacity = 0.5
        containBottomView.layer.shadowOffset = CGSizeZero
        containBottomView.layer.shadowColor = UIColor.blackColor().CGColor
        containBottomView.clipsToBounds = true
        
        if (isType == 0) {
            submitButton.hidden = false
            removeButton.hidden = true
            lockButton.hidden = false
            bottomView.hidden = false
        }else if (isType == 1){
            submitButton.hidden = true
            removeButton.hidden = false
            lockButton.hidden = true
            bottomView.hidden = false
        }else{
            bottomView.hidden = true
            submitButton.hidden = true
            removeButton.hidden = true
            cancelButton.hidden = true
        }
        if self.content!.published == true {
            lockButton.hidden = true
        } else {
            lockButton.hidden = false
        }
        
        
        removeButton.layer.shadowColor = UIColor.blackColor().CGColor
        removeButton.layer.shadowOffset = CGSizeMake(0, 1)
        removeButton.layer.shadowOpacity = 0.5
        removeButton.layer.shadowRadius = 4
        
        submitButton.layer.shadowColor = UIColor.blackColor().CGColor
        submitButton.layer.shadowOffset = CGSizeMake(0, 1)
        submitButton.layer.shadowOpacity = 0.5
        submitButton.layer.shadowRadius = 4
        
        cancelButton.layer.shadowColor = UIColor.blackColor().CGColor
        cancelButton.layer.shadowOffset = CGSizeMake(0, 1)
        cancelButton.layer.shadowOpacity = 0.5
        cancelButton.layer.shadowRadius = 4
        
        originalHeaderTop = -1000
        originalActionsTop = -1000
        originalSocialTop = -1000
        
        // Card View Pager
        vwHeader.roundTopCorners(20)
        
        let cardActionInfoPlay: CardDetailButtonInfo = CardDetailButtonInfo(Id: 0, title: "Play", image: UIImage(named: "playOff"), selectedImage: UIImage(named: "playOn"), tintColor: UIColor(hexString: "#FE2851"))
        var cardActionInfoAsk: CardDetailButtonInfo = CardDetailButtonInfo(Id: 1, title: "Ask", image: UIImage(named: "requestOff"), selectedImage: UIImage(named: "requestOn"), tintColor: UIColor(hexString: "#FFCD00"))
        var cardActionInfoGive: CardDetailButtonInfo = CardDetailButtonInfo(Id: 2, title: "Give", image: UIImage(named: "offerOff"), selectedImage: UIImage(named: "offerOn"), tintColor: UIColor(hexString: "#BD10E0"))
        var cardActionInfoPrize: CardDetailButtonInfo = CardDetailButtonInfo(Id: 3, title: "Prize", image: UIImage(named: "prizeOff"), selectedImage: UIImage(named: "prizeOn"), tintColor: UIColor(hexString: "#0076FF"))
        
        cardActionPlay.bindItem(cardActionInfoPlay)
        cardActionPlay.delegate = self
        cardActionButtons.append(cardActionPlay)
        cardActionPlay.selectButton()
        
        cardActionInfoAsk.Id = cardActionButtons.count
        if (content?.ask_enabled == true) {
            cardActionAsk.bindItem(cardActionInfoAsk)
            cardActionAsk.delegate = self
            cardActionButtons.append(cardActionAsk)
        }
        else {
//            cardActionAsk.hidden = true
            askWidthConstraint.constant = 0
        }
        
        cardActionInfoGive.Id = cardActionButtons.count
        if (content?.ask_enabled == true) {
            cardActionGive.bindItem(cardActionInfoGive)
            cardActionGive.delegate = self
            cardActionButtons.append(cardActionGive)
        }
        else {
//            cardActionAsk.hidden = true
            giveWidthConstraint.constant = 0
        }
        
        cardActionInfoPrize.Id = cardActionButtons.count
        if (content?.ask_enabled == true) {
            cardActionInfoPrize.Id = cardActionButtons.count
            cardActionPrize.bindItem(cardActionInfoPrize)
            cardActionPrize.delegate = self
            cardActionButtons.append(cardActionPrize)
        }
        else {
//            cardActionPrize.hidden = true
            prizeWidthConstraint.constant = 0
        }
        
        view.layoutIfNeeded()
        
        // Card View Header
//        imgCard.clipsToBounds = true
//        imgCard.layer.cornerRadius = 5
//        cardView.isInList = false
//        cardView.content = self.content
//        print(content?.thumbnailURL)
//        if content?.thumbnailURL != nil {
//            backgroundImage.setImageWithUrlFromCardDetail(NSURL(string: content!.thumbnailURL!))
////            imgCard.setImageWithUrlWhiteBackground(NSURL(string: content!.thumbnailURL!) , rect: content!.maskRect)
//        }else{
//            backgroundImage.setImageWithUrlFromCardDetail(content?.imageURL)
////            imgCard.setImageWithUrlWhiteBackground(content?.imageURL , rect: content!.maskRect)
//        }
        
        cardView.content = self.content
        
        cardBackgroundView.content = self.content
        print(cardBackgroundView.frame)
//        cardView.content = self.content
//        cardBackgroundView.content = self.content
        
        let titleAttributes = [NSForegroundColorAttributeName: UIColor.blackColor(), NSFontAttributeName: Helper.proximaNova("bold", font: 22)]
        let descAttributes = [NSForegroundColorAttributeName: UIColor.blackColor(), NSFontAttributeName: Helper.proximaNova("Regular", font: 15)]
        
        let partTitle = NSMutableAttributedString(string: (self.content?.name)! + "\n\n", attributes: titleAttributes)
        let partDesc = NSMutableAttributedString(string: (self.content?.description)!, attributes: descAttributes)
        
        let combination = NSMutableAttributedString()
        
        combination.appendAttributedString(partTitle)
        combination.appendAttributedString(partDesc)
        
        descriptionLabel.attributedText = combination
        
        if let creatorImageURL = content?.creatorThumbnailURL {
            self.creatorImage.af_setImageWithURL(creatorImageURL)
        }
        self.creatorNameAndDate.text = content?.formattedCreatorAndDate
        self.creatorNameAndDate.layer.shadowRadius = 0.5
        self.creatorImage.layer.cornerRadius = 10
        
        let likes = content?.likes ?? 0
        likesLabel.text = String(likes)
        let imageName = (self.content!.liked_by_me == false) ? "grey_heart" : "heartFilled"
        self.likeButton.setImage(UIImage(named: imageName), forState: .Normal)
        let comments = content?.comments.count ?? 0
        commentsLabel.text = String(comments)
        
        onHeaderAction = { action in
            self.handleHeaderCellAction(action)
        }
        
        vwDraggableView.delegate = self
        
        self.view.setNeedsLayout()
    }
    
    // MARK: - Private methods
    
    private func fetchAllDecks() {
        if cardId == nil {
            return
        }
        self.showProgressHUD()
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.GetCard(cardId: cardId!)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't download decks, please try again later")
                    self.hideProgressHUD()
                    return
                }
                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    self.hideProgressHUD()
                    let dic = JSON["card"] as! [String: AnyObject]
                    self.content = ContentNode(dictionary: dic)
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't parse decks, please try again later")
                    self.hideProgressHUD()
                }
            case let .Failure(error):
                print("can't fetch decks: %@", error)
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't download decks, please try again later")
                self.hideProgressHUD()
            }
        }

    }
    
    func handleHeaderCellAction (action: HeaderCellAction) {
        if action == .Loop {
            let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "LoopController") as! LoopController
            vc.content = self.content
            self.presentViewController(vc, animated: true, completion: nil)
        } else if action == .Comment {
            let vc = UIStoryboard.loadViewController(storyboardName: "Comment", viewControllerIdentifier: "CommentsViewController") as! CommentsViewController
            vc.content = content
            self.presentViewController(vc, animated: true, completion: nil)
        } else if action == .Like {
            let liked = content!.liked_by_me
            let provider = MoyaProvider<NetworkService>.createDefaultProvider()
            let target = liked ? NetworkService.UnlikeCard(cardId : content!.id) : NetworkService.LikeCard(cardId : content!.id)
            
            provider.request(target) { result in
                switch result {
                case let .Success(response):
                    if response.statusCode == 200 {
                        self.content!.liked_by_me = !liked
                        self.content!.likes += liked ? -1 : 1
                        
                        // Update View
                        let likeCount = self.content!.likes ?? 0
                        self.likesLabel.text = String(likeCount)
                        let imageName = (self.content!.liked_by_me == false) ? "grey_heart" : "heartFilled"
                        self.likeButton.setImage(UIImage(named: imageName), forState: .Normal)
                    }
                case let .Failure(error):
                    print(error)
                }
            }
        } else {
//            currentMode = action
//            tableView.reloadData()
        }
    }

    
    // MARK: - Actions
    
    @IBAction func didTapOnClose(sender: AnyObject) {
        self.delegate?.onHideFrontBackground()
        self.dismissViewControllerAnimated(true) { 
            
        }
    }
    
    @IBAction func didTapOnMore(sender: AnyObject) {
        
    }
    
    @IBAction func didTapOnLike(sender: AnyObject) {
        onHeaderAction(.Like)
    }
    @IBAction func didTapOnComment(sender: AnyObject) {
        onHeaderAction(.Comment)
    }
    
    // MARK: - CardViewPagerDataSource
    
    
    public override func viewControllers(for viewPagerController: CardViewPagerController) -> [CardSubPageBaseVC] {
        var subPages = [CardSubPageBaseVC]()
        
        cardDetailPlayVC = UIStoryboard.loadViewController(storyboardName: "CardDetail", viewControllerIdentifier: "CardDetailPlayVC") as? CardDetailPlayVC
        cardDetailPlayVC?.delegate = self
        cardDetailPlayVC?.content = self.content
        cardDetailPlayVC?.isType = self.isType
        cardDetailPlayVC?.cardDetailPlayDelegate = self
        
        print(self.isType)
        subPages.append(cardDetailPlayVC!)
        
        if (content?.ask_enabled == true) {
            cardDetailAskVC = UIStoryboard.loadViewController(storyboardName: "CardDetail", viewControllerIdentifier: "CardDetailAskVC") as? CardDetailAskVC
            cardDetailAskVC?.delegate = self
            cardDetailAskVC?.content = self.content
            subPages.append(cardDetailAskVC!)
        }
        
        if (content?.ask_enabled == true) {
            cardDetailGiveVC = UIStoryboard.loadViewController(storyboardName: "CardDetail", viewControllerIdentifier: "CardDetailGiveVC") as? CardDetailGiveVC
            cardDetailGiveVC?.delegate = self
            cardDetailGiveVC?.content = self.content
            subPages.append(cardDetailGiveVC!)
        }
        
        if (content?.ask_enabled == true) {
            cardDetailPrizeVC = UIStoryboard.loadViewController(storyboardName: "CardDetail", viewControllerIdentifier: "CardDetailPrizeVC") as? CardDetailPrizeVC
            cardDetailPrizeVC?.delegate = self
            cardDetailPrizeVC?.content = self.content
            subPages.append(cardDetailPrizeVC!)
        }
        
        return subPages
    }
    
    // MARK: - CardViewPagerProgressDelegate
    
    public func updateIndicator(for viewController: CardViewPagerController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool, borderCrossed: Bool, direction: SwipeDirection) {
//        let changeFlag = borderCrossed ? "---------" : ""
//        print("Border crossed: " + changeFlag)
        
        if (toIndex != -1 && toIndex != cardActionButtons.count) {
            let cardActionButtonFrom = cardActionButtons[fromIndex]
            let cardActionButtonTo = cardActionButtons[max(toIndex, -1)]
            if (fromIndex < toIndex) {
                // Swipe Direction = Left, Transition Direction = Right
                cardActionButtonFrom.transitToRight(withProgressPercentage: progressPercentage)
                cardActionButtonTo.transitFromRight(withProgressPercentage: progressPercentage)
            } else {
                // Swipe Direction = Right, Transition Direction = Left
                cardActionButtonFrom.transitToLeft(withProgressPercentage: progressPercentage)
                cardActionButtonTo.transitFromLeft(withProgressPercentage: progressPercentage)
            }
            
            if (borderCrossed) {
                if (direction == .left) {
                    let cardActionButtonPast = cardActionButtons[max(fromIndex-1, 0)]
                    cardActionButtonPast.deselectButton()
                } else if (direction == .right) {
                    let pastIndex = min(fromIndex+1, cardActionButtons.count-1)
                    let cardActionButtonPast = cardActionButtons[pastIndex]
                    cardActionButtonPast.deselectButton()
                }
            }
        }
        
        if (toIndex < 0 || toIndex >= cardActionButtons.count) {
            let newButton = cardActionButtons[currentIndex]
            newButton.highlightButton()
        } else {
            let oldButton = cardActionButtons[currentIndex != fromIndex ? fromIndex : toIndex]
            let newButton = cardActionButtons[currentIndex]
            oldButton.dehighlightButton()
            newButton.highlightButton()
        }
    }
    
    public func updateIndicator(for viewController: CardViewPagerController, fromIndex: Int, toIndex: Int) {
        
    }
    
    // MARK: - CardDetailButtonDelegate
    
    func didTapOnCardActionButton(buttonID: Int) {
        guard buttonID != currentIndex else { return }
        
        let oldButton = cardActionButtons[currentIndex]
        let newButton = cardActionButtons[buttonID]
        oldButton.deselectButton()
        newButton.selectButton()
        
        moveToViewController(at: buttonID)
        
    }
    
    // MARK: - SubPageScrollViewDidScrollDelegate
    
    override public func subPageScrollViewDidScroll(offset: CGFloat, translation: CGPoint, animated: Bool) {
        super.subPageScrollViewDidScroll(offset, translation: translation, animated: animated)
        
        if (movingFactor != 0 ) {
            return
        }
        
        if (originalHeaderTop == -1000) {
            originalHeaderTop = constraintHeaderTop.constant
        }
        
        if (originalActionsTop == -1000) {
            originalActionsTop = constraintActionsTop.constant
        }
        
        if (originalSocialTop == -1000) {
            originalSocialTop = constraintSocialTop.constant
        }
        
        constraintHeaderTop.constant = originalHeaderTop! - offset
        constraintActionsTop.constant = originalActionsTop! - offset
        constraintSocialTop.constant = originalSocialTop! - offset
        
        if (!animated) {
            self.view.layoutIfNeeded()
        } else {
            UIView.animateWithDuration(0.25, animations: { 
                self.view.layoutIfNeeded()
            })
        }
    }
    
    override public func subPageScrollViewDidBounce(offset: CGFloat) {
        super.subPageScrollViewDidBounce(offset)
        
        constraintContentBottom.constant = 30 + offset
        self.view.layoutIfNeeded()
    }
    
    // MARK: - UITapGestureRecognizer
    
    func handleTap(tapGesture: UITapGestureRecognizer) {
        
        if (content!.parentContent != nil) {
            let fullScreenViewController = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "FullScreenViewController") as! FullScreenViewController
            fullScreenViewController.deckIndex = contentIndex
            fullScreenViewController.cardIndex = 0
            fullScreenViewController.content = content!.parentContent
            fullScreenViewController.modalPresentationStyle = .OverFullScreen
            self.presentViewController(fullScreenViewController, animated: true, completion: nil)
            
        }
    }

    // MARK: - UIPanGestureRecognizer
    
    public func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if (gestureRecognizer == tapGestureRecognizer) {
            let tapLocation = tapGestureRecognizer.locationInView(vwHeader)
            if (vwHeader.pointInside(tapLocation, withEvent: nil)) {
                
                if self.content?.children.count > 0  && self.content?.ask_enabled == true{
                    
                    if self.content?.children[0].type == .Deck {
                        let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "SubmissionDetailViewController") as! SubmissionDetailViewController
                        vc.content = self.content?.children[0]
                        vc.currentContent = self.content
                        vc.isType = self.isType
                        vc.modalPresentationStyle = .OverFullScreen
                        print(vc.isType)
                        if vc.isType == 1 {
                            
                            vc.submittedCallback = { rContent in
                                // Card submitted here
                                
                                self.dismissViewControllerAnimated(false, completion: {
                                    
                                    self.submittedCallback(rContent)
                                })
                                
                            }
                        }
                        presentViewController(vc, animated: true, completion: nil)
                    }
                }
                
                return true
            }
            
            return false
        }
        
        let velocity = panGestureRecognizer.velocityInView(self.vwDraggableView)
        
        let pannableToLeft = (currentPageIndex == 0 && fabs(velocity.x) >= fabs(velocity.y) && velocity.x > 0)
        let pannableToRight = (currentPageIndex == (viewControllers.count - 1) && fabs(velocity.x) >= fabs(velocity.y) && velocity.x < 0)
        
        let currentSubPage = viewControllers[currentPageIndex]
        let pannableFromTop = currentSubPage.contentOffsetY <= 0 && fabs(velocity.x) <= fabs(velocity.y) && velocity.y > 0
        let pannableFromBottom = currentSubPage.touchedBottom && fabs(velocity.x) <= fabs(velocity.y) && velocity.y < 0
        
        let touchLocation = panGestureRecognizer.locationInView(vwHeader)
        
//        if self.isBackType != 1 {
        if self.content?.children.count == 0 {
            let pannableInsideHeader = vwHeader.pointInside(touchLocation, withEvent: nil) && fabs(velocity.x) >= fabs(velocity.y)
            
            
            if (pannableToLeft || pannableToRight || pannableFromTop || pannableFromBottom || pannableInsideHeader) {
                return true
            }
        }
        
        
        return false
    }
    
    public func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if otherGestureRecognizer is UIPanGestureRecognizer || otherGestureRecognizer is UITapGestureRecognizer {
            return true
        }
        
        return false
    }
    
    public func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func handlePan(panGesture: UIPanGestureRecognizer){
        
        vwDraggableView.handlePanGesture(panGestureRecognizer)
        
    }
    
    func generateThumnail(url : NSURL, fromTime:Float64) -> UIImage? {
        
        let asset = AVAsset(URL: url)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        assetImgGenerate.requestedTimeToleranceAfter = kCMTimeZero;
        assetImgGenerate.requestedTimeToleranceBefore = kCMTimeZero;
        
        let time        : CMTime = CMTimeMakeWithSeconds(fromTime, 600)
        var img         : CGImageRef!
        do{
            img = try assetImgGenerate.copyCGImageAtTime(time, actualTime: nil)
            
            
        }catch{
            
        }
        if img != nil {
            let frameImg    : UIImage = UIImage(CGImage: img)
            return frameImg
        } else {
            return nil
        }
        
    }
    
    // MARK: - PannableCardViewDelegate
    
    public func cardSwipedAway() {
        self.delegate?.onHideFrontBackground()
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    public func cardWasDragged(dragPercentage: CGFloat) {
        self.backgroundView.hidden = true
        self.delegate?.onHideFrontBackground()
        self.vwMain.backgroundColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.7 * (1 - (dragPercentage)))
    }
    public func resetCardPosition(){
        self.delegate?.onShowFrontBackground()
        self.vwMain.backgroundColor = UIColor.clearColor()
        self.backgroundView.hidden = false
    }
    public func cardStartedDismissal(duration: NSTimeInterval, dragPercentage: Float) {
        var percentage = dragPercentage
        
        self.delegate?.onHideFrontBackground()
        self.backgroundView.hidden = true
        UIView.animateWithDuration(duration) {
            percentage = 1
            self.vwMain.backgroundColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.7 * (1 - CGFloat(percentage)))
        }
    }
    
    
    @IBAction func onCancelSubmissionAction(sender : AnyObject){
        self.delegate?.onHideFrontBackground()
        self.dismissViewControllerAnimated(true) {
            
        }
    }
    @IBAction func onSubmissionAction(sender : AnyObject){
        
        let alertController = UIAlertController(title: "Select people who can see your content...", message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
        }
        alertController.addAction(cancelAction)
        
        let anyoneAction = UIAlertAction(title: "Anyone", style: .Default) { (action) in
            self.giveCardToDeck(true)
        }
        alertController.addAction(anyoneAction)
        
        let askersAction = UIAlertAction(title: "Only Askers", style: .Default) { (action) in
            self.giveCardToDeck(false)
        }
        alertController.addAction(askersAction)
        
        self.presentViewController(alertController, animated: true) {
        }
    }
    
    func giveCardToDeck (isAnyone: Bool) {
        
        self.showProgressHUD()
        
        let deck_id = self.currentContent!.id
        let card_id = self.content!.id
        print(deck_id)
        print(card_id)
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        let visibility = isAnyone ? nil : Constants.ApiConstants.giveVisibilityAskers as String?
        provider.request(.GiveCardToDeck(deckId: deck_id, cardId: card_id, visibility: visibility), completion: { (result) in
            let JSON = Helper.validateResponse(result, sender: self)
            self.hideProgressHUD()
            if JSON != nil {
                let rContent = ContentNode(dictionary: JSON!["deck"] as! [String: AnyObject])
                //                    self.content = rContent
                // get card obj
                
                self.delegate?.onHideFrontBackground()
                self.dismissViewControllerAnimated(true, completion: {
                    print(self.isType)
                    if self.isType == 0{
                        self.submittedCallback(rContent)
                    }
                    
                })
                
            } else {
                self.showSimpleAlert(withTitle: "", withMessage: "Failed to submit card.")
            }
        })
    }
    
    func onResponseContent(resContent: ContentNode) {
        if self.isType == 1{
            self.submittedCallback(resContent)
        }
    }
    @IBAction func onRemoveAction(sender : AnyObject){
        
        if self.content?.children.count == 0 {
            let alertController = UIAlertController(title: "Remove this item from your Submission? ❎", message: "You’ll still be able to access this in your Drafts.", preferredStyle: .Alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Default){ (action) in
                
            }
            alertController.addAction(cancelAction)
            
            let yesAction = UIAlertAction(title: "Yes", style: .Default) { (action) in
                
                self.showProgressHUD()
                
//                let gives = self.currentContent?.myGives
//                if gives?.count == 0 { return }
//                let cardId = gives?.first?.id
                let cardId = self.content?.id
                let provider = MoyaProvider<NetworkService>.createDefaultProvider()
                provider.request(NetworkService.CancelSubmission(cardId: cardId!), completion: { (result) in
                    let JSON = Helper.validateResponse(result, sender: self)
                    
                    self.hideProgressHUD()
                    
                    if JSON != nil {
                        let rContent = ContentNode(dictionary: JSON!["deck"] as! [String: AnyObject])
                        
                        self.dismissViewControllerAnimated(true, completion: {
                            
                            print(self.isType)
                            
                            if self.isType == 1{
                                self.submittedCallback(rContent)
                            }
                            
                        })
                        
                    } else {
                        self.showSimpleAlert(withTitle: "", withMessage: "Failed to remove card.")
                    }
                })
            }
            
            alertController.addAction(yesAction)
            
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.presentViewController(alertController, animated: true, completion: nil)
            })
        }else{
            let alertController = UIAlertController(title: "Remove collection? ❎", message: "All items in this collection will be removed together. You’ll still be able to access them in your Drafts.", preferredStyle: .Alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Default){ (action) in
                
            }
            alertController.addAction(cancelAction)
            
            let yesAction = UIAlertAction(title: "Yes", style: .Default) { (action) in
                self.showProgressHUD()
                
                let gives = self.currentContent?.myGives
                if gives?.count == 0 { return }
                let cardId = gives?.first?.id
                let provider = MoyaProvider<NetworkService>.createDefaultProvider()
                provider.request(NetworkService.CancelSubmission(cardId: cardId!), completion: { (result) in
                    let JSON = Helper.validateResponse(result, sender: self)
                    
                    self.hideProgressHUD()
                    
                    if JSON != nil {
                        let rContent = ContentNode(dictionary: JSON!["deck"] as! [String: AnyObject])
                        
                        print(self.isType)
                        
                        if self.isType == 1{
                            self.submittedCallback(rContent)
                        }
                        
                    } else {
                        self.showSimpleAlert(withTitle: "", withMessage: "Failed to remove card.")
                    }
                })
            }
            
            alertController.addAction(yesAction)
            
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.presentViewController(alertController, animated: true, completion: nil)
            })
        }
        
    }
    
    @IBAction func onProfilePage (sender : AnyObject){
        let profileVC = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "TabProfileController") as! TabProfileController
        
        let cData : ContentNode = self.content!
        profileVC.userInfo = cData.creatorInfo
        
        profileVC.modalPresentationStyle = .OverFullScreen
        profileVC.isShow = true
        self.presentViewController(profileVC, animated: true, completion: nil)
    }
}
