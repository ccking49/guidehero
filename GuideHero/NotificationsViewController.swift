//
//  NotificationsViewController.swift
//  GuideHero
//
//  Created by Ascom on 11/1/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya



class NotificationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    var notifications: [NotificationNode] = []
    @IBOutlet var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let titleTextAttributes = [
            NSFontAttributeName: UIFont.boldSystemFontOfSize(18),
            NSForegroundColorAttributeName: UIColor.whiteColor()
        ]
        navigationController?.navigationBar.titleTextAttributes = titleTextAttributes
        navigationController?.navigationBar.translucent = true
        
        self.tableView.backgroundColor = UIColor(hexString: "#E7E7E7")
        self.tableView.tableFooterView = UIView.init(frame: CGRectZero)
        
        loadNotifications()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        loadNotifications()
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadNotifications() {
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        
        provider.request(.GetNotifications) { result in
            let JSON = Helper.validateResponse(result, sender: self)
            if JSON != nil {
                self.notifications = []
                for notification in (JSON!["notifications"] as? [[String: AnyObject]] ?? []) {
                    let not = NotificationNode.init(dictionary: notification)
                    self.notifications.append(not)
                }
                if self.notifications.count != 0 {
                    self.navigationController?.tabBarItem.badgeValue = String(self.notifications.count)
                }
                self.tableView.reloadData()
            }
        }
    }

    // MARK: - Navigation

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("NotificationTableCell") as! NotificationTableCell
        
        cell.notification = notifications[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let vc = storyboard?.instantiateViewControllerWithIdentifier("CardDetailViewController") as! CardDetailViewController
        vc.cardId = notifications[indexPath.row].card_id
        tabBarController!.presentViewController(vc, animated: true, completion: nil)
//        navigationController?.pushViewController(vc, animated: true)
    }
}
