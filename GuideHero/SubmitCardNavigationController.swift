//
//  SubmitCardNavigationController.swift
//  GuideHero
//
//  Created by HC on 11/16/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya

class SubmitCardNavigationController: UINavigationController, UINavigationControllerDelegate {

    @IBOutlet var bottomView: UIView!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var cancelButton : UIButton!
    var contentId: String = ""
    var cardIds: NSArray!
    var currentContent: ContentNode! // selected content within nav
    var content: ContentNode!
    //    var drafts = [ContentNode]()
    var submittedCallback: ((_: ContentNode!) -> Void)!

    var isType : Int! // true:submit false:remove
    
    var isSubmit : Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        
        submitButton.layer.shadowColor = UIColor.blackColor().CGColor
        submitButton.layer.shadowOffset = CGSizeMake(0, 1)
        submitButton.layer.shadowOpacity = 0.5
        submitButton.layer.shadowRadius = 4
        
        cancelButton.layer.shadowColor = UIColor.blackColor().CGColor
        cancelButton.layer.shadowOffset = CGSizeMake(0, 1)
        cancelButton.layer.shadowOpacity = 0.5
        cancelButton.layer.shadowRadius = 4
//
        delegate = self
        let textAttributes = [
            NSFontAttributeName: UIFont.boldSystemFontOfSize(18),
            NSForegroundColorAttributeName: UIColor.whiteColor()
        ]
        self.navigationBar.titleTextAttributes = textAttributes
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let size = view.bounds.size
        let height = 44 as CGFloat
        bottomView.frame = CGRect(x: 0, y: size.height-height, width: size.width, height: height)
        view.addSubview(bottomView)
    }
    
    @IBAction func onSubmit(sender: UIButton) {
        
        if isSubmit == true {
            let alertController = UIAlertController(title: "Select people who can see your content...", message: nil, preferredStyle: .ActionSheet)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            }
            alertController.addAction(cancelAction)
            
            let anyoneAction = UIAlertAction(title: "Anyone", style: .Default) { (action) in
                self.giveCardToDeck(true)
            }
            alertController.addAction(anyoneAction)
            
            let askersAction = UIAlertAction(title: "Only Askers", style: .Default) { (action) in
                self.giveCardToDeck(false)
            }
            alertController.addAction(askersAction)
            
            self.presentViewController(alertController, animated: true) {
            }
        }else{
            self.showSimpleAlert(withTitle: "", withMessage: "Choose an item! 😉💫")
        }
        
    }
    
    @IBAction func onCancel(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }

    func giveCardToDeck (isAnyone: Bool) {
        let deck_id = self.content.id
        let card_id = self.currentContent.id
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        
        self.showProgressHUD()
        
        let visibility = isAnyone ? nil : Constants.ApiConstants.giveVisibilityAskers as String?
        provider.request(.GiveCardToDeck(deckId: deck_id, cardId: card_id, visibility: visibility), completion: { (result) in
            let JSON = Helper.validateResponse(result, sender: self)
            
            self.hideProgressHUD()
            if JSON != nil {
                let rContent = ContentNode(dictionary: JSON!["deck"] as! [String: AnyObject])
                //                    self.content = rContent
                // get card obj
                self.submittedCallback(rContent)
            } else {
                self.showSimpleAlert(withTitle: "", withMessage: "Failed to submit card.")
            }
        })
    }
    
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        if viewController.isKindOfClass(SubmissionDetailViewController) {
            let vc = viewController as! SubmissionDetailViewController
            currentContent = vc.content
            vc.isType = self.isType
            print(vc.isType)
            if !currentContent.isDeck {
                submitButton.enabled = true
//                submitButton.alpha = 1
            }
            
            isSubmit = true
            
        } else {
//            submitButton.enabled = false
//            submitButton.alpha = 0.5
            isSubmit = false
            
            
        }
    }
}
