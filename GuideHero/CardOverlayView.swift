//
//  CardOverlayView.swift
//  GuideHero
//
//  Created by Abhishek Sengar on 10/09/2016.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Koloda

class CardOverlayView: OverlayView {

    var card: ContentNode! {
        didSet {
            let cardView = CardView.loadFromNib() as! CardView
            cardView.content = card
            cardView.showAsCard = true
            cardView.showNextCard()
            self.addSubview(cardView)
            cardView.fillSuperview()
        }
    }
}
