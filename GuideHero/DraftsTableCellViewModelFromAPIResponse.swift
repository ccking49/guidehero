//
//  DraftsTableCellViewModelFromAPIResponse.swift
//  GuideHero
//
//  Created by Dino Bartosak on 14/10/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

class DraftsTableCellViewModelFromAPIResponse: NSObject, DraftsTableCellViewModel {

    // MARK: DraftsTableCellViewModel protocol
    
    let cellType: DraftsTableCellContentType
    var deckImageURL: NSURL?;
    var deckText: String?
    
    let id: String
    let title: String;
    let cardDescription: String;
    var authorImageURL: NSURL?;
    let creationDetails: String;
    
    init(withAPIResponse response: [String : AnyObject]) {
        id = response["id"] as! String
        title = response["name"] as! String
        cardDescription = ""
        authorImageURL = NSURL(string: response["creator_thumbnail"] as! String)
        creationDetails = DraftsTableCellViewModelFromAPIResponse.creationDetails(fromResponse: response);
        
        cellType = DraftsTableCellViewModelFromAPIResponse.cardType(fromResponse: response)

        switch cellType {
        case .Image:
            deckImageURL = NSURL(string: response["content"] as! String)
        case .Text:
            deckText = response["content"] as? String
        }
    }
    
    // MARK: Private
    
    private class func creationDetails(fromResponse response: [String : AnyObject]) -> String {
        let creator = response["creator"] as! String
        let date = response["created_at"] as! Int
        return creator + "᛫" + "\(date)"
    }
    
    private class func cardType(fromResponse response: [String : AnyObject]) -> DraftsTableCellContentType {
        var contentType: DraftsTableCellContentType = .Text
        let type = response["type"] as! String
        if type == "text" {
            contentType = .Text
        } else if type == "image" {
            contentType = .Image
        }
        
        return contentType
    }
    
}
