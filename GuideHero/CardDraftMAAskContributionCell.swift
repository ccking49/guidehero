//
//  CardDraftMAAskContributionCell.swift
//  GuideHero
//
//  Created by Promising Change on 01/02/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit

class CardDraftMAAskContributionCell: UITableViewCell {

    // MARK: - @IBOutlet Variables
    
    @IBOutlet weak var tfPrizeOriginal: UITextField!
    @IBOutlet weak var tfPrizeJoin: UITextField!
    
    // MARK: - Variables
    
    weak var delegate: CardDraftMAAskCellDelegate?
    
    // MARK: - Properties
    
    var originalPrizePoint: Int {
        get {
            return Int(tfPrizeOriginal.text!) ?? 0
        }
    }
    
    var joinPrizePoint: Int {
        get {
            return Int(tfPrizeJoin.text!) ?? 0
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Private Methods
    
    func configureView() {
        tfPrizeOriginal.keyboardType = .NumberPad
        tfPrizeJoin.keyboardType = .NumberPad
        
        let attributedString = NSMutableAttributedString(
            string: "0",
            attributes: [NSForegroundColorAttributeName: UIColor(hexString: "FFCD00")!, NSFontAttributeName: UIFont(name: "Boxy-Bold", size: 15.0)!])
        tfPrizeOriginal.attributedPlaceholder = attributedString
        tfPrizeJoin.placeholder = "0"
        
        tfPrizeOriginal.delegate = self
        tfPrizeJoin.delegate = self
    }

}

extension CardDraftMAAskContributionCell: UITextFieldDelegate {
    func textFieldDidEndEditing(textField: UITextField) {
        if (textField == tfPrizeOriginal) {
            delegate?.cellWasUpdatedWithOriginalPoint(Int(tfPrizeOriginal.text!) ?? 0)
        }
    }
}
