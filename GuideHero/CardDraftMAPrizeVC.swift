//
//  CardDraftMAPrizeVC.swift
//  GuideHero
//
//  Created by Promising Change on 31/01/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit

class CardDraftMAPrizeVC: CardSubPageBaseVC {
    
    // MARK: - @IBOutlet Variables
    
    @IBOutlet weak var tbContent: UITableView!
    @IBOutlet weak var footerView : UIView!

    // MARK: - Variables
    
    var aryTableHeader :NSMutableArray = []
    let tbHeaderView: UIView!
    
    var content: ContentNode? {
        didSet {
            if isViewLoaded() {
                tbContent.reloadData()
                self.view.layoutIfNeeded()
            }
        }
    }
    
    var originalPrizePoint: Int? = 0 {
        didSet {
            if (prizeCell != nil) {
                prizeCell?.bindPrizePoint(originalPrizePoint!)
            }
        }
    }
    
    var prizeCell: CardDraftMAPrizePrizeCell?
    var prizeForCell: CardDraftMAPrizeForCell?
    var prizeDistributionCell: CardDraftMAPrizeDistributionCell?
    var prizeEvaluationCell: CardDraftMAPrizeEvaluationCell?
    
    // MARK: - Properties
    
    var prizeForWhom: Int {
        get {
            if (prizeForCell?.prizeForWhom == PrizeForWhom.Top10) {
                return 10
            } else if (prizeForCell?.prizeForWhom == PrizeForWhom.Top5) {
                return 5
            } else if (prizeForCell?.prizeForWhom == PrizeForWhom.Top3) {
                return 3
            }
            
            return 0
        }
    }
    
    var prizeDistributionRule: String {
        get {
            if (prizeDistributionCell?.prizeSplitOption == SplitOptions.Proportionally) {
                return "proportionally"
            } else if (prizeDistributionCell?.prizeSplitOption == SplitOptions.Evenly) {
                return "evenly"
            }
            
            return "evenly"
        }
    }
    
    var evaluationStartDate: String {
        get {
            
            if prizeEvaluationCell == nil {
                let dateString = NSDate().stringWithFormatInGMT(Constants.Literals.apiDateFormat)
                return dateString
            }else{
                return (prizeEvaluationCell?.evaluationStartDate)!
            }
            
        }
    }
    
    var evaluationEndDate: String {
        get {
            
            if prizeEvaluationCell == nil {
                let endDate = NSDate().add(nil, months: nil, weeks: nil, days: nil, hours: 12, minutes: nil, seconds: nil, nanoseconds: nil)
                let dateString = endDate.stringWithFormatInGMT(Constants.Literals.apiDateFormat)
                return dateString
            }else{
                return (prizeEvaluationCell?.evaluationEndDate)!
            }
            
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tbContent.setContentOffset(CGPointMake(tbContent.contentOffset.x, tbContent.contentOffset.y - 1), animated: true)
    }
    
    // MARK: - View Lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        tbHeaderView = UIView()
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        scrollView = tbContent as UIScrollView
        
        configureView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Methods
    
    func configureView() {
        tbHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 540)
        
        tbContent.rowHeight = UITableViewAutomaticDimension
        tbContent.estimatedRowHeight = Constants.UIConstants.contentCellHeight
        tbContent.separatorStyle = .None
        
        tbContent.tableHeaderView = tbHeaderView
        
        footerView.layer.cornerRadius = 20
        footerView.layer.shadowRadius = 5
        footerView.layer.shadowOpacity = 0.5
        footerView.layer.shadowOffset = CGSizeZero
        footerView.layer.shadowColor = UIColor.blackColor().CGColor
        footerView.clipsToBounds = true
        
        self.aryTableHeader = ["Prize", "For", "Distribution", "Evaluation"]
        
        self.view.setNeedsLayout()
    }

    // MARK: - UIScrollViewDelegate
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
    }
    
}

// MARK: - UITableViewDelegate & UITableViewDataSource

extension CardDraftMAPrizeVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return aryTableHeader.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.aryTableHeader[section] as? String
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let contentHeight : CGFloat = 48
        
        let headerView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: self.view.frame.width, height: contentHeight)))
        
        let headerContentView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: self.view.frame.width, height: 48)))
        
        headerView.clipsToBounds = false
        let label = UILabel(frame: CGRect(x: 30, y: 0, width: self.view.frame.width-60, height: 48))
        label.text = aryTableHeader[section] as? String
        label.textAlignment = NSTextAlignment.Left
        
        headerView.backgroundColor = UIColor.whiteColor()
        headerContentView.backgroundColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.1)
        
        label.textColor = UIColor.blackColor()
        label.font = Helper.proximaNova("Bold", font: 20)
        
        headerContentView.addSubview(label)
        
        headerView.addSubview(headerContentView)
        
        return headerView
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
  
        return 48
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        
        let section = indexPath.section
        switch section {
        case 0:
            if (prizeCell != nil) {
                return prizeCell!
            }

            prizeCell = tableView.dequeueReusableCellWithIdentifier("CardDraftMAPrizePrizeCell") as? CardDraftMAPrizePrizeCell
            prizeCell!.bindPrizePoint(originalPrizePoint!)
            cell = prizeCell!
            break
        case 1:
            if (prizeForCell != nil) {
                return prizeForCell!
            }
            
            prizeForCell = tableView.dequeueReusableCellWithIdentifier("CardDraftMAPrizeForCell") as? CardDraftMAPrizeForCell
            cell = prizeForCell!
            break
        case 2:
            if (prizeDistributionCell != nil) {
                return prizeDistributionCell!
            }
            
            prizeDistributionCell = tableView.dequeueReusableCellWithIdentifier("CardDraftMAPrizeDistributionCell") as? CardDraftMAPrizeDistributionCell
            cell = prizeDistributionCell!
            break
        case 3:
            if (prizeEvaluationCell != nil) {
                return prizeEvaluationCell!
            }
            
            prizeEvaluationCell = tableView.dequeueReusableCellWithIdentifier("CardDraftMAPrizeEvaluationCell") as? CardDraftMAPrizeEvaluationCell
            cell = prizeEvaluationCell!
            break
        default:
            break
        }
        
        cell.selectionStyle = .None
        cell.backgroundColor = UIColor.whiteColor()
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Do Nothing
    }
}
