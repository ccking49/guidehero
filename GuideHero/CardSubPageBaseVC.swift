//
//  CardSubPageBaseVC.swift
//  GuideHero
//
//  Created by Promising Change on 09/01/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit

public class CardSubPageBaseVC: UIViewController, UIScrollViewDelegate {

    var contentOffsetY: CGFloat
    var offsetTrans:CGPoint
    var scrollView: UIScrollView?
    var isManualScrolling: Bool
    var touchedBottom: Bool = false
    var singleCard: Bool = false
    
    weak var delegate: CardSubPageScrollDelegate?
    
    required public init?(coder aDecoder: NSCoder) {
        contentOffsetY = 0
        offsetTrans = CGPoint(x: 0, y: 0)
        isManualScrolling = false
        
        super.init(coder: aDecoder)
    }
    
    public func layoutView() {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
    
    public func scrollRefresh() {
        offsetTrans = CGPoint(x: (scrollView?.contentOffset.x)!, y: (scrollView?.contentOffset.y)!)
        delegate?.subPageScrollViewDidScroll((scrollView?.contentOffset.y)!, translation: offsetTrans, animated: true)
    }
    
    public func scrollToOffsetY(offsetY: CGFloat) {
        contentOffsetY = offsetY
        if ((scrollView?.bounds.height)! + contentOffsetY > (scrollView?.contentSize.height)!) {
            contentOffsetY = (scrollView?.contentSize.height)! - (scrollView?.bounds.height)!
        }
        isManualScrolling = true
        scrollView?.contentOffset = CGPoint(x: scrollView!.contentOffset.x, y: contentOffsetY)
        isManualScrolling = false
    }
    
    public func setSingleCard() {
        singleCard = true
    }
    
    // MARK: - UIScrollViewDelegate
    
    public func scrollViewDidScroll(scrollView: UIScrollView) {
        if (!isManualScrolling) {
            contentOffsetY = scrollView.contentOffset.y
            offsetTrans = CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y)
            delegate?.subPageScrollViewDidScroll(scrollView.contentOffset.y, translation: offsetTrans, animated: false)
        }
        
        touchedBottom = false
        if (scrollView.bounds.height + contentOffsetY >= scrollView.contentSize.height) {
            touchedBottom = true
            let bouncedOffsetY = scrollView.bounds.height + contentOffsetY - scrollView.contentSize.height
            delegate?.subPageScrollViewDidBounce(bouncedOffsetY)
        }
    }

}
