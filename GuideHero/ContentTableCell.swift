//
//  ContentTableCell.swift
//  GuideHero
//
//  Created by Ignacio Rodrigo on 10/17/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
//import AlamofireImage
import Moya
import Alamofire
//import ActiveLabel

class ContentTableCell: BaseTableViewCell {

    // MARK: - Properties

    var content: ContentNode! {
        didSet {
            self.updateUI()
        }
    }
    
    
    @IBOutlet var thumbnailView: CardContainerView!
    @IBOutlet var title: UILabel!
    @IBOutlet var details: UILabel!
    @IBOutlet var creatorThumbnail: UIImageView!
    @IBOutlet var creator: UILabel!
    @IBOutlet var likes: UILabel!
    @IBOutlet var likeButton: UIButton!
    
    var showLock = false

    // MARK: - View lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        self.creatorThumbnail.layer.cornerRadius = 10
    }
    
    
    // MARK: - Private methods

    private func updateUI() {

        self.title.text = self.content.name
        self.details.text = self.content.description
        self.creator.text = self.content.formattedCreatorAndDate
        
        thumbnailView.content = content        
        
        updateLock()
        
        if let creatorThumbnailURL = self.content.creatorThumbnailURL {
            self.creatorThumbnail.af_setImageWithURL(creatorThumbnailURL)
        }
        
        let imageName = content.liked_by_me ? "heartFilled" : "heart"
        likeButton.setImage(UIImage(named: imageName), forState: .Normal)
        likes.text = String(content.likes)
    }
    
    @IBAction func onLike(button: UIButton) {
        let liked = self.content.liked_by_me
        
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        let target = liked ? NetworkService.UnlikeCard(cardId : self.content.id) : NetworkService.LikeCard(cardId : self.content.id)
        
        provider.request(target) { result in
            switch result {
                case let .Success(response):
                    if response.statusCode == 200 {
                        self.content.liked_by_me = !liked
                        self.content.likes += liked ? -1 : 1
                        self.updateUI()
                    }
                case let .Failure(error):
                    print(error)
            }
        }
    }
    
    func updateLock() {
        if self.showLock {
            let lock = UIImageView(frame: CGRectMake(10, 10, 20, 20))
            lock.image = UIImage(named: "lock")
            lock.tag = 333
//            thumbnail.insertSubview(lock, aboveSubview: thumbnail)
            thumbnailView.insertSubview(lock, aboveSubview: thumbnailView)
        } else {
            for view in thumbnailView.subviews{
                if view.tag == 333 {
                    view.removeFromSuperview()
                }
            }
        }
    }
}
