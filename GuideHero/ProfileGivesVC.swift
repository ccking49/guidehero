//
//  ProfileAsksVC.swift
//  GuideHero
//
//  Created by Star on 1/31/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import Foundation
import UIKit
import Moya

class ProfileGivesVC: UIViewController , UITableViewDataSource , UITableViewDelegate , UIGestureRecognizerDelegate , DraftTableViewCellDelegate{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView : UIView!
    
    @IBOutlet var footerView : UIView!
    @IBOutlet var footerBottomView : UIView!
    @IBOutlet var whoolView : UIView!
    @IBOutlet weak var imgBackground : UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    
    var userInfo: UserNode!
    
    var listData = [ContentNode]();
    
    var pan = UIPanGestureRecognizer()
    var isRotating = false
    var startPos = CGPointZero
    var startCenter = CGPointZero
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cellNib = UINib(nibName: "DraftCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: "DraftCell")
        
        onSetupPanel()
        
        tableView.delegate = self
        tableView.dataSource = self
        updateTable()
        
    
        
        footerBottomView.layer.cornerRadius = 20
        footerBottomView.layer.shadowRadius = 5
        footerBottomView.layer.shadowOpacity = 0.5
        footerBottomView.layer.shadowOffset = CGSizeZero
        footerBottomView.layer.shadowColor = UIColor.blackColor().CGColor
        footerBottomView.clipsToBounds = true
        
        footerView.frame.size = CGSizeMake(UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height)
        
        tableView.reloadData()
        
        if self.userInfo != nil {
            self.imgProfile.af_setImageWithURL(NSURL(string: self.userInfo.thumbnail_url)!)
            self.imgProfile.layer.cornerRadius = (self.imgProfile.frame.size.height - 2) / 2
            
            self.imgProfile.layer.masksToBounds = true
            self.lblUsername.text = self.userInfo.username
        }
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().statusBarHidden = true
        
    }
    func onSetupPanel(){
        
        pan.addTarget(self, action: #selector(ProfileGivesVC.handlePan(_:)))
        pan.delegate = self
        whoolView.addGestureRecognizer(pan)
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        let velocity = pan.velocityInView(self.tableView?.superview)
        
        if fabs(velocity.x) >= fabs(velocity.y) || (tableView.contentOffset.y <= 0 && velocity.y > 0) {
            return true
        }
        
        return false
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if otherGestureRecognizer is UIPanGestureRecognizer {
            return true
        }
        
        return false
    }
    
    func handlePan(panGesture: UIPanGestureRecognizer){
        
        let factor: CGFloat = 2
        let angleLimit = CGFloat(M_PI_4)
        
        switch panGesture.state {
        case .Began:
            
            startPos = panGesture.locationInView(self.whoolView.superview)
            startCenter = self.whoolView.center
            print(startPos)
            let velocity = panGesture.velocityInView(self.whoolView.superview)
            
            isRotating = fabs(velocity.x) > fabs(velocity.y)
            
        case .Changed:
            
            if isRotating {
                
                let pos = panGesture.locationInView(self.view?.superview)
                
                let rotationAngle = atan((pos.x - startPos.x) / (self.view.bounds.size.height * factor - pos.y))
                var direction:CGFloat = 1
                if pos.x != startPos.x {
                    direction = (startPos.x - pos.x) / fabs(pos.x - startPos.x)
                }
                imgBackground.alpha = 0.5 - fabs(rotationAngle)
                
                var transform = CGAffineTransformMakeTranslation(self.view.bounds.size.width * 0.5 * direction, self.view.bounds.size.height * (factor - 0.5))
                transform = CGAffineTransformRotate(transform, rotationAngle)
                transform = CGAffineTransformTranslate(transform, -self.view.bounds.size.width * 0.5 * direction, -self.view.bounds.size.height * (factor - 0.5))
                self.whoolView.transform = transform;
                
            } else {
                let pos = panGesture.locationInView(self.view?.superview)
                self.whoolView.center = CGPointMake(startCenter.x, startCenter.y + pos.y - startPos.y)
            }
            
        case .Ended:
            
            if isRotating {
                let pos = panGesture.locationInView(self.whoolView?.superview)
                let velocity = panGesture.velocityInView(self.whoolView?.superview)
                var startDirection = 1
                if pos.x != startPos.x {
                    startDirection = Int(round((pos.x - startPos.x) / fabs(pos.x - startPos.x)))
                }
                var velocityDirection = -startDirection
                if velocity.x != 0 {
                    velocityDirection = Int(round(velocity.x / fabs(velocity.x)))
                }
                if startDirection == velocityDirection { // should dismiss
                    UIView.animateWithDuration(0.3, animations: {
                        
                        var transform = CGAffineTransformMakeTranslation(-self.whoolView.bounds.size.width * 0.5 * CGFloat(startDirection), self.whoolView.bounds.size.height * (factor - 0.5))
                        transform = CGAffineTransformRotate(transform, angleLimit * CGFloat(startDirection))
                        transform = CGAffineTransformTranslate(transform, self.whoolView.bounds.size.width * 0.5 * CGFloat(startDirection), -self.whoolView.bounds.size.height * (factor - 0.5))
                        
                        self.whoolView.transform = transform
                        self.imgBackground.alpha = 0
                        
                        }, completion: { completed in
                            self.dismissViewControllerAnimated(false, completion: nil)
                    })
                } else {
                    UIView.animateWithDuration(0.2, animations: {
                        self.whoolView.transform = CGAffineTransformIdentity
                        self.imgBackground.alpha = 0.5
                        }, completion: { completed in
                    })
                }
                
            } else {
                let velocity = panGesture.velocityInView(self.whoolView?.superview)
                if velocity.y > 0 { // should dismiss
                    UIView.animateWithDuration(0.2, animations: {
                        self.whoolView.center = CGPointMake(self.startCenter.x, self.whoolView.bounds.size.height * 1.5)
                        self.imgBackground.alpha = 0
                        }, completion: { completed in
                            self.dismissViewControllerAnimated(false, completion: nil)
                    })
                } else {
                    UIView.animateWithDuration(0.2, animations: {
                        self.whoolView.center = self.startCenter
                        }, completion: { completed in
                    })
                }
            }
            
            
        default:
            break
        }
    }
    
    private func updateTable() {
        if !self.isViewLoaded() {
            return;
        }
        
        self.showProgressHUD()
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.GetContentList(listType: "gives", user_id: self.userInfo.user_id)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't get gives, please try again later")
                    self.hideProgressHUD()
                    return
                }
                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    self.hideProgressHUD()
                    self.listData = []
                    
                    if JSON["gives"] == nil {
                        return
                    }
                    
                    for listDic in (JSON["gives"] as? [[String : AnyObject]])! {
                        self.listData.append(ContentNode(dictionary: listDic))
                    }
                    
                    if (self.listData.count) * 216 < Int(UIScreen.mainScreen().bounds.size.height - self.headerView.frame.size.height){
                        self.footerView.frame.size = CGSizeMake(UIScreen.mainScreen().bounds.size.width, (UIScreen.mainScreen().bounds.size.height - self.headerView.frame.size.height)-CGFloat((self.listData.count) * 216))
                    }else{
                        self.footerView.frame.size = CGSizeMake(0, 50)
                    }
                    self.tableView.reloadData()
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't get gives, please try again later")
                    self.hideProgressHUD()
                }
            case let .Failure(error):
                
                if (self.listData.count) * 216 < Int(UIScreen.mainScreen().bounds.size.height - self.headerView.frame.size.height){
                    self.footerView.frame.size = CGSizeMake(UIScreen.mainScreen().bounds.size.width, (UIScreen.mainScreen().bounds.size.height - self.headerView.frame.size.height)-CGFloat((self.listData.count) * 216))
                }else{
                    self.footerView.frame.size = CGSizeMake(0, 50)
                }
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't get gives, please try again later")
                self.hideProgressHUD()
            }
        }
    }
    
    // MARK: - UITableView
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 216
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count ?? 0
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("DraftCell", forIndexPath: indexPath) as! DraftCell
        
        cell.delegate = self
        cell.content = self.listData[indexPath.row]
        
        //        cell.showLock = false
        //        if !self.drafts[indexPath.row].published {
        //            cell.showLock = true
        //        }
        //        cell.updateLock()
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let cardDetailVC = UIStoryboard.loadViewController(storyboardName: "CardDetail", viewControllerIdentifier: "CardDetailPageVC") as! CardDetailPageVC
        
        cardDetailVC.content = self.listData[indexPath.row]
        
        cardDetailVC.isFirst = false
        
        cardDetailVC.contentIndex = indexPath.row
        
        self.presentViewController(cardDetailVC, animated: true, completion: nil)
    }
    
    
    func onUIUpdate() {
//        updateTable()
    }
    
    @IBAction func onPressBack(sender:AnyObject){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
