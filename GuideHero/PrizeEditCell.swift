//
//  PrizeEditCell.swift
//  GuideHero
//
//  Created by HC on 11/26/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

class PrizeEditCell: BaseTableViewCell, UITextFieldDelegate {

    @IBOutlet var prizeLabel: UILabel!
    
    @IBOutlet var for1Select: UIImageView!
    @IBOutlet var for2Select: UIImageView!
    @IBOutlet var for3Select: UIImageView!
    @IBOutlet var dist1Select: UIImageView!
    @IBOutlet var dist2Select: UIImageView!
    
    @IBOutlet var startDateText: UITextField!
    @IBOutlet var endDateText: UITextField!
    
    
    
    var selectedFor = 1
    var selectedDist = 1
    var editingTextField: UITextField?
    var editingValue: String?
    
    let onImage = UIImage(named: "selectOn")
    let offImage = UIImage(named: "selectOff")
    let startDatePicker = UIDatePicker()
    let endDatePicker = UIDatePicker()
    var content: ContentNode! {
        didSet {
            updateUIWithContent()
        }
    }
    var distributionRule: String? {
        get {
            if selectedDist == 1 {
                return "proportionally"
            } else {
                return "evenly"
            }
        }
    }
    var distributionFor: Int {
        get {
            if selectedFor == 1 {
                return 10
            } else if selectedFor == 2 {
                return 5
            } else if selectedFor == 3 {
                return 3
            } else {
                return 0
            }
        }
    }
    var evaluationStart: String? {
        get {
            let dateStr = startDatePicker.date.stringWithFormatInGMT(Constants.Literals.apiDateFormat)
            return dateStr
        }
    }
    var evaluationEnd: String? {
        get {
            let dateStr = endDatePicker.date.stringWithFormatInGMT(Constants.Literals.apiDateFormat)
            return dateStr
        }
    }
    //distributionRule: String?, distributionFor: Int?, evaluationStart: String?, evaluationEnd: String?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        startDatePicker.addTarget(self, action: #selector(datePickerChanged(_:)), forControlEvents: .ValueChanged)
        startDatePicker.datePickerMode = .DateAndTime
        startDateText.inputView = startDatePicker
        startDateText.text = NSDate().stringWithFormat(Constants.Literals.localDateFormat)
        startDateText.delegate = self
        
        endDatePicker.addTarget(self, action: #selector(datePickerChanged(_:)), forControlEvents: .ValueChanged)
        endDatePicker.datePickerMode = .DateAndTime
        
        endDateText.inputView = endDatePicker
        let endDate = NSDate().add(nil, months: nil, weeks: nil, days: nil, hours: 12, minutes: nil, seconds: nil, nanoseconds: nil)
        endDatePicker.date = endDate
        endDateText.text = endDate.stringWithFormat(Constants.Literals.localDateFormat)
        endDateText.delegate = self
        
        attachToolBar(startDateText)
        attachToolBar(endDateText)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(prizeChanged), name: Constants.Literals.prizePoolChangedNotification, object: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Function
    func updateUIWithContent () {
        prizeLabel.text = "\(content.prize_pool) pts"
        
        if content.distribution_for != nil {
            if content.distribution_for == 10 {
                selectedFor = 1
            } else if content.distribution_for == 5 {
                selectedFor = 2
            } else if content.distribution_for == 3 {
                selectedFor = 3
            }
        }
        updateForSelection()
        
        if content.distribution_rule == Constants.ApiConstants.prizeDistributionProportionally {
            selectedDist = 1
        } else {
            selectedDist = 2
        }
        updateDistSelection()
        
        if content.evaluation_end_dt != nil {
            endDatePicker.date = content.evaluation_end_dt!
            endDateText.text = content.evaluation_end_dt!.stringWithFormat(Constants.Literals.localDateFormat)
        }
        if content.evaluation_start_dt != nil {
            startDatePicker.date = content.evaluation_start_dt!
            startDateText.text = content.evaluation_start_dt!.stringWithFormat(Constants.Literals.localDateFormat)
        }
    }
    
    func updateForSelection () {
        for1Select.image = offImage
        for2Select.image = offImage
        for3Select.image = offImage
        let selects = [for1Select, for2Select, for3Select]
        selects[selectedFor-1].image = onImage
    }
    
    func updateDistSelection () {
        dist1Select.image = offImage
        dist2Select.image = offImage
        let selects = [dist1Select, dist2Select]
        selects[selectedDist-1].image = onImage
    }
    
    func datePickerChanged (datePicker: UIDatePicker) {
        if datePicker == startDatePicker {
            startDateText.text = datePicker.date.stringWithFormat(Constants.Literals.localDateFormat)
        } else {
            endDateText.text = datePicker.date.stringWithFormat(Constants.Literals.localDateFormat)
        }
    }
    
    func prizeChanged (notification: NSNotification) {
        // update prize text as original prize changed in ask edit cell
        let prize = notification.object as? String ?? ""
        let prizeInt = Int(prize) ?? 0
        prizeLabel.text = "\(prizeInt) pts"
    }
    
    func attachToolBar (tField: UITextField) {
        let numberToolbar = UIToolbar(frame: CGRectMake(0, 0, contentView.frame.size.width, 50))
        numberToolbar.barStyle = UIBarStyle.Default
        numberToolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .Plain, target: self, action: #selector(PrizeEditCell.cancelEdit)),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done", style: .Done, target: self, action: #selector(PrizeEditCell.doneEdit))]
        numberToolbar.sizeToFit()
        tField.inputAccessoryView = numberToolbar
    }
    
    func cancelEdit () {
        contentView.endEditing(true)
        editingTextField!.text = editingValue
    }
    
    func doneEdit () {
        contentView.endEditing(true)
    }
    
    // MARK: - @IBAction
    
    @IBAction func onSelectFor1(sender: UIButton) {
        selectedFor = 1
        updateForSelection()
    }
    @IBAction func onSelectFor2(sender: UIButton) {
        selectedFor = 2
        updateForSelection()
    }
    @IBAction func onSelectFor3(sender: UIButton) {
        selectedFor = 3
        updateForSelection()
    }
    @IBAction func onSelectDist1(sender: UIButton) {
        selectedDist = 1
        updateDistSelection()
    }
    @IBAction func onSelectDist2(sender: UIButton) {
        selectedDist = 2
        updateDistSelection()
    }
    
    // MARK: - UITextField
    
    func textFieldDidBeginEditing(textField: UITextField) {
        editingTextField = textField
        editingValue = textField.text
    }
}
