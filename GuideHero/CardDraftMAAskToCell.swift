//
//  CardDraftMAAskToCell.swift
//  GuideHero
//
//  Created by Promising Change on 01/02/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit

protocol CardDraftMAAskCellDelegate: class {
    func cellWasUpdated()
    func cellDidRequestToUpdatePersonList(usernameList: [String]?)
    func cellWasUpdatedWithOriginalPoint(point: Int)
}

enum AskToWhom {
    case Anyone
    case Specific
}

class CardDraftMAAskToCell: UITableViewCell {
    
    // MARK: - @IBOutlet Variables
    
    @IBOutlet weak var btToAnyone: UIButton!
    @IBOutlet weak var btToSpecific: UIButton!
    @IBOutlet weak var vwToPersonList: UIView!
    @IBOutlet weak var constraintToPersonListHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintToPersonListTop: NSLayoutConstraint!
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var constraintUsernameInputHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintUsernameInputTop: NSLayoutConstraint!
    
    // MARK: - Variables
    
    weak var delegate: CardDraftMAAskCellDelegate?
    
    var askTo: String = "Anyone"
    var usernameList: [String] = []
    var usernameInputHeight: CGFloat = 30.0
    var personListHeight: CGFloat = 0.0
    var usernameCellHeight: CGFloat = 36.0
    
    // MARK: - Properties
    
    var askToWhom: AskToWhom {
        get {
            if (askTo == "Anyone") {
                return AskToWhom.Anyone
            }
            
            return AskToWhom.Specific
        }
    }
    
    // MARK: - View Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Private Methods
    
    func configureView() {
        tfUsername.clipsToBounds = true
        tfUsername.layer.cornerRadius = 2.0
        tfUsername.layer.borderWidth = 1.0
        tfUsername.layer.borderColor = UIColor(hexString: "4A4A4A")?.CGColor
        tfUsername.delegate = self
        
        vwToPersonList.clipsToBounds = true
        
        constraintToPersonListTop.constant = 0
        constraintToPersonListHeight.constant = 0
        constraintUsernameInputTop.constant = 0
        constraintUsernameInputHeight.constant = 0
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    func duplicationCheck(list: [String]?) -> [String] {
        if (self.usernameList.count == 0) {
            return list!
        }
        
        var newList = [String]()
        for newUsername in list! {
            let foundIndex = usernameList.indexOf(newUsername)
            if (foundIndex == nil) {
                // Not Found
                // Add it
                newList.append(newUsername)
            } else {
                // Found
                // Discard it
            }
        }
        
        return newList
    }
    
    // MARK: - Public Methods
    
    func appendUsernames(newList: [String]?) {
        let count: Int = (newList?.count)! - 1
        for index in 0...count {
            let username = newList![index]
            
            // Add a new username to the existing list
            usernameList.append(username)
            
            // Add a username cell to the view
            let newCell = CardDraftMAAskToUsernameCell(frame: CGRect(x: 0, y: 0, width: vwToPersonList.frame.width, height: usernameCellHeight))
            newCell.bindUser(username)
            vwToPersonList.addSubview(newCell)
            
            newCell.translatesAutoresizingMaskIntoConstraints = false
            // Set Constraints
            let top = CGFloat(usernameList.count - 1) * usernameCellHeight
            let metricDict = ["top":top,"h":usernameCellHeight]
            
            newCell.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[view(h)]", options:[], metrics: metricDict, views: ["view":newCell]))
            vwToPersonList.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-(top)-[view]", options:[], metrics: metricDict, views: ["view":newCell]))
            vwToPersonList.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[view]-0-|", options:[], metrics: nil, views: ["view":newCell]))
            
            personListHeight = personListHeight + usernameCellHeight
        }
        
        if (constraintToPersonListTop.constant == 0) {
            constraintToPersonListTop.constant = 5.0
        }
        constraintToPersonListHeight.constant = personListHeight
        
        delegate?.cellWasUpdated()
    }
    
    // MARK: - Actions

    @IBAction func didTapOnToAnyone(sender: AnyObject) {
        if (askTo == "Anyone") {
            return
        }
        
        askTo = "Anyone"
        btToAnyone.setImage(UIImage(named: "selectOn"), forState: .Normal)
        btToSpecific.setImage(UIImage(named: "selectOff"), forState: .Normal)
        
        constraintToPersonListTop.constant = 0
        constraintToPersonListHeight.constant = 0
        constraintUsernameInputTop.constant = 0
        constraintUsernameInputHeight.constant = 0
        
        delegate?.cellWasUpdated()
    }
    
    @IBAction func didTapOnToSpecific(sender: AnyObject) {
        if (askTo == "Specific") {
            return
        }
        
        askTo = "Specific"
        btToAnyone.setImage(UIImage(named: "selectOff"), forState: .Normal)
        btToSpecific.setImage(UIImage(named: "selectOn"), forState: .Normal)
        
        if (usernameList.count > 0) {
            constraintToPersonListTop.constant = 5
            constraintToPersonListHeight.constant = personListHeight
        } else {
            constraintToPersonListTop.constant = 0
            constraintToPersonListHeight.constant = 0
        }
        
        constraintUsernameInputTop.constant = 5
        constraintUsernameInputHeight.constant = usernameInputHeight
        
        delegate?.cellWasUpdated()
    }
    
}

// MARK: - UITextFieldDelegate 

extension CardDraftMAAskToCell: UITextFieldDelegate {
    func textFieldDidEndEditing(textField: UITextField) {
        if (textField == self.tfUsername) {
            if (textField.text == "") {
                return
            }
            
            var newUsernames: [String] = [String]()
            let usernames = textField.text?.componentsSeparatedByString(",")
            for username in usernames! {
                let trimmedString = username.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                if trimmedString.hasPrefix("@") {
                    // inputted username conforms to @username format.
                    newUsernames.append(trimmedString.substringFromIndex(trimmedString.startIndex.advancedBy(1)))
                }
            }
            
            if (newUsernames.count > 0) {
                // There are some usernames inputted.
                // Do the duplication check with the existing username list
                let filteredList = duplicationCheck(newUsernames)
                appendUsernames(filteredList)
                tfUsername.text = ""
                delegate?.cellDidRequestToUpdatePersonList(filteredList)
            }
        }
    }
}

