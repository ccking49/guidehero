//
//  CommentsViewController.swift
//  GuideHero
//
//  Created by Ascom on 10/31/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya
import KMPlaceholderTextView
//import IQKeyboardManagerSwift

protocol CommentsUpdateDelegate {
    func onCommentsUpdate()
}

class CommentsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate, PannableCardViewDelegate, UIGestureRecognizerDelegate {

    var content: ContentNode!
    var currentWordIndex: Int = 0
    var usernames = [[String: AnyObject]]()
    var delegate: CommentsUpdateDelegate?
    var isReply: Bool = false
    var commentId: String = ""
    var currentComment: CommentNode?
    
    var prevDraftVC: CardDraftDetailVC? = nil
    var prevMainVC: CardDetailPageVC? = nil
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var vwDraggableView: PannableCardView!
    @IBOutlet weak var vwHeader: UIView!
    @IBOutlet weak var vwBottom: UIView!
    @IBOutlet weak var bottomWrapper: UIView!
    
    @IBOutlet var commentsTable: UITableView!
//    @IBOutlet var commentText: UITextField!
    @IBOutlet var commentText: KMPlaceholderTextView!
    @IBOutlet var usersTable: UITableView!
    @IBOutlet var usersTableHeight: NSLayoutConstraint!
    @IBOutlet weak var vwBottomHeight: NSLayoutConstraint!
    @IBOutlet weak var btnPost: UIButton!
    @IBOutlet weak var commentTextTopPadding: NSLayoutConstraint!
    @IBOutlet weak var commentTextBottomPadding: NSLayoutConstraint!
    @IBOutlet weak var postButtonBottomPadding: NSLayoutConstraint!
    @IBOutlet weak var wrapperTopPadding: NSLayoutConstraint!
    
    var panGestureRecognizer = UIPanGestureRecognizer()
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vwDraggableView.layer.cornerRadius = 20
        usersTable.layer.borderColor = UIColor.lightGrayColor().CGColor
        usersTable.layer.borderWidth = 1
        commentText.placeholder = "Add a Comment..."
        
        commentText.inputAccessoryView = UIView()
        
//        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        panGestureRecognizer.addTarget(self, action: #selector(CommentsViewController.handlePan(_:)))
        panGestureRecognizer.delegate = self
        vwDraggableView.addGestureRecognizer(panGestureRecognizer)
        
        vwMain.roundTopCorners(20)
        vwHeader.roundTopCorners(20)
        vwDraggableView.roundTopCorners(20)
        
        vwDraggableView.roundBottomCorners(20)
        vwBottom.roundBottomCorners(20)
        
        self.view.layoutIfNeeded()
        vwDraggableView.delegate = self
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardDidHide), name: UIKeyboardWillHideNotification , object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardDidShow), name: UIKeyboardWillShowNotification , object: nil)
        
        self.commentText.delegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        self.view.setNeedsLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - IBAction

    @IBAction func onClose(sender: AnyObject) {
        delegate?.onCommentsUpdate()
        dismissViewControllerAnimated(true, completion: nil)
        
        let comments = content?.comments.count ?? 0
        var count = comments
        
        for comment in (content?.comments)! {
            count = count + comment.sub_comments.count
        }
        
        if self.prevMainVC == nil {
            prevDraftVC!.commentsLabel.text = String(count)
        }
        else {
            prevMainVC!.commentsLabel.text = String(count)
        }
    }

    @IBAction func onPost(sender: UIButton) {
        let commentStr = commentText.text ?? ""
        if commentStr == "" {
            return
        }
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        
        if isReply {
            provider.request(.AddComment(cardId: "", commentContent: commentStr, commentId: commentId)) { result in
                let JSON = Helper.validateResponse(result, sender: self)
                if JSON != nil {
                    print(JSON)
                    let comment = CommentNode.init(dictionary: JSON!["comment"] as! [String: AnyObject])
//                    self.content.comments.append(comment)
                    self.currentComment!.sub_comments.append(comment)
                    self.commentsTable.reloadData()
                    NSNotificationCenter.defaultCenter().postNotificationName(Constants.Literals.contentRefreshNotification, object: nil)
                }
                self.commentText.text = ""
                self.vwBottomHeight.constant = 59.5
    //            self.dismissViewControllerAnimated(true, completion: nil)
            }
            
        }
        else {
            provider.request(.AddComment(cardId: self.content.id, commentContent: commentStr, commentId: "")) { result in
                let JSON = Helper.validateResponse(result, sender: self)
                if JSON != nil {
                    print(JSON)
                    let comment = CommentNode.init(dictionary: JSON!["comment"] as! [String: AnyObject])
                    self.content.comments.append(comment)
                    self.commentsTable.reloadData()
                    NSNotificationCenter.defaultCenter().postNotificationName(Constants.Literals.contentRefreshNotification, object: nil)
                }
                //            self.dismissViewControllerAnimated(true, completion: nil)
                self.commentText.text = ""
                self.vwBottomHeight.constant = 59.5
            }
        }
        
        isReply = false
        self.view.endEditing(true)
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        tableView(usersTable, didSelectRowAtIndexPath: indexPath)
//        self.removeList(self.view)
    }

    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.isEqual(commentsTable) {
            let sub_comments = self.content!.comments[section].sub_comments as [CommentNode]
            return sub_comments.count + 1
        } else {
            let count = usernames.count < 3 ? usernames.count : 2
            let height = 44 * CGFloat(count)
            usersTableHeight.constant = height
            return count
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if tableView.isEqual(commentsTable) {
            return self.content?.comments.count ?? 0
        } else {
            return 1
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView.isEqual(commentsTable) {
            
            var width : Float?
            var comment : CommentNode?
            
            if indexPath.row == 0 {
                width = Float(tableView.bounds.size.width) - 33 - 15
                comment = self.content!.comments[indexPath.section]
            }
            else {
                width = Float(tableView.bounds.size.width) - 33 - 35
                
                let sub_comments = self.content!.comments[indexPath.section].sub_comments as [CommentNode]
                comment = sub_comments[indexPath.row - 1] as CommentNode
            }
            
            let customFont: UIFont = UIFont(name: "Lato", size: 15)!
            let contentString: NSString = comment!.content! as NSString
            
            let maximumLabelSize = CGSizeMake(CGFloat(width!), 500)
            let options: NSStringDrawingOptions = [.TruncatesLastVisibleLine, .UsesLineFragmentOrigin]
            let attr : NSDictionary = [NSFontAttributeName: customFont]
            
            let labelSize = contentString.boundingRectWithSize(maximumLabelSize, options: options, attributes: attr as? [String : AnyObject], context: nil) as CGRect
            
            return 75 + labelSize.size.height
        }
        else {
            return 44
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView.isEqual(commentsTable) {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCellWithIdentifier("CommentTableCell", forIndexPath: indexPath) as! CommentTableCell
                
                
                cell.leftPadding.constant = 10
                cell.comment = self.content!.comments[indexPath.section]
                
                var width : Float?
                var comment : CommentNode?
                
                width = Float(tableView.bounds.size.width) - 33 - 15
                comment = self.content!.comments[indexPath.section]
                let contentString: NSString = comment!.content! as NSString
                
                let customFont: UIFont = UIFont(name: "Lato", size: 15)!
                let maximumLabelSize = CGSizeMake(CGFloat(width!), 500)
                let options: NSStringDrawingOptions = [.TruncatesLastVisibleLine, .UsesLineFragmentOrigin]
                let attr : NSDictionary = [NSFontAttributeName: customFont]
                let labelSize = contentString.boundingRectWithSize(maximumLabelSize, options: options, attributes: attr as? [String : AnyObject], context: nil) as CGRect
                
                cell.commentLabelHeight.constant = labelSize.size.height
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCellWithIdentifier("CommentTableCell", forIndexPath: indexPath) as! CommentTableCell
                
                cell.leftPadding.constant = 30
                let sub_comments = self.content!.comments[indexPath.section].sub_comments as [CommentNode]
                cell.isSubcell = true
                cell.comment = sub_comments[indexPath.row - 1] as CommentNode
                
                var width : Float?
                var comment : CommentNode?
                
                width = Float(tableView.bounds.size.width) - 33 - 35
                comment = self.content!.comments[indexPath.section]
                let contentString: NSString = comment!.content! as NSString
                
                let customFont: UIFont = UIFont(name: "Lato", size: 15)!
                let maximumLabelSize = CGSizeMake(CGFloat(width!), 500)
                let options: NSStringDrawingOptions = [.TruncatesLastVisibleLine, .UsesLineFragmentOrigin]
                let attr : NSDictionary = [NSFontAttributeName: customFont]
                let labelSize = contentString.boundingRectWithSize(maximumLabelSize, options: options, attributes: attr as? [String : AnyObject], context: nil) as CGRect
                
                cell.commentLabelHeight.constant = labelSize.size.height
                
                return cell
            }
//            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("UsernameCell")
            cell?.textLabel?.text = usernames[indexPath.row]["username"] as? String
            return cell!
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if usernames.count == 0 {
            return
        }
        
        usersTableHeight.constant = 0
        let word = usernames[indexPath.row]["username"] as! String
        var words = commentText.text.componentsSeparatedByString(" ")
        let keyword = words[currentWordIndex]
        
        
        if keyword.hasPrefix("@") {
            words[currentWordIndex] = "@" + word
        }
        let totalString = words.joinWithSeparator(" ")
        commentText.text = totalString
        usernames = []
        tableView.reloadData()
    }
    
    // MARK: - UITextView

    func textViewDidChange(textView: UITextView) {
        let selectedRange = textView.selectedRange
        let beginning = textView.beginningOfDocument
        let start = textView.positionFromPosition(beginning, offset: selectedRange.location)
        let end = textView.positionFromPosition(start!, offset: selectedRange.length)
//        let textRange = textView.tokenizer.rangeEnclosingPosition(end!, withGranularity: .Word, inDirection: 1)
//        let wordTyped = textView.textInRange(textRange) ?? ""
        let wordsInSentence = textView.text.componentsSeparatedByString(" ")
        
        
        var indexInSavedArray = 0;
        for string in wordsInSentence {
            let nsText = textView.text as NSString
            let range = nsText.rangeOfString(string)
            if selectedRange.location >= range.location && selectedRange.location <= (range.location + range.length) {
                if string.hasPrefix("@") {
                    currentWordIndex = indexInSavedArray
                    loadUsernamesWithWord(string)
                } else {
                    
                }
            }
            indexInSavedArray += 1
        }

        
        let textViewFixedWidth: CGFloat = self.commentText.frame.size.width
        let newSize: CGSize = self.commentText.sizeThatFits(CGSizeMake(textViewFixedWidth, CGFloat(MAXFLOAT)))
        var newFrame: CGRect = self.commentText.frame
        
        var textViewYPosition = self.commentText.frame.origin.y
        let heightDifference = self.commentText.frame.height - newSize.height
        
        if (abs(heightDifference) > 20) {
            newFrame.size = CGSizeMake(fmax(newSize.width, textViewFixedWidth), newSize.height)
//            newFrame.offsetInPlace(dx: 0.0, dy: 0)
        }
        
        if newSize.height <= 159 {
            
            if self.commentText.text == "" {
                self.vwBottomHeight.constant = 56
            }
            else {
                self.commentText.frame = newFrame
                self.vwBottomHeight.constant = newSize.height + 19
            }
            self.commentText.scrollEnabled = false
        }
        else {
            self.commentText.scrollEnabled = true
            self.vwBottomHeight.constant = 159 + 19
        }

        if self.commentText.text != "" {
            self.btnPost.titleLabel!.textColor = UIColor(colorLiteralRed: 0, green: 0.478, blue: 1, alpha: 1)
        }
        else {
            self.btnPost.titleLabel!.textColor = UIColor(colorLiteralRed: 0.6078, green: 0.6078, blue: 0.6078, alpha: 1)
        }
    }
    
    // same in AskEditCell.swift
    func loadUsernamesWithWord(word: String) {
        var string = word
        if word.hasPrefix("@") {
            string = word.substringFromIndex(word.startIndex.advancedBy(1))
        }
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.GetUsernames(searchKey: string)) { result in
            let JSON = Helper.validateResponse(result, sender: self)
            if JSON != nil {
//                let users = JSON!["users"] as! [String: AnyObject]
                self.usernames = JSON!["users"] as? [[String: AnyObject]] ?? []
                self.usersTable.reloadData()
            }
        }
    }
    
    // MARK: - UIPanGestureRecognizer
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        let velocity = panGestureRecognizer.velocityInView(self.vwDraggableView)
        
        /*
        var pannableToLeft = true//(currentPageIndex == 0 && fabs(velocity.x) >= fabs(velocity.y) && velocity.x > 0)
        var pannableToRight = true//(currentPageIndex == (viewControllers.count - 1) && fabs(velocity.x) >= fabs(velocity.y) && velocity.x < 0)
        var pannableFromTop = false
        var pannableFromBottom = false
        */
        
        let velocityTable = panGestureRecognizer.velocityInView(self.commentsTable) as CGPoint
        
        var pannableToLeftOnTable = (fabs(velocity.x) >= fabs(velocity.y) && velocity.x > 0)
        
        var pannableToRightOnTable = (fabs(velocity.x) >= fabs(velocity.y) && velocity.x < 0)
        
        
        let headerTouchLocation = panGestureRecognizer.locationInView(vwHeader)
        let pannableInsideHeader = vwHeader.pointInside(headerTouchLocation, withEvent: nil)// && fabs(velocity.x) >= fabs(velocity.y)
        
        
        let bottomTouchLocation = panGestureRecognizer.locationInView(vwBottom)
        let pannableInsideBottom = vwBottom.pointInside(bottomTouchLocation, withEvent: nil)
        
        //        let insideHeader = pannableInsideHeader ? "Inside Header" : "Outside Header"
        //        print(insideHeader)
        
        if (pannableToLeftOnTable || pannableToRightOnTable || pannableInsideHeader || pannableInsideBottom) {
            return true
        }
        
        return false
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if otherGestureRecognizer is UIPanGestureRecognizer || otherGestureRecognizer is UITapGestureRecognizer {
            return true
        }
        
        return false
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func handlePan(panGesture: UIPanGestureRecognizer){
        
        vwDraggableView.handlePanGesture(panGestureRecognizer)
        
    }
    
    // MARK: - PannableCardViewDelegate
    func resetCardPosition(){
        
    }
    func cardSwipedAway() {
        self.dismissViewControllerAnimated(false, completion: nil)
        
        let comments = content?.comments.count ?? 0
        var count = comments
        
        for comment in (content?.comments)! {
            count = count + comment.sub_comments.count
        }
        
        if self.prevMainVC == nil {
            prevDraftVC!.commentsLabel.text = String(count)
        }
        else {
            prevMainVC!.commentsLabel.text = String(count)
        }
    }
    
    func cardWasDragged(dragPercentage: CGFloat) {
        self.vwMain.backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.7 * (1 - Float(dragPercentage)))
    }
    
    func cardStartedDismissal(duration: NSTimeInterval, dragPercentage: Float) {
        var percentage = dragPercentage
        UIView.animateWithDuration(duration) {
            percentage = 1
            self.vwMain.backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.7 * (1 - percentage))
        }
    }
    
    @IBAction func onLike(sender: AnyObject) {
        let btnLike = sender as! UIButton
        let currentCell = getTVCFromButton(btnLike)
        
        let indexPath = commentsTable.indexPathForCell(currentCell!)! as NSIndexPath
        var comment = self.content!.comments[indexPath.section]
        if indexPath.row > 0 {
            comment = self.content!.comments[indexPath.section].sub_comments[indexPath.row - 1]
        }
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        
        if !comment.isLiked! {
            provider.request(.LikeComment(commentId: comment.id)) { result in
                let JSON = Helper.validateResponse(result, sender: self)
                if JSON != nil {
                    print(JSON)
                    var u_comment = self.content!.comments[indexPath.section]
                    if indexPath.row > 0 {
                        u_comment = self.content!.comments[indexPath.section].sub_comments[indexPath.row - 1]
                    }
                    
                    u_comment.isLiked = true
                    u_comment.likes = u_comment.likes + 1
                    self.commentsTable.reloadData()
                    NSNotificationCenter.defaultCenter().postNotificationName(Constants.Literals.contentRefreshNotification, object: nil)
                }
                //            self.dismissViewControllerAnimated(true, completion: nil)
            }
        }
        else {
            provider.request(.UnlikeComment(commentId: comment.id)) { result in
                let JSON = Helper.validateResponse(result, sender: self)
                if JSON != nil {
                    print(JSON)
                    var u_comment = self.content!.comments[indexPath.section]
                    if indexPath.row > 0 {
                        u_comment = self.content!.comments[indexPath.section].sub_comments[indexPath.row - 1]
                    }
                    
                    u_comment.isLiked = false
                    u_comment.likes = u_comment.likes - 1
                    self.commentsTable.reloadData()
                    NSNotificationCenter.defaultCenter().postNotificationName(Constants.Literals.contentRefreshNotification, object: nil)
                }
                //            self.dismissViewControllerAnimated(true, completion: nil)
            }
        }
    }
    
    @IBAction func OnReply(sender: AnyObject) {
        isReply = true
        
        let btnReply = sender as! UIButton
        let currentCell = getTVCFromButton(btnReply)
        
        let indexPath = commentsTable.indexPathForCell(currentCell!)! as NSIndexPath
        self.currentComment = self.content!.comments[indexPath.section]
        
        self.commentId = self.currentComment!.id
        commentText.becomeFirstResponder()
    }
    
    func getTVCFromButton(buttonObj: UIButton) -> CommentTableCell? {
        var superView = buttonObj.superview
        
        while(superView != nil && superView?.isKindOfClass(CommentTableCell) == false && superView?.isKindOfClass(UIViewController) == false) {
            superView = superView?.superview
        }
        
        if ((superView?.isKindOfClass(CommentTableCell)) == true) {
            return superView as? CommentTableCell
        }
        
        return nil
    }
    
    func keyboardDidHide(notification: NSNotification) {
        vwMain.roundBottomCorners(20)
        vwDraggableView.roundTopCorners(20)
        
        let offset = CGSize(width: 0.2, height: 0.2)
        bottomWrapper.layer.shadowOffset = offset
        bottomWrapper.layer.shadowOpacity = 0
        bottomWrapper.layer.shadowRadius = 1
        bottomWrapper.layer.cornerRadius = 14.0

        commentTextTopPadding.constant = 0
        commentTextBottomPadding.constant = 12
        
        postButtonBottomPadding.constant = 12
        wrapperTopPadding.constant = 9
    }
    
    func keyboardDidShow(notification: NSNotification) {
        vwMain.roundBottomCorners(0)
        vwDraggableView.roundTopCorners(0)

        let offset = CGSize(width: 0.2, height: 0.2)
        bottomWrapper.layer.shadowOffset = offset
        bottomWrapper.layer.shadowOpacity = 1
        bottomWrapper.layer.shadowRadius = 1.0
        bottomWrapper.layer.cornerRadius = 14.0
        bottomWrapper.layer.shadowColor = UIColor(colorLiteralRed: 0.6, green: 0.6, blue: 0.6, alpha: 1).CGColor
        
        commentTextTopPadding.constant = 5
        commentTextBottomPadding.constant = 10
        
        postButtonBottomPadding.constant = 7
        wrapperTopPadding.constant = 11
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
//    func removeList(view: UIView) {
//        
//        if listTableView != nil {
//            listTableView?.hidden = true
//        }
//        
//        let subviews = view.subviews
//        
//        if subviews.count == 0 {
//            return
//        }
//        
//        for view1 in subviews {
//            
//            if view1.isKindOfClass(UITableView) {
//                let tableview = view1 as! UITableView
//                if tableview != self.commentsTable {
//                    let indexPath = NSIndexPath(forRow: 0, inSection: 0)
//                    listTableView = tableview
////                    tableView(listTableView!, didSelectRowAtIndexPath: indexPath)
//                    return
//                }
//            }
//            
//            self.removeList(view1)
//        }
//    }
}
