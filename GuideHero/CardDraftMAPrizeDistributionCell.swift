//
//  CardDraftMAPrizeDistributionCell.swift
//  GuideHero
//
//  Created by Promising Change on 02/02/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit

enum SplitOptions {
    case Proportionally
    case Evenly
}

class CardDraftMAPrizeDistributionCell: UITableViewCell {

    // MARK: - @IBOutlet Variables
    
    @IBOutlet weak var btProportion: UIButton!
    @IBOutlet weak var btEven: UIButton!
    
    // MARK: - Variables
    
    var splitOption: String = "Proportionally"
    
    // MARK: - Properties
    
    var prizeSplitOption: SplitOptions {
        get {
            switch splitOption {
            case "Proportionally":
                return SplitOptions.Proportionally
            case "Evenly":
                return SplitOptions.Evenly
            default:
                return SplitOptions.Proportionally
            }
        }
    }
    
    // MARK: - View Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Private Methods
    
    func configureView() {
        resetButtons()
        
        splitOption = "Proportionally"
        btProportion.setImage(UIImage(named: "selectOn"), forState: .Normal)
    }
    
    func resetButtons() {
        btProportion.setImage(UIImage(named: "selectOff"), forState: .Normal)
        btEven.setImage(UIImage(named: "selectOff"), forState: .Normal)
    }
    
    // MARK: - Actions
    
    @IBAction func didTapOnSplitProportion(sender: AnyObject) {
        if (splitOption == "Proportionally") {
            return
        }
        
        resetButtons()
        splitOption = "Proportionally"
        btProportion.setImage(UIImage(named: "selectOn"), forState: .Normal)
    }
    
    @IBAction func didTapOnSplitEven(sender: AnyObject) {
        if (splitOption == "Evenly") {
            return
        }
        
        resetButtons()
        splitOption = "Evenly"
        btEven.setImage(UIImage(named: "selectOn"), forState: .Normal)

    }

}
