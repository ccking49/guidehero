//
//  BasePostLoginController.swift
//  GuideHero
//
//  Created by Yohei Oka on 8/25/16.
//  Copyright © 2016 Twilio. All rights reserved.
//

import UIKit


class  BasePostLoginController: BaseController {
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.tabBarController?.delegate = self
        for tabBarItem in self.tabBarController!.tabBar.items! {
            tabBarItem.title = ""
            tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
        }
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        
        if tabBarController.selectedIndex == 0 {
            let zeroIndexPath = NSIndexPath(forRow: 0, inSection: 0)
            
            let selectedNavVC = viewController as? UINavigationController
            let mainFeedVC = selectedNavVC?.visibleViewController as? MainFeedViewController
            if let pagerVC = mainFeedVC?.pagerController?.currentContent() as? TrendingViewController {
                if pagerVC.filteredIndexArray.count != 0 {
                    pagerVC.tableView.scrollToRowAtIndexPath(zeroIndexPath, atScrollPosition:.Top, animated: true)
                }
                
            }
        }
    }
    override func viewDidDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "my_push", object: nil)
    }
    
    func checkIfLoggedIn() -> Bool {
        if let _ = prefs.stringForKey("guidehero.authtoken") {
            return true
        }
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("accountManagementController") as! AccountManagementController
        
        if let topController = UIApplication.topViewController() {
            topController.presentViewController(vc, animated: true, completion: nil)
        }
        return false
    }

    func reportDevice() {
        let baseUrl = Constants.ApiConstants.baseApiUrl + "auth/"
        let request = NSMutableURLRequest(URL: NSURL(string: baseUrl + "report_device")!)
        let session = NSURLSession(configuration: NSURLSessionConfiguration.ephemeralSessionConfiguration())
        request.HTTPMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let authToken = prefs.stringForKey("guidehero.authtoken")!
        request.addValue(authToken, forHTTPHeaderField: "Authentication-Token")
        
        var tokenToSend = ""
        if let deviceToken = prefs.stringForKey("guidehero.devicetoken"){
            tokenToSend = deviceToken
        }
        if (tokenToSend == "") {
            return
        }
        
        let params = ["device_token": tokenToSend, "device_type": "ios"] as Dictionary<String, String>
        
        do {
            request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
        } catch {
            return
        }
        
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            guard error == nil else {
                return
            }
        })
        task.resume()
    }
}
