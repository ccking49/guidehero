//
//  VideoCardCreationViewController.swift
//  GuideHero
//
//  Created by SoftDev on 12/13/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Moya
import Alamofire


class VideoCardCreationViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var mainScroll : UIScrollView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var wholeView : UIView!
    
    @IBOutlet weak var playedTime: UILabel!
    @IBOutlet weak var remainingTime: UILabel!
    @IBOutlet weak var startTime: UITextField!
    @IBOutlet weak var endTime: UITextField!
    @IBOutlet weak var cardTitle: UITextField!
    @IBOutlet weak var cardDescription: UITextView!
    @IBOutlet weak var descriptionHint: UILabel!
    @IBOutlet weak var cardTags: UITextView!
    @IBOutlet weak var tagsView: UIView!
    @IBOutlet weak var tagButton: UIButton!
    @IBOutlet weak var imgBackground : UIImageView!
    @IBOutlet weak var tagsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var editButton: UIButton!
    
    var keyboardFlag : Bool = false
    
    var prevViewController: UIViewController?
    var player: AVPlayer?
    
    var playerLayer: AVPlayerLayer! = nil
    
    var videoUrl: NSURL?
    var playTimer: NSTimer?
    var isPlaying: Bool = false
    var screenHeight = UIScreen.mainScreen().bounds.height
    var screenWidth = UIScreen.mainScreen().bounds.width
    var duration: Float = 0
    var tagArray = [String]()
    
    var cardName : String? = nil
    var descStr: String? = nil
    var cardId: String? = nil
    
    var pan = UIPanGestureRecognizer()
    var isRotating = false
    
    var startPos = CGPointZero
    var startCenter = CGPointZero
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.onSetupPanel()
        
        videoView.layer.cornerRadius = 10
        videoView.layer.shadowRadius = 3
        videoView.layer.shadowOffset = CGSizeZero
        videoView.layer.shadowColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.5).CGColor
        videoView.clipsToBounds = true
        videoView.layer.borderWidth = 0.5
        videoView.layer.borderColor = UIColor.colorFromRGB(redValue: 155/255.0, greenValue: 155/255.0, blueValue: 155/255.0, alpha: 0.5).CGColor
        
        wholeView.layer.cornerRadius = 20
        wholeView.layer.shadowRadius = 5
        wholeView.layer.shadowOpacity = 0.5
        wholeView.layer.shadowOffset = CGSizeZero
        wholeView.layer.shadowColor = UIColor.blackColor().CGColor
        wholeView.clipsToBounds = true
        
        
        self.setupVideo()
        
        // add 20170126
        self.cardTitle.text = cardName ?? ""
        if descStr != nil {
            self.cardDescription.text = descStr
            descriptionHint.hidden = descStr == "" ? false : true
        }
        else {
            descriptionHint.hidden = false
        }
        
        tagsViewHeightConstraint.constant = 50.0+6.0+50
        cardTags.hidden = true
        
        cardTags.delegate = self
        cardDescription.delegate = self
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        registerForKeyboardNotifications()
        
        UIApplication.sharedApplication().statusBarHidden = true
        self.tabBarController?.tabBar.hidden = true
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(VideoCardCreationViewController.playerDidFinishPlaying(_:)),
                                                         name: AVPlayerItemDidPlayToEndTimeNotification, object: nil)
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        deregisterFromKeyboardNotifications()
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onBack(sender: AnyObject) {
        
        popScreen()
        
        if self.navigationController != nil {
            UIApplication.sharedApplication().statusBarHidden = false
            self.tabBarController?.tabBar.hidden = false
            self.navigationController?.popViewControllerAnimated(true)
        }
        else {
            self.dismissViewControllerAnimated(false, completion: nil)
        }
    }
    
 
    @IBAction func actionEdit(sender: UIButton) {
        if let navCtrl = storyboard!.instantiateViewControllerWithIdentifier("CameraFunctionNavViewController") as? UINavigationController {
            let camFuncVC = navCtrl.viewControllers.first as! CameraFunctionViewController
            
            camFuncVC.parentVC  = self
            camFuncVC.type      = 2
            
            presentViewController(navCtrl, animated: true, completion: nil)
        }
    }
    
    @IBAction func onDone(sender: AnyObject) {
        self.view.endEditing(true)
        if cardTitle.text == nil || cardTitle.text!.length == 0  {
            self.showSimpleAlert(withTitle: "Error", withMessage: "Please add a card title")
            return
        }
        
        var startSecTime = 0
        let startTimeComponents = startTime.text!.componentsSeparatedByString(":")
        if (startTime.text! != "" && startTimeComponents.count < 2) {
            self.showSimpleAlert(withTitle: "Error", withMessage: "Please input correct start time.")
            return
        }
        else {
            let min = Int(startTimeComponents[0])
            let sec = Int(startTimeComponents[1])
            if (min == nil || sec == nil || min >= 60 || sec >= 60) {
                self.showSimpleAlert(withTitle: "Error", withMessage: "Please input correct start time.")
                return
            }
            else {
                startSecTime = min! * 60 + sec!
                if (Float(startSecTime) > duration) {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Out of time range.")
                    return
                }
            }
        }
        
        var endSecTime = 0
        let endTimeComponents = endTime.text!.componentsSeparatedByString(":")
        if (endTime.text! != "" && endTimeComponents.count < 2) {
            self.showSimpleAlert(withTitle: "Error", withMessage: "Please input correct end time.")
            return
        }
        else {
            let min = Int(endTimeComponents[0])
            let sec = Int(endTimeComponents[1])
            if (min == nil || sec == nil || min >= 60 || sec >= 60) {
                self.showSimpleAlert(withTitle: "Error", withMessage: "Please input correct end time.")
                return
            }
            else {
                endSecTime = min! * 60 + sec!
                if (Float(endSecTime) > duration) {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Out of time range.")
                    return
                }
            }
        }
        
        if (startSecTime >= endSecTime) {
            self.showSimpleAlert(withTitle: "Error", withMessage: "Start time must be less than end time.")
            return
        }
        
        // TODO: cleanup, separate logic in various methods, and handle errors properly
        
        UIApplication.sharedApplication().statusBarHidden = false
        self.tabBarController?.tabBar.hidden = false
        
        
        moveToFinalCardViewController(videoUrl!, startTime: Float(startSecTime), endTime: Float(endSecTime))
    }
    
    func moveToFinalCardViewController(url: NSURL, startTime: Float, endTime: Float) {
        let finalCardVC = storyboard!.instantiateViewControllerWithIdentifier("FinalCardViewController") as! FinalCardViewController
        
        finalCardVC.cardName                = self.cardTitle.text!
        finalCardVC.descriptionText         = self.cardDescription.text
        finalCardVC.cardTags                = self.tagArray
        finalCardVC.videoUrl                = url
        finalCardVC.startTime               = startTime
        finalCardVC.endTime                 = endTime
        finalCardVC.prevViewController      = self
        
        finalCardVC.cardId                  = self.cardId
        
        presentViewController(finalCardVC, animated: true, completion: nil)
    }
    
    func setupVideo() {
        
        let asset = AVURLAsset(URL:videoUrl!, options:nil)
        
        self.player = AVPlayer(URL: videoUrl!)
        
        if self.playerLayer != nil {
            self.playerLayer.removeFromSuperlayer()
        }
        
        self.playerLayer = AVPlayerLayer(player: self.player)
        self.playerLayer.frame = CGRect(x: 0, y: 0, width: 300, height: 300)
        videoView.layer.addSublayer(self.playerLayer)
        
        player!.pause()
        
        timeSlider.minimumValue = 0.0
        timeSlider.value = 0.0
        
        duration = Float(CMTimeGetSeconds(asset.duration))
        timeSlider.maximumValue = duration * 100
        onSlider(self)
        
        self.startTime.text = "00:00"
        self.endTime.text = makeTimeString(Int(duration))
    }
    
    ////////////////////////////////
    
    //  Play
    
    @IBAction func onPlay(sender: AnyObject) {
        isPlaying = !isPlaying
        if (isPlaying) {
            player!.play()
            playButton.setImage(UIImage(named: "pauseIcon"), forState: .Normal)
            playTimer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(onMoveSlider), userInfo: nil, repeats: true)
        }
        else {
            player!.pause()
            playButton.setImage(UIImage(named: "playIcon"), forState: .Normal)
            playTimer?.invalidate()
        }
    }
    
    func playerDidFinishPlaying(note: NSNotification) {
        isPlaying = false
        playButton.setImage(UIImage(named: "playIcon"), forState: .Normal)
        timeSlider.value = 0.0
        onSlider(self)
        playTimer?.invalidate()
    }
    
    @IBAction func onSlider(sender: AnyObject) {
        playedTime.text = makeTimeString(Int(timeSlider.value) / 100)
        remainingTime.text = "-" + makeTimeString(Int(duration * 100) / 100 - Int(timeSlider.value) / 100)
        player!.seekToTime(CMTime(seconds: Double(timeSlider.value) / 100, preferredTimescale: player!.currentItem!.asset.duration.timescale), toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
    }
    
    func onMoveSlider() {
        timeSlider.value = Float(CMTimeGetSeconds(player!.currentItem!.currentTime())) * 100
        playedTime.text = makeTimeString(Int(timeSlider.value) / 100)
        remainingTime.text = "-" + makeTimeString(Int(duration * 100) / 100 - Int(timeSlider.value) / 100)
    }
    
    func makeTimeString(timeInSec: Int) -> String {
        var secString = String(timeInSec % 60)
        if (timeInSec % 60 < 10) {
            secString = "0" + secString
        }
        
        var minString = String(timeInSec % 3600 / 60)
        if (timeInSec % 3600 / 60 < 10) {
            minString = "0" + minString
        }
        
        return minString + ":" + secString
    }
    
    ////////////////////////////////
    
    //  Tags
    
    @IBAction func onTagEdit(sender: AnyObject) {
        for subView in tagsView.subviews {
            subView.removeFromSuperview()
        }
        
        cardTags.hidden = false
        cardTags.becomeFirstResponder()
        tagsView.hidden = true
        tagButton.hidden = true
        tagsViewHeightConstraint.constant = 75.0+6.0+50
        self.view.layoutIfNeeded()
    }
    
    func makeTagViews() {
        var x = 25
        var y = 15
        for tag in tagArray {
            let width = Int(widthForView(tag, font: UIFont(name: "ProximaNova-Semibold", size: 18.0)!, height: 29))
            if (x + width + 25 > Int(screenWidth) - 15) {
                x = 25
                y = y + 39
            }
            let label = UILabel(frame: CGRect(x: x, y: y, width: width + 25, height: 29))
            label.text = tag
            label.textAlignment = NSTextAlignment.Center
            label.backgroundColor = UIColor.whiteColor()
            label.textColor = UIColor(red: 155.0 / 255.0, green: 155.0 / 255.0, blue: 155.0 / 255.0, alpha: 1.0)
            label.layer.cornerRadius = 14
            label.layer.borderColor = UIColor(red: 155.0 / 255.0, green: 155.0 / 255.0, blue: 155.0 / 255.0, alpha: 1.0).CGColor
            label.layer.borderWidth = 1.0
            label.clipsToBounds = true
            tagsView.addSubview(label)
            x = x + width + 35
        }
        
        tagsViewHeightConstraint.constant = CGFloat(y) + 39 + 50.0 + 50
        self.view.layoutIfNeeded()
    }
    
    func widthForView(text: String, font: UIFont, height: CGFloat) -> CGFloat{
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.max, height: height))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        if (text == "") {
            label.text = " "
        }
        else {
            label.text = text
        }
        
        label.sizeToFit()
        return label.frame.width
    }
    
    ////////////////////////////////
    
    //  UITextFieldDelegate
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if (textField.tag == 0 || textField.tag == 1) {
            let timeAlert = UIAlertController(title: "Set at current position? ⏱", message: "Or type manually. 👇", preferredStyle: .Alert)

            timeAlert.addTextFieldWithConfigurationHandler { (textField) in
                textField.placeholder = "mm:ss"
                textField.keyboardType = .NumberPad
                textField.tag = 3
                textField.delegate = self
            }

            timeAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action) -> Void in
                let time = timeAlert.textFields![0] as UITextField
                time.delegate = nil
                textField.resignFirstResponder()
            }))

            timeAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in
                let time = timeAlert.textFields![0] as UITextField
                if (time.text == "") {
                    if (textField.tag == 0) {
                        self.startTime.text = self.makeTimeString(Int(self.timeSlider.value / 100))
                    }
                    else {
                        self.endTime.text = self.makeTimeString(Int(self.timeSlider.value / 100))
                    }
                }
                else {
                    if (textField.tag == 0) {
                        self.startTime.text = time.text
                    }
                    else {
                        self.endTime.text = time.text
                    }
                }
                time.delegate = nil
                textField.resignFirstResponder()
            }))
            
            self.presentViewController(timeAlert, animated: true, completion: nil)
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if (textField.tag == 3) {
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            if (newLength > 5) {
                return false
            }
            
            if (range.location == 1 && range.length == 0) {
                textField.text = textField.text! + string + ":"
                return false
            }
            else if (range.location == 2 && range.length == 1) {
                textField.text = (text as NSString).substringToIndex(1)
                return false
            }
            
            return true
        }
        else {
            return true
        }
    }
    
    ////////////////////////////////
    
    //  UITextViewDelegate
    
    func textViewDidBeginEditing(textView: UITextView) {
        if (textView.tag == 0) {
            descriptionHint.hidden = true
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if (textView.tag == 0) {
            if (textView.text == "") {
                descriptionHint.hidden = false
            }
        }
        else {
            tagArray.removeAll()
            if (textView.text != "") {
                tagArray = textView.text.componentsSeparatedByString(",")
                for (index, tag) in tagArray.enumerate() {
                    tagArray[index] = tag.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                }
                makeTagViews()
            }
            else {
                tagsViewHeightConstraint.constant = 50.0+6.0+50
            }
            cardTags.hidden = true
            tagsView.hidden = false
            tagButton.hidden = false
        }
    }
    func onSetupPanel(){
        
        pan.addTarget(self, action: #selector(VideoCardCreationViewController.handlePan(_:)))
        pan.delegate = self
        self.wholeView.addGestureRecognizer(pan)
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        let velocity = pan.velocityInView(self.mainScroll?.superview)
        
        if fabs(velocity.x) >= fabs(velocity.y) || (mainScroll.contentOffset.y <= 0 && velocity.y > 0) {
            return true
        }
        
        return false
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if otherGestureRecognizer is UIPanGestureRecognizer {
            return true
        }
        
        return false
    }
    
    func handlePan(panGesture: UIPanGestureRecognizer){
        
        switch panGesture.state {
        case .Began:
            
            startPos = panGesture.locationInView(self.wholeView?.superview)
            startCenter = self.wholeView.center
            print(startPos)
            let velocity = panGesture.velocityInView(self.wholeView?.superview)
            
            isRotating = fabs(velocity.x) > fabs(velocity.y)
            
        case .Changed:
            
            if !isRotating {
                
                let pos = panGesture.locationInView(self.view?.superview)
                self.wholeView.center = CGPointMake(startCenter.x, startCenter.y + pos.y - startPos.y)
            }
            
        case .Ended:
            
            if !isRotating {
                
                let velocity = panGesture.velocityInView(self.wholeView?.superview)
                if velocity.y > 0 { // should dismiss
                    UIView.animateWithDuration(0.2, animations: {
                        self.wholeView.center = CGPointMake(self.startCenter.x, self.wholeView.bounds.size.height * 1.5)
                        }, completion: { completed in
                            self.popScreen()
                            self.dismissViewControllerAnimated(false, completion: nil)
                    })
                } else {
                    UIView.animateWithDuration(0.2, animations: {
                        self.wholeView.center = self.startCenter
                        }, completion: { completed in
                    })
                }
            }
            
            
        default:break
        }
    }
    
    func popScreen(){
        player!.pause()
        playButton.setImage(UIImage(named: "playIcon"), forState: .Normal)
        playTimer?.invalidate()
        
        
    }
    
    // MARK: Keyboard management
    
    func registerForKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWasShown), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillBeHidden), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWillBeHidden(notification: NSNotification) {
        
        keyboardFlag = false
    }
    
    func keyboardWasShown(notification: NSNotification) {
        
        keyboardFlag = true
    }
    
    ////////////////////////////////
}
