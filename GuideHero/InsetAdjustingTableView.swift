//
//  InsetAdjustingTableView.swift
//  GuideHero
//
//  Created by Abhishek Sengar on 03/09/2016.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import Foundation

class InsetAdjustingTableView: UITableView {
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        registerForKeyboardNotifications()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        registerForKeyboardNotifications()
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func registerForKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    // MARK: Keyboard Events
    
    func keyboardWillShow(notification: NSNotification) {
        guard let keyboardRect = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.CGRectValue() else { return }
        guard let bottomInScreen = superview?.convertPoint(CGPointMake(frame.minX, frame.maxY), toView: nil) else { return }
        
        let diff = UIScreen.mainScreen().bounds.height - bottomInScreen.y
        
        if diff < keyboardRect.size.height {
            let keyboardTop = keyboardRect.size.height - diff
            
            var insets = contentInset
            insets.bottom = keyboardTop
            contentInset = insets
            
            insets = scrollIndicatorInsets
            insets.bottom = keyboardTop
            scrollIndicatorInsets = insets
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        var insets = contentInset
        insets.bottom = 0
        contentInset = insets
        
        insets = scrollIndicatorInsets
        insets.bottom = 0
        scrollIndicatorInsets = insets
    }
}
