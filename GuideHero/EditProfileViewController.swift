//
//  EditProfileViewController.swift
//  GuideHero
//
//  Created by Ascom on 10/31/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya


class EditProfileViewController: UIViewController {

    @IBOutlet var usernameText: UITextField!
    @IBOutlet var fullnameText: UITextField!
    @IBOutlet var bioText: UITextField!
    @IBOutlet var thumbnail: UIImageView!
    
    var JSON: [String: AnyObject] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Functions
    
    func updateUI() {
        view.layoutIfNeeded()
        
        usernameText.textColor = Constants.UIConstants.yellowColor
        fullnameText.textColor = Constants.UIConstants.yellowColor
        bioText.textColor = Constants.UIConstants.yellowColor
        usernameText.placeholderText = "Username"
        fullnameText.placeholderText = "Fullname"
        bioText.placeholderText = "Bio"
        setupTextField(usernameText)
        setupTextField(fullnameText)
        setupTextField(bioText)
        
        
        let thumbnailUrl = JSON["thumbnail"]! as! String
        if thumbnailUrl.length > 0 {
            let imageUrl = NSURL(string: thumbnailUrl)!
            thumbnail.af_setImageWithURL(imageUrl)
        }
        thumbnail.layer.cornerRadius = CGRectGetWidth(thumbnail.frame)/2
        thumbnail.layer.borderWidth = 5
        thumbnail.layer.borderColor = UIColor.whiteColor().CGColor
        
        
        let first_name = JSON["first_name"] as? String ?? ""
        let last_name = JSON["last_name"] as? String ?? ""
        fullnameText.text = first_name + " " + last_name
        usernameText.text = JSON["name"] as? String ?? ""
        bioText.text = JSON["bio"] as? String ?? ""
    }
    
    func setupTextField(textField : UITextField) {
        textField.textColor = Constants.UIConstants.yellowColor
        let title = textField.placeholderText ?? ""
        var myMutableStringTitle = NSMutableAttributedString()
        myMutableStringTitle = NSMutableAttributedString(string:title, attributes: [NSFontAttributeName:textField.font!]) // Font
        myMutableStringTitle.addAttribute(NSForegroundColorAttributeName, value: Constants.UIConstants.halfYellowColor, range:NSRange(location:0,length:title.length))
        textField.attributedPlaceholder = myMutableStringTitle
    }
    
    // MARK: - IBAction

    @IBAction func onCancel(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
 
    @IBAction func onDone(sender: AnyObject) {
        // Validate fields
        let username = (usernameText.text ?? "")//.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: " "))
        if username == "" {
            self.showSimpleAlert(withTitle: "Oops.", withMessage: "You need a username!")
            return
        }
        
        let name = (fullnameText.text ?? "").stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: " "))
        if name == "" || !name.containsString(" ") {
            self.showSimpleAlert(withTitle: "Oops.", withMessage: "You need both first name and last name!")
            return
        }
        let comps = name.componentsSeparatedByString(" ")
        let firstName = comps.first ?? ""
        let lastName = comps.last ?? ""
        if firstName == "" || lastName == "" {
            self.showSimpleAlert(withTitle: "Oops.", withMessage: "You need both first name and last name!")
            return
        }

        let bio = bioText.text ?? ""
        
        
        self.showProgressHUD()
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.EditProfile(firstName: firstName, lastName: lastName, username: username, bio: bio)) { result in
            self.hideProgressHUD()
            switch result {
            case let .Success(response):
//                if response.statusCode == 200 {
                    if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                        let errorText = JSON["error"] as? String ?? ""
                        if errorText != "" {
                            switch errorText {
                            case "invalid_name":
                                self.showSimpleAlert(withTitle: "Error", withMessage: "You entered invalid name!")
                                break
                            case "username_already_exists":
                                self.showSimpleAlert(withTitle: "Error", withMessage: "Username already exists!")
                                break
                            case "bio_too_long":
                                self.showSimpleAlert(withTitle: "Error", withMessage: "Bio is too long!")
                                break
                            default:  break
                            }
                        } else {
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }
                        return
                    }
//                }
                break
            default:  break
            }
            self.showSimpleAlert(withTitle: "Error", withMessage: "Can't save profile, please try again later")
        }
    }
    

}
