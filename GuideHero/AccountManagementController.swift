
//  AccountManagementController.swift
//  GuideHero
//
//  Created by Yohei Oka on 7/24/16.
//  Copyright © 2016 Twilio. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Moya


class AccountManagementController: BasePreLoginController, FBSDKLoginButtonDelegate, GIDSignInUIDelegate, GIDSignInDelegate {
    
    let baseUrl = Constants.ApiConstants.baseApiUrl + "auth/"

    
    @IBOutlet var fbLoginView: FBSDKLoginButton!
    @IBOutlet var linkedinButton: UIButton!
    @IBOutlet var spinner: UIActivityIndicatorView!

    @IBOutlet var googleSignInButton: GIDSignInButton!
    
    var authToken = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let background = CAGradientLayer().LandingPageColor()
        background.frame = self.view.bounds
        self.view.layer.insertSublayer(background, atIndex: 0)
        
        fbLoginView.readPermissions = ["public_profile", "email", "user_friends"]
        fbLoginView.delegate = self
        
        googleSignInButton.style = GIDSignInButtonStyle.Wide
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        if let _ = prefs.stringForKey("guidehero.authtoken") {
            loginUser()
            return
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        FBSDKLoginManager().logOut()
        GIDSignIn.sharedInstance().signOut()
        
        if let _ = prefs.stringForKey("guidehero.authtoken") {
            loginUser()
            return
        }
    }
    
    // Facebook Delegate Methods
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        spinner.startAnimating()
        if ((error) != nil)
        {
            spinner.stopAnimating()
            self.displayAlert("Error", message: "Please try again")
        }
        else if result.isCancelled {
            // Handle cancellations
            spinner.stopAnimating()
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            
            
            if !(result.grantedPermissions.contains("email"))
            {
                self.displayAlert("Error", message: "Please try again")
                FBSDKLoginManager().logOut()
            }
            let fbToken = result.token.tokenString
            
            let params = ["fb_access_token": fbToken] as Dictionary<String, String>
            let request = NSMutableURLRequest(URL: NSURL(string: baseUrl + "fb_signin")!)
            let session = NSURLSession(configuration: NSURLSessionConfiguration.ephemeralSessionConfiguration())
            request.HTTPMethod = "POST"
            
            do {
                request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
            } catch {
                spinner.stopAnimating()
                self.displayAlert("Error", message: "Please try again")
                return
            }
            
            let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                guard error == nil else {
                    self.dispatchStopActivityIndicatory(self.spinner)
                    self.dispatchAlert("Error", message: "Please try again")
                    return
                }
                
                let json: NSDictionary?
                do {
                    json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as? NSDictionary
                } catch {
                    // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                    self.dispatchStopActivityIndicatory(self.spinner)
                    self.dispatchAlert("Error", message: "Please try again")
                    return
                }
                
                if let parseJSON = json {
                    // check if auth_token is returned
                    if let authToken = parseJSON.valueForKeyPath("auth_token") as! String! {
                        self.dispatchStopActivityIndicatory(self.spinner)
//                        self.setAuthTokenAndLogin(authToken)
                        self.setAuthDataAndLogin(parseJSON)
                        return
                    }
                    if let result = parseJSON.valueForKeyPath("error") as! String! {
                        if result == "not_verified" {
                            self.dispatchStopActivityIndicatory(self.spinner)
                            self.dispatchAlert("Error", message: "This account hasn't been confirmed")
                            return
                        }
                        if result == "no_email_account" {
                            self.dispatchStopActivityIndicatory(self.spinner)
                            self.dispatchAlert("Error", message: "Please try again")
                            return
                        }
                    }
                }
                self.dispatchStopActivityIndicatory(self.spinner)
                self.dispatchAlert("Error", message: "Please try again")
            })
            task.resume()
        }
    
    }
    
    // Google Delegate Methods
    
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!, withError error: NSError!) {
        if user == nil {
            return
        }
        spinner.startAnimating()
        let googleToken = user.authentication.accessToken
        let params = ["google_access_token": googleToken] as Dictionary<String, String>
        let request = NSMutableURLRequest(URL: NSURL(string:baseUrl + "google_signin")!)
        let session = NSURLSession(configuration: NSURLSessionConfiguration.ephemeralSessionConfiguration())
        request.HTTPMethod = "POST"

        do {
            request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
        } catch {
            spinner.stopAnimating()
            self.displayAlert("Error", message: "Please try again")
            return
        }
        
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            guard error == nil else {
                self.dispatchStopActivityIndicatory(self.spinner)
                self.dispatchAlert("Error", message: "Please try again")
                return
            }
            
            let json: NSDictionary?
            do {
                json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as? NSDictionary
            } catch {
                // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                self.dispatchStopActivityIndicatory(self.spinner)
                self.dispatchAlert("Error", message: "Please try again")
                return
            }
            
            if let parseJSON = json {
                // check if auth_token is returned
                if parseJSON.valueForKeyPath("auth_token") != nil {
                    self.dispatchStopActivityIndicatory(self.spinner)
                    self.setAuthDataAndLogin(parseJSON)
//                    Session.sharedSession.setAuthResponse(parseJSON)
                    return
                }
                if let result = parseJSON.valueForKeyPath("error") as! String! {
                    if result == "not_verified" {
                        self.dispatchStopActivityIndicatory(self.spinner)
                        self.dispatchAlert("Error", message: "This account hasn't been confirmed")
                    }
                }
            }
            self.dispatchStopActivityIndicatory(self.spinner)
            self.dispatchAlert("Error", message: "Please try again")
        })
        task.resume()
    }

    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if touch.view is GIDSignInButton {
            GIDSignIn.sharedInstance().signIn()
            return false
        }
        if Constants.dev {
            let provider = MoyaProvider<NetworkService>.createDefaultProvider()
            var email = "chanhung1989@gmail.com"
            email = "chrisryuichi@gmail.com"
            provider.request(.EmailSignIn(email: email), completion: { (result) in
                let JSON = Helper.validateResponse(result, sender: self)
                if JSON != nil {
                    self.setAuthDataAndLogin(JSON!)
                }
            })
        }
        return true
    }
}
