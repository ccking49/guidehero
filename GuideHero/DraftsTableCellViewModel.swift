//
//  DraftsTableCellViewModel.swift
//  GuideHero
//
//  Created by Dino Bartosak on 13/10/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import Foundation

enum DraftsTableCellContentType {
    case Text
    case Image
}

protocol DraftsTableCellViewModel {
    var cellType: DraftsTableCellContentType { get }
    var deckImageURL: NSURL? { get } // will be set only if this deck is of 'Image' type
    var deckText: String? { get } // will be set only if this deck is of 'Text' type
    
    var title: String { get }
    var cardDescription: String { get }
    var authorImageURL: NSURL? { get }
    var creationDetails: String { get }
    var id: String { get }
}
