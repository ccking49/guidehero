//
//  FullScreenViewController.swift
//  GuideHero
//
//  Created by SoftDev on 12/4/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya

class FullScreenViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var cardTableView: UICollectionView!
    @IBOutlet weak var wholeView : UIView!
    @IBOutlet weak var imgBackground : UIImageView!
    
    var content: ContentNode?
    var hasDeck: Bool = false
    let screenWidth = UIScreen.mainScreen().bounds.width
    let screenHeight = UIScreen.mainScreen().bounds.height
    var cardIndexes = [Int]()
    var deckIndex = 0
    var cardIndex = -1
    var bShowControls = true
    var isPlaying : Bool = false
    
    var pan = UIPanGestureRecognizer()
    var startPos = CGPointZero
    var startCenter = CGPointZero
    var isRotating = false
    var isDirection = false
    var isCurrentVideo = false
    
    deinit {
        print("deinit")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        tabBarController!.tabBar.hidden = true
        
        if (content != nil && content?.children.count != 0) {
            for i in 0...content!.children.count - 1 {
                if (i == deckIndex && cardIndex != -1) {
                    cardIndexes.append(cardIndex)
                }
                else {
                    if (content!.children[i].children.count > 0) {
                        cardIndexes.append(0)
                    }
                    else {
                        cardIndexes.append(-1)
                    }
                }
            }
        }else{
            cardIndexes.append(0)
        }
        
        cardTableView.contentOffset = CGPoint(x: screenWidth * CGFloat(deckIndex) + 0.1, y: 0)
        cardTableView.pagingEnabled = true
        
        let layout = cardTableView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.height)
        
        onSetupPanel()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.sharedApplication().statusBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    


    //MARK: - UICollectionViewDelegate
    // tell the collection view how many cells to make
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (content == nil) {
            return 0
        }
        if content?.children.count == 0 {
            return 1
        }
        return content!.children.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("FullScreenCollectionViewCell", forIndexPath: indexPath) as! FullScreenCollectionViewCell
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.delegate = self
        //        cell.selectionStyle = UITableViewCellSelectionStyle.None
        self.deckIndex = indexPath.row

        if content?.children.count == 0 {
            cell.configureCell(content!, index: indexPath.row, cIndex: cardIndexes[indexPath.row], screenWidth: screenWidth, screenHeight: screenHeight)
            let singleTap = UITapGestureRecognizer(target: cell, action: #selector(cell.moveToNextCardFromGive))
            cell.mainView.addGestureRecognizer(singleTap)
            
        }else{
            cell.configureCell(content!.children[indexPath.row], index: indexPath.row, cIndex: cardIndexes[indexPath.row], screenWidth: screenWidth, screenHeight: screenHeight)
            let singleTap = UITapGestureRecognizer(target: cell, action: #selector(cell.moveToNextCard))
            cell.mainView.addGestureRecognizer(singleTap)
            
        }
        
        return cell
    }

    
    @IBAction func onClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onHideControls(sender: AnyObject) {
        let deckIndex = (sender as! UIButton).tag
        let cell = cardTableView.cellForItemAtIndexPath(NSIndexPath(forRow: deckIndex, inSection: 0)) as! FullScreenCollectionViewCell
        cardTableView.scrollEnabled = false
        cell.onShowFullScreen()
        bShowControls = false
    }
    
    @IBAction func onShowControls(sender: AnyObject) {
        let deckIndex = (sender as! UIButton).tag
        let cell = cardTableView.cellForItemAtIndexPath(NSIndexPath(forRow: deckIndex, inSection: 0)) as! FullScreenCollectionViewCell
        cardTableView.scrollEnabled = true
        cell.onHideFullScreen()
        bShowControls = true
    }
    
    func moveCard(deckIndex: Int, cardIndex: Int) {
        bShowControls = true
        cardTableView.scrollEnabled = true
        
        let childCount = content!.children[deckIndex].children.count
        
        if childCount != 0 {
            cardIndexes[deckIndex] = (cardIndexes[deckIndex] + 1) % childCount
            if (cardIndexes[deckIndex] == 0) {
                if (deckIndex < cardIndexes.count - 1) {
                    cardIndexes[deckIndex + 1] = 0
                    cardTableView.scrollToItemAtIndexPath(NSIndexPath(forRow: deckIndex + 1, inSection: 0), atScrollPosition: .None, animated: true)
                }
            }
        }
        
        
        cardTableView.reloadData()
    }
    
    ////////////////////////////////
    
    //  Social
    
    @IBAction func onLike(sender: AnyObject) {
        
        if content?.children.count != 0 {
            let deckIndex = (sender as! UIButton).tag
            let cardIndex = cardIndexes[deckIndex]
            var currentContent: ContentNode?
            if (cardIndex == -1) {
                currentContent = content!.children[deckIndex]
            }
            else {
                currentContent = content!.children[deckIndex].children[cardIndex]
            }
            if (currentContent == nil) {
                return
            }
            
            let cell = cardTableView.cellForItemAtIndexPath(NSIndexPath(forRow: deckIndex, inSection: 0)) as! FullScreenCollectionViewCell
            
            let liked = currentContent!.liked_by_me
            let provider = MoyaProvider<NetworkService>.createDefaultProvider()
            let target = liked ? NetworkService.UnlikeCard(cardId : currentContent!.id) : NetworkService.LikeCard(cardId : currentContent!.id)
            
            provider.request(target) { result in
                switch result {
                case let .Success(response):
                    if response.statusCode == 200 {
                        currentContent!.liked_by_me = !liked
                        currentContent!.likes += liked ? -1 : 1
                        cell.makeSocialUI()
                    }
                case let .Failure(error):
                    print(error)
                }
            }
        }else{
            
            let cell = cardTableView.cellForItemAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as! FullScreenCollectionViewCell
            
            let liked = content!.liked_by_me
            let provider = MoyaProvider<NetworkService>.createDefaultProvider()
            let target = liked ? NetworkService.UnlikeCard(cardId : content!.id) : NetworkService.LikeCard(cardId : content!.id)
            
            provider.request(target) { result in
                switch result {
                case let .Success(response):
                    if response.statusCode == 200 {
                        self.content!.liked_by_me = !liked
                        self.content!.likes += liked ? -1 : 1
                        cell.makeSocialUI()
                    }
                case let .Failure(error):
                    print(error)
                }
            }
            
        }
        
    }
    
    @IBAction func onComment(sender: AnyObject) {
        
        if content?.children.count != 0 {
            let deckIndex = (sender as! UIButton).tag
            let cardIndex = cardIndexes[deckIndex]
            var currentContent: ContentNode?

            if (cardIndex == -1) {
                currentContent = content!.children[deckIndex]
            }
            else {
                currentContent = content!.children[deckIndex].children[cardIndex]
            }

            if (currentContent == nil) {
                return
            }
            
//            let commentsViewController = storyboard!.instantiateViewControllerWithIdentifier("CommentsViewController") as! CommentsViewController
            let commentsViewController = UIStoryboard.loadViewController(storyboardName: "Comment", viewControllerIdentifier: "CommentsViewController") as! CommentsViewController
            commentsViewController.content = currentContent
            commentsViewController.delegate = self
            
            let nav = UINavigationController()
            nav.viewControllers = [commentsViewController]
            nav.modalPresentationStyle = .OverFullScreen
            nav.navigationBarHidden = true
            self.presentViewController(nav, animated: true, completion: nil)
        } else {
//            let commentsViewController = storyboard!.instantiateViewControllerWithIdentifier("CommentsViewController") as! CommentsViewController
            let commentsViewController = UIStoryboard.loadViewController(storyboardName: "Comment", viewControllerIdentifier: "CommentsViewController") as! CommentsViewController
            commentsViewController.content = self.content
            commentsViewController.delegate = self
            
            let nav = UINavigationController()
            nav.viewControllers = [commentsViewController]
            nav.modalPresentationStyle = .OverFullScreen
            nav.navigationBarHidden = true
            self.presentViewController(nav, animated: true, completion: nil)
        }
    }
    
    ////////////////////////////////
    
    func onSetupPanel() {
        pan.addTarget(self, action: #selector(PointViewController.handlePan(_:)))
        pan.delegate = self
        self.wholeView.addGestureRecognizer(pan)
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        let velocity = pan.velocityInView(self.cardTableView?.superview)
        
        if fabs(velocity.y) >= fabs(velocity.x) || (cardTableView.contentOffset.x <= 0 && velocity.x > 0) {
            return true
        }
        
        return false
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if otherGestureRecognizer is UIPanGestureRecognizer {
            return true
        }
        
        return false
    }
    
    func handlePan(panGesture: UIPanGestureRecognizer) {
    
        switch panGesture.state {
        case .Began:

            var indexPath: NSIndexPath! = nil
            for cell in self.cardTableView.visibleCells() {
                indexPath = self.cardTableView.indexPathForCell(cell)
            }
            
            if (indexPath == nil) {
                return
            }
            else {
                self.deckIndex = indexPath.row
            }
            
            self.isCurrentVideo = false
            if content?.children.count != 0 {
                if content!.children[self.deckIndex].children.count > 0 {
                    if content!.children[self.deckIndex].children[self.cardIndexes[self.deckIndex]].type == .Video {
                        self.isCurrentVideo = true
                    }
                }
            }
            else {
                if (content!.children.count > 0) {
                    if content!.children[self.cardIndexes[0]].type == .Video {
                        self.isCurrentVideo = true
                    }
                }
            }
            
            
            startPos = panGesture.locationInView(self.wholeView?.superview)
            startCenter = self.wholeView.center
            print(startPos)
            let velocity = panGesture.velocityInView(self.wholeView?.superview)
            
            isRotating = fabs(velocity.x) > fabs(velocity.y)
            if (velocity.y > 0) {//down
                isDirection = true
            } else {//up
                isDirection = false
            }
            
        case .Changed:
            break
//            if !isRotating {
//                if !isDirection {
//                    if bShowControls {
//                        if self.isCurrentVideo == false { // is video
//                            
//                            let pos = panGesture.locationInView(self.wholeView?.superview)
//                            self.wholeView.center = CGPointMake(startCenter.x, startCenter.y + pos.y - startPos.y)
//                        
//                            imgBackground.alpha = 0.9 - fabs(startCenter.y + pos.y - startPos.y-self.view.bounds.size.height/2.0)/1000
//                        }
//                        
//                    }
//                    else {//hide full screen / show detail
//
//                    }
//                    
//                }
//                else { // show full screen / hide detail
//                    
//                }
//            }

        case .Ended:
            
            if !isRotating {
//                if self.isCurrentVideo { // video
                    
                    if isDirection {
                        if !bShowControls {// hide full screen / show detail
                            var visible: [AnyObject] = cardTableView.indexPathsForVisibleItems()
                            let indexpath: NSIndexPath = (visible[0] as! NSIndexPath)
                            
                            let cell = cardTableView.cellForItemAtIndexPath(indexpath) as! FullScreenCollectionViewCell
                            
                            cardTableView.scrollEnabled = true
                            cell.onHideFullScreen()
                            
                            bShowControls = true
                        }
                    }
                    else { // show full screen / hide detail
                        var visible: [AnyObject] = cardTableView.indexPathsForVisibleItems()
                        let indexpath: NSIndexPath = (visible[0] as! NSIndexPath)
                        let cell = cardTableView.cellForItemAtIndexPath(indexpath) as! FullScreenCollectionViewCell
                        
                        cardTableView.scrollEnabled = false
                        cell.onShowFullScreen()
                        bShowControls = false
                    }

//                }
//                else {
//                    if !isDirection {
//                        if bShowControls {
//                            let velocity = panGesture.velocityInView(self.wholeView?.superview)
//                            if velocity.y < 0 { // should dismiss
//                                UIView.animateWithDuration(0.2, animations: {
//                                    self.wholeView.center = CGPointMake(self.startCenter.x, -self.view.bounds.size.height * 1.5)
//                                    }, completion: { completed in
//                                        self.dismissViewControllerAnimated(false, completion: nil)
//                                })
//                            } else {
//                                UIView.animateWithDuration(0.2, animations: {
//                                    self.wholeView.center = self.startCenter
//                                    self.imgBackground.alpha = 1
//                                    }, completion: { completed in
//                                })
//                            }
//                        }
//                        else {// hide full screen / show detail
//                            var visible: [AnyObject] = cardTableView.indexPathsForVisibleItems()
//                            let indexpath: NSIndexPath = (visible[0] as! NSIndexPath)
//
//                            let cell = cardTableView.cellForItemAtIndexPath(indexpath) as! FullScreenCollectionViewCell
//                            
//                            cardTableView.scrollEnabled = true
//                            cell.onHideFullScreen()
//                            
//                            bShowControls = true
//                        }
//                    }
//                    else { // show full screen / hide detail
//                        var visible: [AnyObject] = cardTableView.indexPathsForVisibleItems()
//                        let indexpath: NSIndexPath = (visible[0] as! NSIndexPath)
//                        let cell = cardTableView.cellForItemAtIndexPath(indexpath) as! FullScreenCollectionViewCell
//                        
//                        cardTableView.scrollEnabled = false
//                        cell.onShowFullScreen()
//                        bShowControls = false
//                    }
//                }
            }
            
        default:break
        }
    }
    
}

private typealias CollectionViewDelegate = FullScreenViewController
extension CollectionViewDelegate: UICollectionViewDelegate {


}

private typealias CellDelegate = FullScreenViewController
extension CellDelegate: FullScreenCollectionViewCellDelegate {

    func moveToNextCard(gestureRecognizer: UIGestureRecognizer) {
        if content?.children.count != 0 {
            let row = gestureRecognizer.view!.tag
            let x = gestureRecognizer.locationInView(self.view).x

            if (cardIndexes[row] >= 0 && bShowControls) {
                let childCount = content!.children[row].children.count
                if (x > screenWidth / 2) {
                    if (cardIndexes[row] == childCount - 1) {
                        if (row < cardIndexes.count - 1) {
                            cardIndexes[row + 1] = 0
                            cardTableView.scrollToItemAtIndexPath(NSIndexPath(forRow: row + 1, inSection: 0), atScrollPosition: .None, animated: true)
                        }
                        return
                    }
                    else {
                        cardIndexes[row] = cardIndexes[row] + 1
                    }
                } else {
                    if (cardIndexes[row] == 0) {
                        if (row > 0) {
                            cardTableView.scrollToItemAtIndexPath(NSIndexPath(forRow: row - 1, inSection: 0), atScrollPosition: .None, animated: true)
                        }
                    }
                    else {
                        cardIndexes[row] = cardIndexes[row] - 1
                    }
                }
                cardTableView.reloadData()
            }
        }
    }

    func onFinishedPlaying(deckIndex: Int, cardIndex: Int) {
        if content?.children.count != 0 {
            moveCard(deckIndex, cardIndex: cardIndex)
        }
    }
}

private typealias CommentsDelegate = FullScreenViewController
extension CommentsDelegate: CommentsUpdateDelegate {

    func onCommentsUpdate() {
        cardTableView.reloadData()
    }
}
