//
//  DraftsViewController.swift
//  GuideHero
//
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya

class DraftsViewController: UIViewController,
UITableViewDelegate, UITableViewDataSource ,DraftTableViewCellDelegate , UIGestureRecognizerDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView : UIView!
    @IBOutlet var extraToolbar: UIToolbar!
    
    private var isInSelectionMode = false;
    
    @IBOutlet var selectButton: UIButton!
    @IBOutlet var backButton : UIButton!
    @IBOutlet var closeButton : UIButton!
    @IBOutlet var lblTitle : UILabel!
    
    @IBOutlet var footerView : UIView!
    @IBOutlet var footerBottomView : UIView!
    @IBOutlet var whoolView : UIView!
    @IBOutlet weak var imgBackground : UIImageView!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    
    var selectedRows = [Int]()
    var drafts = [ContentNode]()
    
    var pan = UIPanGestureRecognizer()
    var isRotating = false
    var startPos = CGPointZero
    var startCenter = CGPointZero
    
    // MARK: UIViewController Lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(DraftsViewController.changedTable(_:)),
                                                         name: "changedTable",
                                                         object: nil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cellNib = UINib(nibName: "DraftCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: "DraftCell")
        
        onOriginalUI()
        
        onSetupPanel()
        
        tableView.delegate = self
        tableView.dataSource = self
        updateTable()
        
        footerBottomView.layer.cornerRadius = 20
        footerBottomView.layer.shadowRadius = 5
        footerBottomView.layer.shadowOpacity = 0.5
        footerBottomView.layer.shadowOffset = CGSizeZero
        footerBottomView.layer.shadowColor = UIColor.blackColor().CGColor
        footerBottomView.clipsToBounds = true
        
        footerView.frame.size = CGSizeMake(UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height)
        
//        self.navigationItem.hidesBackButton = true
//        let myBackButton:UIButton = UIButton()
//        myBackButton.addTarget(self, action: #selector(DraftsViewController.pressedBack(_:)), forControlEvents: UIControlEvents.TouchUpInside)
//        myBackButton.setTitle("Back", forState: UIControlState.Normal)
//        myBackButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
//        myBackButton.sizeToFit()
//        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBackButton)
//        self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
//        navigationController?.navigationBar.barTintColor = UIColor.blackColor()
//        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        tableView.allowsMultipleSelection = false
        extraToolbar.hidden = true
    }
   
    override func viewWillDisappear(animated: Bool) {
        resetSelection()
//        UIApplication.sharedApplication().statusBarHidden = false
        super.viewWillDisappear(animated);
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().statusBarHidden = true
        
        updateTable()
    }

    // MARK: Content Load
    func changedTable(notification: NSNotification) {
        updateTable()
    }
    
    private func updateTable() {
        if !self.isViewLoaded() {
            return;
        }
    
        self.showProgressHUD()
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.GetCreatedCards) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't get drafts, please try again later")
                    self.hideProgressHUD()
                    return
                }
                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    self.hideProgressHUD()
                    self.drafts = []
                    for draftDict in (JSON["created"] as? [[String : AnyObject]])! {
                        self.drafts.append(ContentNode(dictionary: draftDict))
                    }
                    Helper.drafts = self.drafts
                    print("Drafts***")
                    print(self.drafts)
                    
                    if (self.drafts.count) * 216 < Int(UIScreen.mainScreen().bounds.size.height - self.headerView.frame.size.height){
                        self.footerView.frame.size = CGSizeMake(UIScreen.mainScreen().bounds.size.width, (UIScreen.mainScreen().bounds.size.height - self.headerView.frame.size.height)-CGFloat((self.drafts.count) * 216))
                    }else{
                        self.footerView.frame.size = CGSizeMake(0, 50)
                    }
                    self.tableView.reloadData()
                    self.resetSelection()
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't get drafts, please try again later")
                    self.hideProgressHUD()
                }
            case let .Failure(error):
                print("Can't get drafts: %@", error)
                if (self.drafts.count) * 216 < Int(UIScreen.mainScreen().bounds.size.height - self.headerView.frame.size.height){
                    self.footerView.frame.size = CGSizeMake(UIScreen.mainScreen().bounds.size.width, (UIScreen.mainScreen().bounds.size.height - self.headerView.frame.size.height)-CGFloat((self.drafts.count) * 216))
                }else{
                    self.footerView.frame.size = CGSizeMake(0, 50)
                }
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't get drafts, please try again later")
                self.hideProgressHUD()
            }
        }
    }
    
    // MARK: - Cell Selection
    
    private func setSelectionMode(selectionMode: Bool) {
        if isInSelectionMode == selectionMode {
            return
        }
        
        isInSelectionMode = selectionMode;
    }

    // MARK: - Button Press
    
    @IBAction func selectButtonPress(sender: AnyObject) {
        if isInSelectionMode {

        } else {
            tableView.allowsMultipleSelection = true
            setSelectionMode(true)
            lblTitle.text = "0 Selected"
            selectButton.enabled = false
            selectButton.tintColor = UIColor.clearColor()
//            navigationController?.navigationBar.barTintColor = UIColor.colorFromRGB(redValue: 251, greenValue: 45, blueValue: 85, alpha: 1)
            extraToolbar.hidden = false
            tabBarController?.tabBar.hidden = true
            
            closeButton.hidden = true
            backButton.hidden = false
            
        }
    }
    
    func resetSelection() {
        setSelectionMode(false)
        selectButton.enabled = true
        selectButton.tintColor = UIColor.whiteColor()
        lblTitle.text = "Drafts"
        if let indexPaths = tableView.indexPathsForSelectedRows {
            for indexPath in indexPaths {
                tableView.deselectRowAtIndexPath(indexPath, animated: true)
            }
        }
        tableView.allowsMultipleSelection = false
//        navigationController?.navigationBar.barTintColor = UIColor.blackColor()

        extraToolbar.hidden = true
        
        closeButton.hidden = false
        backButton.hidden = true
        
        selectedRows = []
    }
    
    
    // Custom Back button logic
    @IBAction func pressedBack(sender:UIButton){
        if isInSelectionMode {
            resetSelection()
            return
        }
//        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onPressBack(sender:AnyObject){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func clickedDeleteButton(sender: AnyObject) {
        if selectedRows.count < 1 {
            return
        }
        
        let alertController2 = UIAlertController(title: "Are you sure you want to delete these items and everything inside?", message: "You can move or save copies of items to Drafts before deleting them away. \n 🗑👋", preferredStyle: .Alert)
        let cancelAction2 = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            // ...
        }
        let okayAction2 = UIAlertAction(title: "Go Ahead", style: .Default) { (action) in
            // API call
            
            var cardIds = [String]()
            
            for index in self.selectedRows {
                cardIds.append(self.drafts[index].id)
            }
            
            self.showProgressHUD()
            let provider = MoyaProvider<NetworkService>.createDefaultProvider()
            provider.request(.DeleteCards(cardIds: cardIds)) { result in
                switch result {
                case let .Success(response):
                    if response.statusCode != 200 {
                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't delete content, please try again later")
                        self.hideProgressHUD()
                    }
                    else if let JSON = try? response.mapJSON() as! [String: String] {
                        if JSON["result"] == "success" {
                            self.showSuccessHUD()
                            self.updateTable()
                        }
                        else {
                            self.showSimpleAlert(withTitle: "Error", withMessage: "Can't delete content, please try again later")
                            self.hideProgressHUD()
                        }
                    }
                    else {
                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't delete content, please try again later")
                        self.hideProgressHUD()
                    }
                case .Failure(_):
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't delete content, please try again later")
                    self.hideProgressHUD()
                }
            }
        }
        alertController2.addAction(cancelAction2)
        alertController2.addAction(okayAction2)
        self.presentViewController(alertController2, animated: true){}
    }
    @IBAction func clickedCopy(sender: AnyObject) {
        if selectedRows.count < 1 {
            return
        }
        
        var cardIds = [String]()
        
        for index in self.selectedRows {
            cardIds.append(self.drafts[index].id)
        }
        
        self.showProgressHUD()
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.CopyCards(cardIds: cardIds)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't copy content, please try again later")
                    self.hideProgressHUD()
                }
                else if let JSON = try? response.mapJSON() as! [String: String] {
                    if JSON["result"] == "success" {
                        self.showSuccessHUD()
                        self.updateTable()
                    }
                    else {
                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't copy content, please try again later")
                        self.hideProgressHUD()
                    }
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't copy content, please try again later")
                    self.hideProgressHUD()
                }
            case .Failure(_):
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't copy content, please try again later")
                self.hideProgressHUD()
            }
        }
    }
    
    @IBAction func clickedCreateNewDeck(sender: AnyObject) {
        if selectedRows.count < 1 {
            return
        }
        
        var cardIds = [String]()
        var deckTitle = ""
        
        for index in self.selectedRows {
            let item = self.drafts[index]
            cardIds.append(item.id)
            if deckTitle.length == 0 {
                deckTitle = item.name
            }
        }
        
        // API call
        self.showProgressHUD()
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.CreateDeck(title: deckTitle, cardIds: cardIds)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't create deck, please try again later")
                    self.hideProgressHUD()
                }
                else if let JSON = try? response.mapJSON() as! [String: String] {
                    if JSON["result"] == "success" {
                        self.showSuccessHUD()
                        self.updateTable()
                    }
                    else {
                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't create deck, please try again later")
                        self.hideProgressHUD()
                    }
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't parse response, please try again later")
                    self.hideProgressHUD()
                }
            case .Failure(_):
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't create deck, please try again later")
                self.hideProgressHUD()
            }
        }
    }

    @IBAction func clickedMoveDeck(sender: AnyObject) {
        if selectedRows.count == 0 {
            return
        }
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("MoveToNav") as! MoveCardNavigationController
        let cardIds = NSMutableArray()
        for indexPath in selectedRows {
            cardIds.addObject(drafts[indexPath].id)
        }
        vc.cardIds = cardIds
//        vc.drafts = drafts
        presentViewController(vc, animated: true, completion: nil)
    }
    
    // MARK: - UITableView
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 216
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.drafts.count ?? 0
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("DraftCell", forIndexPath: indexPath) as! DraftCell
        
        cell.delegate = self
        cell.content = self.drafts[indexPath.row]
        
//        cell.showLock = false
//        if !self.drafts[indexPath.row].published {
//            cell.showLock = true
//        }
//        cell.updateLock()
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if isInSelectionMode {
            selectedRows.append(indexPath.row)
            lblTitle.text = "\(selectedRows.count) Selected"
        } else {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("EditDeckViewController") as! EditDeckViewController
            vc.content = self.drafts[indexPath.row]
//            vc.showNavOnClose = true
//            self.navigationController?.pushViewController(vc, animated: true)
            presentViewController(vc, animated: true, completion: nil)
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }

    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        if isInSelectionMode {
            selectedRows.removeObject(indexPath.row)
            lblTitle.text = "\(selectedRows.count) Selected"
        }
    }
    
    
    func onUIUpdate() {
        updateTable()
    }
    
    
    
    func onSetupPanel(){
        
        pan.addTarget(self, action: #selector(PointViewController.handlePan(_:)))
        pan.delegate = self
        whoolView.addGestureRecognizer(pan)
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        let velocity = pan.velocityInView(self.tableView?.superview)
        
        if fabs(velocity.x) >= fabs(velocity.y) || (tableView.contentOffset.y <= 0 && velocity.y > 0) {
            return true
        }
        
        return false
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if otherGestureRecognizer is UIPanGestureRecognizer {
            return true
        }
        
        return false
    }

    func handlePan(panGesture: UIPanGestureRecognizer){
        
        let factor: CGFloat = 2
        let angleLimit = CGFloat(M_PI_4)
        
        switch panGesture.state {
        case .Began:
            
            startPos = panGesture.locationInView(self.whoolView.superview)
            startCenter = self.whoolView.center
            print(startPos)
            let velocity = panGesture.velocityInView(self.whoolView.superview)
            
            isRotating = fabs(velocity.x) > fabs(velocity.y)
            
        case .Changed:
            
            if isRotating {
                
                let pos = panGesture.locationInView(self.view?.superview)
                
                let rotationAngle = atan((pos.x - startPos.x) / (self.view.bounds.size.height * factor - pos.y))
                var direction:CGFloat = 1
                if pos.x != startPos.x {
                    direction = (startPos.x - pos.x) / fabs(pos.x - startPos.x)
                }
                imgBackground.alpha = 0.5 - fabs(rotationAngle)
                
                var transform = CGAffineTransformMakeTranslation(self.view.bounds.size.width * 0.5 * direction, self.view.bounds.size.height * (factor - 0.5))
                transform = CGAffineTransformRotate(transform, rotationAngle)
                transform = CGAffineTransformTranslate(transform, -self.view.bounds.size.width * 0.5 * direction, -self.view.bounds.size.height * (factor - 0.5))
                self.whoolView.transform = transform;
                
            } else {
                let pos = panGesture.locationInView(self.view?.superview)
                self.whoolView.center = CGPointMake(startCenter.x, startCenter.y + pos.y - startPos.y)
            }
            
        case .Ended:
            
            if isRotating {
                let pos = panGesture.locationInView(self.whoolView?.superview)
                let velocity = panGesture.velocityInView(self.whoolView?.superview)
                var startDirection = 1
                if pos.x != startPos.x {
                    startDirection = Int(round((pos.x - startPos.x) / fabs(pos.x - startPos.x)))
                }
                var velocityDirection = -startDirection
                if velocity.x != 0 {
                    velocityDirection = Int(round(velocity.x / fabs(velocity.x)))
                }
                if startDirection == velocityDirection { // should dismiss
                    UIView.animateWithDuration(0.3, animations: {
                        
                        var transform = CGAffineTransformMakeTranslation(-self.whoolView.bounds.size.width * 0.5 * CGFloat(startDirection), self.whoolView.bounds.size.height * (factor - 0.5))
                        transform = CGAffineTransformRotate(transform, angleLimit * CGFloat(startDirection))
                        transform = CGAffineTransformTranslate(transform, self.whoolView.bounds.size.width * 0.5 * CGFloat(startDirection), -self.whoolView.bounds.size.height * (factor - 0.5))
                        
                        self.whoolView.transform = transform
                        self.imgBackground.alpha = 0
                        
                        }, completion: { completed in
                            self.dismissViewControllerAnimated(false, completion: nil)
                    })
                } else {
                    UIView.animateWithDuration(0.2, animations: {
                        self.whoolView.transform = CGAffineTransformIdentity
                        self.imgBackground.alpha = 0.5
                        }, completion: { completed in
                    })
                }
                
            } else {
                let velocity = panGesture.velocityInView(self.whoolView?.superview)
                if velocity.y > 0 { // should dismiss
                    UIView.animateWithDuration(0.2, animations: {
                        self.whoolView.center = CGPointMake(self.startCenter.x, self.whoolView.bounds.size.height * 1.5)
                        self.imgBackground.alpha = 0
                        }, completion: { completed in
                            self.dismissViewControllerAnimated(false, completion: nil)
                    })
                } else {
                    UIView.animateWithDuration(0.2, animations: {
                        self.whoolView.center = self.startCenter
                        }, completion: { completed in
                    })
                }
            }
            
            
        default:
            break
        }
    }
    
    @IBAction func onMore(sender:AnyObject){
        
    }

    func onOriginalUI(){
        backButton.hidden = true
        closeButton.hidden = false
        lblTitle.text = "Draft"
    }
}
