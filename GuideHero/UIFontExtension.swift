//
//  UIFontExtension.swift
//  GuideHero
//
//  Created by Justin Holman on 1/27/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {

    enum AppFont: String {
        case standard = "ProximaNova-Regular"
        case bold = "ProximaNova-Bold"
        case semiBold = "ProximaNova-Semibold"
    }

    convenience init!(appFont: AppFont, size: CGFloat = UIFont.systemFontSize()) {
        self.init(name: appFont.rawValue, size: size)
    }
}
