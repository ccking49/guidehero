//
//  DraggableCardView.swift
//  GuideHero
//
//  Created by Promising Change on 12/01/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit
import pop

protocol DraggableCardViewDelegate: class {
    func cardDidSwipeAway()
    func cardWasDragged(withPercentage: CGFloat)
    func cardDidStartDismiss(duration: NSTimeInterval, percentage: Float)
    func resetCardPosition()
}

public class DraggableCardView: UIView {
    
    // MARK: - Variables
    
    weak var delegate: DraggableCardViewDelegate?
    
    var dragBegin = false
    var dragDistance = CGPoint.zero
    var animationDirectionY: CGFloat = 1.0
    var swipePercentageMargin: CGFloat = 1.0
    var firstTouchPoint = CGPoint.zero
    
    //Drag animation constants
    private let rotationMax: CGFloat = 1.0
    private let defaultRotationAngle = CGFloat(M_PI) / 10.0
    private let scaleMin: CGFloat = 0.8
    public let cardSwipeActionAnimationDuration: NSTimeInterval  = 0.4
    
    //Reset animation constants
    private let cardResetAnimationSpringBounciness: CGFloat = 10.0
    private let cardResetAnimationSpringSpeed: CGFloat = 20.0
    private let cardResetAnimationKey = "resetPositionAnimation"
    private let cardResetAnimationDuration: NSTimeInterval = 0.2
    
    private let screenSize = UIScreen.mainScreen().bounds.size
    

    // MARK: - View Lifecycle
    
    // MARK: - Handle Pan Gesture
    
    func handlePanGesture(panGesture: UIPanGestureRecognizer) {
        
        dragDistance = panGesture.translationInView(self)
        
        let touchLocation = panGesture.locationInView(self)
        
        switch panGesture.state {
        case .Began:
            
            firstTouchPoint = panGesture.locationInView(self)
            let newAnchorPoint = CGPoint(x: firstTouchPoint.x / bounds.width, y: firstTouchPoint.y / bounds.height)
            let oldPosition = CGPoint(x: bounds.size.width * layer.anchorPoint.x, y: bounds.size.height * layer.anchorPoint.y)
            let newPosition = CGPoint(x: bounds.size.width * newAnchorPoint.x, y: bounds.size.height * newAnchorPoint.y)
            layer.anchorPoint = newAnchorPoint
            layer.position = CGPoint(x: layer.position.x - oldPosition.x + newPosition.x, y: layer.position.y - oldPosition.y + newPosition.y)
            removeAnimations()
            
            dragBegin = true
            
            animationDirectionY = touchLocation.y >= bounds.size.height / 2 ? -1.0 : 1.0
            layer.rasterizationScale = UIScreen.mainScreen().scale
            layer.shouldRasterize = true
            
        case .Changed:
            let rotationStrength = min(dragDistance.x / bounds.width, rotationMax)
            let rotationAngle = animationDirectionY * defaultRotationAngle * rotationStrength
            let scaleStrength = 1 - ((1 - scaleMin) * fabs(rotationStrength))
            let scale = max(scaleStrength, scaleMin)
            
            var transform = CATransform3DIdentity
            transform = CATransform3DScale(transform, scale, scale, 1)
            transform = CATransform3DRotate(transform, rotationAngle, 0, 0, 1)
            transform = CATransform3DTranslate(transform, dragDistance.x, dragDistance.y, 0)
            layer.transform = transform
            
            delegate?.cardWasDragged(dragPercentage)
            
        case .Ended:
            let pos = panGesture.locationInView(self.superview)
            let velocity = panGesture.velocityInView(self)
            var startDirection = 1
            
            let direction = dragDirection!
            
            if ([CardSwipeResultDirection.left, CardSwipeResultDirection.right].contains(direction)) {
                if pos.x != firstTouchPoint.x {
                    startDirection = Int(round((pos.x - firstTouchPoint.x) / fabs(pos.x - firstTouchPoint.x)))
                }
                
                var velocityDirection = -startDirection
                if velocity.x != 0 {
                    velocityDirection = Int(round(velocity.x / fabs(velocity.x)))
                }
                
                if startDirection == velocityDirection {
                    delegate?.cardDidStartDismiss(cardSwipeActionAnimationDuration, percentage: Float(dragPercentage))
                    swipeAction(dragDirection!)
                } else {
                    resetViewPositionAndTransformations()
                }
            } else if ([CardSwipeResultDirection.up, CardSwipeResultDirection.down].contains(direction)) {
                if pos.y != firstTouchPoint.y {
                    startDirection = Int(round((pos.y - firstTouchPoint.y) / fabs(pos.y - firstTouchPoint.y)))
                }
                
                var velocityDirection = -startDirection
                if velocity.y != 0 {
                    velocityDirection = Int(round(velocity.y / fabs(velocity.y)))
                }
                
                if startDirection == velocityDirection {
                    delegate?.cardDidStartDismiss(cardSwipeActionAnimationDuration, percentage: Float(dragPercentage))
                    swipeAction(dragDirection!)
                } else {
                    resetViewPositionAndTransformations()
                }
            }
            
            layer.shouldRasterize = false
            
        default:
            layer.shouldRasterize = false
            resetViewPositionAndTransformations()
        }
    }
    
    // MARK: - Private
    
    private func resetViewPositionAndTransformations() {
        
        removeAnimations()
        
        let resetPositionAnimation = POPSpringAnimation(propertyNamed: kPOPLayerTranslationXY)
        resetPositionAnimation?.fromValue = NSValue(CGPoint:POPLayerGetTranslationXY(layer))
        resetPositionAnimation?.toValue = NSValue(CGPoint: CGPoint.zero)
        resetPositionAnimation?.springBounciness = cardResetAnimationSpringBounciness
        resetPositionAnimation?.springSpeed = cardResetAnimationSpringSpeed
        resetPositionAnimation?.completionBlock = {
            (_, _) in
            self.layer.transform = CATransform3DIdentity
            self.dragBegin = false
        }
        
        layer.pop_addAnimation(resetPositionAnimation, forKey: "resetPositionAnimation")
        
        let resetRotationAnimation = POPBasicAnimation(propertyNamed: kPOPLayerRotation)
        resetRotationAnimation?.fromValue = POPLayerGetRotationZ(layer)
        resetRotationAnimation?.toValue = CGFloat(0.0)
        resetRotationAnimation?.duration = cardResetAnimationDuration
        
        layer.pop_addAnimation(resetRotationAnimation, forKey: "resetRotationAnimation")
        
        let resetScaleAnimation = POPBasicAnimation(propertyNamed: kPOPLayerScaleXY)
        resetScaleAnimation?.toValue = NSValue(CGPoint: CGPoint(x: 1.0, y: 1.0))
        resetScaleAnimation?.duration = cardResetAnimationDuration
        layer.pop_addAnimation(resetScaleAnimation, forKey: "resetScaleAnimation")
        
        self.delegate?.resetCardPosition()
    }
    
    private func removeAnimations() {
        pop_removeAllAnimations()
        layer.pop_removeAllAnimations()
    }
    
    private var directions: [CardSwipeResultDirection] {
        return [.left, .right, .up, .down]
    }
    
    private var dragDirection: CardSwipeResultDirection? {
        //find closest direction
        let normalizedDragPoint = dragDistance.normalizedDistanceForSize(bounds.size)
        return directions.reduce((distance:CGFloat.infinity, direction:nil)) { closest, direction in
            let distance = direction.point.distanceTo(normalizedDragPoint)
            if distance < closest.distance {
                return (distance, direction)
            }
            return closest
            }.direction
    }
    
    private var dragPercentage: CGFloat {
        guard let dragDirection = dragDirection else { return 0 }
        // normalize dragDistance then convert project closesest direction vector
        let normalizedDragPoint = dragDistance.normalizedDistanceForSize(bounds.size)
        let swipePoint = normalizedDragPoint.scalarProjectionPointWith(dragDirection.point)
        
        // rect to represent bounds of card in normalized coordinate system
        let rect = CardSwipeResultDirection.boundsRect
        
        // if point is outside rect, percentage of swipe in direction is over 100%
        if !rect.contains(swipePoint) {
            return 1.0
        } else {
            let centerDistance = swipePoint.distanceTo(.zero)
            let targetLine = (swipePoint, CGPoint.zero)
            
            // check 4 borders for intersection with line between touchpoint and center of card
            // return smallest percentage of distance to edge point or 0
            return rect.perimeterLines
                .flatMap { CGPoint.intersectionBetweenLines(targetLine, line2: $0) }
                .map { centerDistance / $0.distanceTo(.zero) }
                .minElement() ?? 0
        }
    }
    
    private func swipeMadeAction() {
        
        if let dragDirection = dragDirection  where dragPercentage >= swipePercentageMargin && directions.contains(dragDirection) {
            swipeAction(dragDirection)
        } else {
            resetViewPositionAndTransformations()
        }
    }
    
    private func swipeAction(direction: CardSwipeResultDirection) {
        let translationAnimation = POPBasicAnimation(propertyNamed: kPOPLayerTranslationXY)
        translationAnimation?.duration = cardSwipeActionAnimationDuration
        translationAnimation?.fromValue = NSValue(CGPoint: POPLayerGetTranslationXY(layer))
        translationAnimation?.toValue = NSValue(CGPoint: animationPointForDirection(direction))
        translationAnimation?.completionBlock = { _, _ in
            self.delegate?.cardDidSwipeAway()
        }
        layer.pop_addAnimation(translationAnimation, forKey: "swipeTranslationAnimation")
    }
    
    private func animationPointForDirection(direction: CardSwipeResultDirection) -> CGPoint {
        let point = direction.point
        let animatePoint = CGPoint(x: point.x * 4, y: point.y * 4) //should be 2
        let retPoint = animatePoint.screenPointForSize(screenSize)
        return retPoint
    }

}
