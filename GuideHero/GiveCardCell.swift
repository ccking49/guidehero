//
//  GiveCardCell.swift
//  GuideHero
//
//  Created by Promising Change on 04/01/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit

class GiveCardCell: BaseTableViewCell, UITableViewDelegate, UITableViewDataSource  {

    var content: ContentNode! {
        didSet {
            updateUI()
        }
    }
    var disabled: Bool? {
        didSet {
            contentView.bringSubviewToFront(disableView)
            disableView.hidden = !disabled!
        }
    }
    
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var giveTableView: UITableView!
    @IBOutlet var giversTableView: UITableView!
    @IBOutlet var givePlaceholderHeight: NSLayoutConstraint!
    @IBOutlet var giversPlaceholderHeight: NSLayoutConstraint!
    @IBOutlet var giveTableHeight: NSLayoutConstraint!
    @IBOutlet var giversTableHeight: NSLayoutConstraint!
    @IBOutlet var disableView: UIView!
    
    
    var onSubmit: (() -> Void)!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        submitButton.roundView()
        giveTableView.delegate = self
        giveTableView.dataSource = self
        giversTableView.delegate = self
        giversTableView.dataSource = self
        
        let cellNib = UINib(nibName: "ContentTableCell", bundle: nil)
        giveTableView.registerNib(cellNib, forCellReuseIdentifier: "ContentTableCell")
        let basicCell = UINib(nibName: "UserBasicCell", bundle: nil)
        giversTableView.registerNib(basicCell, forCellReuseIdentifier: "UserBasicCell")
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUI () {
        let isGiver = content.isGiver
        submitButton.enabled = true
        if isGiver == 2 {
            submitButton.setTitle("Cancel Submission", forState: .Normal)
            //            submitButton.enabled = false
        } else if isGiver == 1 {
            submitButton.setTitle("Cancel Submission", forState: .Normal)
        } else {
            submitButton.setTitle("Submit", forState: .Normal)
        }
        giveTableView.reloadData()
        giversTableView.reloadData()
    }
    
    @IBAction func onSubmit(sender: UIButton) {
        onSubmit()
    }
    
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if content == nil {
            return 0
        }
        if tableView.isEqual(giveTableView) {
            let count = content!.myGives.count
            givePlaceholderHeight.constant = count == 0 ? 30 : 0
            //            giveTableHeight
            return count
        } else {
            let count = content!.givers.count
            giversPlaceholderHeight.constant = count == 0 ? 30 : 0
            return count
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView.isEqual(giveTableView) {
            return Constants.UIConstants.contentCellHeight
        } else {
            return 44
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView.isEqual(giveTableView) {
            let cell = tableView.dequeueReusableCellWithIdentifier("ContentTableCell", forIndexPath: indexPath) as! ContentTableCell
            cell.content = self.content!.myGives[indexPath.row]
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("UserBasicCell") as! UserBasicCell
            let user = content.givers[indexPath.row]
            cell.populateWithUser(user)
            return cell
        }
    }
}
