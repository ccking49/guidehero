//
//  AskEditCell.swift
//  GuideHero
//
//  Created by HC on 11/15/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya
import KMPlaceholderTextView

protocol DraftTableViewCellDelegate {
    func onUIUpdate()
}

class DraftCell: BaseTableViewCell, UITextViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPostDate: UILabel!
    @IBOutlet weak var lblUserBio: UILabel!
    @IBOutlet weak var cardView: SubmissionContainerView!
    @IBOutlet weak var lblContentTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblLikeCount: UILabel!
    
    @IBOutlet weak var btnLike: UIButton!

    @IBOutlet weak var imgLock: UIImageView!
    
    @IBOutlet weak var separateView : UIView!
    
    var delegate: DraftTableViewCellDelegate?
    
    var showLock = false
    
    var content: ContentNode! {
        didSet {
            updateUI()
        }
    }
    func updateLock() {
        if self.showLock {
            imgLock.hidden = false
        } else {
            imgLock.hidden = true
        }
    }
    
    private func updateUI() {
        
        self.lblContentTitle.text = self.content.name
        self.lblDescription.text = self.content.description
        self.lblLikeCount.text = String(self.content.likes)
        self.lblFullName.text = String(format: "%@ %@", (self.content.creatorInfo?.first_name)!, (self.content.creatorInfo?.last_name)!)
        self.lblUserBio.text = self.content.creatorInfo?.bio
        self.lblUserName.text = self.content.creatorInfo?.username
        self.lblPostDate.text = self.content.formattedCreationDate
        
        if self.content.ask_enabled == true {
            self.imgLock.hidden = true
        } else {
            self.imgLock.hidden = false
        }
        
//        cardView.layer.shadowRadius = 1
//        cardView.layer.shadowOffset = CGSizeZero
//        cardView.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
        
        cardView.content = content
        
        if let userProfileImageURL = self.content.creatorThumbnailURL {
            self.userProfileImage.af_setImageWithURL(userProfileImageURL)
        } else {
            self.userProfileImage.image = UIImage(named: "me")
        }
        
        let imageName = content.liked_by_me ? "heartFilled" : "heart"
        btnLike.setImage(UIImage(named: imageName), forState: .Normal)
        lblLikeCount.text = String(content.likes)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.layer.shadowRadius = 1
        self.layer.shadowOffset = CGSizeZero
        self.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
        
        
        userProfileImage.layer.cornerRadius = userProfileImage.frame.size.width / 2
        userProfileImage.layer.masksToBounds = true
        
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: - @IBAction
    @IBAction func onLike(sender: AnyObject) {
        let liked = self.content!.liked_by_me
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        let target = liked ? NetworkService.UnlikeCard(cardId : self.content!.id) : NetworkService.LikeCard(cardId : self.content!.id)
        
        provider.request(target) { result in
            switch result {
            case let .Success(response):
                print(response)
                if response.statusCode == 200 {
                    self.content!.liked_by_me = !liked
                    self.content!.likes += liked ? -1 : 1
                    let imageName = self.content.liked_by_me ? "heartFilled" : "heart"
                    self.btnLike.setImage(UIImage(named: imageName), forState: .Normal)
                    self.lblLikeCount.text = String(self.content.likes)
                    if (self.delegate != nil) {
                        self.delegate!.onUIUpdate()
                    }
                }
            case let .Failure(error):
                print(error)
            }
        }
    }
    
}
