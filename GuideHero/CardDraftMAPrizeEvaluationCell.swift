//
//  CardDraftMAPrizeEvaluationCell.swift
//  GuideHero
//
//  Created by Promising Change on 02/02/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit

class CardDraftMAPrizeEvaluationCell: UITableViewCell {

    // MARK: - @IBOutlet Variables
    
    @IBOutlet weak var tfStartDate: UITextField!
    @IBOutlet weak var tfEndDate: UITextField!
    
    // MARK: - Variables
    
    var editingTextField: UITextField?
    var editingValue: String?
    
    let startDatePicker = UIDatePicker()
    let endDatePicker = UIDatePicker()
    
    // MARK: - Properties
    
    var evaluationStartDate: String? {
        get {
            let dateString = startDatePicker.date.stringWithFormatInGMT(Constants.Literals.apiDateFormat)
            return dateString
        }
    }
    
    var evaluationEndDate: String? {
        get {
            let dateString = endDatePicker.date.stringWithFormatInGMT(Constants.Literals.apiDateFormat)
            return dateString
        }
    }
    
    // MARK: - View Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Private Methods
    
    func configureView() {
        startDatePicker.addTarget(self, action: #selector(datePickerChanged(_:)), forControlEvents: .ValueChanged)
        startDatePicker.datePickerMode = .DateAndTime
        tfStartDate.inputView = startDatePicker
        tfStartDate.text = NSDate().stringWithFormat(Constants.Literals.localDateFormat)
        tfStartDate.delegate = self
        
        endDatePicker.addTarget(self, action: #selector(datePickerChanged(_:)), forControlEvents: .ValueChanged)
        endDatePicker.datePickerMode = .DateAndTime
        
        tfEndDate.inputView = endDatePicker
        let endDate = NSDate().add(nil, months: nil, weeks: nil, days: nil, hours: 12, minutes: nil, seconds: nil, nanoseconds: nil)
        endDatePicker.date = endDate
        tfEndDate.text = endDate.stringWithFormat(Constants.Literals.localDateFormat)
        tfEndDate.delegate = self
        
        attachToolBar(tfStartDate)
        attachToolBar(tfEndDate)
    }
    
    func datePickerChanged (datePicker: UIDatePicker) {
        if datePicker == startDatePicker {
            tfStartDate.text = datePicker.date.stringWithFormat(Constants.Literals.localDateFormat)
        } else {
            tfEndDate.text = datePicker.date.stringWithFormat(Constants.Literals.localDateFormat)
        }
    }
    func attachToolBar (tField: UITextField) {
        let numberToolbar = UIToolbar(frame: CGRectMake(0, 0, contentView.frame.size.width, 50))
        numberToolbar.barStyle = UIBarStyle.Default
        numberToolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .Plain, target: self, action: #selector(CardDraftMAPrizeEvaluationCell.cancelEdit)),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done", style: .Done, target: self, action: #selector(CardDraftMAPrizeEvaluationCell.doneEdit))]
        numberToolbar.sizeToFit()
        tField.inputAccessoryView = numberToolbar
    }
    
    func cancelEdit () {
        contentView.endEditing(true)
        editingTextField!.text = editingValue
    }
    
    func doneEdit () {
        contentView.endEditing(true)
    }

}

// MARK: - UITextFieldDelegate

extension CardDraftMAPrizeEvaluationCell: UITextFieldDelegate {
    func textFieldDidBeginEditing(textField: UITextField) {
        editingTextField = textField
        editingValue = textField.text
        
        if (textField == tfStartDate) {
            // Set the Maximum Date
            let endDateString = tfEndDate.text
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = Constants.Literals.localDateFormat
            let endDate = dateFormatter.dateFromString(endDateString!)
            startDatePicker.maximumDate = endDate
        } else if (textField == tfEndDate) {
           // Set the Minimum Date
            let startDateString = tfStartDate.text
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = Constants.Literals.localDateFormat
            let startDate = dateFormatter.dateFromString(startDateString!)
            endDatePicker.minimumDate = startDate
        }
    }
}
