//
//  CardViewPagerControllerError.swift
//  GuideHero
//
//  Created by Promising Change on 03/01/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import Foundation

public enum CardViewPagerControllerError: ErrorType {
    case viewControllerNotContainedInViewPager
}
