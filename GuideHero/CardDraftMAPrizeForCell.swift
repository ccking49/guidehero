//
//  CardDraftMAPrizeForCell.swift
//  GuideHero
//
//  Created by Promising Change on 02/02/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit

public enum PrizeForWhom {
    case Top10
    case Top5
    case Top3
}

class CardDraftMAPrizeForCell: UITableViewCell {

    // MARK: - @IBOutlet Variables
    
    @IBOutlet weak var btTop10: UIButton!
    @IBOutlet weak var btTop5: UIButton!
    @IBOutlet weak var btTop3: UIButton!
    
    // MARK: - Variables
    
    var prizeFor: String = "Top10"
    
    // MARK: - Properties
    
    var prizeForWhom: PrizeForWhom? {
        get {
            switch prizeFor {
            case "Top10":
                return PrizeForWhom.Top10
            case "Top5":
                return PrizeForWhom.Top5
            case "Top3":
                return PrizeForWhom.Top3
            default:
                return PrizeForWhom.Top10
            }
        }
    }
    
    // MARK: - View Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Private Methods
    
    func configureView() {
        resetButtons()
        
        prizeFor = "Top10"
        btTop10.setImage(UIImage(named: "selectOn"), forState: .Normal)
    }
    
    func resetButtons() {
        btTop10.setImage(UIImage(named: "selectOff"), forState: .Normal)
        btTop5.setImage(UIImage(named: "selectOff"), forState: .Normal)
        btTop3.setImage(UIImage(named: "selectOff"), forState: .Normal)
    }
    
    // MARK: - Actions
    
    @IBAction func didTapOnTop10(sender: AnyObject) {
        if (prizeFor == "Top10") {
            return
        }
        
        resetButtons()
        prizeFor = "Top10"
        btTop10.setImage(UIImage(named: "selectOn"), forState: .Normal)
    }
    
    @IBAction func didTapOnTop5(sender: AnyObject) {
        if (prizeFor == "Top5") {
            return
        }
        
        resetButtons()
        prizeFor = "Top5"
        btTop5.setImage(UIImage(named: "selectOn"), forState: .Normal)
    }
    
    @IBAction func didTapOnTop3(sender: AnyObject) {
        if (prizeFor == "Top3") {
            return
        }
        
        resetButtons()
        prizeFor = "Top3"
        btTop3.setImage(UIImage(named: "selectOn"), forState: .Normal)
    }

}
