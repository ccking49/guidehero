//
//  BaseController.swift
//  GuideHero
//
//  Created by Yohei Oka on 8/25/16.
//  Copyright © 2016 Twilio. All rights reserved.
//

import UIKit

class  BaseController: UIViewController, UITextFieldDelegate , UITabBarControllerDelegate {
    
    let prefs = NSUserDefaults.standardUserDefaults()
    
    func dispatchAlert(title: String, message: String) {
        dispatch_async(dispatch_get_main_queue(),{
            self.displayAlert(title, message: message)
        })
    }
    
    func dispatchAlertWithAlertAction(title: String, message: String, alertActions: [UIAlertAction]) {
        dispatch_async(dispatch_get_main_queue(),{
            self.displayAlertWithAlertAction(title, message: message, alertActions: alertActions)
        })
    }
    
    func dispatchStopActivityIndicatory(activityIndicator: UIActivityIndicatorView) {
        dispatch_async(dispatch_get_main_queue(),{
            activityIndicator.stopAnimating()
        })
    }
    
    func displayAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func displayAlertWithAlertAction(title: String, message: String, alertActions: [UIAlertAction]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        for alertAction in alertActions {
            alert.addAction(alertAction)
        }
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func dismissVC() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
