//
//  UIViewExtensions.swift
//  GuideHero
//
//  Created by Abhishek Sengar on 10/09/2016.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import QuartzCore

extension UIView {

    // MARK: - Loading from NIB
    
    class func nib() -> UINib {
        return UINib(nibName: className(), bundle: nil)
    }
    
    class func loadFromNib() -> UIView? {
        return nib().instantiateWithOwner(nil, options: nil)[0] as? UIView
    }

    // MARK: - Snapshot

    func snapshotImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0.0)
        self.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }

    // MARK: - Autolayout

    func centerInSuperview() {
        if let superview = self.superview {
            let constraints = [
                NSLayoutConstraint(
                    item: self,
                    attribute: .CenterX,
                    relatedBy: .Equal,
                    toItem: superview,
                    attribute: .CenterX,
                    multiplier: 1,
                    constant: 0),
                NSLayoutConstraint(
                    item: self,
                    attribute: .CenterY,
                    relatedBy: .Equal,
                    toItem: superview,
                    attribute: .CenterY,
                    multiplier: 1,
                    constant: 0)
            ]
            superview.addConstraints(constraints)
        }
    }

    func matchSuperviewSize() {
        if let superview = self.superview {
            let constraints = [
                NSLayoutConstraint(
                    item: self,
                    attribute: .Width,
                    relatedBy: .Equal,
                    toItem: superview,
                    attribute: .Width,
                    multiplier: 1,
                    constant: 0),
                NSLayoutConstraint(
                    item: self,
                    attribute: .Height,
                    relatedBy: .Equal,
                    toItem: superview,
                    attribute: .Height,
                    multiplier: 1,
                    constant: 0)
            ]
            superview.addConstraints(constraints)
        }
    }

    func fillSuperview() {
        self.matchSuperviewSize()
        self.centerInSuperview()
    }
    
    func roundView() {
        let width = CGRectGetWidth(self.bounds)
        let height = CGRectGetHeight(self.bounds)
        let minValue = min(width, height)
        self.layer.cornerRadius = minValue / 2
        self.layer.masksToBounds = true
    }
    
    func roundTopCorners (radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height), byRoundingCorners: ([.TopLeft, .TopRight]), cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height)
        maskLayer.path = maskPath.CGPath
        layer.mask = maskLayer
        
        /*
        let maskPath = [UIBezierPath bezierPathWithRoundedRect:imageView.bounds
            byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight
            cornerRadii:CGSizeMake(10.0, 10.0)];
        // Create the shape layer and set its path
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = imageView.bounds;
        maskLayer.path = maskPath.CGPath;
        // Set the newly created shape layer as the mask for the image view's layer
        imageView.layer.mask = maskLayer;
 */
    }
    
    func roundBottomCorners (radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height), byRoundingCorners: ([.BottomLeft, .BottomRight]), cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height)
        maskLayer.path = maskPath.CGPath
        layer.mask = maskLayer
    }
}
