//
//  MoyaProviderExtensions.swift
//  GuideHero
//
//  Created by Ignacio Rodrigo on 9/17/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import Moya

extension MoyaProvider {

    class func createDefaultProvider() -> MoyaProvider<NetworkService> {

        let endPointClosure = { (target: NetworkService) -> Endpoint<NetworkService> in

            let url = target.baseURL.URLByAppendingPathComponent(target.path)!.absoluteString

            print(url)
            print(target.parameters)
            var headers = ["Accept": "application/json"]
            if let authToken = NSUserDefaults.standardUserDefaults().stringForKey("guidehero.authtoken") {
                headers["Authentication-Token"] = authToken
                print("Token:" + authToken)
            }

            return Endpoint(URL: url!,
                            sampleResponseClosure: {.NetworkResponse(200, target.sampleData)},
                            method: target.method,
                            parameters: target.parameters,
                            parameterEncoding: .JSON,
                            httpHeaderFields: headers)
        }

        return MoyaProvider<NetworkService>(endpointClosure: endPointClosure)
    }
}
