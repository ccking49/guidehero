//
//  CardDraftMAPrizePrizeCell.swift
//  GuideHero
//
//  Created by Promising Change on 02/02/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit

class CardDraftMAPrizePrizeCell: UITableViewCell {
    
    // MARK: - @IBOutlet Variables
    
    @IBOutlet weak var lbPoint: UILabel!
    
    // MARK: - View Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Private Methods
    
    func configureView() {
        
    }
    
    // MARK: - Public Methods
    
    func bindPrizePoint(point: Int) {
        lbPoint.text = String(point)
    }

}
