//
//  TextToSpeechManager.swift
//  GuideHero
//
//  Created by Ignacio Rodrigo on 9/14/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import AVFoundation

class TextToSpeechManager {

    static let sharedInstance = TextToSpeechManager()

    private let synth = AVSpeechSynthesizer()

    func textToSpeech(text: String, language: String) {
        let utterance = AVSpeechUtterance(string: text)
        utterance.voice = AVSpeechSynthesisVoice(language: language)
        self.synth.speakUtterance(utterance)
    }

}
