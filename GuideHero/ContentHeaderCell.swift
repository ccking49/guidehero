//
//  ContentHeaderCell.swift
//  GuideHero
//
//  Created by HC on 11/8/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya

public enum HeaderCellAction : Int {
    case Loop
    case Comment
    case Like
    case Play = 100
    case Train = 102
    case Ask = 104
    case Give = 106
    case Prize = 108
}


class ContentHeaderCell: BaseTableViewCell {

    @IBOutlet var backgroundImage: MaskImageView!
    @IBOutlet var creatorImage: UIImageView!
    @IBOutlet var creatorNameAndDate: UILabel!
    @IBOutlet var cardView: CardContainerView!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var loopButton: UIButton!
    
    @IBOutlet var likeButton: UIButton!
    @IBOutlet var likesLabel: UILabel!
    @IBOutlet var commentButton: UIButton!
    @IBOutlet var commentsLabel: UILabel!
    
    
    @IBOutlet var modeViewHeight: NSLayoutConstraint!
    @IBOutlet var modeButtons: [UIButton]!
    @IBOutlet var modeBars: [UILabel]!
    @IBOutlet var modeLabels: [UILabel]!
    
    @IBOutlet var prizeView: UIView!
    @IBOutlet var trainView: UIView!
    @IBOutlet var askView: UIView!
    @IBOutlet var giveView: UIView!
    
    @IBOutlet var playButton: UIButton!
    @IBOutlet var askButton: UIButton!
    @IBOutlet var giveButton: UIButton!
    @IBOutlet var trainButton: UIButton!
    @IBOutlet var prizeButton: UIButton!
    
    @IBOutlet var askSwitch: UISwitch!
    @IBOutlet var trainSwitch: UISwitch!
    
    
    var onAction: ((HeaderCellAction) -> Void)!
    var currentMode: HeaderCellAction = .Play
    var content: ContentNode? {
        didSet {
            updateHeader()
        }
    }
    
    var showLock = false {
        didSet {
            updateLock()
        }
    }
    
    var isDraftEditing: Bool = false {
        didSet {
            modeViewHeight.constant = isDraftEditing ? 150 : 110
            askSwitch.hidden = !isDraftEditing
            trainSwitch.hidden = !isDraftEditing
            trainView.hidden = !isDraftEditing
            
            let isAskEnabled = content?.ask_enabled ?? false
            giveView.hidden = !(isDraftEditing || isAskEnabled)
            askView.hidden = !(isDraftEditing || isAskEnabled)
            prizeView.hidden = !(isDraftEditing || isAskEnabled)
            updateCellsUserInteraction()
        }
    }
    var askEnabled: Bool {
        get {
            return askSwitch.on
        }
    }
    
    var askEditCell:AskEditCell? {
        didSet {
//            askEditCell?.userInteractionEnabled = askSwitch.on && isDraftEditing
            updateCellsUserInteraction()
        }
    }
    
    var prizeEditCell:PrizeEditCell? {
        didSet {
//            prizeEditCell?.userInteractionEnabled = askSwitch.on && isDraftEditing
            updateCellsUserInteraction()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layoutIfNeeded()
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        roundTopCorners(Constants.UIConstants.detailCardCornerRadius)
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
//        updateUI()
    }
    
    // MARK: - Function
    
    private func updateHeader() {
        cardView.isInList = false
        cardView.content = content
        backgroundImage.setImageWithUrl(content!.imageURL, rect: content!.maskRect)
        self.descriptionLabel.text = self.content?.description
        
        if let creatorImageURL = content?.creatorThumbnailURL {
            self.creatorImage.af_setImageWithURL(creatorImageURL)
        }
        self.creatorNameAndDate.text = content?.formattedCreatorAndDate
        self.creatorNameAndDate.layer.shadowRadius = 0.5
        self.creatorImage.layer.cornerRadius = 10
        
        
        updateUI()
    }
    
    func updateUI() {
        let likes = content?.likes ?? 0
        likesLabel.text = String(likes)
        let imageName = (content?.liked_by_me == false) ? "heart_s" : "heart_sFilled"
        likeButton.setImage(UIImage(named: imageName), forState: .Normal)
        let comments = content?.comments.count ?? 0
        commentsLabel.text = String(comments)
        
        let ask_enabled = content?.ask_enabled ?? false
        askSwitch.on = ask_enabled
        giveButton.enabled = ask_enabled
        askButton.enabled = ask_enabled
        prizeButton.enabled = ask_enabled
        askView.hidden = !ask_enabled
        giveView.hidden = !ask_enabled
        prizeView.hidden = !ask_enabled
        if content?.published == false {
            trainButton.enabled = trainSwitch.on
        } else {
            trainButton.enabled = true
        }
        trainView.hidden = (content?.published)!
        
//        onModes(playButton)
    }
    
    func updateLock() {
        if self.showLock {
            let lock = UIImageView(frame: CGRectMake(10, 10, 20, 20))
            lock.image = UIImage(named: "lock")
            lock.tag = 333
            cardView.insertSubview(lock, aboveSubview: cardView)
        } else {
            for view in cardView.subviews{
                if view.tag == 333 {
                    view.removeFromSuperview()
                }
            }
        }
    }
    
    func updateCellsUserInteraction () {
        askEditCell?.userInteractionEnabled = askSwitch.on && isDraftEditing
        prizeEditCell?.userInteractionEnabled = askSwitch.on && isDraftEditing
    }
    
    // MARK: - Action
    
    @IBAction func onLike(sender: AnyObject) {
        onAction(.Like)
    }
    @IBAction func onComment(sender: AnyObject) {
        onAction(.Comment)
    }
    @IBAction func onLoop(sender: AnyObject) {
        onAction(.Loop)
    }
    
    @IBAction func onModes(sender: UIButton) {
        for (index, button) in modeButtons.enumerate() {
            button.selected = false
            modeLabels[index].hidden = true
            modeBars[index].hidden = true
        }
//        for button in modeButtons {
//        }
        sender.selected = !sender.selected
        currentMode = HeaderCellAction(rawValue: sender.tag)!
        onAction(currentMode)
        
        let index = modeButtons.indexOf(sender)
        modeLabels[index!].hidden = false
        modeBars[index!].hidden = false
    }
    
    @IBAction func onTrainSwitch(sender: UISwitch) {
        trainButton.enabled = trainSwitch.on
    }

    @IBAction func onAskSwitch(sender: UISwitch) {
//        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
//        provider.request(.AddMode(isAskModeEnabled: sender.on, deckId: content!.id, userIds: [], format: "format_test")) { (result) in
//            let JSON = Helper.validateResponse(result)
//            if JSON != nil {
//                
//            }
//        }
        
        askButton.enabled = askSwitch.on
        giveButton.enabled = askSwitch.on
        prizeButton.enabled = askSwitch.on
        updateCellsUserInteraction()
        if askSwitch.on == true {
            onModes(askButton)
        }
    }
    
}
