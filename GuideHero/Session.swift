//
//  Constants.swift
//  GuideHero
//
//  Created by Yohei Oka on 7/24/16.
//  Copyright © 2016 Twilio. All rights reserved.
//

import Foundation

class Session {
    static var sharedSession = Session()
    var user : UserNode?
    var authToken = ""
    
    init () {
        ////        authToken = ""
        ////        let userNode = user
        let prefs = NSUserDefaults.standardUserDefaults()
        if let userDic = prefs.valueForKey("guidehero.user") {
            user = UserNode.init(dictionary: userDic as! [String:AnyObject])
        }
        authToken = prefs.valueForKey("guidehero.authtoken") as! String
    }
    
    
    func setAuthResponse(json: NSDictionary) {
        Session.sharedSession.user = UserNode.init(dictionary: json.valueForKeyPath("user") as! [String: AnyObject])
        Session.sharedSession.authToken = json.valueForKeyPath("auth_token") as! String
    }
    
}
