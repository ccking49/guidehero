//
//  CurrentPointCell.swift
//  GuideHero
//
//  Created by forever on 12/15/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

class CurrentPointCell: UITableViewCell {

    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var constantY : NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
