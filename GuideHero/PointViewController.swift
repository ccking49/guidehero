//
//  PointViewController.swift
//  GuideHero
//
//  Created by star on 12/15/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya
import CoreStore
class PointViewController: UIViewController, UITableViewDelegate, UITableViewDataSource , UIScrollViewDelegate , UIGestureRecognizerDelegate{

    @IBOutlet var tableView: UITableView!
    @IBOutlet var btnClose: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var imgBackground : UIImageView!
    @IBOutlet weak var footerView : UIView!
    @IBOutlet weak var whoolView : UIView!
    
    var userInfo: UserNode!
    
    var aryTableHeader :NSMutableArray = []
    var aryPoints: NSMutableArray = []
    var aryUsePoints: NSMutableArray = []
    var aryAddPoints: NSMutableArray = []
    
    var pan = UIPanGestureRecognizer()
    
    var panelOpened = true
    
    ///0.5s is too short for interaction, you could set it longer for test.
    let duration: NSTimeInterval = 0.5
    let relayDuration: NSTimeInterval = 0.3
    let diff: CGFloat = 150
    
    var isRotating = false
    
    var startPos = CGPointZero
    var startCenter = CGPointZero
    
    let dataStack = DataStack(modelName: "PointModel")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataStack.addStorage(
            SQLiteStore(fileName: "PointModel.sqlite"),
            completion: { (result) -> Void in
                // ...
            }
        )
        
        onSetupPanel()
        
        // Do any additional setup after loading the view.
        
        if self.userInfo != nil {
            self.imgProfile.af_setImageWithURL(NSURL(string: self.userInfo.thumbnail_url)!)
            self.imgProfile.layer.cornerRadius = (self.imgProfile.frame.size.height - 2) / 2
            self.imgProfile.layer.borderColor = UIColor.whiteColor().CGColor
            self.imgProfile.layer.borderWidth = 1
            self.imgProfile.layer.masksToBounds = true
            self.lblUsername.text = self.userInfo.username
        }
        
        imgBackground.layer.cornerRadius = 20
        imgBackground.layer.shadowRadius = 5
        imgBackground.layer.shadowOpacity = 0.5
        imgBackground.layer.shadowOffset = CGSizeZero
        imgBackground.layer.shadowColor = UIColor.blackColor().CGColor
        imgBackground.clipsToBounds = true
        imgBackground.backgroundColor = UIColor.whiteColor()
        imgBackground.alpha = 1
        
        self.tableView.allowsSelection = false
        
        footerView.layer.cornerRadius = 17
        footerView.layer.shadowRadius = 5
        footerView.layer.shadowOpacity = 0.5
        footerView.layer.shadowOffset = CGSizeZero
        footerView.layer.shadowColor = UIColor.blackColor().CGColor
        footerView.clipsToBounds = true
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        
    }

    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        UIApplication.sharedApplication().statusBarHidden = true
        
        onCoreDataMigration()

        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func resetPointData(){
        
        self.dataStack.beginSynchronous { (transaction) -> Void in
            
            transaction.deleteAll(From(PointMain))
            
            transaction.deleteAll(From(UsePoints))
            
            transaction.deleteAll(From(GetPoints))
            
            transaction.commitAndWait()
        }
        
    }
    func reloadData() {
        
        let user_id: String! = (self.userInfo.user_id)
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        
        provider.request(.GetAllPoints(userId: user_id)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.hideProgressHUD()
                    return
                }
                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    
                    //remove the all point data for update
                    self.resetPointData()
                    
                    var total_points_gold : Int!
                    var total_points_grey : Int!
                    if JSON["total_silver_points"] is NSNull {
                        total_points_gold = 0
                        total_points_grey = 0
                    } else {
                        total_points_gold =  JSON["total_silver_points"]!["gold"] as! Int
                        total_points_grey = JSON["total_silver_points"]!["silver"] as! Int
                    }
                    
                    var cumulative_earnings_gold : Int!
                    var cumulative_earnings_grey : Int!
                    if JSON["cumulative_earnings"] is NSNull {
                        cumulative_earnings_gold = 0
                        cumulative_earnings_grey = 0
                    } else {
                        print(JSON["cumulative_earnings"]!["gold"])
                        cumulative_earnings_gold = JSON["cumulative_earnings"]!["gold"] as! Int
                        cumulative_earnings_grey = JSON["cumulative_earnings"]!["silver"] as! Int
                    }
                    
                    var points_purchased_gold : Int!
                    var points_purchased_grey : Int!
                    if JSON["points_purchased"] is NSNull {
                        points_purchased_gold = 0
                        points_purchased_grey = 0
                    } else {
                        points_purchased_gold = JSON["points_purchased"]!["gold"] as! Int
                        points_purchased_grey = JSON["points_purchased"]!["silver"] as! Int
                    }
                    
                    var points_used_gold : Int!
                    var points_used_grey : Int!
                    
                    if JSON["points_used"] is NSNull {
                        points_used_grey = 0
                        points_used_gold = 0
                    } else {
                        points_used_gold = JSON["points_used"]!["gold"] as! Int
                        points_used_grey = JSON["points_used"]!["silver"] as! Int
                    }
                    
                    //add the data to point main
                    
                    self.dataStack.beginSynchronous { (transaction) -> Void in
                        
                        let mainPoint = transaction.create(Into(PointMain))
                        mainPoint.cumulative_earnings_gold = Int64(cumulative_earnings_gold)
                        mainPoint.cumulative_earnings_grey = Int64(cumulative_earnings_grey)
                        
                        mainPoint.points_purchased_gold = Int64(points_purchased_gold)
                        mainPoint.points_purchased_grey = Int64(points_purchased_grey)
                        
                        mainPoint.points_used_gold = Int64(points_used_gold)
                        mainPoint.points_used_grey = Int64(points_used_grey)
                        
                        mainPoint.total_points_gold = Int64(total_points_gold)
                        mainPoint.total_points_grey = Int64(total_points_grey)
                        
                        transaction.commitAndWait()
                    }
                    
                    let use_Points = JSON["use_points"] as! [[String: AnyObject]]

                    for item in use_Points{
                        
                        var use_name : String!
                        if item["name"] is NSNull {
                            use_name = ""
                        }else{
                            use_name = item["name"] as! String
                        }
                        
                        
                        self.dataStack.beginSynchronous { (transaction) -> Void in
                            
                            let usePoint = transaction.create(Into(UsePoints))
                            usePoint.use_name = use_name
                            
                            transaction.commitAndWait()
                        }
                        
                    }
                    
                    let get_Points = JSON["get_points"] as! [[String: AnyObject]]
                    
                    for item in get_Points{
                        
                        var get_name : String!
                        if item["name"] is NSNull {
                            get_name = ""
                        }else{
                            get_name = item["name"] as! String
                        }
                        
                        var get_point : Int!
                        if item["points"] == nil{
                            get_point = -1
                        } else {
                            
                            get_point = item["points"] as! Int
                        }

                        
                        self.dataStack.beginSynchronous { (transaction) -> Void in
                            
                            let getPoint = transaction.create(Into(GetPoints))
                            getPoint.get_name = get_name
                            getPoint.get_point = Int64(get_point)
                            
                            transaction.commitAndWait()
                        }
                        
                    }
                    
                    self.showPointData()
                    
                    self.hideProgressHUD()
                }
                else {
                    self.hideProgressHUD()
                }
            case .Failure(_):
                self.hideProgressHUD()
            }
        }
        
    }
    
    func checkPointData() -> Bool{
        
        let mainPoint = self.dataStack.fetchAll(From(PointMain))

        let getPoint = self.dataStack.fetchAll(From(GetPoints))
            
        let usePoint = self.dataStack.fetchAll(From(UsePoints))
        
        if mainPoint == nil || getPoint == nil || usePoint == nil {
            
            return false
            
        }else{
            
            if mainPoint?.count == 0 || getPoint?.count == 0 || usePoint?.count == 0 {
                
                return false
            }else{
                return true
            }

        }
        
        
    }
    func showPointData(){
        
        let mainPoint = self.dataStack.fetchAll(From(PointMain))
        
        let getPoint = self.dataStack.fetchAll(From(GetPoints))
        
        let usePoint = self.dataStack.fetchAll(From(UsePoints))
        
        if mainPoint != nil && getPoint != nil && usePoint != nil {
            
            if mainPoint!.count != 0 && getPoint!.count != 0 && usePoint!.count != 0 {
                self.aryPoints.removeAllObjects()
                self.aryUsePoints.removeAllObjects()
                self.aryAddPoints.removeAllObjects()
                
                let mainPointMax = mainPoint!.count
                
                self.aryPoints.addObject(NSNumber(longLong: mainPoint![mainPointMax-1].total_points_gold))
                self.aryPoints.addObject(NSNumber(longLong: mainPoint![mainPointMax-1].total_points_grey))
                self.aryPoints.addObject(NSNumber(longLong: mainPoint![mainPointMax-1].cumulative_earnings_gold))
                self.aryPoints.addObject(NSNumber(longLong: mainPoint![mainPointMax-1].cumulative_earnings_grey))
                self.aryPoints.addObject(NSNumber(longLong: mainPoint![mainPointMax-1].points_purchased_gold))
                self.aryPoints.addObject(NSNumber(longLong: mainPoint![mainPointMax-1].points_purchased_grey))
                self.aryPoints.addObject(NSNumber(longLong: mainPoint![mainPointMax-1].points_used_gold))
                
                for item in usePoint! {
                    
                    let jsonObject = [
                        
                        "name": item.use_name!
                        
                    ]
                    
                    self.aryUsePoints.addObject(jsonObject)
                    
                }
                
                for item in getPoint! {
                    
                    let jsonObj = ["name":item.get_name!, "points":String(item.get_point)]
                    
                    self.aryAddPoints.addObject(jsonObj)
                    
                }
                
                
                self.aryTableHeader = ["Current Points", "Cumulative Earnings", "Points Purchased", "Points Used", "Use Points", "Get Points"]
                
                self.tableView.reloadData()
                
                self.imgBackground.backgroundColor = UIColor.blackColor()
                self.imgBackground.alpha = 0.5
                self.imgBackground.clipsToBounds = false
                self.imgBackground.layer.cornerRadius = 0
                self.imgBackground.layer.shadowRadius = 0
            }
            
            
        }
        
        
    }
    
    //Button Action
    @IBAction func onClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func onMore(sender: AnyObject) {
        
    }
    
    //UITableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return aryTableHeader.count
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.aryTableHeader[section] as? String
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: self.view.frame.width, height: 48)))
        headerView.clipsToBounds = false
        let label = UILabel(frame: CGRect(x: 12, y: 0, width: self.view.frame.width-16, height: 48))
        label.text = aryTableHeader[section] as? String
        label.textAlignment = NSTextAlignment.Left
        headerView.backgroundColor = UIColor.colorFromRGB(redValue: 231, greenValue: 231, blueValue: 231, alpha: 1.0)
        label.textColor = UIColor.blackColor()
        label.font = Helper.proximaNova("Bold", font: 20)
        headerView.addSubview(label)
        
        return headerView

    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UILabel(frame: CGRect(origin: CGPointZero, size: CGSize(width: self.view.frame.width, height: 1)))
        footerView.backgroundColor = UIColor.whiteColor()
        return footerView
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        return 48
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if aryPoints.count == 0 {
                return 0
            }else {
                return 2
            }
        } else if section == 1 {
            if aryPoints.count == 0 {
                return 0
            }else {
                return 2
            }
        } else if section == 2 {
            if aryPoints.count == 0 {
                return 0
            }else {
                return 1
            }
        } else if section == 3 {
            if aryPoints.count == 0 {
                return 0
            } else {
                return 2
            }
        } else if section == 4 {
            return aryUsePoints.count
        } else if section == 5 {
            return aryAddPoints.count
        }
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 || indexPath.section == 1 || indexPath.section == 3 {
            let cell = tableView.dequeueReusableCellWithIdentifier("CurrentPointCell", forIndexPath: indexPath) as! CurrentPointCell
            
            let largeNumber = (aryPoints[indexPath.section] ) as! NSNumber
            let numberFormatter = NSNumberFormatter()
            numberFormatter.numberStyle = .DecimalStyle
            
            cell.lblPoints.text = numberFormatter.stringFromNumber(largeNumber)
            
            if indexPath.row == 0 {
                cell.lblPoints.textColor = UIColor.colorFromRGB(redValue: 253, greenValue: 222, blueValue: 87, alpha: 1.0)
                
                cell.constantY.constant = 8.0
            }else{
                cell.lblPoints.textColor = UIColor.colorFromRGB(redValue: 210, greenValue: 210, blueValue: 210, alpha: 1.0)
                
                cell.constantY.constant = -8.0
            }
            
            return cell
        }else if indexPath.section == 2 {
            
            let cell = tableView.dequeueReusableCellWithIdentifier("CurrentPointCell", forIndexPath: indexPath) as! CurrentPointCell
            
            let largeNumber = (aryPoints[indexPath.section] ) as! NSNumber
            let numberFormatter = NSNumberFormatter()
            numberFormatter.numberStyle = .DecimalStyle
            
            cell.lblPoints.text = numberFormatter.stringFromNumber(largeNumber)
            cell.lblPoints.textColor = UIColor.colorFromRGB(redValue: 210, greenValue: 210, blueValue: 210, alpha: 1.0)
            
            return cell
            
        } else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCellWithIdentifier("AddPointCell", forIndexPath: indexPath) as! AddPointCell
            let data = aryUsePoints[indexPath.row] as! [String: AnyObject]
            cell.lblTitle.text = data["name"] as? String
            cell.btnGo.tag = indexPath.row
            cell.btnBorder.tag = indexPath.row
            
            return cell
        } else if indexPath.section == 5 {
            let cell = tableView.dequeueReusableCellWithIdentifier("AddPointCell", forIndexPath: indexPath) as! AddPointCell
            let data = aryAddPoints[indexPath.row] as! [String: AnyObject]
            cell.lblTitle.text = data["name"] as? String
            cell.btnGo.tag = indexPath.row
            cell.btnBorder.tag = indexPath.row + 6
            
            if data["points"] == nil {
                cell.btnGo.setTitle("Go", forState: UIControlState.Normal)
                cell.btnGo.setTitleColor(UIColor.colorFromRGB(redValue: 210, greenValue: 210, blueValue: 210, alpha: 1.0), forState: UIControlState.Normal)
            } else {
                
                if data["points"] as! String == "-1" {
                    cell.btnGo.setTitle("Go", forState: UIControlState.Normal)
                    cell.btnGo.setTitleColor(UIColor.colorFromRGB(redValue: 210, greenValue: 210, blueValue: 210, alpha: 1.0), forState: UIControlState.Normal)
                }else{
                    let largeNumber = (data["points"]) as! String
                    let numberFormatter = NSNumberFormatter()
                    numberFormatter.numberStyle = .DecimalStyle
                    
                    cell.btnGo.setTitle(largeNumber, forState: UIControlState.Normal)
                    cell.btnGo.setTitleColor(UIColor.colorFromRGB(redValue: 210, greenValue: 210, blueValue: 210, alpha: 1.0), forState: UIControlState.Normal)
                    

                }
                
            }
            
//            cell.needsRoundBottom = true
//            cell.setNeedsLayout()
//            cell.layoutIfNeeded()
            return cell
        }
        
        
        
        return UITableViewCell()
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 60
        } else if indexPath.section == 1 {
            return 60
        } else if indexPath.section == 2 {
            return 84
        } else if indexPath.section == 3 {
            return 60
        } else if indexPath.section == 4 {
            return 50
        } else if indexPath.section == 5 {
            return 50
        }
        return 1
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    @IBAction func onGo(sender: AnyObject) {
        let button = sender as! UIButton
        let index = button.tag
        if (index == 3) {
            let usePointsViewController = storyboard?.instantiateViewControllerWithIdentifier("UsePointsViewController") as! UsePointsViewController
            usePointsViewController.modalPresentationStyle = .OverFullScreen
            presentViewController(usePointsViewController, animated: false, completion: {
                usePointsViewController.didFinishPresent()
            })
        }
        else if (index == 4) {
            let usePointsForCardViewController = storyboard?.instantiateViewControllerWithIdentifier("UsePointsForCardViewController") as! UsePointsForCardViewController
            usePointsForCardViewController.modalPresentationStyle = .OverFullScreen
            usePointsForCardViewController.cardType = "Amazon"
            presentViewController(usePointsForCardViewController, animated: false, completion: {
                usePointsForCardViewController.didFinishPresent()
            })
        }
        else if (index == 5) {
            let usePointsForCardViewController = storyboard?.instantiateViewControllerWithIdentifier("UsePointsForCardViewController") as! UsePointsForCardViewController
            usePointsForCardViewController.modalPresentationStyle = .OverFullScreen
            usePointsForCardViewController.cardType = "Starbucks"
            presentViewController(usePointsForCardViewController, animated: false, completion: {
                usePointsForCardViewController.didFinishPresent()
            })
        }
        else if (index >= 7 && index <= 9) {
            let getPointsViewController = storyboard?.instantiateViewControllerWithIdentifier("GetPointsViewController") as! GetPointsViewController
            getPointsViewController.modalPresentationStyle = .OverFullScreen
            
            var amount: CGFloat = 0.0
            
            switch index {
            case 7:
                amount = 0.99
                break
            case 8:
                amount = 4.99
                break
            case 9:
                amount = 9.99
                break
            default:
                break
            }
            getPointsViewController.transferAmount = amount
            presentViewController(getPointsViewController, animated: false, completion: {
                getPointsViewController.didFinishPresent()
            })
        }
    }
    
    
///////////// reuseable to movement card
    
    func onSetupPanel(){
        
        pan.addTarget(self, action: #selector(PointViewController.handlePan(_:)))
        pan.delegate = self
        whoolView.addGestureRecognizer(pan)
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        let velocity = pan.velocityInView(self.tableView?.superview)
        
        if fabs(velocity.x) >= fabs(velocity.y) || (tableView.contentOffset.y <= 0 && velocity.y > 0) {
            return true
        }
        
        return false
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if otherGestureRecognizer is UIPanGestureRecognizer {
            return true
        }
        
        return false
    }
    
    func handlePan(panGesture: UIPanGestureRecognizer){
        
        let factor: CGFloat = 2
        let angleLimit = CGFloat(M_PI_4)
        
        switch panGesture.state {
        case .Began:
            
            startPos = panGesture.locationInView(self.whoolView?.superview)
            startCenter = self.whoolView.center
            print(startPos)
            let velocity = panGesture.velocityInView(self.whoolView?.superview)
            
            isRotating = fabs(velocity.x) > fabs(velocity.y)
            
        case .Changed:
            
            if isRotating {
                
                let pos = panGesture.locationInView(self.view?.superview)
                
                let rotationAngle = atan((pos.x - startPos.x) / (self.view.bounds.size.height * factor - pos.y))
                var direction:CGFloat = 1
                if pos.x != startPos.x {
                    direction = (startPos.x - pos.x) / fabs(pos.x - startPos.x)
                }
                imgBackground.alpha = 0.5 - fabs(rotationAngle)
                
                var transform = CGAffineTransformMakeTranslation(self.view.bounds.size.width * 0.5 * direction, self.view.bounds.size.height * (factor - 0.5))
                transform = CGAffineTransformRotate(transform, rotationAngle)
                transform = CGAffineTransformTranslate(transform, -self.view.bounds.size.width * 0.5 * direction, -self.view.bounds.size.height * (factor - 0.5))
                self.whoolView.transform = transform;
                
            } else {
                let pos = panGesture.locationInView(self.view?.superview)
                self.whoolView.center = CGPointMake(startCenter.x, startCenter.y + pos.y - startPos.y)
            }
            
        case .Ended:
            
            if isRotating {
                let pos = panGesture.locationInView(self.whoolView?.superview)
                let velocity = panGesture.velocityInView(self.whoolView?.superview)
                var startDirection = 1
                if pos.x != startPos.x {
                    startDirection = Int(round((pos.x - startPos.x) / fabs(pos.x - startPos.x)))
                }
                var velocityDirection = -startDirection
                if velocity.x != 0 {
                    velocityDirection = Int(round(velocity.x / fabs(velocity.x)))
                }
                if startDirection == velocityDirection { // should dismiss
                    UIView.animateWithDuration(0.3, animations: {
                        
                        var transform = CGAffineTransformMakeTranslation(-self.whoolView.bounds.size.width * 0.5 * CGFloat(startDirection), self.whoolView.bounds.size.height * (factor - 0.5))
                        transform = CGAffineTransformRotate(transform, angleLimit * CGFloat(startDirection))
                        transform = CGAffineTransformTranslate(transform, self.whoolView.bounds.size.width * 0.5 * CGFloat(startDirection), -self.whoolView.bounds.size.height * (factor - 0.5))
                        
                        self.whoolView.transform = transform
                        
                        }, completion: { completed in
                            self.dismissViewControllerAnimated(false, completion: nil)
                    })
                } else {
                    UIView.animateWithDuration(0.2, animations: {
                        self.whoolView.transform = CGAffineTransformIdentity
                        }, completion: { completed in
                    })
                }
                
            } else {
                let velocity = panGesture.velocityInView(self.whoolView?.superview)
                if velocity.y > 0 { // should dismiss
                    UIView.animateWithDuration(0.2, animations: {
                        self.whoolView.center = CGPointMake(self.startCenter.x, self.whoolView.bounds.size.height * 1.5)
                        }, completion: { completed in
                            self.dismissViewControllerAnimated(false, completion: nil)
                    })
                } else {
                    UIView.animateWithDuration(0.2, animations: {
                        self.whoolView.center = self.startCenter
                        }, completion: { completed in
                    })
                }
            }
            
            
        default:break
        }
    }
    
    func onCoreDataMigration(){
        
        if !checkPointData() {
            
            self.showProgressHUD()
            
            reloadData()
            
        }else{
            showPointData()
            
            reloadData()
        }
        
        
    }
    
}
