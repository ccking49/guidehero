//
//  ProfileTextField.swift
//  GuideHero
//
//  Created by Ascom on 10/31/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

public class ProfileTextField: UITextField {

    required public override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override public func drawRect(rect: CGRect) {
        let title = placeholderText ?? ""
        var myMutableStringTitle = NSMutableAttributedString()
        myMutableStringTitle = NSMutableAttributedString(string:title, attributes: [NSFontAttributeName:font!]) // Font
        myMutableStringTitle.addAttribute(NSForegroundColorAttributeName, value: Constants.UIConstants.halfYellowColor, range:NSRange(location:0,length:title.length))    // Color
        attributedPlaceholder = myMutableStringTitle
    }
    
    func initialize () {
        textColor = Constants.UIConstants.yellowColor
        
        
        
    }
}
