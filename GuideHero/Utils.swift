//
//  Utils.swift
//  GuideHero
//
//  Created by Yohei Oka on 7/24/16.
//  Copyright © 2016 Twilio. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func replace(string:String, replacement:String) -> String {
        return self.stringByReplacingOccurrencesOfString(string, withString: replacement, options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(" ", replacement: "")
    }
    
    func toDateInGMT (format: String) -> NSDate? {
        let dateFormatter = NSDateFormatter()
        let timeZone = NSTimeZone(name: "UTC")
        dateFormatter.timeZone = timeZone
        dateFormatter.dateFormat = format
        let date = dateFormatter.dateFromString(self)
        return date
    }
    
    var length: Int {
        return characters.count
    }
}

extension CAGradientLayer {
    
    func LandingPageColor() -> CAGradientLayer {
        let topColor = UIColor(red: (0/255.0), green: (59/255.0), blue: (155/255.0), alpha: 1)
        let bottomColor = UIColor(red: (0/255.0), green: (3/255.0), blue: (17/255.0), alpha: 1)
        
        let gradientColors = [topColor.CGColor, bottomColor.CGColor]
        let gradientLocations = [0.0, 1.0]
        
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.locations = gradientLocations
        
        return gradientLayer
    }
}


extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.sharedApplication().keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}

extension RangeReplaceableCollectionType where Generator.Element : Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func removeObject(object : Generator.Element) {
        if let index = self.indexOf(object) {
            self.removeAtIndex(index)
        }
    }
}

extension UIStoryboard {
    static func loadViewController(storyboardName storyboardName: String, viewControllerIdentifier: String) -> UIViewController? {
        let mainBundle = NSBundle.mainBundle()
        let storyboard = UIStoryboard(name: storyboardName, bundle: mainBundle)
        let viewController = storyboard.instantiateViewControllerWithIdentifier(viewControllerIdentifier) 
        
        return viewController
    }
}
