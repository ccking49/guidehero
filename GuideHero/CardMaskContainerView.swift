//
//  CardContainerView.swift
//  GuideHero
//
//  Created by Ascom on 11/2/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AVFoundation

public class CardMaskContainerView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var imageView: MaskImageView!
    
    var isDeck: Bool! {
        didSet {
            
        }
    }
    
    func generateThumnail(url : NSURL, fromTime:Float64) -> UIImage? {
        
        let asset = AVAsset(URL: url)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        assetImgGenerate.requestedTimeToleranceAfter = kCMTimeZero;
        assetImgGenerate.requestedTimeToleranceBefore = kCMTimeZero;

        let time        : CMTime = CMTimeMakeWithSeconds(fromTime, 600)
        var img         : CGImageRef!
        do{
            img = try assetImgGenerate.copyCGImageAtTime(time, actualTime: nil)
            
            
        }catch{
            
        }
        if img != nil {
            let frameImg    : UIImage = UIImage(CGImage: img)
            return frameImg
        } else {
            return nil
        }
        
    }
    
    var content: ContentNode! {
        get {
            return self.content
        }
        set (newContent) {
            imageView.hidden = true
            
            switch (newContent.type) {
            case .Image :
                imageView.setImageWithUrlMaskBackground(newContent.imageURL!, rect: newContent.maskRect)
                imageView.hidden = false
                break
            case .Video:
                
                let qualityOfServiceClass = QOS_CLASS_BACKGROUND
                let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
                dispatch_async(backgroundQueue, {
                    print("This is run on the background queue")
                    
                    var convertData = self.generateThumnail(newContent.videoURL!, fromTime: Float64(1))
                    
                    if let img =  convertData{
                        let ratio = self.bounds.size.width / self.bounds.size.height
                        let ratioImage = img.size.width / img.size.height
                        
                        if ratio > ratioImage {
                            self.imageView.layer.contentsRect = CGRect(x: 0, y: 0, width: 1, height: ratio * ratioImage)
                        } else {
                            self.imageView.layer.contentsRect = CGRect(x: 0, y: 0, width: 1, height: 1)
                        }
                    }
                    self.imageView.contentMode = .ScaleAspectFill
                    self.imageView.image = convertData
                    
                })
                
                
                imageView.hidden = false
                break
            case .Deck:
                if newContent.children.count == 0 {
                    break
                } else {
                    self.content = newContent.children[0]
                }
                break
            default:
                break
            }
            
        }
    }
    
    var isInList: Bool = true {
        didSet {
//            let fontSize = isInList ? 14 : 22 as CGFloat
            
        }
    }
    
    required public override init(frame: CGRect) {
        super.init(frame: frame)
        initilize()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initilize()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        let width = CGRectGetWidth(bounds)
        let height = CGRectGetHeight(bounds)
        imageView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        
        
    }
    
    func initilize() {
        backgroundColor = UIColor.whiteColor()
        clipsToBounds = false
        
        imageView = MaskImageView(frame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height))
        imageView.contentMode = .ScaleAspectFill
        
        imageView.clipsToBounds = true
        
        setEffects(imageView)
        imageView.hidden = true
        
        addSubview(imageView)
    }
    
    func setEffects (view: UIView) {
        view.layer.cornerRadius = 4
        
        view.layer.shadowColor = UIColor.colorFromRGB(redValue: 155/255.5, greenValue: 155/255.5, blueValue: 155/255.5, alpha: 0.5).CGColor//Helper.colorFromRGB(0x9b9b9b, alpha: 0.5).CGColor
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowRadius = 1
        view.layer.shadowOpacity = 0.5
        
        view.backgroundColor = UIColor.clearColor()
//        view.layer.borderColor = UIColor.colorFromRGB(redValue: 155/255.5, greenValue: 155/255.5, blueValue: 155/255.5, alpha: 0.5).CGColor
//        view.layer.borderWidth = 0.5
    }
}
