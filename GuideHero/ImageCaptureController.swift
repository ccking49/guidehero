//
//  ImageCaptureController.swift
//  GuideHero
//
//  Created by Ignacio Rodrigo on 9/29/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AVFoundation

class ImageCaptureController: UIViewController {

    // MARK: - Properties

    @IBOutlet var captureButton: UIButton!

    let captureSession = AVCaptureSession()
    let stillImageOutput = AVCaptureStillImageOutput()
    var previewLayer: AVCaptureVideoPreviewLayer? = nil
    
    var parentVC: UIViewController? = nil
    var type: Int = 0

    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCamera()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(updateCameraOrientation),
                                                         name: UIDeviceOrientationDidChangeNotification,
                                                         object: nil)
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIDeviceOrientationDidChangeNotification, object: nil)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.previewLayer?.frame = self.view.bounds
    }

    // MARK: - Actions

    @IBAction func takePicture(sender: AnyObject) {
        if self.type == 2 {
            self.showSimpleAlert(withTitle: "Warning", withMessage: "Please select video!")
            return
        }
        
        if let videoConnection = self.stillImageOutput.connectionWithMediaType(AVMediaTypeVideo) {
            stillImageOutput.captureStillImageAsynchronouslyFromConnection(videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                self.showCardCreationWithImage(UIImage(data: imageData))
            }
        }
        else {
            self.showCameraError()
        }
    }

    @IBAction func close(sender: AnyObject) {
        self.tabBarController?.selectedIndex = Constants.UIConstants.learnTabIndex
    }
    
    // MARK: - Camera setup

    private func setupCamera() {

        // get back camera

        let devices = AVCaptureDevice.devices().filter{ $0.hasMediaType(AVMediaTypeVideo) && $0.position == AVCaptureDevicePosition.Back }
        if let captureDevice = devices.first as? AVCaptureDevice  {

            // setup capture session and add back camera as an imput

            if let input = try? AVCaptureDeviceInput(device: captureDevice) {
                if self.captureSession.canAddInput(input) {
                    self.captureSession.addInput(input)
                }
                else {
                    self.showCameraError()
                    return
                }
            }
            else {
                self.showCameraError()
                return
            }

            captureSession.sessionPreset = AVCaptureSessionPresetPhoto
            captureSession.startRunning()

            // add a still image output to the capture session

            self.stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
            if self.captureSession.canAddOutput(self.stillImageOutput) {
                self.captureSession.addOutput(self.stillImageOutput)
            }
            else {
                self.showCameraError()
                return
            }

            // add the preview layer

            if let previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession) {
                self.previewLayer = previewLayer
                previewLayer.frame = self.view.bounds
                previewLayer.position = self.view.layer.position
                previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
                previewLayer.connection.videoOrientation = self.videoOrientationFromDeviceOrientation(UIDevice.currentDevice().orientation)
                self.view.layer.insertSublayer(previewLayer, atIndex: 0)
            }
            else {
                self.showCameraError()
            }
        }
        else {
            self.showCameraError()
        }
    }

    func videoOrientationFromDeviceOrientation(deviceOrientation: UIDeviceOrientation) -> AVCaptureVideoOrientation {
        var videoOrientation: AVCaptureVideoOrientation = .Portrait
        switch deviceOrientation {
        case .Portrait:
            videoOrientation = .Portrait;
        case .PortraitUpsideDown:
            videoOrientation = .PortraitUpsideDown
        case .LandscapeLeft:
            videoOrientation = .LandscapeRight
        case .LandscapeRight:
            videoOrientation = .LandscapeLeft
        default:
            break
        }
        return videoOrientation
    }

    private func showCameraError() {
        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't access camera, please try again later")
    }

    func updateCameraOrientation() {
        let deviceOrientation = UIDevice.currentDevice().orientation
        let videoOrientation = self.videoOrientationFromDeviceOrientation(deviceOrientation)
        self.previewLayer?.connection.videoOrientation = videoOrientation
    }

    // MARK: - Navigation

    func showCardCreationWithImage(image: UIImage?) {
        if self.type == 0 {
            self.performSegueWithIdentifier("ShowImageCardCreationController", sender: image)
        }
        else if self.type == 1 {
            if let vc = self.parentVC as? ImageCardCreationController {
                vc.image = image
                vc.setupImageView()
                navigationController!.dismissViewControllerAnimated(true, completion: nil)
            }
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? ImageCardCreationController {
            vc.image = sender as? UIImage
        }
    }

}
