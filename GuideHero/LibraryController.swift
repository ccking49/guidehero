//
//  LibraryController.swift
//  GuideHero
//
//  Created by forever on 1/5/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit
import Photos

class LibraryController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var cameraRollContentButton: UIButton! {
        didSet {
            cameraRollContentButton.setImage(UIImage(appImage: .cameraRollContentNormal), forState: .Normal)
        }
    }

    @IBOutlet weak var cameraContentButton: UIButton! {
        didSet {
            cameraContentButton.setImage(UIImage(appImage: .cameraContentNormal), forState: .Normal)
        }
    }
    
    @IBOutlet weak var topNavBar: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var assetThumbnailSize : CGSize!
    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    
    let screenWidth : CGFloat = UIScreen.mainScreen().bounds.size.width
    let screenHeight : CGFloat = UIScreen.mainScreen().bounds.size.height
    let sectionInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    
    var photosAsset: PHFetchResult!
    weak var panToCloseGestureRecognizer: UIPanGestureRecognizer!
    
    var parentVC: UIViewController? = nil
    var type: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        layout.itemSize = CGSize(width: (screenWidth - 15)/3, height: (screenWidth - 15)/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 5
        
        collectionView.collectionViewLayout = layout
        collectionView.bounces = false // The bouncing is disabled in favor of pan-to-close gesture
        
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
//            let cellSize = layout.itemSize
            self.assetThumbnailSize = CGSize(width: 1000, height: 1000)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().statusBarHidden = true

        self.checkForLibraryAuth()

        // The pan gesture belongs to the parent view. Switch delegate to control it here.
        panToCloseGestureRecognizer.delegate = self
    }

    func checkForLibraryAuth() {
        let status = PHPhotoLibrary.authorizationStatus()

        switch status {
        case .Authorized:
            self.retrieveNewestImage()
            self.collectionView.reloadData()
        case .Denied, .Restricted:
            self.showSimpleAlert(withTitle: "Photo Library", withMessage: "Access not granted")
        case .NotDetermined:
            PHPhotoLibrary.requestAuthorization({ (status) in
                switch status {
                case .Authorized:
                    self.retrieveNewestImage()
                    self.collectionView.reloadData()
                case .Denied, .Restricted, .NotDetermined:
                    self.showSimpleAlert(withTitle: "Photo Library", withMessage: "Access not granted")
                }
            })

        }
    }

//    func requestAuth(){
//        PHPhotoLibrary.requestAuthorization {
//            [weak self](status: PHAuthorizationStatus) in
//
//
//            switch status{
//            case PHAuthorizationStatus.Authorized:
//                self!.retrieveNewestImage()
//                self!.collectionView.reloadData()
//            default:
//                print("Not authorized")
//            }
//
//            let qualityOfServiceClass = QOS_CLASS_BACKGROUND
//            let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
//            dispatch_async(backgroundQueue, {
//                dispatch_async(dispatch_get_main_queue(), { () -> Void in
////                    switch status{
////                    case PHAuthorizationStatus.Authorized:
////                        self!.retrieveNewestImage()
////                        self!.collectionView.reloadData()
////                    default:
////                        print("Not authorized")
////                    }
//                })
//            })
//            
//        }
//    }
    
    func retrieveNewestImage() {
        
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        PHAsset.fetchAssetsWithOptions(options)
        let assetResults = PHAsset.fetchAssetsWithOptions(options)
        
        photosAsset = assetResults
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        var count = 0

        if (self.photosAsset != nil) {
            count = self.photosAsset.count
        }
        
        return count
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("AlbumCell", forIndexPath: indexPath) as! AlbumCell
        let asset: PHAsset = self.photosAsset[indexPath.item] as! PHAsset

        cell.setImage(UIImage(appImage: .placeholderPhoto))

        let manager = PHImageManager.defaultManager()

//        if cell.tag != 0 {
//            manager.cancelImageRequest(PHImageRequestID(cell.tag))
//        }

        cell.tag = Int(manager.requestImageForAsset(asset, targetSize: self.assetThumbnailSize, contentMode: .AspectFit, options: nil, resultHandler: { (result, info) in

            cell.setImage(result!)
            
            if let destinationCell = collectionView.cellForItemAtIndexPath(indexPath) as? AlbumCell {
                destinationCell.setImage(result!)

                if asset.mediaType == .Video {
                    cell.timeLabel.hidden = false

                    manager.requestAVAssetForVideo(asset, options: nil, resultHandler: { (asset1: AVAsset?, audioMix: AVAudioMix?, options: [NSObject: AnyObject]?) in
                        if asset1 != nil {
                            let videoDuration = Float(CMTimeGetSeconds((asset1?.duration)!))
                                                        destinationCell.timeLabel.text = self.makeTimeString(Int(videoDuration))
                        }
                    })
                }
            }
        }))
        
//        PHImageManager.defaultManager().requestImageForAsset(asset, targetSize: self.assetThumbnailSize, contentMode: PHImageContentMode.AspectFit, options: nil, resultHandler: {(result, info)in
//            if let image = result {
//                cell.setImage(image)
//
//                if asset.mediaType == .Video {
//                    cell.timeLabel.hidden = false
//
//                    let videoManager = PHImageManager.defaultManager()
//                    let requestOptions = PHVideoRequestOptions()
//
//                    requestOptions.deliveryMode = .Automatic
//                    requestOptions.networkAccessAllowed = true
//                    requestOptions.version = .Current
//                    requestOptions.progressHandler = {(progress: Double,
//                        error: NSError?,
//                        stop: UnsafeMutablePointer<ObjCBool>,
//                        info: [NSObject : AnyObject]?) in
//                        
//                    }
//                    
//                    videoManager.requestAVAssetForVideo(asset, options: requestOptions, resultHandler: { (asset1:AVAsset?, audioMix: AVAudioMix?, options:[NSObject : AnyObject]?) in
//                        if asset1 != nil {
//                            let videoDuration = Float(CMTimeGetSeconds((asset1?.duration)!))
//                            cell.timeLabel.text = self.makeTimeString(Int(videoDuration))
//                        }
//                    })
//                }
//            }
//        })

        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let asset: PHAsset = self.photosAsset[indexPath.item] as! PHAsset
        
        switch asset.mediaType {
        case .Image:
            
            if self.type == 2 {
                self.showSimpleAlert(withTitle: "Warning", withMessage: "Please select video file!")
                return
            }
            
            self.showProgressHUD()
            let imageManager = PHImageManager.defaultManager()
            let requestOptions = PHImageRequestOptions()
            requestOptions.synchronous = true
            imageManager.requestImageForAsset(asset, targetSize: self.assetThumbnailSize, contentMode: PHImageContentMode.AspectFill, options: requestOptions, resultHandler: {(result, info)in
                if result != nil {
                    asset.requestContentEditingInputWithOptions(PHContentEditingInputRequestOptions(), completionHandler: { (input, _) in
                            self.gotoCreateImageCard(result!)
                            self.hideProgressHUD()
                        })
                    
                } else {
                    self.hideProgressHUD()
                }
            })
            break
        case .Video:

            if self.type == 1 {
                self.showSimpleAlert(withTitle: "Warning", withMessage: "Please select image file!")
                return
            }
            
            self.showProgressHUD()
            let videoManager = PHImageManager.defaultManager()
            let requestOptions = PHVideoRequestOptions()
            requestOptions.deliveryMode = .Automatic
            requestOptions.networkAccessAllowed = true
            requestOptions.version = .Current
            requestOptions.progressHandler = {(progress: Double,
                error: NSError?,
                stop: UnsafeMutablePointer<ObjCBool>,
                info: [NSObject : AnyObject]?) in
                
            }
            
            videoManager.requestAVAssetForVideo(asset, options: requestOptions,
                                                resultHandler: { (asset1:AVAsset?, audioMix: AVAudioMix?, options:[NSObject : AnyObject]?) in
                if asset1 != nil {
                    let path = (asset1 as! AVURLAsset).URL
                    self.gotoCreateVideoCard(path)
                    self.hideProgressHUD()
                } else {
                    self.hideProgressHUD()
                }
            })
            break
        default:
            print("unknown")
        }
       
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: (self.screenWidth - 15) / 3, height: (self.screenWidth - 15) / 3)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return self.sectionInsets
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        //description is a String variable defined in the class
        let size:CGSize = CGSizeMake(self.collectionView.frame.width, 80)
        return size
    }
    
    func makeTimeString(timeInSec: Int) -> String {
        var secString = String(timeInSec % 60)
        if (timeInSec % 60 < 10) {
            secString = "0" + secString
        }
        
        var minString = String(timeInSec % 3600 / 60)
        if (timeInSec % 3600 / 60 < 10) {
            minString = "0" + minString
        }
        
        return minString + ":" + secString
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if (scrollView.contentOffset.y > 0 && scrollView.contentOffset.y < (scrollView.contentSize.height - scrollView.frame.size.height)){
            //not top and not bottom
            topNavBar.alpha = 0.5
        }
        else {
            topNavBar.alpha = 1.0
        }
    }
    
    func gotoCreateVideoCard(url:NSURL) {
        dispatch_async(dispatch_get_main_queue(),{
            
            if self.type == 0 {
                let vc = self.storyboard!.instantiateViewControllerWithIdentifier("VideoCardCreationViewController") as! VideoCardCreationViewController
                vc.videoUrl = url
                vc.prevViewController = self
                self.navigationController!.pushViewController(vc, animated: true)
            }
            else if self.type == 2 {
                if let vc = self.parentVC as? VideoCardCreationViewController {
                    vc.videoUrl = url
                    vc.setupVideo()
                    self.navigationController!.dismissViewControllerAnimated(true, completion: nil)
                }
            }
        })
    }
    
    func gotoCreateImageCard(image:UIImage) {
        dispatch_async(dispatch_get_main_queue(),{
            if self.type == 0 {
                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ImageCardCreationController") as! ImageCardCreationController
                vc.image = image
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if self.type == 1 {
                if let vc = self.parentVC as? ImageCardCreationController {
                    vc.image = image
                    vc.setupImageView()
                    self.navigationController!.dismissViewControllerAnimated(true, completion: nil)
                }
            }
        })
    }
    
    @IBAction func onClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onCameraRollContent(sender: AnyObject) {
        
    }
    
    @IBAction func onCameraContentButton(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName("GotoCameraScreen", object: self, userInfo: nil)
//        let finalCardViewController = self.storyboard!.instantiateViewControllerWithIdentifier("FinalCardViewController") as! FinalCardViewController
//        finalCardViewController.cardName = "What are the key takeaways?"
//        finalCardViewController.descriptionText = "Lean In by Sheryl Sandberg. Chapter 1."
//        
//        finalCardViewController.prevViewController = self
//        self.presentViewController(finalCardViewController, animated: true, completion: nil)
    }
    
}

// MARK: - UIGestureRecognizerDelegate
extension LibraryController: UIGestureRecognizerDelegate {
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        // Allow pan to close gesture only when the content offset is 0
        return (collectionView.contentOffset.y <= 0)
    }
}
