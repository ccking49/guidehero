//
//  ContentCreationController.swift
//  GuideHero
//
//  Created by Ignacio Rodrigo on 9/29/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

class ContentCreationController: BasePostLoginController {

    @IBOutlet var containerView: UIView!
    @IBOutlet var toolbar: UIToolbar!

    @IBOutlet weak var toolBarBottomConstraint: NSLayoutConstraint!
    
    var currentChildController: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().statusBarHidden = true
        self.tabBarController?.tabBar.hidden = true
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.sharedApplication().statusBarHidden = false
        self.tabBarController?.tabBar.hidden = false
    }
    // MARK: - @IBAction
    
    @IBAction func createImageCardFromWebSearch(sender: UIBarButtonItem) {
        let vc = self.storyboard!.instantiateViewControllerWithIdentifier("ImageCardCreationFromSearchNavigationController")
        self.moveToViewController(vc)
        self.highlightButton(sender)
    }
    
    @IBAction func createVideoCardFromCapture(sender: UIBarButtonItem) {
//        let vc = self.storyboard!.instantiateViewControllerWithIdentifier("VideoCardCreationFromCaptureNavigationController")
        let vStoryBoard = UIStoryboard(name: "VideoRecord", bundle: NSBundle.mainBundle())
        let vc = vStoryBoard.instantiateViewControllerWithIdentifier("VideoRecordNav")
        self.moveToViewController(vc)
        self.highlightButton(sender)
    }

    @IBAction func createCardFromCameraRoll(sender: UIBarButtonItem) {
        let vc = self.storyboard!.instantiateViewControllerWithIdentifier("CardCreationFromCameraRollNavigationController")
        self.moveToViewController(vc)
        self.highlightButton(sender)
    }
    
    @IBAction func createImageCardFromCamera(sender: UIBarButtonItem) {
        let vc = self.storyboard!.instantiateViewControllerWithIdentifier("ImageCardCreationNavigationController")
        self.moveToViewController(vc)
        self.highlightButton(sender)
    }

    @IBAction func createTextCard(sender: UIBarButtonItem) {
        let vc = self.storyboard!.instantiateViewControllerWithIdentifier("TextContentCreationController")
        self.moveToViewController(vc)
        self.highlightButton(sender)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        self.currentChildController = segue.destinationViewController
    }

    private func moveToViewController(viewController: UIViewController) {

        self.currentChildController?.removeFromParentViewController()

        self.addChildViewController(viewController)

        // remove any existing views
        for subview in self.containerView.subviews {
            subview.removeFromSuperview()
        }

        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.containerView.addSubview(viewController.view)

        // add visual constraints
        viewController.view.fillSuperview()

        viewController.didMoveToParentViewController(self)
        self.currentChildController = viewController
    }

    private func highlightButton(button: UIBarButtonItem) {
        for button in self.toolbar.items! {
            button.tintColor = UIColor.lightGrayColor()
        }
        button.tintColor = Constants.UIConstants.pinkColor
    }

}
