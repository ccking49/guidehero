//
//  AskEditCell.swift
//  GuideHero
//
//  Created by HC on 11/15/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya
import KMPlaceholderTextView


class AskCell: BaseTableViewCell, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {

    @IBOutlet var askedTableView: UITableView!
    @IBOutlet var askersTableView: UITableView!
    @IBOutlet var formatLabel: UILabel!
    @IBOutlet var joinButton: UIButton!
    @IBOutlet var prizeOriginalLabel: UILabel!
    @IBOutlet var prizeToJoinLabel: UILabel!
    
    var onJoinAsk: (() -> Void)!
    var content: ContentNode! {
        didSet {
            updateUI()
        }
    }
//    var isAnyone: Bool = true
//    var usernames = [[String: AnyObject]]()
//    var totalUsernames = [[String: AnyObject]]()
//    var currentWordIndex: Int = 0
//    var contentHeaderCell: ContentHeaderCell
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        askedTableView.delegate = self
        askedTableView.dataSource = self
        askersTableView.delegate = self
        askersTableView.dataSource = self
        let cellNib = UINib(nibName: "UserBasicCell", bundle: nil)
        askedTableView.registerNib(cellNib, forCellReuseIdentifier: "UserBasicCell")
        let cellNib1 = UINib(nibName: "UserBasicCell", bundle: nil)
        askersTableView.registerNib(cellNib1, forCellReuseIdentifier: "UserBasicCell")
        joinButton.roundView()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    // MARK: - @IBAction
    
    @IBAction func onJoinAsk(sender: UIButton) {
        /*
        sender.enabled = false
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(NetworkService.JoinAsk(deckId: self.content!.id), completion: { (result) in
            let JSON = Helper.validateResponse(result)
            if JSON != nil {
                
                // do reload
                var asked_users = self.content.asked_users
                asked_users.append(Session.sharedSession.user!)
                self.content.asked_users = asked_users
                self.onJoinAsk()
            }
            sender.enabled = true
        })
        */
        onJoinAsk()
    }

    // MARK: - Func
    
    func updateUI() {
        askedTableView.reloadData()
        askersTableView.reloadData()
        formatLabel.text = content.format
        let joinButtonTitle = content.isJoined ? "Unjoin The Ask" : "Join The Ask"
        joinButton.setTitle(joinButtonTitle, forState: .Normal)
        joinButton.enabled = !content.isAskCreator
        prizeOriginalLabel.text = String(content.prize_pool)
        prizeToJoinLabel.text = String(content.prize_to_join)
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.isEqual(askedTableView) {
            let count = content.asked_users.count
            return count
        } else {
            let count = content.joined_users.count
            return count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UserBasicCell") as! UserBasicCell
        if tableView.isEqual(askedTableView) {
            let user = content.asked_users[indexPath.row]
            cell.populateWithUser(user)
        } else {
            let user = content.joined_users[indexPath.row]
            cell.populateAskerCellWithUser(user)
        }
        return cell
    }

}
