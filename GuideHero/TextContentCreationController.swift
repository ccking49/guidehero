//
//  TextContentCreationController.swift
//  GuideHero
//
//  Created by Ignacio Rodrigo on 9/28/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya
import KMPlaceholderTextView

class TextContentCreationController: BasePostLoginController, UIPickerViewDataSource, UIPickerViewDelegate, UITextViewDelegate {

    // MARK: - Properties

    @IBOutlet var cardTextView: KMPlaceholderTextView!
    @IBOutlet var cardTitleTextField: UITextField!
    @IBOutlet var cardDescription: UITextField!
    @IBOutlet var speakerButton: UIButton!
    @IBOutlet var cancelButton: NSLayoutConstraint!
    @IBOutlet var doneButton: UIButton!
    @IBOutlet var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var cardView: UIView!

    let availableLanguageNames = ["None", "Japanese"]
    let availableLanguageCodes = ["", "ja-JP"]
    var selectedLanguageIndex: Int = 0
    var languagePickerTextField: UITextField! // Hidden text field presenting the language picker
    var languagePicker: UIPickerView!


    // MARK: - Actions

    @IBAction func selectLanguage(sender: AnyObject) {
        self.languagePickerTextField.becomeFirstResponder()
    }

    @IBAction func done(sender: AnyObject) {

        // validate data

        let cardText: String = self.cardTextView.text ?? ""
        if cardText.length == 0 {
            self.showSimpleAlert(withTitle: "Error", withMessage: "Card text is required")
            return
        }

        let cardTitle: String? = self.cardTitleTextField.text
        let cardDescription: String? = self.cardDescription.text
        var language: String? = self.availableLanguageCodes[self.selectedLanguageIndex]
        if language?.length == 0 {
            language = nil
        }

        // API call
        self.showProgressHUD()
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.CreateTextCard(cardTitle: cardTitle, cardDescription: cardDescription, cardText: cardText, language: language)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't create text card, please try again later")
                    self.hideProgressHUD()
                }
                else if let JSON = try? response.mapJSON() as! [String: String] {
                    if JSON["result"] == "success" {
                        self.showSuccessHUD()
                        self.clearData()
                    }
                    else {
                        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't create text card, please try again later")
                        self.hideProgressHUD()
                    }
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't parse response, please try again later")
                    self.hideProgressHUD()
                }
            case let .Failure(error):
                print("can't create text card: %@", error)
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't create text card, please try again later")
                self.hideProgressHUD()
            }
        }
    }

    @IBAction func cancel(sender: AnyObject) {
        self.clearData()
        self.tabBarController?.selectedIndex = Constants.UIConstants.learnTabIndex
    }

    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLanguagePicker()
        self.setupCardView()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.adjustTextViewVerticalAlignment()
        self.cardTextView.addObserver(self, forKeyPath: "contentSize", options: .New, context: nil)
    }

    override func viewWillDisappear(animated: Bool) {
        self.cardTextView.removeObserver(self, forKeyPath: "contentSize")
    }

    // MARK: - Private methods

    private func clearData() {
        self.cardTextView.text = nil
        self.cardTitleTextField.text = nil
        self.cardDescription.text = nil
        self.selectedLanguageIndex = 0
        self.languagePicker.selectRow(0, inComponent: 0, animated: false)
    }

    private func setupCardView() {
        self.cardTextView.placeholder = "Card text"
        self.cardTextView.placeholderColor = UIColor.lightGrayColor()
        self.cardView.layer.cornerRadius = 20
        self.cardView.layer.shadowColor = UIColor.blackColor().CGColor
        self.cardView.layer.shadowOffset = CGSizeMake(0, 0)
        self.cardView.layer.shadowOpacity = 0.5
        self.cardView.layer.shadowRadius = 10
    }

    // MARK: - KVO

    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if let textView = object as? UITextView where textView == self.cardTextView {
            self.adjustTextViewVerticalAlignment()
        }
    }

    // MARK: - Text view

    private func adjustTextViewVerticalAlignment() {
        self.textViewHeightConstraint.constant = min(self.cardTextView.contentSize.height + 20, self.cardTextView.superview!.bounds.height)
        self.cardTextView.layoutIfNeeded()
    }
    
    func textViewDidChange(textView: UITextView) {
        cardTitleTextField.text = textView.text
    }
    
    // MARK: - Language picker

    private func setupLanguagePicker() {

        languagePicker = UIPickerView()
        languagePicker.dataSource = self
        languagePicker.delegate = self
        languagePicker.showsSelectionIndicator = true

        let doneButton = UIButton(type: .Custom)
        doneButton.setTitle("Done", forState: .Normal)
        doneButton.backgroundColor = UIColor.blackColor()
        doneButton.addTarget(self, action: #selector(dismissLanguagePicker), forControlEvents: .TouchUpInside)
        doneButton.sizeToFit()

        self.languagePickerTextField = UITextField()
        self.languagePickerTextField.hidden = true
        self.languagePickerTextField.inputView = languagePicker
        self.languagePickerTextField.inputAccessoryView = doneButton
        self.languagePickerTextField.delegate = self

        self.view.addSubview(self.languagePickerTextField)
    }

    func dismissLanguagePicker(sender: UIBarButtonItem) {
        self.languagePickerTextField.resignFirstResponder()
    }

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.availableLanguageNames.count
    }

    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.availableLanguageNames[row]
    }

    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedLanguageIndex = row
    }
    
}
