//
//  LoopTutorialController.swift
//  GuideHero
//
//  Created by Ignacio Rodrigo on 10/27/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

protocol LoopTutorialControllerDelegate: class {
    func didCompleteTutorial()
}

class LoopTutorialController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    // MARK: - Constants

    static let pageCount = 5

    // MARK: - Properties

    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var dismissTutorialButton: UIButton!

    weak var tutorialDelegate: LoopTutorialControllerDelegate!

    // TODO: find a more elegant implementation, lazy var doesn't work because createContentControllers needs a reference to self.storyboard
    private var contentControllersCache: [UIViewController]?
    private var contentControllers: [UIViewController] {
        get {
            if self.contentControllersCache == nil {
                self.contentControllersCache = self.createContentControllers()
            }
            return self.contentControllersCache!
        }
    }

    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupPageController()
    }

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? UIPageViewController {
            vc.setViewControllers([self.contentControllers.first!], direction: .Forward, animated: false, completion: nil)
            vc.dataSource = self
            vc.delegate = self
        }
    }

    // MARK: - Actions

    @IBAction func dismissTutorial(sender: AnyObject) {
        self.tutorialDelegate.didCompleteTutorial()
    }

    // MARK: - Private methods

    private func updateDismissButtonTitle() {
        let isLastPage = (self.pageControl.currentPage == LoopTutorialController.pageCount - 1)
        let dismissButtonTitle = isLastPage ? "You're ready" : "Got it! Don't show me next time"
        self.dismissTutorialButton.setTitle(dismissButtonTitle, forState: .Normal)
    }

    private func setupPageController() {
        self.pageControl.numberOfPages = self.contentControllers.count
    }

    private func createContentControllers() -> [UIViewController] {

        var viewControllers: [UIViewController] = []

        for i in 1...LoopTutorialController.pageCount {
            let vc = self.storyboard!.instantiateViewControllerWithIdentifier("LoopTutorialPage\(i)")
            vc.view.backgroundColor = UIColor.clearColor()
            viewControllers.append(vc)
        }

        return viewControllers
    }

    // MARK: - UIPageViewControllerDataSource

    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let nextIndex = self.contentControllers.indexOf(viewController)! + 1
        return (nextIndex < self.contentControllers.count) ? self.contentControllers[nextIndex] : nil
    }

    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let previousIndex = self.contentControllers.indexOf(viewController)! - 1
        return (previousIndex >= 0) ? self.contentControllers[previousIndex] : nil
    }

    // MARK: - UIPageViewControllerDelegate

    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            let newViewController = pageViewController.viewControllers!.first!
            self.pageControl.currentPage = self.contentControllers.indexOf(newViewController)!
            self.updateDismissButtonTitle()
        }
    }

}
