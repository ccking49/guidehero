//
//  CardDetailButton.swift
//  GuideHero
//
//  Created by Promising Change on 03/01/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit

protocol CardDetailButtonDelegate {
    func didTapOnCardActionButton(buttonID: Int)
}

public class CardDetailButton: UIView {
    
    var cardDetailButtonInfo: CardDetailButtonInfo?

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var btAction: UIButton!
    @IBOutlet weak var vwLine: UIView!
    @IBOutlet weak var lbActionTitle: UILabel!
    @IBOutlet weak var constraintLineLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintLineTrailing: NSLayoutConstraint!
    
    var delegate: CardDetailButtonDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configureView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.configureView()
    }
    
    /// Configures & Initializes the view.
    private func configureView() {
        NSBundle.mainBundle().loadNibNamed("CardDetailButton", owner: self, options: nil)
        guard let content = contentView else { return }
        content.frame = self.bounds
        content.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.addSubview(content)
        
        self.setNeedsLayout()
    }
    
    public func bindItem(item: CardDetailButtonInfo) {
        self.cardDetailButtonInfo = item
        
        btAction.setImage(item.buttonImage, forState: .Normal)
        if let _ = item.buttonSelectedImage {
            btAction.setImage(item.buttonSelectedImage, forState: .Selected)
        } else {
            btAction.setImage(item.buttonImage, forState: .Selected)
        }
        btAction.selected = false
        
        lbActionTitle.text = item.title
        lbActionTitle.textColor = item.tintColor
        lbActionTitle.alpha = 0
        
        vwLine.backgroundColor = item.tintColor
        
        constraintLineLeading.constant = 0
        constraintLineTrailing.constant = contentView.bounds.width
        layoutIfNeeded()
    }
    
    public func selectButton() {
        btAction.selected = true
        lbActionTitle.alpha = 1
        
        constraintLineLeading.constant = 0
        constraintLineTrailing.constant = 0
        layoutIfNeeded()
    }
    
    public func deselectButton() {
        btAction.selected = false
        lbActionTitle.alpha = 0
        
        constraintLineLeading.constant = 0
        constraintLineTrailing.constant = contentView.bounds.width
        layoutIfNeeded()
    }
    
    public func highlightButton() {
        btAction.selected = true
        lbActionTitle.alpha = 1
    }
    
    public func dehighlightButton() {
        btAction.selected = false
        lbActionTitle.alpha = 0
    }
    
    /// Transition from Right to Left; Right is this
    /// Transition is being made from this to left.
    /// |--------==|========--|
    public func transitToLeft(withProgressPercentage progressPercentage: CGFloat) {
        constraintLineLeading.constant = 0
        constraintLineTrailing.constant = contentView.bounds.width * progressPercentage
        
        layoutIfNeeded()
    }
    
    /// Transition is being made by another from the left side to this.
    /// |--------==|========--|
    public func transitFromLeft(withProgressPercentage progressPercentage: CGFloat) {
        constraintLineLeading.constant = contentView.bounds.width * (1 - progressPercentage)
        constraintLineTrailing.constant = 0
        
        layoutIfNeeded()
    }
    
    /// Transition from Left to Right; Left is this
    /// Transition is being made from this to right.
    /// |--========|==--------|
    public func transitToRight(withProgressPercentage progressPercentage: CGFloat) {
        constraintLineLeading.constant = contentView.bounds.width * progressPercentage
        constraintLineTrailing.constant = 0
        
        layoutIfNeeded()
    }
    
    /// Transition is being made by another from the right side to this.
    /// |--========|==--------|
    public func transitFromRight(withProgressPercentage progressPercentage: CGFloat) {
        constraintLineLeading.constant = 0
        constraintLineTrailing.constant = contentView.bounds.width * (1 - progressPercentage)
        
        layoutIfNeeded()
    }
    
    // MARK: - Action
    
    @IBAction func didTapOnActionButton(sender: AnyObject) {
        delegate?.didTapOnCardActionButton((cardDetailButtonInfo?.Id)!)
    }
    
}
