//
//  CardDetailGiveVC.swift
//  GuideHero
//
//  Created by Promising Change on 23/12/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AlamofireImage
import Moya

class CardDetailGiveVC: CardSubPageBaseVC, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Variables
    
    @IBOutlet weak var tbContent: UITableView!
    
    @IBOutlet weak var footerView : UIView!
    
    
    let tbHeaderView: UIView!
    var aryTableHeader: [String]
    let empowerString: String = "Go empower some people! 😉🙌"
    let inspireString: String = "Still waiting! 💫 Be the first one! ☝️"
    
    var content: ContentNode? {
        didSet {
            if isViewLoaded() {
                tbContent.reloadData()
                self.view.layoutIfNeeded()
            }
        }
    }
    
    // MARK: - View Lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        tbHeaderView = UIView()
        aryTableHeader = ["Your Give", "Givers"]
        
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        refreshData()
        tbContent.setContentOffset(CGPointMake(tbContent.contentOffset.x, tbContent.contentOffset.y - 1), animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        scrollView = tbContent as UIScrollView
        
        configureView()
        refreshData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Methods
    
    func configureView() {
        tbHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 540)
        
        let contentNib = UINib(nibName: "ContentTableCell", bundle: nil)
        tbContent.registerNib(contentNib, forCellReuseIdentifier: "ContentTableCell")
        let basicNib = UINib(nibName: "UserBasicCell", bundle: nil)
        tbContent.registerNib(basicNib, forCellReuseIdentifier: "UserBasicCell")
        
        let cellNib = UINib(nibName: "CardDetailTableViewCell", bundle: nil)
        tbContent.registerNib(cellNib, forCellReuseIdentifier: "CardDetailTableViewCell")
        
        tbContent.rowHeight = UITableViewAutomaticDimension
        tbContent.estimatedRowHeight = Constants.UIConstants.contentCellHeight
        tbContent.separatorStyle = .None
        
        tbContent.tableHeaderView = tbHeaderView
        
        footerView.layer.cornerRadius = 20
        footerView.layer.shadowRadius = 5
        footerView.layer.shadowOpacity = 0.5
        footerView.layer.shadowOffset = CGSizeZero
        footerView.layer.shadowColor = UIColor.blackColor().CGColor
        footerView.clipsToBounds = true
        
        self.view.setNeedsLayout()
    }
    
    func refreshData() {
        tbContent.reloadData()
    }
    
    // MARK: - UITableViewDelegate & UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return self.aryTableHeader.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.aryTableHeader[section] as String
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let contentHeight: CGFloat = 48
        
        let headerView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: self.tbContent.bounds.width, height: contentHeight)))
        let headerContentView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: self.tbContent.bounds.width, height: 48)))
        
        let label = UILabel(frame: CGRect(x: 30, y: 0, width: self.tbContent.frame.width - 60, height: 48))
        label.text = aryTableHeader[section] as String
        label.textAlignment = NSTextAlignment.Left
        label.textColor = UIColor.blackColor()
        label.font = Helper.proximaNova("Bold", font: 20)
        
        headerView.clipsToBounds = false
        headerView.backgroundColor = UIColor.whiteColor()
        headerContentView.backgroundColor = UIColor.colorFromRGB(redValue: 0, greenValue: 0, blueValue: 0, alpha: 0.1)
        
        headerContentView.addSubview(label)
        headerView.addSubview(headerContentView)
        
        return headerView
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 49
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if self.content!.myGives.count > 0 {
                
                return 2
                
            }else{
                return 2
            }
        
            
        case 1:
            
            if self.content!.givers.count == 0 {
                return 1
            }else{
                return self.content!.givers.count+1
            }
            
        default:
            return 0
        }
    }
    
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let section = indexPath.section
        let row = indexPath.row
        
        if (section == 0) {
            
            if self.content!.myGives.count != 0 {
                
                // Your Give
                if (row == 1) {
                    // Submit cell
                    return 70
                } else {
                    // Give cell
                    return 216
                }
            }else{
                // Your Give
                if (row == 0) {
                    // Empower cell
                    return 44
                } else {
                    // Submit cell
                    return 70
                }
            }
            
        } else if (section == 1) {
            // Givers
            
            if self.content!.givers.count == 0 {
                return 64
            }else{
                
                if (row == 0) {
                    return 12.5
                }else{
                    return 30
                }
                
            }
            
        }
        
        return 0
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        
        var tbCell : UITableViewCell!
        
        if (section == 0) {
            
            if self.content!.myGives.count != 0 {
                
                // Your Give
               if (row == 1) {
                    // Submit cell
                    let cell = tableView.dequeueReusableCellWithIdentifier("GiveSubmitCell", forIndexPath: indexPath) as! GiveSubmitCell
                    
                    cell.btSubmit.addTarget(self, action: #selector(handleSubmit(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                    cell.btSubmit.roundView()
                    
                    let isGiver = self.content!.isGiver
                    cell.btSubmit.enabled = true
                    if isGiver == 2 {
                        cell.btSubmit.setTitle("Edit Submission", forState: .Normal)
                        //            cell.btSubmit.enabled = false
                    } else if isGiver == 1 {
                        cell.btSubmit.setTitle("Edit Submission", forState: .Normal)
                    } else {
                        cell.btSubmit.setTitle("Submit", forState: .Normal)
                    }
                    
                    cell.setNeedsLayout()
                    cell.layoutIfNeeded()
                    
                    tbCell = cell
               } else{
                
                    let cell = tableView.dequeueReusableCellWithIdentifier("CardDetailTableViewCell", forIndexPath: indexPath) as! CardDetailTableViewCell
                
                    cell.content = self.content!.myGives[row]
                
                    if self.content!.myGives.count > 1 {
                        cell.cardView.backView1.hidden = false
                        cell.cardView.backView2.hidden = false
                    }else{
                        cell.cardView.backView1.hidden = true
                        cell.cardView.backView2.hidden = true
                    }
                
                    cell.btnProfilePhoto.tag = row
                    cell.btnProfileName.tag = row
                    cell.btnProfileName.addTarget(self, action: #selector(CardDetailGiveVC.onGotoProfilePage(_:)), forControlEvents: .TouchUpInside)
                    cell.btnProfilePhoto.addTarget(self, action: #selector(CardDetailGiveVC.onGotoProfilePage(_:)), forControlEvents: .TouchUpInside)
                
                
                    cell.imgSeparate.hidden = false
                    
                    cell.separateView.hidden = true
                    cell.imgSeparate.layer.shadowRadius = 2
                    
                    cell.imgSeparate.layer.shadowOffset = CGSizeZero
                    cell.imgSeparate.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
                    
                    cell.cardView.tag = indexPath.row
                    let singleTap = UITapGestureRecognizer(target: self, action: #selector(CardDetailPlayVC.showFullScreen))
                    cell.cardView.addGestureRecognizer(singleTap)
                    
                    cell.layer.shadowRadius = 1
                    cell.layer.shadowOffset = CGSizeZero
                    cell.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
                
                    cell.giveImage.hidden = false
                
                    cell.tag = indexPath.row
                
                    let childTap = UITapGestureRecognizer(target: self, action: #selector(CardDetailGiveVC.showChildScreen))
                    cell.addGestureRecognizer(childTap)
                
                    tbCell = cell
                }
                
            }else{
                // Your Give
                if (row == 0) {
                    // Empower cell
                    let cell = tableView.dequeueReusableCellWithIdentifier("GiveSubHeaderCell", forIndexPath: indexPath) as! GiveSubHeaderCell
                    
                    cell.lbTitle.text = empowerString
                    
                    cell.setNeedsLayout()
                    cell.layoutIfNeeded()
                    
                    tbCell = cell
                } else {
                    // Submit cell
                    let cell = tableView.dequeueReusableCellWithIdentifier("GiveSubmitCell", forIndexPath: indexPath) as! GiveSubmitCell
                    
                    cell.btSubmit.addTarget(self, action: #selector(handleSubmit(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                    cell.btSubmit.roundView()
                    
                    let isGiver = self.content!.isGiver
                    cell.btSubmit.enabled = true
                    if isGiver == 2 {
                        cell.btSubmit.setTitle("Edit Submission", forState: .Normal)
                        //            cell.btSubmit.enabled = false
                    } else if isGiver == 1 {
                        cell.btSubmit.setTitle("Edit Submission", forState: .Normal)
                    } else {
                        cell.btSubmit.setTitle("Submit", forState: .Normal)
                    }
                    
                    cell.setNeedsLayout()
                    cell.layoutIfNeeded()
                    
                    tbCell = cell
                }
            }
            
        } else if (section == 1) {
            // Givers
            
            if self.content?.givers.count == 0 {
                // Inspire cell
                let cell = tableView.dequeueReusableCellWithIdentifier("GiveSubHeaderCell", forIndexPath: indexPath) as! GiveSubHeaderCell
                
                cell.lbTitle.text = inspireString
                
                cell.setNeedsLayout()
                cell.layoutIfNeeded()
                
                tbCell = cell
            }else{
                // Giver cell
                
                if (row == 0) {
                    return tableView.dequeueReusableCellWithIdentifier("Blank")!
                }else{
                    let cell = tableView.dequeueReusableCellWithIdentifier("UserBasicCell") as! UserBasicCell
                    let user = self.content!.givers[row-1]
                    cell.populateWithUser(user)
                    cell.setNeedsLayout()
                    cell.layoutIfNeeded()
                    
                    tbCell = cell
                }
                
                
                
            }
            
        }
        
        return tbCell
    }
    func onGotoProfilePage(sender : UIButton){
        let profileVC = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "TabProfileController") as! TabProfileController
        
        let cData : ContentNode = self.content!.myGives[sender.tag]
        profileVC.userInfo = cData.creatorInfo
        profileVC.isShow = true
        profileVC.modalPresentationStyle = .OverFullScreen
        self.presentViewController(profileVC, animated: true, completion: nil)
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Do Nothing
        print("sss")
    }

    // MARK: - UIScrollViewDelegate
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
    }
    
    // MARK: - Handlers
    func addSubmissionCard(){
        let alertController = UIAlertController(title: "Select your submission", message: nil, preferredStyle: .ActionSheet)
        
        let draftAction = UIAlertAction(title: "Choose From Drafts", style: .Default) { (action) in
            self.chooseFromDraft()
        }
        alertController.addAction(draftAction)
        
        let createAction = UIAlertAction(title: "Create New Card", style: .Default) { (action) in
            self.createNewCard()
        }
        alertController.addAction(createAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
        }
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true) {
        }
    }
    func handleSubmit(sender: AnyObject) {
        if self.content?.isGiver == 0 {
            
            addSubmissionCard()
            
        } else {
            
            let alertController = UIAlertController(title: "Edit Submission", message: nil, preferredStyle: .ActionSheet)
            
            let addAction = UIAlertAction(title: "Add Item", style: .Default) { (action) in
                self.addSubmissionCard()
            }
            alertController.addAction(addAction)
            
            let removeAction = UIAlertAction(title: "Remove Item", style: .Default) { (action) in
                self.onShowRemoveAction()
            }
            alertController.addAction(removeAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            }
            alertController.addAction(cancelAction)
            
            self.presentViewController(alertController, animated: true) {
            }
            
        }
    }
    
    
    
    func chooseFromDraft() {

        let navigationController = UINavigationController()
        navigationController.navigationBarHidden = true
        let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "SubmitCardViewController") as! SubmitCardViewController
        vc.isType = 0
        vc.content = self.content
        vc.currentContent = self.content
        vc.submittedCallback = { rContent in
            // Card submitted here
            self.content = rContent
            self.tbContent.reloadData()
            vc.dismissViewControllerAnimated(true, completion: nil)
        }

        
        
        navigationController.viewControllers = [vc]
        presentViewController(navigationController, animated: true, completion: nil)
        
    }
    func onShowRemoveAction(){
        
//        let navigationController = UINavigationController()
//        navigationController.navigationBarHidden = true
        let vc = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "SubmissionRemovePageVC") as! SubmissionRemovePageVC
        vc.isType = 1
 
        var newContent : ContentNode!
        newContent = self.content!.myGives.first
        newContent.children = []
        var gives = [ContentNode]()
        
        var subContent = [ContentNode]()
        subContent = self.content!.myGives
        gives.append(self.content!)
        for card in subContent {
            if card.creator == Session.sharedSession.user?.username && card.isAnswer && card.id != newContent.id {
                gives.append(card)
            }
        }
        
        newContent.children = gives
        
        vc.content = newContent
        vc.currentContent = self.content
        vc.isBackType = 1
        vc.submittedCallback = { rContent in
            // Card submitted here
            self.content = rContent
            
            self.tbContent.reloadData()
            vc.dismissViewControllerAnimated(true, completion: nil)
        }
//        navigationController.viewControllers = [vc]
        
        presentViewController(vc, animated: true, completion: nil)
        
    }
    
    func onCancelSubmission(){
        // Cancel Submission
        let gives = self.content?.myGives
        if gives?.count == 0 { return }
        let cardId = gives?.first?.id
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(NetworkService.CancelSubmission(cardId: cardId!), completion: { (result) in
            let JSON = Helper.validateResponse(result, sender: self)
            if JSON != nil {
                let rContent = ContentNode(dictionary: JSON!["deck"] as! [String: AnyObject])
                self.content = rContent
                self.tbContent.reloadData()
            } else {
                self.showSimpleAlert(withTitle: "", withMessage: "Failed to submit card.")
            }
        })
    }
    
    func createNewCard() {
        let navigationController = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "CameraFunctionNavViewController") as! UINavigationController
        let cameraFunctionViewController = navigationController.viewControllers.first as! CameraFunctionViewController
        cameraFunctionViewController.decId = (self.content?.id)!
        
        cameraFunctionViewController.submittedCallback = { rContent in
            // Card submitted here
            
            if let _ = rContent {
                self.content = rContent
                self.tbContent.reloadData()
            }
            
            navigationController.dismissViewControllerAnimated(false, completion: nil)
        }
        presentViewController(navigationController, animated: true, completion: nil)
        
    }
    func showFullScreen(gestureRecognizer: UIGestureRecognizer) {
        let index = gestureRecognizer.view!.tag
        
        let fullScreenViewController = UIStoryboard.loadViewController(storyboardName: "Main", viewControllerIdentifier: "FullScreenViewController") as! FullScreenViewController
        
        fullScreenViewController.content = self.content?.myGives[index]
        
        fullScreenViewController.deckIndex = index
        
        fullScreenViewController.modalPresentationStyle = .OverFullScreen
        self.presentViewController(fullScreenViewController, animated: true, completion: nil)
        
    }
    func showChildScreen(gestureRecognizer: UIGestureRecognizer) {
        let nIndex = gestureRecognizer.view!.tag
        
        if (content?.myGives.count ?? 0) != 0 {
            
            let cardDetailVC = UIStoryboard.loadViewController(storyboardName: "CardDetail", viewControllerIdentifier: "CardDetailPageVC") as! CardDetailPageVC
            
            var newContent : ContentNode!
            newContent = self.content!.myGives.first
            newContent.children = []
            var gives = [ContentNode]()
            
            var subContent = [ContentNode]()
            subContent = self.content!.myGives
            for card in subContent {
                if card.creator == Session.sharedSession.user?.username && card.isAnswer && card.id != newContent.id {
                    gives.append(card)
                }
            }
            
            newContent.children = gives
            
            cardDetailVC.content = newContent
            
            cardDetailVC.contentIndex = nIndex
            presentViewController(cardDetailVC, animated: true, completion: nil)
            
        }
        
    }
}
