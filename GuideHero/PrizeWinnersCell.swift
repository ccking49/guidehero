//
//  AskEditCell.swift
//  GuideHero
//
//  Created by HC on 11/15/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya
import KMPlaceholderTextView


class PrizeWinnersCell: BaseTableViewCell, UITextViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var titleView : UIView!
    @IBOutlet weak var rankView : UIView!
    @IBOutlet weak var lblRank : UILabel!
    @IBOutlet weak var imgPhoto : UIImageView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblLikers : UILabel!
    @IBOutlet weak var lblPoint : UILabel!
    
//    var contentHeaderCell: ContentHeaderCell
    var content: ContentNode! {
        didSet {
            updateUI()
        }
    }
    
    func updateUI() {
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: - @IBAction
    
    
}
