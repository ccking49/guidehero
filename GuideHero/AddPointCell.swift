//
//  AddPointCell.swift
//  GuideHero
//
//  Created by forever on 12/15/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

class AddPointCell: BaseTableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnGo: UIButton!
    @IBOutlet weak var btnBorder : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.btnBorder.layer.borderColor = UIColor.groupTableViewBackgroundColor().CGColor
        self.btnBorder.layer.borderWidth = 2.0
        self.btnBorder.layer.cornerRadius = 5
        self.btnBorder.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
