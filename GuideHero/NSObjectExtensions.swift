//
//  NSObjectExtensions.swift
//  GuideHero
//
//  Created by Abhishek Sengar on 02/09/2016.
//  Copyright © 2016 Twilio. All rights reserved.
//

import Foundation

extension NSObject {
    
    class func className() -> String {
        return NSStringFromClass(self).componentsSeparatedByString(".").last!
    }
    
    class func reuseIdentifier() -> String {
        return "\(className())Identifier"
    }
    
    class func storyboardIdentifier() -> String {
        return reuseIdentifier()
    }
}
