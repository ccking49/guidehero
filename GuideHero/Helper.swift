//
//  File.swift
//  GuideHero
//
//  Created by Ascom on 10/28/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import Foundation
import Result
import Moya

struct Helper {
    
    static var drafts = [ContentNode]() // Used as temporary storage for drafts when moving cards
    
    static func validateResponse (result: Result<Moya.Response, Moya.Error>, sender: UIViewController) -> [String: AnyObject]? {
        switch result {
        case let .Success(response):
            if response.statusCode != 200 {
                sender.hideProgressHUD()
                return nil
            }
            if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                sender.hideProgressHUD()
                return JSON
            }
            else {
                sender.hideProgressHUD()
                return nil
            }
        case .Failure(_):
            sender.hideProgressHUD()
            return nil
        }
    }
    
    static func validateResponse (result: Result<Moya.Response, Moya.Error>) -> [String: AnyObject]? {
        switch result {
        case let .Success(response):
            if response.statusCode != 200 {
                let str = String(data: response.data, encoding: NSASCIIStringEncoding)
                return nil
            }
            if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                return JSON
            } else {
                return nil
            }
        case .Failure(_):
            return nil
        }
    }
    
    static func imageWithColor (color: UIColor) -> UIImage {
        let rect = CGRect(x: 0,y: 0,width: 10,height: 10)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()!
        CGContextSetFillColorWithColor(context, color.CGColor)
        CGContextFillRect(context, rect)
        let image=UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    static func colorFromRGB (rgbValue: Int, alpha: CGFloat) -> UIColor {
        return UIColor.colorFromRGB(redValue: ((CGFloat)((rgbValue & 0xFF0000) >> 16))/255.0, greenValue: ((CGFloat)((rgbValue & 0xFF00) >> 8))/255.0, blueValue: ((CGFloat)(rgbValue & 0xFF))/255.0, alpha: alpha)
    }

    @available(*, deprecated, message = "Use UIFont(appFont, size)")
    static func proximaNova (weight: String, font: CGFloat) -> UIFont {
        return UIFont(name: "ProximaNova-"+weight, size: font)!
    }
}
