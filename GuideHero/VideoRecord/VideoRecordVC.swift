//
//  VideoRecordVC.swift
//  GuideHero
//
//  Created by Star on 12/31/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import SCRecorder

class VideoRecordVC: BasePostLoginController {
    
    private var recorder: SCRecorder!
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var switchButton: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var zoomSlider: ZoomSlider!
    @IBOutlet weak var imgBackground : UIImageView!
    @IBOutlet weak var btnClose : UIButton!
    @IBOutlet weak var btnLibrary : UIButton!
    @IBOutlet weak var btnCapture : UIButton!
    weak var focusView: FocusView!
    
    var filePath: NSURL?
    
    // MARK: -
//    deinit {
//        recorder.previewView = nil
//    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .Portrait
    }
    
    // MARK: view life-cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        filePath = documentsURL.URLByAppendingPathComponent("temp.mov")
        do {
            try NSFileManager.defaultManager().removeItemAtURL(filePath!)
        }
        catch {
            
        }
        
        recorder = SCRecorder.sharedRecorder()
        recorder.captureSessionPreset = SCRecorderTools.bestCaptureSessionPresetCompatibleWithAllDevices()
        recorder.delegate = self
        recorder.autoSetVideoOrientation = false
        recorder.flashMode = .On
        recorder.previewView = previewView
        recorder.initializeSessionLazily = false
        recorder.autoFocusAtPoint(CGPointMake(0.5, 0.5))
        
        let focusView = FocusView()
        focusView.frame = CGRectMake(0, 0, 60, 60)
        focusView.backgroundColor = UIColor.clearColor()
        focusView.hidden = true
        previewView.addSubview(focusView)
        self.focusView = focusView
        
        do {
            try recorder.prepare()
        } catch {
            // handle exception
        }
        
        timeLabel.font = Helper.proximaNova("Bold", font: 35)
        
        imgBackground.alpha = 0.1
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().statusBarHidden = true
        self.tabBarController?.tabBar.hidden = true
        
        prepareSession()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.sharedApplication().statusBarHidden = false
        self.tabBarController?.tabBar.hidden = false
        
        recorder.stopRunning()
        recorder.unprepare()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        recorder.startRunning()
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        recorder.previewViewFrameChanged()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: private methods
    func setupZoomSlider() {
        let maxZoom = recorder.maxVideoZoomFactor
        zoomSlider.minimumValue = 1
        zoomSlider.maximumValue = min(maxZoom, 6)
        zoomSlider.value = recorder.videoZoomFactor
    }
    
    func prepareSession() {
        
        if recorder.session == nil {
            let session = SCRecordSession()
            session.fileType = AVFileTypeQuickTimeMovie
            recorder.session = session
        }
        
        updateTimeLabel()
    }
    
    func saveSession(session: SCRecordSession, completionHandler: ((String?, NSError?) -> Void)? = nil) {
        
        session.mergeSegmentsUsingPreset(AVAssetExportPresetHighestQuality) { (url, error) in
            
            self.timeLabel.textColor = UIColor.whiteColor()
            
            self.filePath = url
            
            self.recorder.session?.removeAllSegments()
            self.updateTimeLabel()
            self.recordButton.selected = false
            self.recordButton.userInteractionEnabled = true
            
            self.hideProgressHUD()
            
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            let videoCardCreationViewController = mainStoryBoard.instantiateViewControllerWithIdentifier("VideoCardCreationViewController") as! VideoCardCreationViewController
            videoCardCreationViewController.videoUrl = self.filePath!
            videoCardCreationViewController.prevViewController = self

            videoCardCreationViewController.modalPresentationStyle = .OverFullScreen
            self.presentViewController(videoCardCreationViewController, animated: true, completion: nil)
            
//            self.addChildViewController(videoCardCreationViewController);
//            self.view.addSubview(videoCardCreationViewController.view);
        
            /*
            SCSaveToCameraRollOperation().saveVideoURL(url, completion: { (path, saveError) in
                completionHandler?(path, saveError)
 
            })
            */
        }
    }
    
    func updateTimeLabel() {
        
        var currentTime = kCMTimeZero
        
        if let session = recorder.session {
            currentTime = session.duration
        }
        
        let duration = Int(CMTimeGetSeconds(currentTime))
        let min = duration / 60
        let sec = duration % 60
        
        timeLabel.text = String(format: "%02d:%02d", min, sec)
    }
    
    func animateFocusAtPoint(point: CGPoint) {
        
        focusView.center = point
        focusView.alpha = 0
        focusView.hidden = false
        
        UIView.animateWithDuration(
            0.4,
            animations: {
                self.focusView.alpha = 1
            },
            completion: { finished in
                UIView.animateWithDuration(
                    0.4,
                    animations: {
                        self.focusView.alpha = 0
                    },
                    completion: { finished in
                        
                })
        })
    }
    
    // MARK: actions
    @IBAction func toggleRecord(button:UIButton!) {
        
        if recorder.isRecording {
            button.userInteractionEnabled = false
            if let image = UIImage(named: "shutterOff") {
                button.setImage(image, forState: .Normal)
            }
            
            menuAppear()
            
            self.showProgressHUD()
            
            recorder.pause({
                // do UI changge
                self.saveSession(self.recorder.session!, completionHandler: { (path, error) in
                    
                    
                })
            })
        } else {
            
            menuDisappear()
            
            timeLabel.textColor = UIColor.yellowColor()
            
            recorder.record()
            if let image = UIImage(named: "shutterOn") {
                button.setImage(image, forState: .Normal)
            }
            button.selected = true
        }
    }
    
    func menuAppear(){
        
        btnClose.hidden = false
        btnCapture.hidden = false
        btnLibrary.hidden = false
    }
    
    func menuDisappear(){
        
        btnClose.hidden = true
        btnCapture.hidden = true
        btnLibrary.hidden = true
    }
    
    @IBAction func toggleFlash(button: UIButton!) {
        
        if recorder.flashMode == .Light {
            recorder.flashMode = .Off
            button.selected = false
        } else {
            recorder.flashMode = .Light
            button.selected = true
        }
    }
    
    @IBAction func toggleSwitch(button: UIButton!) {
        recorder.switchCaptureDevices()
        setupZoomSlider()
    }
    
    @IBAction func onZoomSliderChanged(slider: ZoomSlider) {
        recorder.videoZoomFactor = slider.value
    }
    
    @IBAction func onZoomSliderEnded(slider: ZoomSlider) {
        recorder.autoFocusAtPoint(CGPoint(x: 0.5, y: 0.5))
    }
    
    @IBAction func toggleZoomIn(button: UIButton!){
        
        zoomSlider.value += 1
        recorder.videoZoomFactor = zoomSlider.value
        setupZoomSlider()
        
    }
    
    @IBAction func toggleZoomOut(button: UIButton!){
        
        zoomSlider.value -= 1
        recorder.videoZoomFactor = zoomSlider.value
        setupZoomSlider()
        
    }
    
    @IBAction func toggleClose(button:UIButton){
        
        if let image = UIImage(named: "shutterOff") {
            recordButton.setImage(image, forState: .Normal)
        }
        
        recorder.pause({
            // do UI changge
            self.recorder.session?.removeAllSegments()
            self.updateTimeLabel()
            
            self.timeLabel.textColor = UIColor.whiteColor()
            
            self.recordButton.selected = false
            
            
            
            self.tabBarController?.selectedIndex = Constants.UIConstants.learnTabIndex
        })
        
        
    }
    
    @IBAction func onTapFocus(gesture: UIGestureRecognizer!) {
        let location = gesture.locationInView(self.view)
        let pos = recorder.convertToPointOfInterestFromViewCoordinates(location)
//        let pos = CGPointMake(location.x / previewView.bounds.width, location.y / previewView.bounds.height)
        recorder.continuousFocusAtPoint(pos)
    }
    
        
}

// MARK: - recorder delegate
extension VideoRecordVC : SCRecorderDelegate {
    
    func recorder(recorder: SCRecorder, didCompleteSession session: SCRecordSession) {
        saveSession(session)
    }
    
    func recorder(recorder: SCRecorder, didAppendVideoSampleBufferInSession session: SCRecordSession) {
        updateTimeLabel()
    }
    
    func recorder(recorder: SCRecorder, didInitializeVideoInSession session: SCRecordSession, error: NSError?)  {
        setupZoomSlider()
    }
    
    func recorderDidStartFocus(recorder: SCRecorder) {
        let poi = recorder.focusPointOfInterest
        let poiInView = recorder.convertPointOfInterestToViewCoordinates(poi)
        animateFocusAtPoint(poiInView)
    }
    
   
}

