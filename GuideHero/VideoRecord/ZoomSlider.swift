//
//  ZoomSlider.swift
//  GuideHero
//
//  Created by Star on 12/30/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

class ZoomSlider: UIControl {
    
    var trackTint = UIColor.whiteColor().colorWithAlphaComponent(0.5) {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var thumbSolidTint = UIColor.whiteColor() {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var trackWidth: CGFloat = 4 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var thumbRadius: CGFloat = 10 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var continuous = true
    
    var value: CGFloat = 0 {
        didSet {
            value = min(max(value, minimumValue), maximumValue)
            setNeedsDisplay()
        }
    }
    
    var minimumValue: CGFloat = 0 {
        didSet {
            if value < minimumValue {
                value = minimumValue
            }
            setNeedsDisplay()
        }
    }
    
    var maximumValue: CGFloat = 1 {
        didSet {
            if value > maximumValue {
                value = maximumValue
            }
            setNeedsDisplay()
        }
    }
    
    private var prevPoint: CGPoint?
    
    // MARK: tracking events
    func mapPointToValue(point:CGPoint, sendEvent: Bool = true) {
        let val = max(min((point.x - thumbRadius) / (self.bounds.size.width - thumbRadius * 2), 1), 0)
        self.value = val * (maximumValue - minimumValue) + minimumValue
        if sendEvent {
            sendActionsForControlEvents(.ValueChanged)
        }
    }
    
    override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        prevPoint = touch.locationInView(self)
        mapPointToValue(prevPoint!, sendEvent: continuous)
        return true
    }
    
    override func continueTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        prevPoint = touch.locationInView(self)
        mapPointToValue(prevPoint!, sendEvent: continuous)
        return true
    }
    
    override func endTrackingWithTouch(touch: UITouch?, withEvent event: UIEvent?) {
        if let touch = touch {
            mapPointToValue(touch.locationInView(self), sendEvent: true)
        } else if let pt = prevPoint {
            mapPointToValue(pt, sendEvent: true)
        }
        prevPoint = nil
    }
    
    // MARK: Drawing
    override func drawRect(rect: CGRect) {
        if let context = UIGraphicsGetCurrentContext() {
            drawInContext(context)
        }
    }
    
    func drawInContext(context: CGContext) {
        
        let size = self.bounds.size
        
        CGContextSetShouldAntialias(context, true)
        CGContextClearRect(context, self.bounds)
        
        
        ///////////////////////////////////////////////////////////
        ///////////////// convert the value to [0...1] ////////////
        let minVal = minimumValue
        var maxVal = maximumValue
        var val = value;
        
        if maxVal <= minVal {
            maxVal = minVal + 1
            val = minVal
        }
        
        val = (val - minVal) / (maxVal - minVal)
        
        ///////////////////////////////////////////////////////////
        /////////////////// draw track ///////////////////
        
        
        let path = UIBezierPath(
            roundedRect: CGRect(x: 0, y: size.height * 0.5 - trackWidth * 0.5, width: size.width, height: trackWidth),
            cornerRadius: trackWidth * 0.5)
        CGContextBeginPath(context)
        CGContextAddPath(context, path.CGPath)
        CGContextClosePath(context)
        CGContextSetFillColorWithColor(context, trackTint.CGColor)
        CGContextFillPath(context)
        
        
        ///////////////////////////////////////////////////////////
        /////////////////// draw thumb ///////////////////
        
        let strokeWidth: CGFloat = 0
        
        CGContextSetFillColorWithColor(context, thumbSolidTint.CGColor)
        CGContextSetLineWidth(context, strokeWidth)
        let center = CGPoint(x: (size.width - thumbRadius * 2) * val + thumbRadius,
                             y: size.height * 0.5)
        let ellipseRect = CGRect(
            x: center.x - thumbRadius + strokeWidth,
            y: center.y - thumbRadius + strokeWidth,
            width: thumbRadius * 2 - strokeWidth * 2,
            height: thumbRadius * 2 - strokeWidth * 2)
        CGContextFillEllipseInRect(context, ellipseRect)
    }
}
