//
//  CommentNode.swift
//  GuideHero
//
//  Created by Ascom on 10/31/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

public class CommentNode: CustomDebugStringConvertible {
    
    var id: String
    var name: String
    var content: String? // can be nil or anything from text, image URL, video ID, etc...
    var thumbnail_url: String
    var createdAt: Int // unix timestamp
    var updatedAt: Int // unix timestamp
    var sub_comments: [CommentNode]
    var likes: Int
    var parentComment: CommentNode?
    var parentContent: ContentNode?
    var isLiked: Bool?
    
    
    // MARK: - Calculated properties
    
    public var debugDescription: String {
        get {
            let dic: [String: String] = [
                "id": self.id,
                "content": self.content ?? "None",
                "name": self.name,
                "thumbnail_url": self.thumbnail_url
            ]
            return dic.debugDescription
        }
    }
    
    var formattedCreationDate: String {
        get {
            let creationDate = NSDate(timeIntervalSince1970: NSTimeInterval(self.createdAt))
            return NSDateComponentsFormatter.sharedFormatter.stringFromDate(creationDate, toDate: NSDate()) ?? ""
        }
    }
    
    
    // MARK: - Object lifecycle
    
    init(dictionary: [String: AnyObject]) {
        self.id = dictionary["id"] as! String
        self.name = dictionary["name"] as! String
        self.content = dictionary["content"] as? String
        self.thumbnail_url = dictionary["thumbnail_url"] as! String
        self.createdAt = dictionary["created_at"] as! Int
        self.updatedAt = dictionary["updated_at"] as! Int
        self.likes = dictionary["likes"] as! Int
        self.isLiked = dictionary["liked_by_current_user"] as? Bool
        if self.isLiked == nil {
            self.isLiked = false
        }
        self.parentComment = nil
        
//        self.sub_comments = dictionary["sub_comments"] as! [CommentNode]
        
        self.sub_comments = []
        for comment in (dictionary["sub_comments"] as? [[String: AnyObject]] ?? []) {
            let commentContent = CommentNode(dictionary: comment)
            commentContent.parentComment = self
            self.sub_comments.append(commentContent)
        }
    }
}
