//
//  UsePointsForCardViewController.swift
//  GuideHero
//
//  Created by SoftDev on 1/14/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit
import Moya

class UsePointsForCardViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var topTitle: UILabel!
    @IBOutlet weak var points: UILabel!
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var confirmView: UIView!
    @IBOutlet weak var bigPoints: UILabel!
    
    @IBOutlet weak var mainViewTopConstraint: NSLayoutConstraint!
    
    var cardType = "Amazon"
    
    var pan = UIPanGestureRecognizer()
    var panelOpened = true
    
    var startPos = CGPointZero
    var startCenter = CGPointZero
    
    var isRotating = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        backgroundView.layer.shadowRadius = 5
        backgroundView.layer.shadowOpacity = 0.5
        backgroundView.layer.shadowOffset = CGSizeZero
        backgroundView.layer.shadowColor = UIColor.blackColor().CGColor
        backgroundView.clipsToBounds = true
        backgroundView.alpha = 0
        
        mainViewTopConstraint.constant = UIScreen.mainScreen().bounds.size.height
    
        mainView.layer.cornerRadius = 17
        mainView.layer.shadowRadius = 5
        mainView.layer.shadowOpacity = 0.5
        mainView.layer.shadowOffset = CGSizeZero
        mainView.layer.shadowColor = UIColor.blackColor().CGColor
        mainView.clipsToBounds = true
        
        topTitle.text = "Convert to " + cardType + " Gift Card"
        
        onSetupPanel()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    ////////////////////////////////
    
    @IBAction func onClose(sender: AnyObject) {
        closeWithSliding()
    }
    
    @IBAction func onDone(sender: AnyObject) {
        
        let total_points = NSUserDefaults.standardUserDefaults().integerForKey("total_points")
        
        if total_points < 2500 {
            
            self.showSimpleAlert(withTitle: "Error", withMessage: "Not enough points! 😱")
            return
        }
        
        confirmView.hidden = false
    }
    
    @IBAction func onCancel(sender: AnyObject) {
        confirmView.hidden = true
    }
    
    @IBAction func onOkay(sender: AnyObject) {
        self.showProgressHUD()
        let user_id: String! = (Session.sharedSession.user?.user_id)!
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        
        provider.request(.UsePoints(userId: user_id, paymentType: cardType, username: "", email: email.text!)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.hideProgressHUD()
                    self.confirmView.hidden = true
                    self.showSimpleAlert(withTitle: "GuideHero", withMessage: "Error")
                    return
                }
                
                print("Success")
                let total_points = NSUserDefaults.standardUserDefaults().integerForKey("total_points")
                
                NSUserDefaults.standardUserDefaults().setInteger((total_points - 2500), forKey: "total_points")
                NSUserDefaults.standardUserDefaults().synchronize()
                
                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    print(JSON)
                    self.hideProgressHUD()
                    self.confirmView.hidden = true
                }
                else {
                    self.hideProgressHUD()
                    self.confirmView.hidden = true
                    self.showSimpleAlert(withTitle: "GuideHero", withMessage: "Error")
                }
                
                
                dispatch_async(dispatch_get_main_queue(),{
                    self.closeWithSliding()
                })
                
            case .Failure(_):
                self.hideProgressHUD()
                self.confirmView.hidden = true
                self.showSimpleAlert(withTitle: "GuideHero", withMessage: "Error")
            }
        }
    }
    
    ////////////////////////////////
    
    func onSetupPanel(){
        pan.addTarget(self, action: #selector(UsePointsViewController.handlePan(_:)))
        pan.delegate = self
        mainView.addGestureRecognizer(pan)
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if otherGestureRecognizer is UIPanGestureRecognizer {
            return true
        }
        
        return false
    }
    
    func handlePan(panGesture: UIPanGestureRecognizer){
        
        let factor: CGFloat = 2
        let angleLimit = CGFloat(M_PI_4)
        
        switch panGesture.state {
        case .Began:
            
            startPos = panGesture.locationInView(self.mainView.superview)
            startCenter = self.mainView.center
            print(startPos)
            let velocity = panGesture.velocityInView(self.mainView.superview)
            
            isRotating = fabs(velocity.x) > fabs(velocity.y)
            
        case .Changed:
            
            if isRotating {
                
                let pos = panGesture.locationInView(self.view?.superview)
                
                let rotationAngle = atan((pos.x - startPos.x) / (self.view.bounds.size.height * factor - pos.y))
                var direction:CGFloat = 1
                if pos.x != startPos.x {
                    direction = (startPos.x - pos.x) / fabs(pos.x - startPos.x)
                }
                backgroundView.alpha = 0.5 - fabs(rotationAngle)
                
                var transform = CGAffineTransformMakeTranslation(self.view.bounds.size.width * 0.5 * direction, self.view.bounds.size.height * (factor - 0.5))
                transform = CGAffineTransformRotate(transform, rotationAngle)
                transform = CGAffineTransformTranslate(transform, -self.view.bounds.size.width * 0.5 * direction, -self.view.bounds.size.height * (factor - 0.5))
                self.mainView.transform = transform;
                
            } else {
                let pos = panGesture.locationInView(self.view?.superview)
                self.mainView.center = CGPointMake(startCenter.x, startCenter.y + pos.y - startPos.y)
            }
            
        case .Ended:
            
            if isRotating {
                let pos = panGesture.locationInView(self.mainView?.superview)
                let velocity = panGesture.velocityInView(self.mainView?.superview)
                var startDirection = 1
                if pos.x != startPos.x {
                    startDirection = Int(round((pos.x - startPos.x) / fabs(pos.x - startPos.x)))
                }
                var velocityDirection = -startDirection
                if velocity.x != 0 {
                    velocityDirection = Int(round(velocity.x / fabs(velocity.x)))
                }
                if startDirection == velocityDirection { // should dismiss
                    UIView.animateWithDuration(0.3, animations: {
                        
                        var transform = CGAffineTransformMakeTranslation(-self.mainView.bounds.size.width * 0.5 * CGFloat(startDirection), self.mainView.bounds.size.height * (factor - 0.5))
                        transform = CGAffineTransformRotate(transform, angleLimit * CGFloat(startDirection))
                        transform = CGAffineTransformTranslate(transform, self.mainView.bounds.size.width * 0.5 * CGFloat(startDirection), -self.mainView.bounds.size.height * (factor - 0.5))
                        
                        self.mainView.transform = transform
                        self.backgroundView.alpha = 0
                        
                    }, completion: { completed in
                        self.dismissViewControllerAnimated(false, completion: nil)
                    })
                } else {
                    UIView.animateWithDuration(0.2, animations: {
                        self.mainView.transform = CGAffineTransformIdentity
                        self.backgroundView.alpha = 0.5
                    }, completion: { completed in
                    })
                }
                
            } else {
                let velocity = panGesture.velocityInView(self.mainView?.superview)
                if velocity.y > 0 { // should dismiss
                    UIView.animateWithDuration(0.2, animations: {
                        self.mainView.center = CGPointMake(self.startCenter.x, self.mainView.bounds.size.height * 1.5)
                        self.backgroundView.alpha = 0
                    }, completion: { completed in
                        self.dismissViewControllerAnimated(false, completion: nil)
                    })
                } else {
                    UIView.animateWithDuration(0.2, animations: {
                        self.mainView.center = self.startCenter
                    }, completion: { completed in
                    })
                }
            }
            
            
        default:
            break
        }
    }
    
    func closeWithSliding() {
        UIView.animateWithDuration(0.2, animations: {
            self.mainView.center = CGPointMake(self.mainView.center.x, self.mainView.bounds.size.height * 1.5)
            self.backgroundView.alpha = 0
            }, completion: { completed in
                self.dismissViewControllerAnimated(false, completion: nil)
        })
    }
    
    func didFinishPresent() {
        dispatch_async(dispatch_get_main_queue(),{
            self.mainViewTopConstraint.constant = 0
            UIView.animateWithDuration(0.3, animations: {
                self.backgroundView.alpha = 0.5
                self.view.layoutIfNeeded()
            })
        })
    }
}
