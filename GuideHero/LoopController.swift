//
//  LoopController.swift
//  GuideHero
//
//  Created by Abhishek Sengar on 10/09/2016.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Koloda

class LoopController: UIViewController, LoopTutorialControllerDelegate {

    // MARK: - Properties

    @IBOutlet weak var kolodaView: KolodaView!
    @IBOutlet var progressView: UIView!
    
    var content: ContentNode! {
        didSet {
            self.createCardViews()
        }
    }

    // MARK: - Private vars

    private var cardViews: [CardView] = []
    private var progressBar: UIView?

    private var tutorialViewController: LoopTutorialController?

    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupKolodaView()
        self.showTutorialIfNeeded()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.resetProgressBar()
    }

    // MARK: - Actions

    @IBAction func close(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func againButtonTapped(sender: UIButton) {
        if isOutOfCards() {
            self.navigationController?.popViewControllerAnimated(true)
        } else {
            self.kolodaView.swipe(.Left)
        }
    }
    
    @IBAction func doneButtonTapped(sender: UIButton) {
        if isOutOfCards() {
            self.navigationController?.popViewControllerAnimated(true)
        } else {
            self.kolodaView.swipe(.Right)
        }
    }

    // MARK: - Tutorial

    private func showTutorialIfNeeded() {
        if LoopTutorialHelper.shouldDisplayLoopTutorial {
            LoopTutorialHelper.didDisplayLoopTutorial()
            self.showTutorial()
        }
    }

    private func showTutorial() {
        let storyboard = UIStoryboard(name: "LoopTutorial", bundle: NSBundle.mainBundle())
        self.tutorialViewController = storyboard.instantiateInitialViewController() as? LoopTutorialController
        self.tutorialViewController!.tutorialDelegate = self
        self.addChildViewController(self.tutorialViewController!)
        self.view.addSubview(self.tutorialViewController!.view)
        self.tutorialViewController!.view.fillSuperview()
        self.tutorialViewController!.didMoveToParentViewController(self)
    }

    private func hideTutorial() {
        UIView.animateWithDuration(0.5, animations: { 
            self.tutorialViewController?.view.alpha = 0
        }, completion: { finished in
            self.tutorialViewController?.view.removeFromSuperview()
            self.tutorialViewController?.removeFromParentViewController()
            self.tutorialViewController = nil
        })
    }

    // MARK: - LoopTutorialControllerDelegate

    func didCompleteTutorial() {
        self.hideTutorial()
    }

    // MARK: - Private methods

    private func setupKolodaView() {
        self.kolodaView.delegate = self
        self.kolodaView.dataSource = self
    }

    private func isOutOfCards() -> Bool {
        // swipableCardsView.countOfVisibleCards == 0 doesn't work
        return self.kolodaView.currentCardIndex == Int(kolodaNumberOfCards(self.kolodaView))
    }

    private func resetProgressBar() {
        // remove any existing progress bar
        self.progressBar?.removeFromSuperview()

        // create a new progress bar
        self.progressBar = UIView(frame: self.progressView.bounds)
        self.progressBar!.backgroundColor = Constants.UIConstants.yellowColor
        self.progressView.addSubview(self.progressBar!)

        // animate progress to zero
        var frame = self.progressBar!.frame
        frame.size.width = 0
        UIView.animateWithDuration(10, delay: 0, options: .CurveLinear, animations: {
            self.progressBar!.frame = frame
        }, completion: { finished in
            if finished {
                // TODO: cancel any existing dragging action
                // automatic loop
                self.kolodaView.swipe(.Left)
            }
        })

    }

    private func createCardViews() {
        self.cardViews = []
        for content in self.content.children {
            let cardView = CardView.loadFromNib() as! CardView
            cardView.content = content
            cardView.showAsCard = true
            self.cardViews.append(cardView)
        }
    }
}

// MARK: -

extension LoopController: KolodaViewDelegate {

    func koloda(koloda: KolodaView, didSwipeCardAtIndex index: UInt, inDirection direction: SwipeResultDirection) {

        self.resetProgressBar()

        if direction.horizontalDirection == .Left {
            // loop, send card to the back
            let cardView = self.cardViews[Int(index)]
            self.cardViews.append(cardView)
            self.kolodaView.reloadData() // TODO: maybe we can only reload the last card?
        }
    }

}

// MARK: -

extension LoopController: KolodaViewDataSource {
    
    func kolodaNumberOfCards(koloda: KolodaView) -> UInt {
        return UInt(self.cardViews.count)
    }
    
    func koloda(koloda: KolodaView, viewForCardAtIndex index: UInt) -> UIView {
        return self.cardViews[Int(index)]
    }
    
    func koloda(koloda: KolodaView, viewForCardOverlayAtIndex index: UInt) -> OverlayView? {
        let overlay = CardOverlayView.loadFromNib() as! CardOverlayView
        overlay.card = self.cardViews[Int(index)].content
        return overlay
    }
}
