//
//  CardDraftMAPlayVC.swift
//  GuideHero
//
//  Created by Promising Change on 31/01/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import UIKit

class CardDraftMAPlayVC: CardSubPageBaseVC {
    
    // MARK: - @IBOutlet Variables
    
    @IBOutlet weak var tbCardList: UITableView!
    @IBOutlet weak var footerView : UIView!
    @IBOutlet weak var topView : UIView!
    @IBOutlet weak var vwHeader: UIView!
    
    // MARK: - Variables
    
    let tbHeaderView: UIView!
    var blankCellHeight: CGFloat!
    
    // MARK: - Properties
    
    var content: ContentNode? {
        didSet {
            if isViewLoaded() {
                tbCardList.reloadData()
                self.view.layoutIfNeeded()
            }
        }
    }
    
    // MARK: - View Lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        tbHeaderView = UIView()
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        blankCellHeight = max(UIScreen.mainScreen().bounds.size.height - 591, 0)
        scrollView = tbCardList as UIScrollView
        
        configureView()
        
        if content?.children.count == 1 {
            singleCard = true
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Methods
    
    func configureView() {
        tbHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 540)
        
        let blankCellNib = UINib(nibName: "RoundedBlankTableViewCell", bundle: nil)
        tbCardList.registerNib(blankCellNib, forCellReuseIdentifier: "RoundedBlankTableViewCell")
        
        tbCardList.rowHeight = UITableViewAutomaticDimension
        tbCardList.estimatedRowHeight = Constants.UIConstants.contentCellHeight
        tbCardList.separatorStyle = .None
        
        tbCardList.tableHeaderView = tbHeaderView
        
        footerView.layer.cornerRadius = 20
        footerView.layer.shadowRadius = 5
        footerView.layer.shadowOpacity = 0.7
        footerView.layer.shadowOffset = CGSizeZero
        footerView.layer.shadowColor = UIColor.blackColor().CGColor
        footerView.clipsToBounds = true
        
        self.view.setNeedsLayout()
    }
    
    // MARK: - UIScrollViewDelegate
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
    }
}

// MARK: - UITableViewDelegate & UITableViewDataSource

extension CardDraftMAPlayVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return blankCellHeight
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let separatorView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 1))
        separatorView.backgroundColor = UIColor(colorLiteralRed: 0.84, green: 0.84, blue: 0.84, alpha: 1)
        
        return separatorView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("RoundedBlankTableViewCell", forIndexPath: indexPath) as! BaseTableViewCell
        
        cell.layer.shadowRadius = 1
        cell.layer.shadowOffset = CGSizeZero
        cell.layer.shadowColor = UIColor.colorFromRGB(redValue: 74/255.0, greenValue: 74/255.0, blueValue: 74/255.0, alpha: 1.0).CGColor
        
        cell.textLabel?.text = ""
        
        cell.needsRoundBottom = true
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
}
