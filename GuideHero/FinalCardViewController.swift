//
//  FinalCardViewController.swift
//  GuideHero
//
//  Created by SoftDev on 12/9/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Moya
import Alamofire

class FinalCardViewController: UIViewController , UIGestureRecognizerDelegate {

    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var imgGradientBottom : UIImageView!
    @IBOutlet weak var imgGradientTop : UIImageView!
    
    @IBOutlet weak var progressBackground: UIView!
    @IBOutlet weak var progress: UIView!
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userId: UILabel!
    @IBOutlet weak var userBio: UILabel!
    @IBOutlet weak var cardTime: UILabel!
    @IBOutlet weak var imgBackground : UIImageView!
    
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var cardTitle: UILabel!
    @IBOutlet weak var cardDescription: UILabel!
    
    @IBOutlet weak var hideControlsButton: UIButton! {
        didSet {
            hideControlsButton.setImage(UIImage(appImage:. arrowUp), forState: .Normal)
        }
    }

    @IBOutlet weak var showControlsButton: UIButton! {
        didSet {
            showControlsButton.setImage(UIImage(appImage: .arrowDown), forState: .Normal)
        }
    }
    
    @IBOutlet weak var scaleTypeSelectionView: UIView!
    @IBOutlet var scaleTypeButtons: [UIButton]!
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var thumbnailView: UIImageView!
    @IBOutlet weak var videoControlsView: UIView!
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var skipBackButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var skipForwardButton: UIButton!
    @IBOutlet weak var playedTime: UILabel!
    @IBOutlet weak var remainingTime: UILabel!
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var userView : UIView!
    
    @IBOutlet weak var progressWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var hideY : NSLayoutConstraint!
    @IBOutlet weak var bgViewY: NSLayoutConstraint!
    
    var prevViewController: UIViewController?
    
    var scaleType = 0
    var cardName = ""
    var descriptionText: String?
    var cardTags = [String]()
    
    var image: UIImage?
    var cropImage: UIImage?
    var rect: CGRect?
    
    var videoUrl: NSURL?
    var thumbnailImage: UIImage?
    var startTime: Float = 0
    var endTime: Float = 0
    var videoFileName = ""
    
    var screenWidth = UIScreen.mainScreen().bounds.width
    var screenHeight = UIScreen.mainScreen().bounds.height
    
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?
    var playTimer: NSTimer?
    var isPlaying: Bool = false
    var duration: Float = 0.0
    
    var pan = UIPanGestureRecognizer()
    var startPos = CGPointZero
    var startCenter = CGPointZero
    var isRotating = false
    var isDirection = false
    
    var cardId: String? = nil //check whether to create or edit
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 20
//        mainView.layer.borderColor = UIColor.whiteColor().CGColor
//        mainView.layer.borderWidth = 2.5
        
        userView.layer.shadowColor = UIColor.blackColor().CGColor
        userView.layer.shadowOffset = CGSizeMake(0, 2)
        userView.layer.shadowOpacity = 0.5
        userView.layer.shadowRadius = 10
        
        hideControlsButton.layer.shadowColor = UIColor.blackColor().CGColor
        hideControlsButton.layer.shadowOffset = CGSizeMake(0, 2)
        hideControlsButton.layer.shadowOpacity = 0.5
        hideControlsButton.layer.shadowRadius = 4
        
        cardTitle.layer.shadowColor = UIColor.blackColor().CGColor
        cardTitle.layer.shadowOffset = CGSizeMake(0, 2)
        cardTitle.layer.shadowOpacity = 0.5
        cardTitle.layer.shadowRadius = 4
        
        cardDescription.layer.shadowColor = UIColor.blackColor().CGColor
        cardDescription.layer.shadowOffset = CGSizeMake(0, 2)
        cardDescription.layer.shadowOpacity = 0.5
        cardDescription.layer.shadowRadius = 4
        
        showControlsButton.layer.shadowColor = UIColor.blackColor().CGColor
        showControlsButton.layer.shadowOffset = CGSizeMake(0, 2)
        showControlsButton.layer.shadowOpacity = 0.5
        showControlsButton.layer.shadowRadius = 2
        
        videoControlsView.layer.shadowColor = UIColor.blackColor().CGColor
        videoControlsView.layer.shadowOffset = CGSizeMake(0, 2)
        videoControlsView.layer.shadowOpacity = 0.5
        videoControlsView.layer.shadowRadius = 4
        
        backgroundImage.alpha = 0.8
        
        makeUI()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.sharedApplication().statusBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.sharedApplication().statusBarHidden = false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    ////////////////////////////////
    
    func makeUI() {
        makeProfileUI()
        makeContentUI()
    }
    
    func makeProfileUI() {
        let user = Session.sharedSession.user
        if (user != nil) {
            
            let nameAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: Helper.proximaNova("bold", font: 20)]
            let idAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: Helper.proximaNova("semibold", font: 15)]
            let bioAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: Helper.proximaNova("semibold", font: 13)]
            _ = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: Helper.proximaNova("semibold", font: 13)]
            
            let partName = NSMutableAttributedString(string: user!.first_name + " " + user!.last_name + "     ", attributes: nameAttributes)
            let partId = NSMutableAttributedString(string: user!.username + "\n", attributes: idAttributes)
            let partBio = NSMutableAttributedString(string: user!.bio + "     ", attributes: bioAttributes)
            
            
            let combination = NSMutableAttributedString()
            
            combination.appendAttributedString(partName)
            combination.appendAttributedString(partId)
            combination.appendAttributedString(partBio)
            
            userName.attributedText = combination
            
            
            let imageCache = GuideHeroImageCache.sharedInstance.getCache()
            let imageDownloader = userPhoto.af_imageDownloader ?? UIImageView.af_sharedImageDownloader
            if let imageUrl = NSURL(string: user!.thumbnail_url) {
                let request = NSMutableURLRequest(URL: imageUrl)

                let cachedImage = imageCache.imageForRequest(request)
                if cachedImage != nil {
                    self.userPhoto.image = cachedImage
                } else {
                    imageDownloader.downloadImage(URLRequest: request) { response in
                        if response.result.isSuccess {
                            let image = response.result.value
                            self.userPhoto.image = image
                            imageCache.addImage(image!, forRequest: request)
                        }
                    }

                }
            }
        }
        
        progressBackground.layer.cornerRadius = 2.0
        progressBackground.clipsToBounds = true
        progress.layer.cornerRadius = 2.0
        progress.clipsToBounds = true
    }
    
    func makeContentUI() {
        
        cardTitle.text = cardName
        cardDescription.text = descriptionText!

        showControlsButton.hidden = true
        
        if (videoUrl != nil) {
            backgroundImage.hidden = true
            scaleTypeSelectionView.hidden = true
            cardImage.hidden = true
            videoControlsView.hidden = true
            videoView.hidden = false
            
            self.showProgressHUD()
            
            if videoUrl!.fileURL { // create
                cropVideo(videoUrl!, start: startTime, end: endTime)
            }
            else { // edit
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    let manager = NSFileManager.defaultManager()
                    guard let documentDirectory = try? manager.URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true) else {return}
                    
                    let downloadURL = documentDirectory.URLByAppendingPathComponent("download-temp.mov")
                    
                    let urlData = NSData(contentsOfURL: self.videoUrl!)
                    do {
                        try urlData!.writeToURL(downloadURL!, options: NSDataWritingOptions.DataWritingAtomic)
                        
                        self.cropVideo(downloadURL!,start: self.startTime, end: self.endTime)
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        self.hideProgressHUD()
                    }
                    
                })
            }
            
        }
        else {
            backgroundImage.hidden = false
            scaleTypeSelectionView.hidden = false
            cardImage.hidden = false
            videoView.hidden = true
            videoControlsView.hidden = true
            cardImage.image = image
            
            
            //{
                cardImage.image = nil
                backgroundImage.image = image
                
                if (screenWidth / image!.size.width > screenHeight / image!.size.height) {
                    self.backgroundImage.contentMode = .ScaleAspectFill
                    bgViewY.constant = -(screenHeight - screenWidth * image!.size.height/image!.size.width)
                }
                else {
                    self.backgroundImage.contentMode = .ScaleAspectFit
                    bgViewY.constant = -(screenHeight - screenWidth * image!.size.height/image!.size.width)
                }

            //}
        }
 
        pan.addTarget(self, action: #selector(FinalCardViewController.handlePan(_:)))
        pan.delegate = self
        mainView.addGestureRecognizer(pan)
    }
    
    func cropVideo(sourceUrl: NSURL, start:Float, end:Float)
    {
        let manager = NSFileManager.defaultManager()
        
        guard let documentDirectory = try? manager.URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true) else {return}
        
        let asset = AVAsset(URL: sourceUrl)
        let length = Float(asset.duration.value) / Float(asset.duration.timescale)
        print("video length: \(length) seconds")
        
        let outputURL = documentDirectory.URLByAppendingPathComponent("output-temp.mov")
        
        //Remove existing file
        _ = try? manager.removeItemAtURL(outputURL!)
        
        
        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else {return}
        exportSession.outputFileType = AVFileTypeMPEG4
        exportSession.outputURL = outputURL
        exportSession.shouldOptimizeForNetworkUse = true
        
        let startTime = CMTime(seconds: Double(start), preferredTimescale: 1000)
        let endTime = CMTime(seconds: Double(end ?? duration), preferredTimescale: 1000)
        let timeRange = CMTimeRange(start: startTime, end: endTime)
        
        exportSession.timeRange = timeRange
        
        exportSession.exportAsynchronouslyWithCompletionHandler {
            switch exportSession.status {
            case .Completed:
                self.videoUrl = outputURL!
                self.setupPlayer()
                self.createThumbnail()
            case .Failed:
                self.hideProgressHUD()
                print("failed \(exportSession.error)")
                
            case .Cancelled:
                self.hideProgressHUD()
                print("cancelled \(exportSession.error)")
                
            default:
                break
            }
        }
    }
    
    func setupPlayer() {
        player = AVPlayer(URL: videoUrl!)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer!.addObserver(self, forKeyPath: "readyForDisplay", options: [], context: nil)
   
        player!.pause()

        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        dispatch_async(backgroundQueue, {
            print("This is run on the background queue")
            self.duration = Float(CMTimeGetSeconds(self.player!.currentItem!.asset.duration))
            
            
            self.timeSlider.maximumValue = self.duration * 100

        })
    }
    
    func createThumbnail() {
        let asset = AVURLAsset(URL: videoUrl!, options: nil)
        let imgGenerator = AVAssetImageGenerator(asset: asset)
        do {
            let cgImage = try imgGenerator.copyCGImageAtTime(CMTimeMake(0, 1), actualTime: nil)
            thumbnailImage = UIImage(CGImage: cgImage)
//            thumbnailView.image = thumbnailImage
        }
        catch {
            
        }
        
        self.hideProgressHUD()
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if (keyPath == "readyForDisplay") {
            playerLayer!.frame = self.videoView.frame
            self.videoView.layer.addSublayer(playerLayer!)
            self.playButton.enabled = true
            self.skipBackButton.enabled = true
            self.skipForwardButton.enabled = true
            self.timeSlider.enabled = true
            self.playerLayer!.removeObserver(self, forKeyPath:"readyForDisplay")
            self.onPlay(self)
            print("ready")
        }
    }
    
    ////////////////////////////////
    
    @IBAction func onHideControls(sender: AnyObject) {
        hideControls(true)
        hideAll()
        
        mainView.layer.cornerRadius = 0
    }
    
    @IBAction func onShowControls(sender: AnyObject) {
        hideControls(false)
        
        progressBackground.hidden = false
        progress.hidden = false
        
        mainView.layer.cornerRadius = 20
    }
    
    func hideControls(isHide: Bool) {
        hideControlsButton.hidden = isHide
        showControlsButton.hidden = !isHide
        cardTitle.hidden = isHide
        cardDescription.hidden = isHide
        videoControlsView.hidden = videoUrl == nil || !isHide
        scaleTypeSelectionView.hidden = videoUrl != nil || isHide
        bottomView.hidden = isHide
    }
    
    ////////////////////////////////
    
    @IBAction func onBack(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onSave(sender: AnyObject) {
        if (videoUrl != nil) {
            uploadVideoToServer(videoUrl!)
        }
        else {
            var image_x = rect!.origin.x
            var image_y = rect!.origin.y
            let image_width = rect!.size.width
            let image_height = rect!.size.height
            
            // API requests flow:
            // 1. Get image upload parameters from GuideHero API
            // 2. Upload image to S3
            // 3. Upload card data to GuideHero API
            
            let fileName = "guidehero-ios-card-picture-\(NSDate().timeIntervalSince1970).jpg"
            
            self.showProgressHUD()
            
            let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
            let fileURL = NSURL(fileURLWithPath: documentsPath + "/" + fileName)
            
            let provider = MoyaProvider<NetworkService>.createDefaultProvider()
            provider.request(.GenerateImageUploadParameters(fileName: fileName)) { result in
                switch result {
                case let .Success(response):
                    if response.statusCode != 200 {
                        self.hideProgressHUD()
                        self.showSimpleAlert(withTitle: "Error",
                                             withMessage: "Can't generate image upload parameters, please try again later")
                    }
                    else if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                        
                        let result = JSON["result"] as! [String: AnyObject]
                        let fields = result["fields"] as! [String: String]
                        var headers = fields
                        headers["Content-Type"] = "image/jpeg"
                        
                        var imageData:NSData!;
                        if self.scaleType >= 2 {
                            imageData = UIImageJPEGRepresentation(self.cropImage!, 0.1)
                            image_x = 0.0
                            image_y = 0.0
                        }
                        else {
                            imageData = UIImageJPEGRepresentation(self.image!, 0.1)
                        }
                        
                        if !imageData.writeToURL(fileURL, atomically: true) {
                            // TODO: handle errors
                            print("can't save file")
                            self.hideProgressHUD()
                            return
                        }
                        // TODO: delete temp image
                        
                        // clean any additional HTTP headers that S3 might not like
                        Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = [:]
                        
                        // upload the image to amazon S3
                        Alamofire.upload(.POST, result["url"] as! String,
                                         headers: headers,
                                         multipartFormData:
                            { multipartFormData in
                                multipartFormData.appendBodyPart(data: fields["key"]!.dataUsingEncoding(NSUTF8StringEncoding)!,
                                    name: "key")
                                multipartFormData.appendBodyPart(fileURL: fileURL, name: "file")
                                
                        }, encodingCompletion: { encodingResult in
                            
                            switch encodingResult {
                            case .Success(let request, _, _):
                                
                                request.session.configuration.HTTPAdditionalHeaders = [:]
                                request.response { (request, response, data, error) in
                                    
                                    let imageFileName = fields["key"]!
                                    
                                    if self.cardId == nil { // create
                                        let request: NetworkService = .CreateImageCard(
                                            title: self.cardName,
                                            description:self.descriptionText!,
                                            image: imageFileName,
                                            image_x: image_x, image_y: image_y,
                                            image_width: image_width, image_height: image_height,
                                            tags: self.cardTags, image_scale: self.scaleType)
                                        
                                        provider.request(request) { result in
                                            switch result {
                                            case let .Success(response):
                                                
                                                guard response.statusCode == 200 else {
                                                    self.showSimpleAlert(withTitle: "Error",
                                                        withMessage: "Can't generate image upload parameters, please try again later")
                                                    self.hideProgressHUD()
                                                    return
                                                }
                                                guard let JSON = try? response.mapJSON() as! [String: String] else {
                                                    self.showSimpleAlert(withTitle: "Error",
                                                        withMessage: "Can't create image card, please try again later")
                                                    self.hideProgressHUD()
                                                    return
                                                }
                                                
                                                guard JSON["result"] == "success" else {
                                                    self.showSimpleAlert(withTitle: "Error",
                                                        withMessage: "Can't create image card, please try again later")
                                                    self.hideProgressHUD()
                                                    return
                                                }
                                                
                                                self.hideProgressHUD()
                                                
                                                let navCtrl = self.prevViewController?.navigationController
                                                
                                                if let rootVC = navCtrl?.viewControllers.first as? CameraFunctionViewController {
                                                    if let cardId = JSON["created_card_id"] {
                                                        if let _ = rootVC.submittedCallback {
                                                            self.showChoiceforGive(cardId)
                                                        } else {
                                                            self.dismissViewControllerAnimated(true) {
                                                                rootVC.navigationController?.dismissViewControllerAnimated(false, completion: nil)
                                                            }
                                                        }
                                                    }
                                                }
                                                else {
                                                    self.dismissViewControllerAnimated(true) {
                                                        self.prevViewController?.dismissViewControllerAnimated(false, completion: nil)
                                                    }
                                                }
                                                
                                            case let .Failure(error):
                                                print(error)
                                                self.hideProgressHUD()
                                                self.showSimpleAlert(withTitle: "Error",
                                                    withMessage: "Can't create image card, please try again later")
                                            }
                                        }
                                    }
                                    else {
                                        let request: NetworkService = .EditImageCard(
                                            id: self.cardId!,
                                            title: self.cardName,
                                            description:self.descriptionText!,
                                            image: imageFileName,
                                            image_x: image_x, image_y: image_y,
                                            image_width: image_width, image_height: image_height,
                                            tags: self.cardTags, image_scale: self.scaleType)
                                        
                                        provider.request(request) { result in
                                            switch result {
                                            case let .Success(response):
                                                
                                                guard response.statusCode == 200 else {
                                                    self.showSimpleAlert(withTitle: "Error",
                                                        withMessage: "Can't generate image upload parameters, please try again later")
                                                    self.hideProgressHUD()
                                                    return
                                                }
                                                
                                                self.hideProgressHUD()
                                                
                                                self.dismissViewControllerAnimated(true) {
                                                    self.prevViewController?.dismissViewControllerAnimated(false) {
                                                        let notify = NSNotification(name: "changedTable", object: nil)
                                                        NSNotificationCenter.defaultCenter().postNotification(notify)
                                                    }
                                                }
                                                
                                            case let .Failure(error):
                                                print(error)
                                                self.hideProgressHUD()
                                                self.showSimpleAlert(withTitle: "Error",
                                                    withMessage: "Can't edit image card, please try again later")
                                            }
                                        }
                                    }
                                }
                                
                            case .Failure(let encodingError):
                                print(encodingError)
                                self.hideProgressHUD()
                                self.showSimpleAlert(withTitle: "Error",
                                    withMessage: "Can't encode multipart content, please try again later")
                            }
                        })
                        
                    }
                    else {
                        self.hideProgressHUD()
                        self.showSimpleAlert(withTitle: "Error",
                            withMessage: "Can't parse response, please try again later")
                    }
                case let .Failure(error):
                    print(error)
                    self.hideProgressHUD()
                    self.showSimpleAlert(withTitle: "Error",
                        withMessage: "Can't create image card, please try again later")
                }
            }
        }
 
    }
    
    func uploadVideoToServer(sourceUrl: NSURL) {
        let fileName = "guidehero-ios-card-video-\(NSDate().timeIntervalSince1970).mp4"
        
        self.showProgressHUD()
        
        let data = NSData(contentsOfURL: sourceUrl)
        print("File size before compression: \(Double(data!.length / 1048576)) mb")
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.GenerateVideoUploadParameters(fileName: fileName)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.hideProgressHUD()
                    self.showSimpleAlert(withTitle: "Error",
                        withMessage: "Can't generate video upload parameters, please try again later")
                }
                else if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    
                    let result = JSON["result"] as! [String: AnyObject]
                    let fields = result["fields"] as! [String: String]
                    var headers = fields
                    
                    headers["Content-Type"] = "video/mpeg"
                    
                    Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = [:]
                    
                    let compressedURL = NSURL.fileURLWithPath(NSTemporaryDirectory() + NSUUID().UUIDString + ".m4v")
                    self.compressVideo(sourceUrl, outputURL: compressedURL) { (session) in
                        switch session.status {
                        case .Unknown:
                            break
                        case .Waiting:
                            break
                        case .Exporting:
                            break
                        case .Completed:
                            // upload the video to amazon S3
                            Alamofire.upload(.POST, result["url"] as! String,
                                             headers: headers,
                                             multipartFormData: { multipartFormData in
                                multipartFormData.appendBodyPart(data: fields["key"]!.dataUsingEncoding(NSUTF8StringEncoding)!,
                                    name: "key")
                                multipartFormData.appendBodyPart(fileURL: compressedURL, name: "file")
                                
                                }, encodingCompletion: { encodingResult in
                                    switch encodingResult {
                                    case .Success(let request, _, _):
                                        
                                        request.session.configuration.HTTPAdditionalHeaders = [:]
                                        request.response { (request, response, data, error) in
                                            
                                            // upload thumbnail image
                                            
                                            self.videoFileName = fields["key"]!
                                            
                                            if (self.thumbnailImage != nil) {
                                                self.uploadThumbnail()
                                                return
                                            }
                                            
                                            if self.cardId == nil {
                                                let length = Int(self.endTime - self.startTime)

                                            // uplpoad card to GuideHero API
                                                let request: NetworkService = .CreateVideoCard(
                                                    title: self.cardName,
                                                    description: self.descriptionText!,
                                                    video: self.videoFileName,
                                                    thumbnail_url:"", tags: self.cardTags,
                                                video_length: length)
                                                
                                                provider.request(request) { result in
                                                    switch result {
                                                    case let .Success(response):
                                                        
                                                        guard response.statusCode == 200 else {
                                                            self.showSimpleAlert(withTitle: "Error",
                                                                withMessage: "Can't generate video upload parameters, please try again later")
                                                            self.hideProgressHUD()
                                                            return
                                                        }
                                                        
                                                        guard let JSON = try? response.mapJSON() as! [String: String] else {
                                                            self.showSimpleAlert(withTitle: "Error",
                                                                withMessage: "Can't create video card, please try again later")
                                                            self.hideProgressHUD()
                                                            return
                                                        }
                                                        
                                                        guard JSON["result"] == "success" else {
                                                            self.showSimpleAlert(withTitle: "Error",
                                                                withMessage: "Can't create video card, please try again later")
                                                            self.hideProgressHUD()
                                                            return
                                                        }
                                                        
                                                        self.hideProgressHUD()
                                                        
                                                        let navCtrl = self.prevViewController?.navigationController
                                                        if let rootVC = navCtrl?.viewControllers.first as? CameraFunctionViewController {
                                                            if let _ = rootVC.submittedCallback {
                                                                self.dismissViewControllerAnimated(true) {
                                                                    rootVC.submittedCallback(nil)
                                                                }
                                                            }
                                                            else {
                                                                self.dismissViewControllerAnimated(true) {
                                                                    rootVC.navigationController?.dismissViewControllerAnimated(false, completion: nil)
                                                                }
                                                            }
                                                        }
                                                        else {
                                                            self.dismissViewControllerAnimated(true) {
                                                                self.prevViewController?.dismissViewControllerAnimated(false, completion: nil)
                                                            }
                                                        }
                                                        
                                                    case let .Failure(error):
                                                        print(error)
                                                        self.hideProgressHUD()
                                                        self.showSimpleAlert(withTitle: "Error",
                                                            withMessage: "Can't create video card, please try again later")
                                                    }
                                                }
                                            }
                                            else {
                                                let request: NetworkService = .EditVideoCard(
                                                    id: self.cardId!,
                                                    title: self.cardName,
                                                    description: self.descriptionText!,
                                                    video: self.videoFileName,
                                                    thumbnail_url:"", tags: self.cardTags)
                                                
                                                provider.request(request) { result in
                                                    switch result {
                                                    case let .Success(response):
                                                        guard response.statusCode == 200 else {
                                                            self.showSimpleAlert(withTitle: "Error",
                                                                withMessage: "Can't generate video upload parameters, please try again later")
                                                            self.hideProgressHUD()
                                                            return
                                                        }
                                                        
                                                        self.hideProgressHUD()
                                                        
                                                        self.dismissViewControllerAnimated(true) {
                                                            self.prevViewController?.dismissViewControllerAnimated(false) {
                                                                let notify = NSNotification(name: "changedTable", object: nil)
                                                                NSNotificationCenter.defaultCenter().postNotification(notify)
                                                            }
                                                        }
                                                        
                                                    case let .Failure(error):
                                                        print(error)
                                                        self.hideProgressHUD()
                                                        self.showSimpleAlert(withTitle: "Error",
                                                            withMessage: "Can't create video card, please try again later")
                                                    }
                                                }
                                            }
                                        }
                                        
                                    case .Failure(let encodingError):
                                        print(encodingError)
                                        self.hideProgressHUD()
                                        self.showSimpleAlert(withTitle: "Error",
                                            withMessage: "Can't encode multipart content, please try again later")
                                    }
                            })
                            let data = NSData(contentsOfURL: compressedURL)
                            print("File size after compression: \(Double(data!.length / 1048576)) mb")
                        case .Failed:
                            break
                        case .Cancelled:
                            break
                        }
                    }
                }
                else {
                    self.hideProgressHUD()
                    self.showSimpleAlert(withTitle: "Error",
                        withMessage: "Can't parse response, please try again later")
                }
            case let .Failure(error):
                print(error)
                self.hideProgressHUD()
                self.showSimpleAlert(withTitle: "Error",
                    withMessage: "Can't create video card, please try again later")
            }
        }
    }
    
    func compressVideo(inputURL: NSURL, outputURL: NSURL, handler:(session: AVAssetExportSession)-> Void) {
        let urlAsset = AVURLAsset(URL: inputURL, options: nil)
        if let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) {
            exportSession.outputURL = outputURL
            exportSession.outputFileType = AVFileTypeQuickTimeMovie
            exportSession.shouldOptimizeForNetworkUse = true
            exportSession.exportAsynchronouslyWithCompletionHandler { () -> Void in
                handler(session: exportSession)
            }
        }
    }
    
    
    func uploadThumbnail() {
        let fileName = "guidehero-ios-card-video-thumbnail-\(NSDate().timeIntervalSince1970).jpg"
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        let fileURL = NSURL(fileURLWithPath: documentsPath + "/" + fileName)

        // TODO: cleanup, separate logic in various methods, and handle errors properly
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.GenerateImageUploadParameters(fileName: fileName)) { result in
            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.hideProgressHUD()
                    self.showSimpleAlert(withTitle: "Error",
                                         withMessage: "Can't generate image upload parameters, please try again later")
                }
                else if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    
                    let result = JSON["result"] as! [String: AnyObject]
                    let fields = result["fields"] as! [String: String]
                    var headers = fields
                    
                    headers["Content-Type"] = "image/jpeg"
                    
                    let imageData = UIImageJPEGRepresentation(self.thumbnailImage!, 0.1)!
                    
                    if !imageData.writeToURL(fileURL, atomically: true) {
                        // TODO: handle errors
                        print("can't save file")
                        self.hideProgressHUD()
                        return
                    }
                    // TODO: delete temp image
                    
                    // clean any additional HTTP headers that S3 might not like
                    Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = [:]
                    
                    // upload the image to amazon S3
                    Alamofire.upload(.POST, result["url"] as! String,
                                     headers: headers, multipartFormData: { multipartFormData in
                        multipartFormData.appendBodyPart(data: fields["key"]!.dataUsingEncoding(NSUTF8StringEncoding)!, name: "key")
                        multipartFormData.appendBodyPart(fileURL: fileURL, name: "file")
                        }, encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .Success(let request, _, _):
                                
                                request.session.configuration.HTTPAdditionalHeaders = [:]
                                request.response { (request, response, data, error) in
                                    
                                    // TODO: handle errors
                                    
                                    // upload card to GuideHero API
                                    
                                    let thumbFileName = fields["key"]!
                                    
                                    if self.cardId == nil {
                                        let length = Int(self.endTime - self.startTime)

                                        let request: NetworkService = .CreateVideoCard(
                                            title: self.cardName,
                                            description: self.descriptionText!,
                                            video: self.videoFileName,
                                            thumbnail_url:thumbFileName,
                                            tags: self.cardTags,
                                            video_length: length)
                                        
                                        provider.request(request) { result in
                                            switch result {
                                            case let .Success(response):
                                                guard response.statusCode == 200 else {
                                                    self.showSimpleAlert(withTitle: "Error",
                                                        withMessage: "Can't generate video upload parameters, please try again later")
                                                    self.hideProgressHUD()
                                                    return
                                                }
                                                
                                                guard let JSON = try? response.mapJSON() as! [String: String] else {
                                                    self.showSimpleAlert(withTitle: "Error",
                                                        withMessage: "Can't create video card, please try again later")
                                                    self.hideProgressHUD()
                                                    return
                                                }
                                                
                                                guard JSON["result"] == "success" else {
                                                    self.showSimpleAlert(withTitle: "Error",
                                                        withMessage: "Can't create video card, please try again later")
                                                    self.hideProgressHUD()
                                                    return
                                                }
                                                
                                                self.hideProgressHUD()
                                                        
                                                let navCtrl = self.prevViewController?.navigationController
                                                if let rootVC = navCtrl?.viewControllers.first as? CameraFunctionViewController {
                                                    if let cardId = JSON["created_card_id"] {
                                                        if let _ = rootVC.submittedCallback {
                                                            self.showChoiceforGive(cardId)
                                                        }
                                                        else {
                                                            self.dismissViewControllerAnimated(true) {
                                                                rootVC.navigationController?.dismissViewControllerAnimated(false, completion: nil)
                                                            }
                                                        }
                                                        return
                                                    }
                                                }

                                                self.dismissViewControllerAnimated(true) {
                                                    self.prevViewController?.dismissViewControllerAnimated(false, completion: nil)
                                                }
                                            
                                            case let .Failure(error):
                                                print(error)
                                                self.hideProgressHUD()
                                                self.showSimpleAlert(withTitle: "Error",
                                                    withMessage: "Can't create video card, please try again later")
                                            }
                                        }
                                    }
                                    else {
                                        
                                        let request: NetworkService = .EditVideoCard(
                                            id: self.cardId!,
                                            title: self.cardName,
                                            description: self.descriptionText!,
                                            video: self.videoFileName,
                                            thumbnail_url:thumbFileName,
                                            tags: self.cardTags)
                                        
                                        provider.request(request) { result in
                                            switch result {
                                            case let .Success(response):
                                                guard response.statusCode == 200 else {
                                                    self.showSimpleAlert(withTitle: "Error",
                                                        withMessage: "Can't generate video upload parameters, please try again later")
                                                    self.hideProgressHUD()
                                                    return
                                                }
                                                
                                                self.hideProgressHUD()
                                                
                                                self.dismissViewControllerAnimated(true) {
                                                    self.prevViewController?.dismissViewControllerAnimated(false) {
                                                        let notify = NSNotification(name: "changedTable", object: nil)
                                                        NSNotificationCenter.defaultCenter().postNotification(notify)
                                                    }
                                                }
                                                
                                            case let .Failure(error):
                                                print(error)
                                                self.hideProgressHUD()
                                                self.showSimpleAlert(withTitle: "Error",
                                                    withMessage: "Can't create video card, please try again later")
                                            }
                                        }
                                    }
                                }
                                
                            case .Failure(let encodingError):
                                print(encodingError)
                                self.hideProgressHUD()
                                self.showSimpleAlert(withTitle: "Error",
                                    withMessage: "Can't encode multipart content, please try again later")
                            }
                    })
                    
                }
                else {
                    self.hideProgressHUD()
                    self.showSimpleAlert(withTitle: "Error",
                                         withMessage: "Can't parse response, please try again later")
                }
            case let .Failure(error):
                print(error)
                self.hideProgressHUD()
                self.showSimpleAlert(withTitle: "Error",
                                     withMessage: "Can't create image card, please try again later")
            }
        }
    }

    ////////////////////////////////

    @IBAction func onSelectScaleType(sender: AnyObject) {
        let index = (sender as! UIButton).tag / 2
        if (index == scaleType) {
            return
        }
        
        switch index {
        case 0:
            cardImage.image = nil
            backgroundImage.image = image
            
            if (screenWidth / image!.size.width > screenHeight / image!.size.height) {
                self.backgroundImage.contentMode = .ScaleAspectFill
            }
            else {
                self.backgroundImage.contentMode = .ScaleAspectFit
                bgViewY.constant = CGFloat( -Int(screenHeight - screenWidth * image!.size.height/image!.size.width) )
            }
            
            break
        case 1:
            bgViewY.constant = 0
            cardImage.image = nil
            backgroundImage.image = image
            if (screenHeight / image!.size.height > screenWidth / image!.size.width) {
                backgroundImage.contentMode = .ScaleAspectFill
            }
            else {
                backgroundImage.contentMode = .ScaleAspectFit
            }
            break
        case 2:
            cardImage.image = cropImage
            backgroundImage.image = nil
            cardImage.contentMode = .ScaleAspectFit
            break
        case 3:
            bgViewY.constant = 0
            cardImage.image = nil
            backgroundImage.image = cropImage
            backgroundImage.contentMode = .ScaleAspectFill
            break

        default:
            break
        }
        
        scaleTypeButtons[scaleType].setImage(UIImage(named: "whiteSelectOffIcon"), forState: .Normal)
        scaleTypeButtons[index].setImage(UIImage(named: "selectOn"), forState: .Normal)
        scaleType = index
    }
    
    ////////////////////////////////
    
    @IBAction func onSkipBack(sender: AnyObject) {
        var currentPosition = Float(CMTimeGetSeconds(player!.currentItem!.currentTime()))
        currentPosition = currentPosition - 15
        if (currentPosition < 0) {
            currentPosition = 0
        }
        timeSlider.value = currentPosition * 100
        onSlider(self)
    }
    
    @IBAction func onSkipForward(sender: AnyObject) {
        var currentPosition = Float(CMTimeGetSeconds(player!.currentItem!.currentTime()))
        currentPosition = currentPosition + 15
        if (currentPosition > self.duration) {
            currentPosition = self.duration
        }
        timeSlider.value = currentPosition * 100
        onSlider(self)
    }
    
    @IBAction func onPlay(sender: AnyObject) {
        isPlaying = !isPlaying
        if (isPlaying) {
            
            player!.play()
            playButton.setImage(UIImage(named: "videoPauseIcon"), forState: .Normal)
            playTimer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(onMoveSlider), userInfo: nil, repeats: true)
        }
        else {
            player!.pause()
            playButton.setImage(UIImage(named: "videoPlayIcon"), forState: .Normal)
            playTimer?.invalidate()
        }
    }
    
    @IBAction func onSlider(sender: AnyObject) {
        playedTime.text = makeTimeString(Int(timeSlider.value) / 100)
        remainingTime.text = "-" + makeTimeString(Int(duration * 100) / 100 - Int(timeSlider.value) / 100)
        player!.seekToTime(CMTime(seconds: Double(timeSlider.value) / 100, preferredTimescale: player!.currentItem!.asset.duration.timescale), toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
        progressWidthConstraint.constant = CGFloat(timeSlider.value / timeSlider.maximumValue) * (screenWidth - 10)
    }
    
    func onMoveSlider() {
        timeSlider.value = Float(CMTimeGetSeconds(player!.currentItem!.currentTime())) * 100.0
        playedTime.text = makeTimeString(Int(timeSlider.value) / 100)
        remainingTime.text = "-" + makeTimeString(Int(duration * 100) / 100 - Int(timeSlider.value) / 100)
        progressWidthConstraint.constant = CGFloat(timeSlider.value / timeSlider.maximumValue) * (screenWidth - 10)
        if (timeSlider.value == Float(duration * 100.0)) {
            timeSlider.value = 0.0
            onSlider(self)
//            player!.play()
            self.onPlay(playButton)
            if hideControlsButton.hidden {
                
                if videoControlsView.hidden == true {
                    showAll()
                }
            }
        }
    }
    
    func makeTimeString(timeInSec: Int) -> String {
        var secString = String(timeInSec % 60)
        if (timeInSec % 60 < 10) {
            secString = "0" + secString
        }
        
        var minString = String(timeInSec % 3600 / 60)
        if (timeInSec % 3600 / 60 < 10) {
            minString = "0" + minString
        }
        
        return minString + ":" + secString
    }
    
    func showAll(){
        if (self.videoUrl != nil) {
            videoControlsView.hidden = false
        } else {
            videoControlsView.hidden = true
        }
        showControlsButton.hidden = false
        userView.hidden = false
        progressBackground.hidden = false
        progress.hidden = false
        imgGradientTop.hidden = false
        imgGradientBottom.hidden = false
    }

    func hideAll(){
        if (self.videoUrl != nil) {
            videoControlsView.hidden = true
        } else {
            videoControlsView.hidden = true
        }
        
        showControlsButton.hidden = true
        userView.hidden = true
        
        progressBackground.hidden = true
        progress.hidden = true
        
        imgGradientTop.hidden = true
        imgGradientBottom.hidden = true
    }

    @IBAction func onTapFocus(gesture: UIGestureRecognizer!) {
        if hideControlsButton.hidden {
            
            if (self.videoUrl != nil) {
                hideY.constant = 155
                if videoControlsView.hidden == true {
                    showAll()
                }else{
                    hideAll()
                }
                
                imgGradientTop.hidden = true
                imgGradientBottom.hidden = true
                
            } else {
                hideY.constant = 18
                
                if userView.hidden {
                    showControlsButton.hidden = false
                    userView.hidden = true
                    progressBackground.hidden = true
                    progress.hidden = true
                } else {
                    hideAll()
                }
                
                imgGradientTop.hidden = true
                imgGradientBottom.hidden = true
            }
        }
    }

    func handlePan(panGesture: UIPanGestureRecognizer){
        
        switch panGesture.state {
        case .Began:
            
            startPos = panGesture.locationInView(mainView.superview)
            startCenter = mainView.center
            print(startPos)
            let velocity = panGesture.velocityInView(mainView?.superview)
            
            isRotating = fabs(velocity.x) > fabs(velocity.y)
            if (velocity.y > 0) {//down
                isDirection = true
            } else {//up
                isDirection = false
            }
            
        
        case .Ended:
            
            if !isRotating {
                
                if isDirection {
                    hideControls(false)
                    showAll()
                    progressBackground.hidden = false
                    progress.hidden = false
                    userView.hidden = false
                    mainView.layer.cornerRadius = 20
                    showControlsButton.hidden = true
                    videoControlsView.hidden = true
                } else { // show full screen / hide detail
                    
                    hideControls(true)
                    hideAll()
                    
                    userView.hidden = true
                    mainView.layer.cornerRadius = 0
                }
                
            }
            
        default:break
        }
    }
    
    ////////////////////////////////
    
    
    
    //GIVE CARD TO DEC
    
    func showChoiceforGive(cardId: String) {
        let alertController = UIAlertController(title: "Select people who can see your content...", message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            let rootVC = self.prevViewController?.navigationController?.viewControllers.first as! CameraFunctionViewController
            self.dismissViewControllerAnimated(true, completion: {
                rootVC.submittedCallback(nil)
            })
        }
        alertController.addAction(cancelAction)
        
        let anyoneAction = UIAlertAction(title: "Anyone", style: .Default) { (action) in
            self.giveCardToDeck(true, cardId: cardId)
        }
        alertController.addAction(anyoneAction)
        
        let askersAction = UIAlertAction(title: "Only Askers", style: .Default) { (action) in
            self.giveCardToDeck(false, cardId: cardId)
        }
        alertController.addAction(askersAction)
        
        self.presentViewController(alertController, animated: true) {
        }
    }
    
    func giveCardToDeck (isAnyone: Bool, cardId: String) {
        
        let rootVC = self.prevViewController?.navigationController?.viewControllers.first as! CameraFunctionViewController
        
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        let visibility = isAnyone ? nil : Constants.ApiConstants.giveVisibilityAskers as String?
        provider.request(.GiveCardToDeck(deckId: rootVC.decId, cardId: cardId, visibility: visibility), completion: { (result) in
            let JSON = Helper.validateResponse(result, sender: self)
            if JSON != nil {
                let rContent = ContentNode(dictionary: JSON!["deck"] as! [String: AnyObject])
                self.dismissViewControllerAnimated(true, completion: {
                    rootVC.submittedCallback(rContent)
                })
                
            } else {
                self.showSimpleAlert(withTitle: "", withMessage: "Failed to submit card.")
            }
        })
    }

}
