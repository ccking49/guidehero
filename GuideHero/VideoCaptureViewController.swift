//
//  VideoCameraRollViewController.swift
//  GuideHero
//
//  Created by SoftDev on 12/13/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation

class VideoCaptureViewController: UIViewController, AVCaptureFileOutputRecordingDelegate {

    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var switchCameraButton: UIButton!
    @IBOutlet weak var cameracontentbutton: UIButton!
    @IBOutlet weak var cameraRollContentButton: UIButton!
    @IBOutlet weak var zoomSilder: UISlider!
    @IBOutlet weak var zoomView: UIView!
    @IBOutlet weak var captureTimeLabel: UILabel!
    
    let captureSession = AVCaptureSession()
    var videoFileOutput = AVCaptureMovieFileOutput()
    var previewLayer: AVCaptureVideoPreviewLayer? = nil
    var isRecording = false
    var filePath: NSURL?
    var timer = NSTimer()
    var recordedTimer: Double = 0
    weak var panToCloseGestureRecognizer: UIPanGestureRecognizer!
    
    var parentVC: UIViewController? = nil
    var type: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        filePath = documentsURL.URLByAppendingPathComponent("temp.mov")
        do {
            try NSFileManager.defaultManager().removeItemAtURL(filePath!)
        }
        catch {
            
        }
        //for test
        self.setupCamera()
        //
        
        self.zoomSilder!.maximumValue = 5.0
        self.zoomSilder!.minimumValue = 1.0
        self.zoomSilder!.value = 1.0
        
        captureTimeLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
        captureTimeLabel.layer.shadowOpacity = 1
        captureTimeLabel.layer.shadowRadius = 1
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        captureTimeLabel.text = "00 : 00 : 00"
        recordedTimer = 0
        isRecording = false
        
        UIApplication.sharedApplication().statusBarHidden = true
        
        captureButton.setImage(UIImage(named: "shutterOff"), forState: UIControlState.Normal)
        cameracontentbutton.setImage(UIImage(named: "CameraContent"), forState: UIControlState.Normal)
        cameraRollContentButton.setImage(UIImage(named: "CameraRollContent"), forState: UIControlState.Normal)
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(updateCameraOrientation),
                                                         name: UIDeviceOrientationDidChangeNotification,
                                                         object: nil)
        
        // The pan gesture belongs to the parent view. Switch delegate to control it here.
        panToCloseGestureRecognizer.delegate = self
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.previewLayer?.frame = self.view.bounds
        
        let pinchRecognizer = UIPinchGestureRecognizer(target: self, action:#selector(VideoCaptureViewController.pinchToZoom(_:)))
        pinchRecognizer.delegate = self
        self.view.addGestureRecognizer(pinchRecognizer)

//        setZoom(CGFloat(1.0), velocity: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setupCamera() {
        
        // get back camera
        let devices = AVCaptureDevice.devices().filter{ $0.hasMediaType(AVMediaTypeVideo) && $0.position == AVCaptureDevicePosition.Back }
        if let captureDevice = devices.first as? AVCaptureDevice  {
            self.captureSession.beginConfiguration()
            // setup capture session and add back camera as an imput
            
            videoFileOutput = AVCaptureMovieFileOutput()
            videoFileOutput.movieFragmentInterval = kCMTimeInvalid
            self.captureSession.addOutput(videoFileOutput)
            let devices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
            for  device in devices!  {
                let captureDevice = device as! AVCaptureDevice
                if (captureDevice.position == AVCaptureDevicePosition.Back) {
                    let avFlashMode = AVCaptureFlashMode.Auto
                    if (captureDevice.isFlashModeSupported(avFlashMode)) {
                        do {
                            try captureDevice.lockForConfiguration()
                        } catch {
                            return
                        }
                        captureDevice.flashMode = avFlashMode
                        captureDevice.unlockForConfiguration()
                    }
                }
            }
            self.captureSession.commitConfiguration()
            
            if let input = try? AVCaptureDeviceInput(device: captureDevice) {
                if self.captureSession.canAddInput(input) {
                    self.captureSession.addInput(input)
                }
                else {
                    self.showCameraError()
                    return
                }
            }
            else {
                self.showCameraError()
                return
            }
//            // add a still image output to the capture session
//            
//            self.stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
//            if self.captureSession.canAddOutput(self.stillImageOutput) {
//                self.captureSession.addOutput(self.stillImageOutput)
//            }
//            else {
//                self.showCameraError()
//                return
//            }
            
            // add the preview layer
            
            if let previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession) {
                self.previewLayer = previewLayer
                previewLayer.frame = self.view.bounds
                previewLayer.position = self.view.layer.position
                previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
                previewLayer.connection.videoOrientation = self.videoOrientationFromDeviceOrientation(UIDevice.currentDevice().orientation)
                self.view.layer.insertSublayer(previewLayer, atIndex: 0)
                captureSession.startRunning()
            }
            else {
                self.showCameraError()
            }
        }
        else {
            self.showCameraError()
        }
    }

    @IBAction func onClose(sender: AnyObject) {
        if !isRecording {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    @IBAction func onCapture(sender: AnyObject) {
        
        if self.type == 1 { // image
            self.showSimpleAlert(withTitle: "Warning", withMessage: "Please select image in the library")
            return
        }
        //for test
//        let finalCardViewController = storyboard!.instantiateViewControllerWithIdentifier("FinalCardViewController") as! FinalCardViewController
//        finalCardViewController.cardName = "test"
//        finalCardViewController.descriptionText = "test description"
//        finalCardViewController.startTime = 0
//        finalCardViewController.endTime = 10
//        finalCardViewController.prevViewController = self
//        presentViewController(finalCardViewController, animated: true, completion: nil)
        
        
        isRecording = !isRecording
        if (isRecording) {
            captureButton.setImage(UIImage(named: "cameraControls"), forState: UIControlState.Normal)
            cameracontentbutton.hidden = true
            cameraRollContentButton.hidden = true
            
            videoFileOutput.startRecordingToOutputFileURL(filePath, recordingDelegate: self)
            
            timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector (VideoCaptureViewController.updateTimer(_:)), userInfo: nil, repeats:true)
            captureTimeLabel.textColor = UIColor.yellowColor()
        }
        else {
            toggleFlash(true)
            self.videoFileOutput.stopRecording()
            captureButton.setImage(UIImage(named: "shutterOff"), forState: UIControlState.Normal)
            cameracontentbutton.hidden = false
            cameraRollContentButton.hidden = false
            timer.invalidate()
            captureTimeLabel.textColor = UIColor.whiteColor()
        }
 
    }
    
    @IBAction func onFlashCamera(sender: AnyObject) {
        
        toggleFlash(false)
    }
    
    @IBAction func onSwitchCamera(sender: AnyObject) {
        
        if isRecording {
            return
        }
        
        let currentCameraInput: AVCaptureInput = self.captureSession.inputs[0] as! AVCaptureInput
        self.captureSession.removeInput(currentCameraInput)
        var newCamera: AVCaptureDevice
        
        if (currentCameraInput as! AVCaptureDeviceInput).device.position == AVCaptureDevicePosition.Back {
            newCamera = self.cameraWithPosition(AVCaptureDevicePosition.Front)!
            
            self.flashButton.alpha = 0
            self.flashButton.enabled = false
            
            self.zoomView.alpha = 0
            self.zoomSilder.enabled = false
            
        } else {
            newCamera = self.cameraWithPosition(AVCaptureDevicePosition.Back)!
            
            self.flashButton.alpha = 1
            self.flashButton.enabled = true
            
            self.zoomView.alpha = 1
            self.zoomSilder.enabled = true

        }
        let newVideoInput = try? AVCaptureDeviceInput(device: newCamera)
        self.captureSession.addInput(newVideoInput)
    }

    func cameraWithPosition(position: AVCaptureDevicePosition) -> AVCaptureDevice? {
        
        let devices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
        for device in devices {
            if (device as AnyObject).position == position {
                return device as? AVCaptureDevice
            }
        }
        return nil
    }
    
    @IBAction func onZoomMinus(sender: AnyObject) {
        var zoomValue = self.zoomSilder!.value
        if zoomValue <= 0 {
            zoomValue = 0
        } else {
            zoomValue -= 0.5
        }
        setZoom(CGFloat(zoomValue), velocity: 0)
    }
    
    @IBAction func onZoomPlus(sender: AnyObject) {
        var zoomValue = self.zoomSilder!.value
        if zoomValue >= 5 {
            zoomValue = 5
        } else {
            zoomValue += 0.5
        }
        setZoom(CGFloat(zoomValue), velocity: 0)
    }
    
    @IBAction func onCameraContent(sender: AnyObject) {
        captureTimeLabel.text = "00 : 00 : 00"
        recordedTimer = 0
    }
    
    @IBAction func onCameraRollContent(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName("GotoCameraRollScreen", object: self, userInfo: nil)
        captureTimeLabel.text = "00 : 00 : 00"
        recordedTimer = 0
    }
    
    func toggleFlash(forceOff: Bool) {
        let device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                if device.torchMode == AVCaptureTorchMode.On || forceOff {
                    device.torchMode = AVCaptureTorchMode.Off
                    device.flashMode = AVCaptureFlashMode.Off
                    
                    let flashOffImage = UIImage(named: "flashOff")
                    flashButton.setImage(flashOffImage, forState: UIControlState())
                } else {
                    do {
                        try device.setTorchModeOnWithLevel(1.0)
                        
                        device.flashMode = AVCaptureFlashMode.On
                        device.torchMode = AVCaptureTorchMode.On
                        let flashOnImage = UIImage(named: "flashOn")
                        flashButton.setImage(flashOnImage, forState: UIControlState())
                    } catch {
                        print(error)
                    }
                }
                device.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    
    func captureOutput(captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAtURL outputFileURL: NSURL!, fromConnections connections: [AnyObject]!, error: NSError!) {
        if self.type == 0 {
            let videoCardCreationVC = storyboard!.instantiateViewControllerWithIdentifier("VideoCardCreationViewController") as! VideoCardCreationViewController
            videoCardCreationVC.videoUrl = filePath!
            videoCardCreationVC.prevViewController = self
            navigationController!.pushViewController(videoCardCreationVC, animated: true)
        }
        else if self.type == 2 {
            if let vc = self.parentVC as? VideoCardCreationViewController {
                vc.videoUrl = filePath!
                vc.setupVideo()
                navigationController!.dismissViewControllerAnimated(true, completion: nil)
            }
        }
        return
    }
    
    func captureOutput(captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAtURL fileURL: NSURL!, fromConnections connections: [AnyObject]!) {
        
        return
    }
    
    func videoOrientationFromDeviceOrientation(deviceOrientation: UIDeviceOrientation) -> AVCaptureVideoOrientation {
        var videoOrientation: AVCaptureVideoOrientation = .Portrait
        switch deviceOrientation {
        case .Portrait:
            videoOrientation = .Portrait;
        case .PortraitUpsideDown:
            videoOrientation = .PortraitUpsideDown
        case .LandscapeLeft:
            videoOrientation = .LandscapeRight
        case .LandscapeRight:
            videoOrientation = .LandscapeLeft
        default:
            break
        }
        return videoOrientation
    }
    
    private func showCameraError() {
        self.showSimpleAlert(withTitle: "Error", withMessage: "Can't access camera, please try again later")
    }
    
    @IBAction func OnZoomSlider(sender: AnyObject) {
        setZoom(CGFloat(self.zoomSilder!.value), velocity: 0)
    }
    
    // update timer
    func updateTimer(timer:NSTimer) {
        recordedTimer += 1
        let seconds = recordedTimer % 60
        let minutes = (recordedTimer / 60) % 60
        let hours = ((recordedTimer / 60) / 60) % 24
        
        let strRecordedTime = String(format: "%.02d : %.02d : %.02d", Int(hours), Int(minutes), Int(seconds))
        captureTimeLabel.text = strRecordedTime
    }
    
    func updateCameraOrientation() {
        let deviceOrientation = UIDevice.currentDevice().orientation
        let videoOrientation = self.videoOrientationFromDeviceOrientation(deviceOrientation)
        self.previewLayer?.connection.videoOrientation = videoOrientation
    }
    
    //Pinch Gesture
    func pinchToZoom(sender:UIPinchGestureRecognizer) {
        
        let zoomFactor = CGFloat(self.zoomSilder!.value)
        setZoom(zoomFactor, velocity: sender.velocity)
    }
    
    func setZoom(zoomFactor:CGFloat, velocity:CGFloat) {
        var device: AVCaptureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        var error:NSError!
        do {
            try device.lockForConfiguration()
            defer {device.unlockForConfiguration()}
            if (zoomFactor <= device.activeFormat.videoMaxZoomFactor) {
                
                let desiredZoomFactor:CGFloat = zoomFactor + atan2(velocity, 5.0) / 2;
                device.videoZoomFactor = max(1.0, min(desiredZoomFactor, device.activeFormat.videoMaxZoomFactor));
                self.zoomSilder!.value = Float(device.videoZoomFactor)
            }
            else {
                NSLog("Unable to set videoZoom: (max %f, asked %f)", device.activeFormat.videoMaxZoomFactor, zoomFactor);
            }
        }
        catch error as NSError{
            NSLog("Unable to set videoZoom: %@", error.localizedDescription);
        }
        catch _{
        }
    }
}

// MARK: - UIGestureRecognizerDelegate
extension VideoCaptureViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return !isRecording
    }
}
