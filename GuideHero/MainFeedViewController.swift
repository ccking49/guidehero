//
//  MainFeedViewController.swift
//  GuideHero
//
//  Created by forever on 12/9/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit
import Moya

class MainFeedViewController: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var pagerController : ViewPagerController?
    var statusbarTintColor: UIColor!
    
    let highlightedColor: UIColor = UIColor.colorFromRGB(redValue: 255, greenValue: 59, blueValue: 96, alpha: 1)
    
    let titles = ["All", "Tutorials", "Ongoing Contests", "Trending", "Food", "Academic", "Cambridge", "Boston", "Harvard", "MIT"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let pagerController = ViewPagerController()
        pagerController.setParentController(self, parentView: self.mainView)
        
        var appearance = ViewPagerControllerAppearance()
        
        appearance.scrollViewMinPositionY = 20.0
        appearance.scrollViewObservingType = .header
        
        appearance.tabMenuAppearance.backgroundImage = UIImage(appImage: .navBackground)
        appearance.tabMenuAppearance.selectedViewBackgroundColor = highlightedColor
        appearance.tabMenuAppearance.selectedViewInsets = UIEdgeInsets(top: 28, left: 5, bottom: 7, right: 5)
        
        pagerController.updateAppearance(appearance)
        
        pagerController.updateSelectedViewHandler = { selectedView in
            selectedView.layer.cornerRadius = selectedView.frame.size.height * 0.5
        }
        
        pagerController.willBeginTabMenuUserScrollingHandler = { selectedView in
            selectedView.alpha = 0.0
        }
        
        pagerController.didEndTabMenuUserScrollingHandler = { selectedView in
            selectedView.alpha = 1.0
        }
        
        pagerController.didShowViewControllerHandler = { controller in
            NSNotificationCenter.defaultCenter().postNotificationName("UpdatePage", object: self, userInfo: ["tagIndex": (controller.view.tag)])
            let category = self.titles[controller.view.tag]
            print("show \(category)")
            NSUserDefaults.standardUserDefaults().lastFeedTab = category
        }
        /*
        pagerController.changeObserveScrollViewHandler = { controller in
//            print("call didShowViewControllerObservingHandler")
            return detailController.tableView
        }*/
        
        pagerController.didChangeHeaderViewHeightHandler = { height in

        }
        
        pagerController.didScrollContentHandler = { percentComplete in

        }
        
        for index in 0..<titles.count {
            let controller = self.storyboard?.instantiateViewControllerWithIdentifier("TrendingViewController") as! TrendingViewController
            controller.categoryType = titles[index]
            controller.view.frame = self.mainView.frame
            controller.view.clipsToBounds = true
            controller.title = titles[index]
            controller.view.tag = index
//            (controller as! TrendingViewController).trendingVCDelegate = self
            pagerController.addContent(titles[index], viewController: controller)
            
            NSNotificationCenter.defaultCenter().removeObserver(controller, name: "UpdatePage", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(controller, selector: #selector(TrendingViewController.updatePageChanged(_:)), name: "UpdatePage", object: nil)
        }
        
        self.pagerController = pagerController
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.Default
        statusbarTintColor = self.navigationController?.navigationBar.tintColor
        self.navigationController?.navigationBar.tintColor = UIColor.blackColor()
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ChangeBottomConstraint", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainFeedViewController.changeBottomConstraint(_:)), name: "ChangeBottomConstraint", object: nil)
        
        fetchAllDecks()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        self.navigationController?.navigationBar.tintColor = statusbarTintColor
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ChangeBottomConstraint", object: nil)
    }
    
    func changeBottomConstraint(data:NSNotification) {
        let constant = data.userInfo!["constraint"]
        self.bottomConstraint.constant = constant as! CGFloat
//        print("ChangeBottomConstraint \(constant)")
    }
    
    private func fetchAllDecks() {
        let provider = MoyaProvider<NetworkService>.createDefaultProvider()
        provider.request(.GetAllDecks) { result in
            self.hideProgressHUD()

            switch result {
            case let .Success(response):
                if response.statusCode != 200 {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't download decks, please try again later")
                    return
                }

                if let JSON = try? response.mapJSON() as! [String: AnyObject] {
                    if Constants.showPrintDebug {
                        print("Cards***")
                        print(JSON)
                    }
                    // artificial top level content container
                    let rootDir: [String: AnyObject] = [
                        "type": "deck",
                        "name": "Learn",
                        "children": JSON["all_decks"]!,
                        "id": "",
                        "creator": "",
                        "creator_thumbnail": "",
                        "creator_full_name": "",
                        "creator_bio": "",
                        "created_at": 0,
                        "updated_at": 0,
                        ]

                    DataStore.mainContent = ContentNode(dictionary: rootDir)

                    // find the one the user wants
                    let defaultFeed = NSUserDefaults.standardUserDefaults().lastFeedTab

                        for viewController in self.pagerController!.childViewControllers {
                        if let trendingVC = viewController as? TrendingViewController {
                            if trendingVC.categoryType == defaultFeed {
                                trendingVC.loadData()
                            }
                        }
                    }
                }
                else {
                    self.showSimpleAlert(withTitle: "Error", withMessage: "Can't parse decks, please try again later")
                }
            case let .Failure(error):
                print("can't fetch decks: %@", error)
                self.showSimpleAlert(withTitle: "Error", withMessage: "Can't download decks, please try again later")
            }
        }
    }
}



//extension MainFeedViewController: TrendingViewControllerDelegate {
//    
//}
