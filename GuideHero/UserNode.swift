//
//  CommentNode.swift
//  GuideHero
//
//  Created by Ascom on 10/31/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

public class UserNode: CustomDebugStringConvertible {
    var user_id: String
    var username: String
    var active: Bool
    var ask_creator: Bool
    var contribution: Int
    var bio: String
    var email: String
    var first_name: String
    var last_name: String
    var source: String
    var stripped_email: String
    var thumbnail_url: String
    var tier: String
    var isRelation : Bool
    
    var following_count : Int
    var followers_count : Int
    var current_point : Int
    
//    var total_points: Int
//    var confirmed_at: Int // unix timestamp

    
    // MARK: - Calculated properties
    
    public var debugDescription: String {
        get {
            let dic: [String: String] = [
                "user_id": self.user_id,
                "username": self.username,
                "first_name": self.first_name,
                "email": self.email,
                "thumbnail_url": self.thumbnail_url
            ]
            return dic.debugDescription
        }
    }
    
    // MARK: - Object lifecycle
    
    init(dictionary: [String: AnyObject]) {
        
        let userId = dictionary["user_id"] ?? dictionary["id"]
        self.user_id = userId as! String
        self.username = dictionary["username"] as! String
        self.active = dictionary["active"] as? Bool ?? false
        
        self.isRelation = dictionary["follow_by_current_user"] as? Bool ?? false
        
        self.ask_creator = dictionary["ask_creator"] as? Bool ?? false
        self.thumbnail_url = dictionary["thumbnail_url"] as? String ?? ""
        self.contribution = dictionary["contribution"] as? Int ?? 0

        self.bio = dictionary["bio"] as? String ?? ""
        self.email = dictionary["email"] as? String ?? ""
        self.stripped_email = dictionary["stripped_email"] as? String ?? ""
        self.first_name = dictionary["first_name"] as? String ?? ""
        self.last_name = dictionary["last_name"] as? String ?? ""
        self.source = dictionary["source"] as? String ?? ""
        self.tier = dictionary["tier"] as? String ?? ""
        
        if dictionary.keys.contains("total_points") {
            var total_points: Int!
            if dictionary["total_points"] is NSNull {
                total_points = 0
            } else {
                total_points = dictionary["total_points"] as! Int
            }
            
            NSUserDefaults.standardUserDefaults().setInteger(total_points, forKey: "total_points")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        
        self.followers_count = 0
        self.following_count = 0
        self.current_point = 0
    }

}
