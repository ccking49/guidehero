//
//  SwipeDirection.swift
//  GuideHero
//
//  Created by Promising Change on 03/01/17.
//  Copyright © 2017 GuideHero. All rights reserved.
//

import Foundation

public enum SwipeDirection {
    case left
    case right
    case none
}
