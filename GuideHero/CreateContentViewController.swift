//
//  CreateContentViewController.swift
//  GuideHero
//
//  Created by forever on 12/30/16.
//  Copyright © 2016 GuideHero. All rights reserved.
//

import UIKit

class CreateContentViewController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    var showScreen: Bool!
    private let dismissInteractor = DismissInteractor()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        showScreen = false
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if showScreen == false {
            let navigationController = storyboard!.instantiateViewControllerWithIdentifier("CameraFunctionNavViewController") as! UINavigationController
            let cameraFunctionViewController = navigationController.viewControllers.first as! CameraFunctionViewController
            
            navigationController.transitioningDelegate = self
            cameraFunctionViewController.dismissInteractor = dismissInteractor
            
            presentViewController(navigationController, animated: true, completion: nil)
        } else {
            self.tabBarController?.selectedIndex = Constants.UIConstants.learnTabIndex
        }
        showScreen = !showScreen
    }
}

// MARK: - UIViewControllerTransitioningDelegate
extension CreateContentViewController: UIViewControllerTransitioningDelegate {
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissAnimator()
    }
    
    func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return dismissInteractor.hasStarted ? dismissInteractor : nil
    }
}

// MARK: - DismissInteractor & Animator
class DismissInteractor: UIPercentDrivenInteractiveTransition {
    var hasStarted = false
    var shouldFinish = false
}

class DismissAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.4
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey),
            let toVC = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) else {
                return
        }
        
        transitionContext.containerView().insertSubview(toVC.view, belowSubview: fromVC.view)
        
        let screenBounds = UIScreen.mainScreen().bounds
        let bottomLeftCorner = CGPoint(x: 0, y: screenBounds.height)
        let finalFrame = CGRect(origin: bottomLeftCorner, size: screenBounds.size)
        
        UIView.animateWithDuration(transitionDuration(transitionContext), delay: 0, options: [.CurveEaseIn], animations: {
            fromVC.view.frame = finalFrame
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        })
    }
}
